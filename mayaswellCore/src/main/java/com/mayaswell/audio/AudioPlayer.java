package com.mayaswell.audio;

import android.os.Handler;
import android.os.Message;

public abstract class AudioPlayer {

	public class State {
		public final static int STOPPED=0;
		public final static int PLAYING=1;
		public final static int PAUSED=2;
	}
	
	public abstract boolean isPlaying();
	public abstract boolean stop(boolean andTriggerEvent);

	protected int playState = State.STOPPED;
	public AudioPlayer() {
		super();
	}
}