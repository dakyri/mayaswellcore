#ifndef _DISTORTION
#define _DISTORTION

const float	distortionNormalizeSoft = 0.25;	//2000
const float	distortionNormalizeTube = 0.25;	//1000


class Distortion
{
public:
	Distortion(){
		dstAmount = tubeAmount = 0.0;
	}

	virtual ~Distortion(){
	}

						
	long apply(float *sig, long nFrames, short nChan);
	void setParameters(float d, float t);
	
	float				dstAmount;
	float				tubeAmount;

};


/* Soft clipping: dist = x - 1/3 * x^3	*/
inline float dist(float x)
{
	return (x - .33333333 * x*x*x);
//	return (x>1.0)? (.66666667):
//			(x<-1.0)? (-.66666667):
//			(x - .33333333 * x*x*x);
}

inline float tube(float x)
{
/* Tube-ish distortion: dist = (x +.5)^2 -.25	*/
/* this does not work with a feedback guitar */

	return ((x+.5)*(x+.5) - .25);
}

inline long
Distortion::apply(float *outSig, long nFrames, short nChan)
{
	long	outSigLen = nFrames * nChan;

	for (short i=0; i<outSigLen; i++) {
		float dist0 = 	dstAmount?
							(1-dstAmount)*outSig[i] +
							(dstAmount*distortionNormalizeSoft*dist(outSig[i]/distortionNormalizeSoft))
						  : outSig[i];
		float dist1 = 	tubeAmount?
							(1-tubeAmount)*dist0 +
							(tubeAmount*distortionNormalizeTube*tube(dist0/distortionNormalizeTube))
						  : dist0;
		outSig[i] = wideClip(dist1);
	}
	return nFrames;
}




inline void
Distortion::setParameters(float d, float t)
{
	dstAmount = d;
	tubeAmount = t;
}

#endif
