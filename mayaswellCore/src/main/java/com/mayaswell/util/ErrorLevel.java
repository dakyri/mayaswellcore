package com.mayaswell.util;

/**
 * Created by dak on 5/30/2016.
 */
public class ErrorLevel {
	public static final int NO_ERROR = 0; /** no problem at all!!! */
	public static final int WARNING_EVENT = 1; /** a message but not interfering with operation */
	public static final int ERROR_EVENT = 2; /** a problem but recoverable */
	public static final int FNF_ERROR_EVENT = 3; /** recoverable by browsing for a replacement */
	public static final int FATAL_EVENT = 4; /** not recoverable */
}

