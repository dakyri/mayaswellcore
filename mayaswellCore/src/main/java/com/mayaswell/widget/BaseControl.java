package com.mayaswell.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class BaseControl extends RelativeLayout
{
	protected final String schemaName = "http://schemas.android.com/apk/res-auto"; 

	protected DisplayMode displayMode = DisplayMode.SHOW_NAME;
	protected String valueDisplayFormat = "%1.2f";
	protected TextView label = null;

	public BaseControl(Context context) {
		super(context);
	}

	public BaseControl(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BaseControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setValueDisplayPrecision(int i)
	{
		if (i < 1) i = 1; else if (i > 5) i = 5;
		valueDisplayFormat = "%1."+Integer.toString(i)+"f";
	}
	
	public DisplayMode getDisplayMode() {
		return displayMode;
	}
	public void setDisplay(DisplayMode dm)
	{
		displayMode = dm;
	}

	public void setName(String name)
	{
		if (label != null) {
			label.setText(name);
		} else {
		}
	}
	
	@SuppressWarnings("unused")
	protected LayoutParams rlParams(int below, int right)
	{
		return rlParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				below, right);
	}
	
	protected LayoutParams rlParams(int wx, int hx, int below, int right)
	{
		Log.d("fxp", String.format("b %d r %d", below, right));
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(wx, hx);
		if (right < 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		} else {
			relativeParams.addRule(RelativeLayout.RIGHT_OF, right);
		}
		if (below < 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			relativeParams.addRule(RelativeLayout.BELOW, below);
		}
		return relativeParams;
	}

	protected void checkFullyOpenedSiblings(Class<?> c, DisplayMode baseDisplay) {
		ViewGroup parent;

		try {
			parent = (ViewGroup) getParent();
		} catch (ClassCastException e) {
			return;
		}
		if (parent == null) {
			return;
		}
		for (int i=0; i<parent.getChildCount(); i++) {
			View v = parent.getChildAt(i);
			BaseControl b;
			try {
				b = (BaseControl) v;
				if (b.getClass().equals(c)) {
					if (b != this) {
						if (b.getDisplayMode() != DisplayMode.SHOW_NAME) {
							b.setDisplay(baseDisplay);
						}
					}
				}
			} catch (ClassCastException e) {
			}
		}
	}


}
