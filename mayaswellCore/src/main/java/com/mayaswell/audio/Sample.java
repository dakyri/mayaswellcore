package com.mayaswell.audio;

/**
 * container class for useful sample oriented helpers
 */
public class Sample {
	public final static int DIR_FORWARD=0;
	public final static int DIR_BACKWARD=1;
	public final static int DIR_FWDBACK=2;
	public final static int DIR_BACKFWD=3;
	
	public enum Direction {
		FORWARD(DIR_FORWARD), BACKWARD(DIR_BACKWARD), FWDBACK(DIR_FWDBACK), BACKFWD(DIR_BACKFWD);
		
		private byte id;
		Direction(int id2)
		{
			id = (byte) id2;
		}
		public byte code()
		{
			return id;
		}
		
		@Override
		public String toString()
		{
			switch (this) {
			case FORWARD: return "fwd";
			case BACKWARD: return "back";
			case FWDBACK: return "fwdback";
			case BACKFWD: return "backfwd";
			}
			return super.toString();
		}

		public static Direction parse(int code)
		{
			if (code == DIR_BACKFWD) return BACKFWD;
			if (code == DIR_BACKWARD) return BACKWARD;
			if (code == DIR_FWDBACK) return FWDBACK;
			return FORWARD;
		}

		public static Direction parse(String s)
		{
			if (s.equals("backfwd")) return BACKFWD;
			if (s.equals("back")) return BACKWARD;
			if (s.equals("fwdback")) return FWDBACK;
			return FORWARD;
		}
		
		public Direction reverse()
		{
			switch (this) {
			case FORWARD: return BACKWARD;
			case BACKWARD: return FORWARD;
			case FWDBACK: return BACKFWD;
			case BACKFWD: return FWDBACK;
			}
			return BACKWARD;
		}
	}

}
