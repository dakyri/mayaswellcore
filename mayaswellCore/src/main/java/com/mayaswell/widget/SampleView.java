package com.mayaswell.widget;

import com.mayaswell.R;
import com.mayaswell.audio.SamplePlayer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

import rx.functions.Action1;

public abstract class SampleView extends View
{
	protected static final int kScrollerRightEdgeMargin = 40;
    protected static final int kMinimumScrollerWidth = 20;

	public interface SampleViewListener {
    	public void onRegionStartChanged(long fr);
    	public void onRegionLengthChanged(long fr);
    	public void onMarkerChanged(int type, Object m, int ind, long fr, float v);
    	public void onMarkerSelected(int type, Object m, int ind);
    	public void onMarkerRegionSelected(int type, Object m, int ind);
    }

    protected final String schemaName = "http://schemas.android.com/apk/res-auto";

    protected static final int[] STATE_SCROLLING = {R.attr.state_sv_scrolling};
	protected static final int[] STATE_SELECTING = {R.attr.state_sv_selecting};
	
	protected boolean isScrolling=false;
	protected boolean isSelecting=false;
	protected boolean isScaling=false;

	
	protected float densityMultiplier = getContext().getResources().getDisplayMetrics().density;

	protected boolean showGrid = true;
	
	protected Paint textBrush = null;
	protected Paint gridBrush = null;
	protected Paint regionBrush = null;
	protected Paint dataBrush = null;
	protected Paint envelopeBrush = null;
	protected Paint cursorBrush = null;
	
	protected Drawable scrollerKnob = null;
	protected Drawable scrollerBack = null;
	protected int scrollerHeight = 0;
	protected Rect scrollerBackRect = null;
	protected Rect scrollerKnobRect = null;
	protected int maxScrollerKnobX = 0;
	protected int minScrollerKnobW = 0;
	protected float scrollL=1;
	protected float scrollX=0;

	protected int touchOffset=0;

	protected int svW=0;
	protected int svH=0;
	protected Rect svRect = null;
	
	float textScaledPx = 10 * densityMultiplier;

	protected SamplePlayer currentPlayer = null;
	protected Path[] samplePaths = null;
	protected Bitmap sampleBitmap = null;
	protected Rect sampleBitmapSrc = null;
	
	protected SampleViewListener listener = null;
	protected long regionStart = 0;
	protected long regionLength = 0;

	protected int crsX = 0;

	protected int cursorColor = 0xffaa3333;
	protected int textColor = 0xdd4285F4;
	protected int regionColor = 0x66F7A60A;
	protected int dataColor = 0xff333377;
	protected int envelopeColor = 0xff111111;
	protected float pxRs = 0;
	protected float pxRe = 0;
	
	protected int gridMinorCol = 0xaa888888;
	protected int gridMajorCol = 0xaa444444;
	protected float gridWidth = 10;
//	protected float gridInterval = 0.1f;
//	protected long gridStartPx = 0;
//	protected int colorIdx0;
    protected int gridMajorSubdivision = 10;
    protected int gridMinorSubdivision = 10;
	


	protected float defaultSampleRate=44100;
	private float minScrollerLen = 0.1f;

	public SampleView(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public SampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public SampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
//		currentPad = null;
		
		Log.d("sampleview", "setup base");
		
		svW = getWidth(); 
		svH = getHeight();
		
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = 10 * densityMultiplier;
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setStyle(Paint.Style.STROKE);
		
		cursorBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		cursorBrush.setStyle(Paint.Style.STROKE);

		regionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		regionBrush.setStyle(Paint.Style.FILL);
		
		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
		
		envelopeBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		envelopeBrush.setStyle(Paint.Style.STROKE);
		
		scrollerBack = ResourcesCompat.getDrawable(getResources(), R.drawable.mw_scroller_bg, null);
		scrollerKnob = ResourcesCompat.getDrawable(getResources(), R.drawable.mw_scroller_knob, null);
		
		scrollerHeight = getResources().getDimensionPixelSize(R.dimen.mwSvScrollerHeight);
		scrollerBackRect = new Rect();
		scrollerKnobRect = new Rect();
		setScroller(scrollX, scrollL);
		if (attrs != null) {
			TypedArray svAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SampleView, 0, 0);
			TypedArray mvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);

			textColor  = mvAttrs.getColor(R.styleable.MWView_labelColor, textColor);
			gridMinorCol  = svAttrs.getColor(R.styleable.SampleView_gridMinColor, gridMinorCol);
			gridMajorCol  = svAttrs.getColor(R.styleable.SampleView_gridMajColor, gridMajorCol);
			dataColor  = svAttrs.getColor(R.styleable.SampleView_dataColor, dataColor);
			envelopeColor  = svAttrs.getColor(R.styleable.SampleView_envelopeColor, envelopeColor);
			regionColor  = svAttrs.getColor(R.styleable.SampleView_regionColor, regionColor);
			cursorColor  = svAttrs.getColor(R.styleable.SampleView_cursorColor, cursorColor);

			svAttrs.recycle();
			mvAttrs.recycle();
		}
		textBrush.setColor(textColor);
		gridBrush.setColor(gridMinorCol);
		cursorBrush.setColor(cursorColor);
		regionBrush.setColor(regionColor);
		dataBrush.setColor(dataColor);
		envelopeBrush.setColor(envelopeColor);
	}
	
	public void setDefaultSampleRate(float r)
	{
		defaultSampleRate = r;
	}

	public boolean setToPlayer(SamplePlayer p)
	{
		Log.d("sampleview", String.format("setting to pad %d %d", svW, svH));
		if (p == null) {
			samplePaths  = null;
			currentPlayer = null;
			return false;
		}
		currentPlayer = p;
		p.getSamplePaths(svW, svH-scrollerHeight).subscribe(new Action1<Path[]>() {
			@Override
			public void call(Path[] paths) {
				Log.d("SampleView", "got sample paths");
				setSamplePaths(paths);
				invalidate();
			}
		});
		setScroller(0,1);
		setRegionStart(p.getRegionStart());
		setRegionLength(p.getRegionLength());
		setGridParameters();
		invalidate();
		return true;
	}

	public SamplePlayer getCurrentPlayer() {
		return currentPlayer;
	}

	public void setListener(SampleViewListener l)
	{
		listener  = l;
	}
	
	/**
	 * @param start
	 * @param len
	 */
	protected void setScroller(float start, float len)
	{
		if (len < minScrollerLen) len = minScrollerLen ; else if (len > 1) len = 1;
		if (start < 0) start = 0; else if (start > 1) start = 1;
		scrollX = start;
		scrollL = len;
		
		int l = (int) ((svW-2)*len);
		if (l < minScrollerKnobW) l = minScrollerKnobW;
		maxScrollerKnobX = (svW-2-l);
		int x = (int) (start*maxScrollerKnobX);
		if (x < 0) x = 0;
		scrollerKnobRect.set(x+1, svH-scrollerHeight+2, x+l, svH-2);
//		Log.d("sampleview", "set scroller "+x+", "+start+", "+len);
		setSampleBitmapSrc();

		if (currentPlayer != null && svW > 0 && scrollL != 0) {
			setGridParameters();
//			Log.d("setup grid", String.format("%g displayed px 0 %g int %g secs p grid %g px start %d", secsDisplayed, screenPx0, gridInterval, gridWidth, gridStartPx));
		}
		setCheckRegionValid();
	}
	
	public abstract void setGridParameters();

	public void setSamplePaths(Path[] sampleImage)
	{
		samplePaths = sampleImage;
		if (samplePaths != null) {
			Bitmap b = Bitmap.createBitmap(svW, svH-scrollerHeight, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(b);
			for (int k=0; k<samplePaths.length; k++) {
				canvas.drawPath(samplePaths[k], dataBrush);
			}
			sampleBitmap = b;
			setSampleBitmapSrc();
		} else {
			sampleBitmap = null;
		}
	}

	protected void setSampleBitmapSrc()
	{
		if (sampleBitmap != null) {
			int h = sampleBitmap.getHeight();
			int w = sampleBitmap.getWidth();
			int b = (int) (scrollX*(1-scrollL)*w);
			sampleBitmapSrc  = new Rect(b, 0, (int) (b+scrollL*w), h);
		}
	}

	/**
	 * @param start
	 */
	protected void setScrollerX(float start)
	{
		setScroller(start, scrollL);
	}
	
	/**
	 * @param len
	 */
	protected void setScrollerL(float len)
	{
		setScroller(scrollX, len);
	}
	
	/**
	 * sets the region for the scroller background ... should only be called if when view is resized
	 */
	protected void setScrollBack()
	{
		scrollerBackRect.set(0, svH-scrollerHeight, svW, svH);
	}

	/**
	 * @param w width of scroller knob in pixels
	 * @return scroller proportional length in range [0,1]
	 */
	protected float getLenForScrollerW(int w)
	{
		float l = ((float)w)/(svW-2);
		if (l < 0) l = 0; else if (l > 1) l = 1;
		return l;
	}
	
	/**
	 * @param x position of scroller knob in pixels
	 * @return scroller proportional position in range [0,1]
	 */
	protected float getStartForScrollerX(int x)
	{
		if (x < 0) return 0; else if (x >maxScrollerKnobX) return 1;
		if (maxScrollerKnobX <= 0) return 0;
		return ((float)x)/maxScrollerKnobX;
	}
	
	public void setRegionStart(long start)
	{
		regionStart = start;
		setCheckRegionValid();
	}
	
	public void setRegionLength(long length)
	{
		regionLength = length;
		setCheckRegionValid();
	}
	
	protected void setCheckRegionValid()
	{
		float pxRsO = pxRs;
		float pxReO = pxRe;
		pxRs = frame2px(regionStart);
		pxRe = frame2px(regionStart+regionLength);
		if (pxRs < pxRsO) pxRsO = pxRs;
		if (pxRe > pxReO) pxReO = pxRe;
//		Log.d("sample view", String.format("bounds of region %f %f w %d",pxRs, pxRe, svW));
		invalidate((int)pxRsO-20,0,(int)pxReO+20,svH);
	}
	
	protected long px2frame(float px)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentPlayer == null) return 0;
		float p = (scrollX*(1-scrollL)+scrollL*(px/svW));
//		Log.d("sc", String.format("px2f %f %f %f", p, scrollX, scrollL));
//		if (p < 0) p = 0; else if (p > 1) p = 1;
		return (long) (currentPlayer.getFrameCount()*p);
	}
	
	protected float frame2px(long frame)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentPlayer == null) return 0;
		float p = (((float)frame)/currentPlayer.getFrameCount())-scrollX*(1-scrollL);
		return svW*p/scrollL;
	}
	
	protected float dur2w(long frame)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentPlayer == null) return 0;
		float p = (((float)frame)/currentPlayer.getFrameCount());
		return svW*p/scrollL;
	}
	
	protected float secs2px(float s)
	{
		return frame2px((long) (s*defaultSampleRate));
	}
	
	protected float px2secs(int px)
	{
		return ((float)px2frame(px))/defaultSampleRate;
	}
	
/*************************************
 * OVERRIDES OF CORE VIEW ROUTINES
 *************************************/
	
	/**
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 */
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		svW = w;
		svH = h;
		svRect  = new Rect(0, 0, svW, svH-scrollerHeight);
		if (currentPlayer != null) {
			setToPlayer(currentPlayer);
		}
		setScrollBack();
		setScroller(scrollX, scrollL);
	}

	/**
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw (Canvas canvas)
	{
		if (currentPlayer != null) {
			drawGrid(canvas);
			// draw region
			if (pxRs < svW && pxRe > 0) {
//				if (pxRs < 0) pxRs = 0;
//				if (pxRe > svW) pxRe = svW;
				canvas.drawRect(pxRs, 0, pxRe, svH-scrollerHeight, regionBrush);
			}
			if (sampleBitmap != null) {
				canvas.drawBitmap(sampleBitmap, sampleBitmapSrc, svRect, null);
			}

			if (crsX >= 0 && crsX <= svW) {
				canvas.drawLine(crsX, 0, crsX, svH, cursorBrush);
			}
		}
		scrollerBack.setBounds(scrollerBackRect);	
		scrollerBack.draw(canvas);
		
		scrollerKnob.setBounds(scrollerKnobRect);	
		scrollerKnob.draw(canvas);
	}

	protected void drawGrid(Canvas canvas) {
		if (gridWidth > 0 && showGrid) {
			float gridX = pxRs;
			int i = 0;
			while (gridX < svW) {
				if (gridX >= 0) {
					drawGridLine(canvas, gridX, i);
				}
				i++;
				gridX += gridWidth;
			}
			gridX = pxRs-gridWidth;
			i = (gridMinorSubdivision*gridMajorSubdivision)-1;
			while (gridX > 0) {
				drawGridLine(canvas, gridX, i);
				i--;
				gridX -= gridWidth;
			}
		}
	}

	protected void drawGridLine(Canvas canvas, float gridX, int i) {
		if (i % gridMinorSubdivision == 0) {
			if (i % (gridMinorSubdivision*gridMajorSubdivision) == 0) {
				gridBrush.setStrokeWidth(2);
			} else {
				gridBrush.setStrokeWidth(1);			
			}
			gridBrush.setColor(gridMajorCol);
		} else {
			gridBrush.setStrokeWidth(1);			
			gridBrush.setColor(gridMinorCol);
		}	
		canvas.drawLine(gridX, 0, gridX, svH, gridBrush);
	}
	
	public void updateCursor() {
		invalidate(crsX-1, 0, crsX+1, svH);
		updateCursorX();
		invalidate(crsX-1, 0, crsX+1, svH);
	}

	protected void updateCursorX() {
		if (currentPlayer != null) {
			crsX = (int)(pxRs+currentPlayer.getCurrentPosition()*(pxRe-pxRs));
//			Log.d("cursor", String.format("%d = %g %g %g", crsX, currentPlayer.getCurrentPosition(), pxRe, pxRs));
		}
	}
	/**
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		int action = e.getActionMasked();
		int x0 = (int) e.getX();
		int y0 = (int) e.getY();
		if (action == MotionEvent.ACTION_DOWN) {
			if (scrollerBackRect.contains(x0, y0)) {
				if (Math.abs(scrollerKnobRect.right-x0) < kScrollerRightEdgeMargin) {
					setScaling(true);
				} else {
					touchOffset = x0 - scrollerKnobRect.left;
					if (touchOffset < 0) touchOffset = 0;
					setScrolling(true);					
				}
			} else {
				onTouchDown(e);
				setSelecting(true);
			}
		}
		
		if (isSelecting) {
			onTouchSelecting(e);
		} else if (isScrolling) {
			if (isScaling) {
				if (x0 > scrollerKnobRect.left+kMinimumScrollerWidth) {
					setScrollerL(getLenForScrollerW((int) (x0-scrollerKnobRect.left)));
				}
			} else {
				setScrollerX(getStartForScrollerX(x0-touchOffset));
				if (e.getPointerCount() > 1) {
					int x1 = (int) e.getX(1);
					if (x1 > x0) {
						setScrollerL(getLenForScrollerW(x1-x0));
					}
				} else {
				}
			}
			invalidate();
		}
		/*
		for (int i=0; i<e.getPointerCount(); i++) {
			float pnx = (((float)e.getX(i))/svW);
			float pny = 1-((float)e.getY(i))/svH;
			if (pnx < 0) pnx = 0; else if (pnx > 1) pnx = 1;
			if (pny < 0) pny = 0; else if (pny > 1) pny = 1;
		}
		*/
		if (action == MotionEvent.ACTION_UP) {
			if (isScrolling) {
				setScrolling(false);
				invalidate();
			}
			if (isSelecting) {
				setSelecting(false);
			}
			if (isScaling) {
				setScaling(false);
			}
		} else {
//			clearXYCATrack(tracked);
		}
		return true;
	}

	protected void onTouchDown(MotionEvent e) {
	}

	protected void onTouchSelecting(MotionEvent e) {
		invalidate();
	}
	

	/**
	 * @see android.view.View#onDragEvent(android.view.DragEvent)
	 */
	@Override
	public boolean onDragEvent (DragEvent event)
	{
		return super.onDragEvent(event);
	}
	
	/** 
	 * @see android.view.View#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
	    final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isSelecting) {
        	mergeDrawableStates(drawableState, STATE_SELECTING);
        } else if (isScrolling) {
        	mergeDrawableStates(drawableState, STATE_SCROLLING);
        }
	    return drawableState;
	}
	
	/** 
	 */
	@Override
	protected void drawableStateChanged ()
	{
		super.drawableStateChanged();
		scrollerKnob.setState(getDrawableState());
	}
	
	/**
	 * @param s
	 */
	protected void setScrolling(boolean s)
	{
		isScrolling = s;
		refreshDrawableState();
	}
	
	protected void setScaling(boolean b) {
		isScaling = isScrolling = b;
		refreshDrawableState();
	}

	/**
	 * @param s
	 */
	protected void setSelecting(boolean s)
	{
		isScrolling = false;
		isScaling = false;
		isSelecting = s;
		refreshDrawableState();
	}



}
