#ifndef _ENVELOPESEGMENT_H
#define _ENVELOPESEGMENT_H

#include "Lookup.h"

/**
 * exponential envelope from yStart to yEnd, over Duration samples
 * alpha controls the curvature (the more negative the value, the faster
 * envelopeVal approaches yEnd. F. R. Moore - Elements of Computer Music p184.
 * reset restarts the envelope.
 */

class ExpEnvelope
{
public:
					ExpEnvelope();
					~ExpEnvelope();
						
	ExpEnvelope&	setAttackTime(unsigned long duration);
	ExpEnvelope&	setHoldTime(unsigned long duration);
	ExpEnvelope&	setDecayTime(unsigned long duration);
	ExpEnvelope&	setReleaseTime(unsigned long duration);
	ExpEnvelope&	setDecayAlpha(float alpha);
	ExpEnvelope&	setAttackAlpha(float alpha);
	ExpEnvelope&	setEndpoints(float start,float end);


	ExpEnvelope&	reset(void);
	
	float			operator()(void);

private:
	float			theEnv;

	unsigned long	curTime;
	
	float			yStart,yEndmStart;
	float			sustainLevel;
	float			d_alpha;
	float			a_alpha;
	
	unsigned long	attackTime;
	unsigned long	holdTime;
	unsigned long	decayTime;
	unsigned long	releaseTime;
	
	float			onemEAlpha; //(1.-exp(alpha)
	float			onepLAlpha; //(1.+exp(alpha)
	float 			eMone;
	BoundedLookup	*emap;
	BoundedLookup	*amap;
};

#include <math.h>

#define TABLE_LENGTH	512
#define FALLOFF	200.0

inline ExpEnvelope::ExpEnvelope()
{
	eMone=exp((float)1.0)-1;
	FunctionTable* ft = new FunctionTable( TABLE_LENGTH );
	for(short i=0;i<=TABLE_LENGTH;i++) {
		(*ft)[i]=1. - exp(((float)
						(i-(TABLE_LENGTH/2.))/(TABLE_LENGTH/2.))
						*FALLOFF);
	}
	emap = new BoundedLookup(ft,-200,200);
	
	FunctionTable* gt = new FunctionTable( TABLE_LENGTH );
	for(short i=0;i<=TABLE_LENGTH;i++) {
		(*gt)[i]=log((((float)i/TABLE_LENGTH)*FALLOFF+1));
	}
	amap = new BoundedLookup(gt,0,200);
	
	reset();
	setAttackAlpha(200);
	setAttackTime(0);
	setHoldTime(0);
	setDecayTime(2);
	setDecayAlpha(-7);
	setReleaseTime(0);
}


inline ExpEnvelope&
ExpEnvelope::setEndpoints(float start,float end)
{
	yStart=start;
	yEndmStart=end-start;
	return *this;
}
	

inline ExpEnvelope&
ExpEnvelope::reset(void)
{
	curTime=0;
	return *this;
}


inline ExpEnvelope&
ExpEnvelope::setAttackTime(unsigned long attduration)
{
	attackTime=attduration;
	return *this;
}

inline ExpEnvelope&
ExpEnvelope::setHoldTime(unsigned long holdt)
{
	holdTime=holdt;
	return *this;
}

inline ExpEnvelope&
ExpEnvelope::setDecayTime(unsigned long duration)
{
	decayTime=duration;
	return *this;
}

inline ExpEnvelope&
ExpEnvelope::setReleaseTime(unsigned long duration)
{
	releaseTime=duration;
	return *this;
}

inline ExpEnvelope&
ExpEnvelope::setDecayAlpha(float alpha)
{
	this->d_alpha=alpha;
	//onemEAlpha=1.-exp(d_alpha);
	onemEAlpha=(*emap)[d_alpha];
	return *this;
}
	

inline ExpEnvelope&
ExpEnvelope::setAttackAlpha(float alpha)
{
	a_alpha=alpha;
	onepLAlpha=(*amap)[a_alpha];
	return *this;
}

inline float
ExpEnvelope::operator()(void)
{
//		fprintf(stderr, "@ %d\n", curTime);
	unsigned		segTime = curTime;
	if (segTime < attackTime) {
// can't do this with the current maps... us it worth it?
//		theEnv =  yStart*(exp(((float)segTime)/((float)attackTime))-1)/
//							EMone;
		theEnv =  yStart*(((float)segTime)/((float)attackTime)); // TODO should maybe be an exponential
	} else {
		segTime -= attackTime;
		if (segTime < holdTime || holdTime == SAMPLE_FOREVER) {
			theEnv = yStart;
		} else {
			segTime -= holdTime;
			if (segTime < decayTime) {
				theEnv =  yStart + 
							yEndmStart*
								(((*emap)[ (segTime/(float)decayTime)*d_alpha]) /onemEAlpha);
			} else {
				theEnv = yStart + yEndmStart;
			}
		}
	}
	curTime++;
	return theEnv;
}

inline ExpEnvelope::~ExpEnvelope() {}
#endif
