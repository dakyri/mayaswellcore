package com.mayaswell.audio;

import android.util.Log;

public class SampleFrame extends Number implements Comparable<SampleFrame>
{
	private static final long serialVersionUID = 1L;
	
	public static int dfltSampleRate = 44100;

	public SampleFrame() {
		value = 0;
	}
	
	public static long parseSampleFrame(String s)
	{
		return parseSampleFrame(s, dfltSampleRate);
	}
	
	public static long parseSampleFrame(String s, int sampleRate)
	{
		if (sampleRate <= 0) {
			throw new NumberFormatException("badly formed sample frame");
		}
		if (s==null) return 0;
		long frs = 0;
		long secs = 0;
		long mins = 0;
		String[]m1 = s.split(":");
		if (m1.length <= 1) {
			return Long.parseLong(s);
		} else if (m1.length == 2) {
			secs = Long.parseLong(m1[0]);
			frs = Long.parseLong(m1[1]);
		} else {
			frs = Long.parseLong(m1[2]);
			mins = Long.parseLong(m1[0]);
			secs = Long.parseLong(m1[1]);
		}
//		Log.d("sf parse", String.format("%d %d %d", mins, secs, frs));
		return (mins*60+secs)*sampleRate+frs;
	}
	
	public static String toString(long v, int sampleRate)
	{
		if (sampleRate <= 0) {
			throw new NumberFormatException("badly formed sample frame");
		}
		long frames = v % sampleRate;
		long tsecs = v / sampleRate;
		long secs = tsecs % 60;
		long mins = tsecs / 60;
//		Log.d("sample frame", String.format("%d %d %d", mins, secs, frames));
		if (mins == 0) {
			if (secs == 0) return Long.toString(frames);
			return String.format("%d:%d", secs, frames);
		}
		return String.format("%d:%d:%d", mins, secs, frames);
	}	
	
	public static String toString(long v)
	{
		return toString(v, SampleFrame.dfltSampleRate);
	}
	
	public static String toString(SampleFrame v, int sampleRate)
	{
		return toString(v.longValue(), sampleRate);
	}
	
	public static String toString(SampleFrame v)
	{
		return toString(v.longValue(), dfltSampleRate);
	}
	
	public String toString()
	{
		return toString(value);
	}

	@Override
	public int compareTo(SampleFrame v)
	{
		long thisVal = this.value;
		long anotherVal = v.value;
		return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof SampleFrame) {
			return value == ((SampleFrame)obj).longValue();
		}
		return false;
	}
	
	@Override
	public double doubleValue() {
		return value;
	}

	@Override
	public float floatValue() {
		return value;
	}

	@Override
	public int intValue() {
		return (int) value;
	}

	@Override
	public long longValue() {
		return value;
	}

	private long value;
}
