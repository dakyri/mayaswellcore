package com.mayaswell.widget;

import com.mayaswell.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class MenuControl extends RelativeLayout {
    public interface MenuControlListener {
    	public void onValueChanged(int position);
    }

    private final String schemaName = "http://schemas.android.com/apk/res-auto";
	
	private TextView label = null;
	private Spinner spinner = null;

    private MenuControlListener listener=null;

	private int val=0;
    
	public MenuControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public MenuControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public MenuControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setup(context, attrs);
	}
	
	public SpinnerAdapter getAdapter()
	{
		if (spinner == null) return null;
		return spinner.getAdapter();
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		TypedArray mwvAttrs = null;
		TypedArray mwsAttrs = null;

		LayoutInflater li = (LayoutInflater)getContext ().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		int layoutId = R.layout.mw_select_control;
		if (attrs != null) {
			layoutId = attrs.getAttributeResourceValue(schemaName, "layoutId", R.layout.mw_select_control);
		}
		li.inflate (layoutId, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		spinner = (Spinner)findViewById (R.id.mwCtlSpinner);
		
		if (spinner != null) {
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() 
				{	
				    @Override
				    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
				    {
				    	notifyListener(position);
				    }
		
				    @Override
				    public void onNothingSelected(AdapterView<?> parentView) 
				    {
				    }
				});
		}

		if (attrs != null) {
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwsAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWSpinner, 0, 0);
			
			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				label.setText(lbl);
			}
			int px = (int) mwvAttrs.getDimension(R.styleable.MWView_labelWidth, -1);
			if (px > 0) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}

			px = (int) mwsAttrs.getDimension(R.styleable.MWSpinner_spinnerWidth, -1);
			if (px > 0 && spinner != null) {
				LayoutParams params = (LayoutParams) spinner.getLayoutParams();
				params.width = px;
			}
		}
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwsAttrs != null) mwsAttrs.recycle();
	}
	
    public void setValueListener(MenuControlListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(int v)
	{
		if (listener != null)
			listener.onValueChanged(v);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param pos
	 */
	public void setSelection(int pos)
	{
		if (spinner != null) spinner.setSelection(pos);
	}
	
	public int getSelectedPosition()
	{
		if (spinner == null) return -1;
		return spinner.getSelectedItemPosition();
	}
	
	public int getValue()
	{
		return val;
	}

	public void setAdapter(ArrayAdapter<?> adapter)
	{
		if (spinner != null) {
			spinner.setAdapter(adapter);
		}
	}
}
