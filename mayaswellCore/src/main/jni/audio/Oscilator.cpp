/*
 * Oscilator.cpp
 *
 *  Created on: Dec 10, 2015
 *      Author: dak
 */

#include <android/log.h>

#include "Oscilator.h"

#include "com_mayaswell_audio_Controllable_OscilatorWave.h"

Oscilator::Oscilator()
	: type(com_mayaswell_audio_Controllable_OscilatorWave_SAW)
{
	setFrequency(220);
}

Oscilator::~Oscilator() {
}

void
Oscilator::reset() {
	__android_log_print(ANDROID_LOG_DEBUG, "Sawtooth", "reset, inc %d", type);
	switch (type) {
		case com_mayaswell_audio_Controllable_OscilatorWave_UNITY: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SQUARE: {
			square.reset();
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_TRIANGLE: {
			triangle.setPhase(0);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SAW: {
			saw.reset();
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SINE: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_NOISE: {
			break;
		}
	}
}

void
Oscilator::setFrequency(float f) {
	frequency = f;
	switch (type) {
		case com_mayaswell_audio_Controllable_OscilatorWave_UNITY: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SQUARE: {
			square.setFrequency(f);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SAW: {
			saw.setFrequency(f);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_TRIANGLE: {
			triangle.setFrequency(f);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SINE: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_NOISE: {
			break;
		}
	}
}

void
Oscilator::setShape(float w) {
	switch (type) {
		case com_mayaswell_audio_Controllable_OscilatorWave_UNITY: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SQUARE: {
			square.setWidth(w);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SAW: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_TRIANGLE: {
			triangle.setSkew(w);
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_SINE: {
			break;
		}
		case com_mayaswell_audio_Controllable_OscilatorWave_NOISE: {
			break;
		}
	}
}

void Oscilator::setWaveform(int wf) {
	if (type != wf) {
		type = wf;
		setFrequency(frequency);
		reset();
	}
}
