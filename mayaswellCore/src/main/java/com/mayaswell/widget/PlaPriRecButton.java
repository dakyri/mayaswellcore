package com.mayaswell.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageButton;

import com.mayaswell.R;

/**
 * Created by dak on 5/30/2016.
 */
public class PlaPriRecButton extends ImageButton {
	private static final int[] STATE_PLAYING = {R.attr.state_playing};
	private static final int[] STATE_PRIMED = {R.attr.state_primed};
	private static final int[] STATE_RECORDING = {R.attr.state_recording};
	private boolean isPrimed=false;
	private boolean isPlaying=false;
	private boolean isRecording=false;

	public PlaPriRecButton(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public PlaPriRecButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public PlaPriRecButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	protected void setup(Context context, AttributeSet attrs, int defStyle) {

	}

	/**
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	public int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
//		Log.d("draw button", "merge drawable "+", "+Boolean.toString(isPlaying)+", "+Boolean.toString(isRecording)+", "+Boolean.toString(isPrimed));
		if (isPrimed) {
			mergeDrawableStates(drawableState, STATE_PRIMED);
		}
		if (isPlaying) {
			mergeDrawableStates(drawableState, STATE_PLAYING);
		} else if (isRecording) {
			mergeDrawableStates(drawableState, STATE_RECORDING);
		}
		return drawableState;
	}

	public void setPlaying(boolean s) {
		isPlaying = s;
		isPrimed = false;
		refreshDrawableState();
	}

	public void setPrimed(boolean s) {
		isPrimed = s;
		refreshDrawableState();
	}

	public void setRecording(boolean s) {
		isRecording = s;
		isPrimed = false;
		refreshDrawableState();
	}


}
