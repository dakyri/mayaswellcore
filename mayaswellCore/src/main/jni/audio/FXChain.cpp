/*
 * FXChain.cpp
 *
 *  Created on: Dec 6, 2014
 *      Author: dak
 */

#include "FXChain.h"


FXChain::FXChain(short _id, int maxFX, int maxFXparam, TimeSource &t, bool lock)
{
	id = _id;
	for (int i=0; i<maxFX; i++) {
		fx.push_back(Effect(t, lock));
	}
	sideSrcBuffer = sideDstBuffer = NULL;
	hasTail = false;
}

FXChain::~FXChain()
{
}


bool
FXChain::setFXType(int whichFX, int type, int nParams, float *params)
{
	if (whichFX < 0 || whichFX >= fx.size()) {
		return false;
	}
	bool b = fx[whichFX].setType(type, nParams, params);
	checkTail();
//	for (Effect f: fx) {
//		__android_log_print(ANDROID_LOG_DEBUG, "FXChain", "::setFXType %d %d %d %g", id, f.type, nParams, nParams>0?params[0]:0);
//	}
	return b;
}

bool
FXChain::setFXEnable(int whichFX, bool en)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "FXChain", "setFXEnable %d %d", whichFX, en);
	if (whichFX < 0 || whichFX >= fx.size()) {
		return false;
	}
	fx[whichFX].setEnable(en);
	return true;
}

bool
FXChain::setFXParam(int whichFX, int whichParam, float val, bool cacheValue)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "FXChain", "setFXParam %d par %d val %g", whichFX, whichParam, val);
	if (whichFX < 0 || whichFX >= fx.size()) {
		return false;
	}
	return fx[whichFX].setParam(whichParam, val, cacheValue);
}

float
FXChain::getParam(int whichFX, int p)
{
	if (whichFX < 0 || whichFX >= fx.size()) {
		return 0;
	}
	return fx[whichFX].getParam(p);
}

int
FXChain::nParams(int whichFX)
{
	if (whichFX < 0 || whichFX >= fx.size()) {
		return 0;
	}
	return fx[whichFX].nParams();
}

bool
FXChain::setFXParams(int whichFX, int nParams, float *params)
{
	if (whichFX < 0 || whichFX >= fx.size()) {
		return false;
	}
	;
	return fx[whichFX].setParams(nParams, params);
}

void
FXChain::setSideChainSrc(float *src)
{
	sideSrcBuffer = src;
}

void
FXChain::setSideChainDst(float *dst)
{
	sideDstBuffer = dst;
}


unsigned int
FXChain::applyFX(float *buf, int nFrame)
{
	for (int i=0; i<fx.size(); i++) {
		if (fx[i].enable && fx[i].type != com_mayaswell_audio_FX_UNITY) {
//			__android_log_print(ANDROID_LOG_DEBUG, "FXChain", "applyFX %d %d", fx[i].enable, fx[i].type);
			fx[i].apply(buf, nFrame);
		}
	}
	return nFrame;
}

void
FXChain::checkTail()
{
	hasTail = false;
	for (int i=0; i<fx.size(); i++) {
		if (fx[i].hasTail) {
			hasTail = true;
			break;
		}
	}
}

