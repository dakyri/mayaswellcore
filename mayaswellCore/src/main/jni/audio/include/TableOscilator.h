#ifndef TABLEOSCILI_H
#define TABLEOSCILI_H

#include "AudioSignal.h"
#include "FunctionTable.h"
//#include <android/log.h>

class TableOscilator{	
public:
						TableOscilator(FunctionTable *wavetable=0,
										float phase = 0.);		
						~TableOscilator();
					
	TableOscilator&		setFunkTable(FunctionTable *wavetable);
	TableOscilator&		setPhase(float phase);	//0 <= phase <=1.
	TableOscilator&		setFrequency(float freq, float sampleRate);
	TableOscilator&		setFrequency(float freq);
	TableOscilator&		setRelativeFrequency(float freq);
	
	Sample				operator ()	(void);
	void				operator ()	(Signal& outSig);

	Sample				operator *(void);

	float 				phs; //0. to wavetable->Length;
	float 				si;
	FunctionTable 		*wavetable;
};


//inline members

inline TableOscilator::TableOscilator(FunctionTable *wavetable, float phase) 
						:phs(phase), wavetable(wavetable) { phs=0; si=1; }

inline TableOscilator::~TableOscilator() {}

inline TableOscilator&
TableOscilator::setFunkTable(FunctionTable *wavetable)
{
	this->wavetable=wavetable;
	return *this;
}

inline TableOscilator&
TableOscilator::setPhase(float phase)
{	
	phs = phase*wavetable->Length();
	return *this;
}

inline TableOscilator&
TableOscilator::setFrequency(float freq, float sampleRate)
{
	si=((float)wavetable->Length())*freq/sampleRate;
	return *this;
}

inline TableOscilator&
TableOscilator::setFrequency(float freq)
{
	si=((float)wavetable->Length())*freq/signalSampleRate;
	return *this;
}

inline TableOscilator&
TableOscilator::setRelativeFrequency(float rFreq)
{
	si=((float)wavetable->Length())*rFreq;
	return *this;
}


/*
 * Wed 10/23/2013... ah sweet memory .... still not sure what caused the issue, but the class seems ok now
 *
 * the optimizer causes this to crash with a seg fault under some circumstances
 * this may be a compiler bug, or an indication of a deeper bug of
 * mine that has not yet surfaced. The non-o\inline version of this
 * routine is living in Rack747.cpp
 */
inline Sample
TableOscilator::operator()(void)
{
	unsigned long idx = (unsigned long)phs;
	float frac = phs - idx;
		
	phs+=si;
	if(phs>(int)wavetable->Length())
		phs-=wavetable->Length();
					
	return (*wavetable)[idx] +
				((*wavetable)[idx+1] - (*wavetable)[idx])*frac;
}

inline void
TableOscilator::operator()(Signal& outSig)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		outSig[i]=operator()();
}

inline Sample
TableOscilator::operator*(void)
{
	unsigned long idx = (unsigned long)phs;
	float frac = phs - idx;

	return (*wavetable)[idx] +
				((*wavetable)[idx+1] - (*wavetable)[idx])*frac;
}

#endif
