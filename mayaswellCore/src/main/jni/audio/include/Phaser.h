/*
 * Phaser.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */

#ifndef PHASER_H_
#define PHASER_H_

#include "AudioSignal.h"

class Phaser {
public:
	Phaser();
	virtual ~Phaser();

	void initialize();
	void reset();
	void release();

	void setParameters(float rate, float width, float fb, int stages, float mix);
	void setRate (float v);
	void setWidth (float v);
	void setFeedback (float v);
	void setStages (int v);
	void setMix (float v);
	void setSweep(void);

	void apply(float *signal, long sampleFrames);
protected:
	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	int paramStages;			// ditto
	float paramMix;		// ditto
	double sweepRate;			// actual calc'd sweep rate
	double width;				// 0-100%
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 or 1.0
	int stages;				// calc'd # of stages 2-10

	double wp;					// freq param for equation
	double minwp;
	double maxwp;
	double sweepFactor;		// amount to multiply the freq by with each sample

	// the all pass line
	double lx1;
	double ly1;
	double lx2;
	double ly2;
	double lx3;
	double ly3;
	double lx4;
	double ly4;
	double lx5;
	double ly5;
	double lx6;
	double ly6;
	double lx7;
	double ly7;
	double lx8;
	double ly8;
	double lx9;
	double ly9;
	double lx10;
	double ly10;
};


#define BOTTOM_FREQ 100
#define PIN(n,min,max) ((n) > (max) ? max : ((n) < (min) ? (min) : (n)))

#endif /* PHASER_H_ */
