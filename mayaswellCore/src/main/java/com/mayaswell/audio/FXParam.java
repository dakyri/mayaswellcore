package com.mayaswell.audio;
public class FXParam
{
	public enum Type {
		INT, FLOAT, TIME;
	}
	private int id;
	
	private Type type;
	private String displayName;
	private String saveName;
	private String shortName;
	
	private float min = 0;
	private float max = 1;
	private float defaultValue = 0;
	private int labelDisplayEms = 0;
	private int valueDisplayEms = 0;
	private boolean logScale = false;
	
	public FXParam(int id, FXParam.Type t, String displayName, String saveName, String shortName, float min, float defaultValue, float max)
	{
		this(id, t, displayName, saveName, shortName, min, defaultValue, max, false);
	}
	
	public FXParam(int id, FXParam.Type t, String displayName, String saveName, String shortName, float min, float defaultValue, float max, boolean isLogScale)
	{
		this.id = id;
		this.displayName = displayName;
		this.saveName = saveName;
		this.shortName = shortName;
		this.min = min;
		this.max = max;
		this.defaultValue = defaultValue;
		this.displayName = displayName;
		this.setType(t);
		this.logScale = isLogScale;
	}
	
	public void setDisplayParams(int lde, int vde)
	{
		labelDisplayEms = lde;
		valueDisplayEms = vde;
	}
	
	public int getLabelDisplayEms()
	{
		return labelDisplayEms;
	}

	public int getValueDisplayEms()
	{
		return labelDisplayEms;
	}

	public float getMin() {
		return min;
	}

	public void setMin(float min) {
		this.min = min;
	}

	public float getMax() {
		return max;
	}

	public void setMax(float max) {
		this.max = max;
	}
	
	public String toString()
	{
		return displayName;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getSaveName()
	{
		return saveName;
	}
	
	public String getShortName()
	{
		return shortName;
	}

	public float getCenterValue() {
		return defaultValue;
	}

	public boolean isLogScale() {
		return logScale;
	}

	public void setLogScale(boolean logScale) {
		this.logScale = logScale;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
}
