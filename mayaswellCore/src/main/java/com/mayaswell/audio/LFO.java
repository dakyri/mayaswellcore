package com.mayaswell.audio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;


public class LFO extends Modulator  implements Parcelable
{
	public interface Host extends Controllable {
		boolean setNLFO(int n);
		boolean setLFOList(ArrayList<LFO> lfo);
		boolean setLFORate(int xyci, float amt);
		boolean setLFOPhase(int xyci, float amt);
		boolean setLFOLock(int xyci, boolean amt);
		boolean setLFOReset(int xyci, ResetMode amt);
		boolean setLFOWave(int xyci, LFWave amt);
		boolean setNLFOTarget(int xyci, int n);
		boolean setLFOTarget(int xyci, int xycj, ICControl target, float amt);
		boolean setLFOTargetAmt(int xyci, int xycj, float amt);
		int maxLFO();
		int maxLFOTgt();
		ControlsAdapter getLFOTgtAdapter();
		ControlsAdapter getInterfaceTgtAdapter();
		ArrayList<LFO> lfos();
		
		boolean alwaysLockedTempo();
	}
	public static final float NormalizedMinRate = 0.1f;
	public static final float NormalizedMaxRate = 32;
	public LFWave waveform;
	public float rate;
	public float phase;
	
	public LFO(LFWave wf, boolean tempoLock2, ResetMode trigger2, float rate2, float phase2)
	{
		waveform = wf;
		tempoLock = tempoLock2;
		trigger = trigger2;
		rate = rate2;
		phase = phase2;
		target = new ArrayList<ModulationTarget>(); 
	}

	public LFO()
	{
		this(LFWave.SIN, true, ResetMode.ONFIRE, 1, 0);
	}

	public LFO clone()
	{
		LFO lfo = new LFO(waveform, tempoLock, trigger, rate, phase);
		for (ModulationTarget m: target) {
			lfo.add(m.clone());
		}
		return lfo;
	}

	public static final Creator<LFO> CREATOR = new Creator<LFO>() {
		@Override
		public LFO createFromParcel(Parcel in) {
			return new LFO(in);
		}

		@Override
		public LFO[] newArray(int size) {
			return new LFO[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	protected LFO(Parcel in) {
		rate = in.readFloat();
		phase = in.readFloat();

		tempoLock = in.readInt() != 0;
		trigger = ResetMode.parse(in.readString());
		target = new ArrayList<ModulationTarget>();
		in.readTypedList(target, ModulationTarget.CREATOR);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeFloat(rate);
		dest.writeFloat(phase);

		dest.writeInt(tempoLock? 1 : 0);
		dest.writeString(trigger.toString());
		dest.writeTypedList(target);
	}


}
