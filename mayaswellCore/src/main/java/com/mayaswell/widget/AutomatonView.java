package com.mayaswell.widget;

import java.util.ArrayList;

import com.mayaswell.util.Automaton;
import com.mayaswell.util.Automaton.Node;
import com.mayaswell.util.Automaton.NodeConnection;
import com.mayaswell.util.CellDataType;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;

public abstract class AutomatonView<CDT extends CellDataType> extends View {

	public static int[] regginbow = null;
	
	public interface Listener {
		void nodeSelected(int nid);
		void cellSelected(int nid, int cdid);
		void cnxSelected(int nid, int cnxid);
		void nodePlayed(int nid);
	}

	private Listener listener = null;

	protected Automaton<CDT> automaton = null;
	protected float gapY = 40;
	protected float marginY = 20;
	protected float marginX = 15;
	protected float cellMarginY = 5;
	protected float cellMarginX = 5;
	protected float cellLabelWidth = 40;
	protected float cnxHeight = 40;
	protected float cnxWidth = 40;
	private float cnxMarginY = 5;
	
	protected int highlightNodeId=-1;
	protected int highlightCnxId=-1;

	protected Paint outlineBrush;
	protected Paint textBrush;
	protected Paint fillBrush;
	private float densityMultiplier;
	protected float textScaledPx;

	private ArrayList<Automaton<CDT>.Node> drawn = new  ArrayList<Automaton<CDT>.Node>();
	private int nnxCnxCnt;
	private float minHeight=300;
	private float minWidth=300;
	private float thisx0=0;
	private float thisy0=0;
	protected Automaton<CDT>.NodeConnection cnxFrom=null;
	protected Automaton<CDT>.Node cnxNodePossibility = null;
	protected Automaton<CDT>.Node selectedNode = null;
	protected Automaton<CDT>.Node cnxFromNode = null;
	protected boolean isMakingConnection = false;
	private float cnxFromX;
	private float cnxFromY;
	
	private int nodeOutlnColCnxPossible = 0xffff0000;
	private int nodeOutlnColSelected = 0xff0000ff;
	private int nodeOutlnCol = 0xff000000;
	private int nodeCnxOutlnCol = 0xff22aa22;
	private int nodeCnxFillCol = 0xffdddddd;
	private int nodeFillColActive = 0xffaaddaa;
	private int cnxSelectLineCol = 0xff0000ff;
	private int cnxLineCol = 0xff222222;
	protected int textCol = 0xff000000;
	private int nodeFillCol = 0xffffffff;
	
	public AutomatonView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public AutomatonView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public AutomatonView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		outlineBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		outlineBrush.setStyle(Paint.Style.STROKE);
		outlineBrush.setColor(nodeOutlnCol);
		
		fillBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		fillBrush.setStyle(Paint.Style.FILL);
		fillBrush.setColor(nodeFillCol );
		
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = densityMultiplier * 10;
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.RIGHT);
		textBrush.setColor(textCol);
		int col = 0xff000000;
		if (regginbow != null) {
			for (int i=0; i<regginbow.length; i++) {
				regginbow[i] = col;
				col += 0x101010;
			}
		}
	}
	
	public void setRegginbow(int [] rb) {
		regginbow = new int[rb.length];
		for (int i=0; i<regginbow.length; i++) {
			regginbow[i] = rb[i];

		}
	}

	public void setTo(Automaton<CDT> alg) {
		automaton = alg;
		layoutAutomaton(alg);
		invalidate();
	}

	private ArrayList<Automaton<CDT>.Node> laid = new  ArrayList<Automaton<CDT>.Node>();
	protected float[] rowX;
	protected float[] rowY;
	protected float[] rowO;
	private float maxNodeWidth = 0;
	private int maxLevel = 0;
	protected float avh = 0;
	protected float avw = 0;
	private int[] nodeRow;
	private int[] rowCounto;
	private int[] rowCounti;

	protected int cnxcolind = 0;
	protected boolean isScrolling = false;
	protected float boundsL = 0;
	protected float boundsR = 0;
	protected float boundsB = 0;
	private float lastx0 = 0;
	private float lasty0 = 0;
	private float lastrx = 0;
	private float lastry = 0;

	private boolean editLocked = false;

	
	public void forceAutomatonLayout() {
		layoutAutomaton(automaton);
	}

	protected void layoutAutomaton(Automaton<CDT> alg) {
		if (alg == null) {
			return;
		}
		laid.clear();
		rowY = new float[alg.countNodes()+1];
		rowX = new float[alg.countNodes()+1];
		rowO = new float[alg.countNodes()+1];
		nodeRow = new int[alg.countNodes()+1];
		rowCounto = new int[alg.countNodes()+1];
		rowCounti = new int[alg.countNodes()+1];
		maxNodeWidth = 0;
		maxLevel = 0;
		avh = 0;
		avw = 0;
		for (int i=0; i<rowX.length; i++) {
			rowX[i] = marginX;
			rowY[i] = marginY;
			rowO[i] = 0;
			nodeRow[i] = -1;
		}
		if (alg != null) {
//			Log.d("allocate", "root allocation "+automaton.countNodes()+" nodes");
			layoutAutomatonNode(alg.getRoot(), 0);
			for (int i=0; i<automaton.countNodes(); i++) {
//				Log.d("allocate", "node "+i);
				Automaton<CDT>.Node n = automaton.getNode(i);
				layoutAutomatonNode(n, maxLevel+1);
			}
			float w = getMeasuredWidth();

			for (int i=0; i<rowX.length; i++) {
				rowO[i] = (w - rowX[i]) / 2;
			}
			boundsR = boundsL = boundsB = 0;
			automaton.getRoot().offset(rowO[0], 0);
			for (int i=0; i<automaton.countNodes(); i++) {
				Automaton<CDT>.Node n = automaton.getNode(i);
				int ni = n.getId();
				if (ni >= 0 && ni < nodeRow.length) {
					int nri = nodeRow[n.getId()];
					if (nri >= 0 && nri<rowX.length) {
//						Log.d("automaton", "offseting "+n.getId()+" "+nri+" by "+rowO[nri]);
						n.offset(rowO[nri], 0);
						if (n.nodeRect.right > maxNodeWidth) {
							maxNodeWidth = n.nodeRect.right;
						}
						if (n.nodeRect.right > boundsR) {
							boundsR = n.nodeRect.right;
						}
						if (n.nodeRect.left < boundsL) {
							boundsL = n.nodeRect.left;
						}
						if (n.nodeRect.bottom > boundsB) {
							boundsB = n.nodeRect.bottom;
						}
					}
				}
			}
		}
	}
	
	protected void layoutAutomatonNode(Automaton<CDT>.Node n, int lvl) {
		if (n == null || laid.contains(n)) {
			return;
		}
		if (lvl > maxLevel) {
			maxLevel = lvl;
		}
		float y = rowY[lvl];
		float x = rowX[lvl];
//		Log.d("automaton", "pre allocation lvl "+lvl+" rowx "+rowX[lvl]+" "+n.getId());
		allocateCellRect(n, x, y);
		if (n.getId() < nodeRow.length && n.getId() >= 0) {
			nodeRow[n.getId()] = lvl;
		} else {
			Log.d("layout", "layoutAutomatonNode(): invalid node id  "+n.getId()+" lvl "+lvl);
		}
		if (n.nodeRect.bottom + gapY > avh) {
			avh = n.nodeRect.bottom + gapY;
			if (lvl < rowY.length-1 && avh > rowY[lvl+1]) {
				rowY[lvl+1] = avh;
			}
		}
		rowX[lvl] = n.nodeRect.right+marginX;
//		Log.d("automaton", "x "+lvl+" = "+rowX[lvl]+" "+n.getId()+" is at right "+n.nodeRect.right+" left "+n.nodeRect.left);
		if (rowX[lvl] > maxNodeWidth) {
			maxNodeWidth = rowX[lvl];
		}
		laid.add(n);
		for (int i=0; i< n.countCnx(); i++) {
			Automaton<CDT>.NodeConnection ncnx = n.getCnx(i);
			if (ncnx != null) {
				Automaton<CDT>.Node cn = ncnx.getNode();
				if (!laid.contains(cn)) {
					layoutAutomatonNode(cn, lvl+1);
				} else {
				}
			}
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Automaton<CDT> a = automaton;
		if (a != null) {
			drawn.clear();
			cnxcolind = 0;
			nnxCnxCnt = 1;
			for (int i=0; i<rowCounto.length; i++) {
				rowCounto[i]=0;
				rowCounti[i]=0;
			}
			drawNodeChildren(canvas, a.getRoot(), 0);
			for (int i=0; i<automaton.countNodes(); i++) {
				Automaton<CDT>.Node n = automaton.getNode(i);
				drawNodeChildren(canvas, n, maxLevel+1);
			}
			if (isMakingConnection) {
				outlineBrush.setStrokeWidth(1);
				outlineBrush.setColor(cnxSelectLineCol);
				canvas.drawLine(cnxFromX, cnxFromY, thisx0, thisy0, outlineBrush);
			}
		}
	}
	
	private void drawNodeChildren(Canvas c, Automaton<CDT>.Node n, int lvl) {
		if (drawn.contains(n)) {
			return;
		}
		drawNode(c, n);
		for (int i=0; i< n.countCnx(); i++) {
			Automaton<CDT>.NodeConnection ncnx = n.getCnx(i);
			if (ncnx != null) {
				Automaton<CDT>.Node cn = ncnx.getNode();
				if (!drawn.contains(cn)) {
					drawNodeChildren(c, cn, lvl+1);
					drawConnection(c, n, i, cn, true);
				} else {
					boolean simplez = false;
					if (cn.getId() >= 0 && cn.getId() < nodeRow.length && nodeRow[cn.getId()] == lvl+1) {
						simplez = true;
					}
					drawConnection(c, n, i, cn, simplez);
				}
			}
		}
	}

	private void drawNode(Canvas c, Automaton<CDT>.Node n) {
		if (n == null) return;
		if (n == automaton.getCurrent()) {
			fillBrush.setColor(nodeFillColActive);
		} else {
			fillBrush.setColor(nodeFillCol);
		}
		c.drawRect(n.nodeRect, fillBrush);
		drawCell(c, n);
		if ((highlightNodeId >= 0 && n.getId() == highlightNodeId) || n == cnxNodePossibility) {
			outlineBrush.setStrokeWidth(2);
			outlineBrush.setColor(nodeOutlnColCnxPossible);
		} else if (n == selectedNode ) {
			outlineBrush.setStrokeWidth(2);
			outlineBrush.setColor(nodeOutlnColSelected);
		} else {
			outlineBrush.setStrokeWidth(1);
			outlineBrush.setColor(nodeOutlnCol);
		}
		c.drawRect(n.nodeRect, outlineBrush);
		if (n.getId() >= 0) {
			c.drawText(Integer.toString(n.getId()), n.nodeRect.left+cellLabelWidth-5, n.nodeRect.top+20, textBrush);
		}
		for (int i=0; i<n.cnxRect.size(); i++) {
			Automaton<CDT>.NodeConnection nc = n.getCnx(i);
			RectF r = n.cnxRect.get(i);
			if (r != null) {
				fillBrush.setColor(nodeCnxFillCol);
				c.drawRect(r, fillBrush);				
				outlineBrush.setColor(nodeCnxOutlnCol);
				outlineBrush.setStrokeWidth(1);
				c.drawRect(r, outlineBrush);
				if (nc != null) {
					c.drawText(Integer.toString(nc.getWeight()), r.left+20, r.top+20, textBrush);
				}
			}
		}
		drawn.add(n);
	}

	protected int getNodeId4(float x, float y) {
		if (automaton == null) return -1;
		if (automaton.getRoot().nodeRect.contains(x, y)) {
			return automaton.countNodes();
		}
		for (int i=0; i<automaton.countNodes(); i++) {
			Automaton<CDT>.Node n = automaton.getNode(i);
			if (n != null && n.nodeRect.contains(x, y)) {
				return n.getId();
			}
		}
		return -1;
	}

	protected int getCellCnxId4(int g, float x, float y) {
		if (automaton == null) return -1;
		if (g == -1) return -1;
		Automaton<CDT>.Node n = automaton.getNode(g);
		if (n != null) {
			for (int i=0; i<n.cnxRect.size(); i++) {
				if (n.cnxRect.get(i).contains(x,y)) {
					return i;
				}
			}
		}
		return -1;
	}

	protected void drawConnection(Canvas c, Automaton<CDT>.Node n, int cnxi, Automaton<CDT>.Node cn, boolean nxtRow) {
		if (n == null || cn == null) return;
		RectF r = n.cnxRect.get(cnxi);
		int col;
		if (regginbow == null || regginbow.length == 0) {
			col = cnxLineCol;
		} else {
			if (++cnxcolind >= regginbow.length) {
				cnxcolind = 0;
			}
			col = regginbow[cnxcolind];
		}
		outlineBrush.setStrokeWidth(3);
		if (nxtRow) {
			outlineBrush.setColor(cnxLineCol);
			c.drawLine((r.left+r.right)/2, r.bottom, (cn.nodeRect.left+cn.nodeRect.right)/2, cn.nodeRect.top, outlineBrush);
		} else {
			int crf = -1, crt = -1;
			int rowf = -1, rowt = -1;
			if (n.getId() >= 0) {
				rowf = nodeRow[n.getId()];
				if (rowf < 0) rowf = 0;
				crf = rowCounto[rowf];
			}
			
			if (cn.getId() >= 0) {
				rowt = nodeRow[cn.getId()];
				if (rowt < 0) rowt = 0;
				crt = rowCounti[rowt];
			}
			
			outlineBrush.setColor(col);
			float x=(r.left+r.right)/2;
			float y=r.bottom;
			c.drawLine(x, y, x, y=(y+(crf+1)*3) , outlineBrush);
			c.drawLine(x, y, x=(maxNodeWidth+nnxCnxCnt*cnxMarginY), y, outlineBrush);
			if (x+marginX > avw) {
				avw = x+marginX;
			}
			if (x+marginX > boundsR) {
				boundsR = x+marginX;
			}
			c.drawLine(x, y, x, y=cn.nodeRect.top-(crt+1)*3, outlineBrush);
			c.drawLine(x, y, x=(cn.nodeRect.left+cn.nodeRect.right)/2, y, outlineBrush);
			c.drawLine(x, y, x, y=cn.nodeRect.top, outlineBrush);
			nnxCnxCnt++;
			if (rowt >= 0) rowCounti[rowt]++;
			if (rowf >= 0) rowCounto[rowf]++;
		}
	}

	protected abstract void drawCell(Canvas canvas, Automaton<CDT>.Node n);
	protected abstract void allocateCellRect(Automaton<CDT>.Node n, float x, float y);
	protected abstract int getCellDataId4(int g, float x, float y);
	protected abstract boolean onTouchCellData(int nodeId, MotionEvent e, boolean clear);
	protected abstract boolean checkConnectionDelete(MotionEvent e);

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		Automaton<CDT> a = automaton;
		if (a == null) {
			return super.onTouchEvent(e);
		}
		Automaton<CDT>.Node n = null;
		Automaton<CDT>.NodeConnection nc = null;
		
		lastx0 = thisx0;
		lasty0 = thisy0;
		
		int action = e.getActionMasked();
		float x0 = e.getX() + getScrollX();
		float y0 = e.getY() + getScrollY();
		thisx0 = x0;
		thisy0 = y0;
//		Log.d("automaton", String.format("touch %g %g, %g %g, %d %d, %g %g", x0, y0, lastx0, lasty0, getScrollX(), getScrollY(), lastrx, lastry));
		
		int nodeId=-1;
		int cnxId=-1;
		int cellSlotId=-1;
		nodeId = getNodeId4(x0, y0);
		if (nodeId >= 0) {
			n = a.getNode(nodeId);
			if (n == null) {
				return super.onTouchEvent(e);
			}
			cnxId = getCellCnxId4(nodeId, x0, y0);
			if (cnxId >= 0 && cnxId < n.countCnx()) {
				nc = n.getCnx(cnxId);
				if (nc == null) {
					return super.onTouchEvent(e);
				}
			}
		}

		if (isScrolling) {
			if (action == MotionEvent.ACTION_MOVE) {
				float dx = lastrx - e.getX();
				float dy = lastry - e.getY();
				if (getScrollY() + dy < 0) {
					dy = 0; //-getScrollY();
				} else if (getScrollY() + getMeasuredHeight() + dy > boundsB) {
					dy = 0; //boundsB - getScrollY() - getMeasuredHeight();
				}
				if (getScrollX() + dx < boundsL) {
					dx = 0; //boundsL - getScrollX();
				} else if (getScrollX() + dx + getMeasuredWidth() > boundsR) {
					dx = 0; //boundsR - getScrollX() - getMeasuredWidth();
				}
				scrollBy((int)dx, (int)dy);
				lastrx = e.getX();
				lastry = e.getY();
			} else if (action == MotionEvent.ACTION_UP) {
				lastrx = e.getX();
				lastry = e.getY();
				if (isScrolling ) {
					isScrolling = false;
				} else {
				}
			}
			return true;
		}
		
		if (action == MotionEvent.ACTION_DOWN) {
			if (n != null && editLocked && listener != null) {
				listener.nodePlayed(nodeId);
				return false;
			}
		}

		if (!isMakingConnection) {
			if (cnxId < 0 && cnxFromNode == null) {
				if (onTouchCellData(nodeId, e, false)) {
					return true;
				}
			}
		} else {
			if (cnxFromNode != null && cnxFrom != null) {
				if (checkConnectionDelete(e)) {
					cnxFromNode.removeCnx(cnxFrom.getNode());
					layoutAutomaton(automaton);
					cnxFrom = null;
					cnxFromNode = null;
					isMakingConnection = false;
					return false;
				}
			}
		}
		if (action == MotionEvent.ACTION_DOWN) {
			if (n != null) {
				if (cnxId >= n.countCnx()) {
					startMakeCnx(n, null);
				} else if (nc != null) {
					startMakeCnx(n, nc);
					if (listener != null) {
						listener.nodeSelected(nodeId);
						listener.cnxSelected(nodeId, cnxId);
					}
				}
			} else {
				lastrx = e.getX();
				lastry = e.getY();
				isScrolling = true;
			}
		} else if (action == MotionEvent.ACTION_POINTER_DOWN) {
		} else if (action == MotionEvent.ACTION_UP) {
			if (isMakingConnection) {
				if (cnxFromNode != null) {
					finishMakeCnx();
				}
			}
//			cancelLongTouch();
		}
		if (cnxFromNode != null && cnxFromNode != n) {
			isMakingConnection  = true;
			highlightNodeId = nodeId;
			cnxNodePossibility  = n;
			onTouchCellData(nodeId, e, true);
		} else {
			highlightNodeId = -1;
		}
		if (isMakingConnection) {
			invalidate();
		}
		return true;
	}
	

	private void finishMakeCnx() {
		if (cnxFromNode != null) {
			if (cnxNodePossibility != null) {
				if (cnxFrom != null) {
					cnxFromNode.removeCnx(cnxFrom.getNode());
				}
				cnxFromNode.addCnx(cnxNodePossibility);
				layoutAutomaton(automaton);
			} else {
				
			}
		}
		isMakingConnection = false;
		cnxFromNode = null;
		cnxNodePossibility = null;
		cnxFrom = null;
		invalidate();
	}

	private void startMakeCnx(Automaton<CDT>.Node n, Automaton<CDT>.NodeConnection nc) {
		cnxFrom = nc;
		cnxFromNode = n;
		cnxFromX = thisx0;
		cnxFromY = thisy0;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
	    int heightSize = MeasureSpec.getSize(heightMeasureSpec);
	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//    	widthMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) 500, View.MeasureSpec.EXACTLY);
//    	heightMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) 500, View.MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public void setListener(Listener listener) {
		this.listener = listener;
	}


	public void invalidateNode(Automaton<CDT>.Node n) {
		layoutAutomaton(automaton);
		invalidate();
	}
	
	public void redrawNode(Automaton<CDT>.Node n) {
		if (n == null || n.nodeRect == null) return;
		postInvalidate((int)n.nodeRect.left, (int)n.nodeRect.top, (int)n.nodeRect.right, (int)n.nodeRect.bottom);
	}
	
	public void invalidateCell(CDT selectCell) {
		layoutAutomaton(automaton);
		invalidate();
	}

	public void invalidateCnx(Automaton<CDT>.NodeConnection selectCnx) {
		invalidate();
	}

	public boolean isLocked() {
		return editLocked;
	}

	public void setLocked(boolean locked) {
		editLocked  = locked;
	}

}
