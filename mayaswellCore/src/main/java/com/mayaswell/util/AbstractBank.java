package com.mayaswell.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator;
import com.mayaswell.audio.Controllable.ControllableManager;
import com.mayaswell.audio.Controllable.RXControlAssign;
import com.mayaswell.audio.Controllable.SXControlAssign;
import com.mayaswell.audio.Controllable.SensorControlManager;
import com.mayaswell.audio.Controllable.SensorInfo;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.util.AbstractPatch.Action;

public abstract class AbstractBank<T extends AbstractPatch, C extends Control> implements IBank {

	public ArrayList<T> patch = new ArrayList<T>();
	
	protected void fa(XmlSerializer xml, String a, float f) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.attribute("", a, Float.toString(f)); }
	protected void la(XmlSerializer xml, String a, long f) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.attribute("", a, Long.toString(f)); }
	protected void sa(XmlSerializer xml, String a, String f) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.attribute("", a, f); }
	protected void ba(XmlSerializer xml, String a, boolean f) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.attribute("", a, Boolean.toString(f)); }
	protected void xs(XmlSerializer xml, String a) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.startTag("", a); }
	protected void xe(XmlSerializer xml, String a) throws IllegalArgumentException, IllegalStateException, IOException
	{ xml.endTag("", a); }
	
	public abstract boolean save(FileOutputStream fp);
	protected abstract boolean doBank(XmlPullParser xpp) throws XmlPullParserException, IOException;
	protected abstract C createControl(String s);
	protected abstract ArrayList<FX> fxList();
	public abstract T clonePatch(int i);
	public abstract T newPatch();
	public abstract T saveCopy(int i, T p);

	/**
	 * @return
	 */
	public int numPatches()
	{
		return patch.size();
	}

	/**
	 * @param patchno
	 * @return
	 */
	public int deletePatch(int patchno)
	{
		if (patchno >= patch.size() || patchno < 0) return -1;
		patch.remove(patchno);
		if (patchno >= patch.size()) patchno--;
		return patchno;
	}

	public int add(T pss)
	{
		patch.add(pss);
		return patch.size()-1;
	}

	public T set(int i, T p) {
		if (i >= 0 && i < patch.size()) {
			return patch.set(i, p);
		}
		return p;
	}
	
	public T get(int i) {
		if (i >= 0 && i < patch.size()) {
			return patch.get(i);
		}
		return null;
	}
	
	public int find(T p) {
		if (p == null || patch == null) {
			return -1;
		}
		return patch.indexOf(p);
	}

	public String makeIncrementalName(String name) {
		Pattern p = Pattern.compile("(\\D*)(\\d*)");
		Matcher m = p.matcher(name);
		if (m.matches()) {
			String base = "default";
			int n = 0;
			if (m.group(1) != null && m.group(1).length() > 0) {
				base = m.group(1);
			}
			if (m.group(2) != null && m.group(2).length() > 0) {
				n = Integer.parseInt(m.group(2));
			}
			boolean found;
			String nnm;
			do {
				n++;
				nnm = base+Integer.toString(n);
				found = false;
				for (NamedPatch pp: patch) {
					if (pp.name.equals(nnm)) {
						found = true;
						break;
					}
				}
			} while (found);
			return nnm;
		} else {
			return "default";
		}
	}


	/**
	 * @param xpp
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected void doSkipUnexpected(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		// TODO Auto-generated method stub
		
	}
	
	protected void putEnv(XmlSerializer xml, Envelope psse) throws IllegalArgumentException, IllegalStateException, IOException
	{
		if (psse != null && psse.countValidTargets() > 0) {
			xs(xml, "env");
			if (psse.attackT != 0) {
				fa(xml, "att", psse.attackT);
			}
			if (psse.decayT != 0) {
				fa(xml, "dec", psse.decayT);
			}
			if (psse.sustainT != 0) {
				fa(xml, "sus", psse.sustainT);
			}
			if (psse.sustainL != 0) {
				fa(xml, "lev", psse.sustainL);
			}
			if (psse.releaseT != 0) {
				fa(xml, "rel", psse.releaseT);
			}
			if (!psse.tempoLock) {
				ba(xml, "lock", psse.tempoLock);
			}
			if (psse.trigger != ResetMode.ONFIRE) {
				sa(xml, "trigger", psse.trigger.toString());
			}
			for (ModulationTarget pssemt: psse.target) {
				if (pssemt != null && pssemt.target.ccontrolId() != Control.NOTHING) {
					xs(xml, "tgt");
					sa(xml, "type", pssemt.target.ccontrolName());
					fa(xml, "amount", pssemt.amount);
					xe(xml, "tgt");
				}
			}
			xe(xml, "env");
		}
	}

	protected void putLFO(XmlSerializer xml, LFO psslf) throws IllegalArgumentException, IllegalStateException, IOException
	{
		if (psslf != null && psslf.countValidTargets() > 0) {
			xs(xml, "lfo");
			fa(xml, "rate", psslf.rate);
			if (psslf.phase != 0) {
				fa(xml, "phase", psslf.phase);
			}
			if (!psslf.tempoLock) {
				ba(xml, "lock", psslf.tempoLock);
			}
			if (psslf.waveform != LFWave.SIN) {
				sa(xml, "wave", psslf.waveform.toString());
			}
			if (psslf.trigger != ResetMode.ONFIRE) {
				sa(xml, "trigger", psslf.trigger.toString());
			}
			for (ModulationTarget psslfmt: psslf.target) {
				if (psslfmt != null && psslfmt.target.ccontrolId() != Control.NOTHING) {
					xs(xml, "tgt");
					sa(xml, "type", psslfmt.target.ccontrolName());
					fa(xml, "amount", psslfmt.amount);
					xe(xml, "tgt");
				}
			}
			xe(xml, "lfo");
		}
	}

	protected void putFX(XmlSerializer xml, FXState f) throws IllegalArgumentException, IllegalStateException, IOException
	{
		if (f != null && f.getFx().getId() != FX.UNITY) {
			xs(xml, "fx");
			sa(xml, "type", f.getFx().getSaveName());
			if (!f.getEnable()) {
				ba(xml, "enable", f.getEnable());
			}
			int i=0;
			boolean started = false;
			for (FXParam p: f.getFx().params()) {
				if (i < f.countParams() && f.param(i) != p.getCenterValue()) {
					if (!started) {
						xs(xml, "fxp");
						started = true;
					}
					fa(xml, p.getSaveName(), f.param(i));
				}
				i++;
			}
			if (started) xe(xml, "fxp");
			xe(xml, "fx");
		}
	}
	

	protected FXState doFX(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("fx")) return null;
		
		FXState fss = new FXState(FX.unityFX, true);
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("type")){
				fss.setType(FX.parse(v, fxList()));
			} else if (n.equals("enable")) {
				fss.setEnable(!v.equals("false"));
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return fss;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if (doFXParam(xpp, fss)) {
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("fx")) { // tag consumed by doPatch()
					return fss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pad'");
	}

	protected boolean doFXParam(XmlPullParser xpp, FXState fss) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("fxp")) return false;
		
		FXParam fp = null;
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if ((fp = fss.getFx().parseParam(n))!= null) {
				fss.param(fp.getId(), Float.parseFloat(v));
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				doSkipUnexpected(xpp);
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("fxp")) { // tag consumed by doPatch()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pad'");
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected LFO doLFO(XmlPullParser xpp)  throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("lfo")) return null;
		
		LFO lfo = new LFO(LFWave.SIN, true, ResetMode.ONFIRE, 1, 0);
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("rate")){
				lfo.rate = Float.parseFloat(v);
			} else if (n.equals("phase")){
				lfo.phase = Float.parseFloat(v);
			} else if (n.equals("lock")){
				lfo.tempoLock = Boolean.parseBoolean(v);
			} else if (n.equals("trigger")){
				lfo.trigger = ResetMode.parse(v);
			} else if (n.equals("wave")){
				lfo.waveform = LFWave.parse(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in lfo element, "+n);
			}
		}
		
		if (xpp.isEmptyElementTag()) {
			return lfo;
		}
		
		int eventType = xpp.getEventType();
		ModulationTarget mt = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((mt=doTgt(xpp, lfo)) != null) {
					lfo.add(mt);
//					Log.d("read", String.format("lfo has %d target", lfo.countValidTargets()));
				} else {
					
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("lfo")) { // tag consumed in loop of doBank()
					return lfo;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'lfo'");
	}
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected Envelope doEnvelope(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("env")) return null;
		
		Envelope env = new Envelope(true, ResetMode.ONFIRE, 0, 0, 0, 0, 0);
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("att")){
				env.attackT = Float.parseFloat(v);
			} else if (n.equals("dec")){
				env.decayT = Float.parseFloat(v);
			} else if (n.equals("sus")){
				env.sustainT = Float.parseFloat(v);
			} else if (n.equals("lev")){
				env.sustainL = Float.parseFloat(v);
			} else if (n.equals("rel")){
				env.releaseT = Float.parseFloat(v);
			} else if (n.equals("lock")){
				env.tempoLock = Boolean.parseBoolean(v);
			} else if (n.equals("trigger")){
				env.trigger = ResetMode.parse(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in lfo element, "+n);
			}
		}
		
		if (xpp.isEmptyElementTag()) {
			return env;
		}
		
		int eventType = xpp.getEventType();
		ModulationTarget mt = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((mt=doTgt(xpp, env)) != null) {
					env.add(mt);
					Log.d("read", String.format("env add %d target", env.countValidTargets()));
				} else {
					
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("env")) { // tag consumed in loop of doBank()
					return env;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'env'");
	}

	/**
	 * @param xpp
	 * @param m
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected ModulationTarget doTgt(XmlPullParser xpp, Modulator m) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("tgt")) return null;
		
		ModulationTarget mt = m.createTarget();
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("type")){
				mt.target = createControl(v);
			} else if (n.equals("amount")){
				mt.amount = Float.parseFloat(v);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return mt;
		}
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
			} else if(eventType == XmlPullParser.END_TAG) {
				if (tag.equals("tgt")) { // tag consumed in loop of doBank()
					return mt;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'tgt'");
	}
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected XYControlAssign doController(XmlPullParser xpp, ControllableManager cm) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("controller")) return null;
		
		XYControlAssign xss = new XYControlAssign();
		ControlsAdapter ca = null;
		int na=xpp.getAttributeCount();
		String dt = null;
		String xt = null;
		String yt = null;
		int di = 0;
		int xi = 0;
		int yi = 0;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			
			if (n.equals("ctlType")) {
				xss.controllerType = v;
			} else if (n.equals("ctlId")) {
				xss.controllerId = Integer.parseInt(v);
			} else if (n.equals("dstType")) {
				dt = v;
			} else if (n.equals("dstId")) {
				di = Integer.parseInt(v);
			} else if (n.equals("xpType")) {
				xt = v;
			} else if (n.equals("xpId")) {
				xi = Integer.parseInt(v);
			} else if (n.equals("ypType")) {
				yt = v;
			} else if (n.equals("ypId")) {
				yi = Integer.parseInt(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in 'controller' element, "+n);
//				Log.d("bank error", "Unknown attribute in 'controller' element, "+n);
			}

		}
		Log.d("doController", dt+":"+Integer.toString(di)+", "+xt+":"+Integer.toString(xi)+", "+yt+":"+Integer.toString(yi)+", ");
		if (dt != null && di >= 0) {
			xss.controllable = cm.findControllable(dt, di);
		}
		Log.d("doController", xss.controllable!=null?xss.controllable.abbrevName()+";":"null");
		if (xss.controllable!=null && ((ca = xss.controllable.getInterfaceTgtAdapter()) != null)) {
			if (xt != null && xi >= 0) {
				xss.xp = ca.findControl(xt, xi);
			}
			if (yt != null && yi >= 0) {
				xss.yp = ca.findControl(yt, yi);
			}
		}
		if (xss.controllable == null) {
			throw new XmlPullParserException("Fail to find paramaters in controllable in 'controller' element");
		}
		if (xss.xp == null) {
			throw new XmlPullParserException("Fail to find paramaters for x param in 'controller' element");
		}
		if (xss.yp == null) {
			throw new XmlPullParserException("Fail to find paramaters for y param in 'controller' element");
		}
		if (xpp.isEmptyElementTag()) {
			return xss;
		}
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("controller")) { // tag consumed by doPatch()
					return xss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'controller'");
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected RXControlAssign doRibbon(XmlPullParser xpp, ControllableManager cm) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null) return null;
		if (!tag.equals("ribbon")) return null;
		
		int na=xpp.getAttributeCount();
//		Log.d("load ribbon", String.format("attribs %d", na));
		String dt = null;
		String xt = null;
		int di = 0;
		int xi = 0;
		String st = null;
		int si = 0;
		float mapMin = 0;
		float mapCenter = 0.5f;
		float mapMax = 1;
		float rangeMin = 0;
		float rangeCenter = 0.5f;
		float rangeMax = 1;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			
			if (n.equals("ctlType")) {
				st = v;
			} else if (n.equals("ctlId")) {
				si = Integer.parseInt(v);
			} else if (n.equals("dstType")) {
				dt = v;
			} else if (n.equals("dstId")) {
				di = Integer.parseInt(v);
			} else if (n.equals("xpType")) {
				xt = v;
			} else if (n.equals("xpId")) {
				xi = Integer.parseInt(v);
			} else if (n.equals("mapMin")) {
				mapMin = Float.parseFloat(v);
			} else if (n.equals("mapCenter")) {
				mapCenter = Float.parseFloat(v);
			} else if (n.equals("mapMax")) {
				mapMax = Float.parseFloat(v);
			} else if (n.equals("rangeMin")) {
				rangeMin = Float.parseFloat(v);
			} else if (n.equals("rangeCenter")) {
				rangeCenter = Float.parseFloat(v);
			} else if (n.equals("rangeMax")) {
				rangeMax = Float.parseFloat(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in 'sensor' element, "+n);
//				Log.d("bank error", "Unknown attribute in 'ribbon' element, "+n);
			}

		}

		Controllable controllable=null;
		Control xp=null;
		ControlsAdapter ca = null;
		if (dt != null && di >= 0) {
			controllable = cm.findControllable(dt, di);
		}
		if (xt != null && xi >= 0 && ((ca = controllable.getInterfaceTgtAdapter()) != null)) {
			xp = ca.findControl(xt, xi);
		}
		if (controllable == null || xp == null) {
			throw new XmlPullParserException("Fail to find paramaters in controllable in 'ribbon' element");
		}
		RXControlAssign xss = new RXControlAssign(controllable, xp);
		xss.setMapParams(mapMin, mapCenter, mapMax);
		xss.setRangeParams(rangeMin, rangeCenter, rangeMax);
		
		if (xpp.isEmptyElementTag()) {
			return xss;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("ribbon")) { // tag consumed by doPatch()
					return xss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'ribbon'");
	}


	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected SXControlAssign doSensor(XmlPullParser xpp, ControllableManager cm, SensorControlManager sm) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null) return null;
		if (!tag.equals("sensor")) return null;
		
		int na=xpp.getAttributeCount();
//		Log.d("load sensor", String.format("attribs %d", na));
		String dt = null;
		String xt = null;
		int di = 0;
		int xi = 0;
		String st = null;
		int si = 0;
		float mapMin = 0;
		float mapCenter = 0.5f;
		float mapMax = 1;
		float rangeMin = 0;
		float rangeCenter = 0.5f;
		float rangeMax = 1;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			
			if (n.equals("ctlType")) {
				st = v;
			} else if (n.equals("ctlId")) {
				si = Integer.parseInt(v);
			} else if (n.equals("dstType")) {
				dt = v;
			} else if (n.equals("dstId")) {
				di = Integer.parseInt(v);
			} else if (n.equals("xpType")) {
				xt = v;
			} else if (n.equals("xpId")) {
				xi = Integer.parseInt(v);
			} else if (n.equals("mapMin")) {
				mapMin = Float.parseFloat(v);
			} else if (n.equals("mapCenter")) {
				mapCenter = Float.parseFloat(v);
			} else if (n.equals("mapMax")) {
				mapMax = Float.parseFloat(v);
			} else if (n.equals("rangeMin")) {
				rangeMin = Float.parseFloat(v);
			} else if (n.equals("rangeCenter")) {
				rangeCenter = Float.parseFloat(v);
			} else if (n.equals("rangeMax")) {
				rangeMax = Float.parseFloat(v);
			} else {
//				throw new XmlPullParserException("Unknown attribute in 'sensor' element, "+n);
//				Log.d("bank error", "Unknown attribute in 'sensor' element, "+n);
			}

		}

		Controllable controllable=null;
		Control xp=null;
		ControlsAdapter ca = null;
		SensorInfo s=null;
		if (st != null && si >= 0) {
			s = sm.findSensor(st, si);
		}
		if (dt != null && di >= 0) {
			controllable = cm.findControllable(dt, di);
		}
		if (xt != null && xi >= 0 && ((ca = controllable.getInterfaceTgtAdapter()) != null)) {
			xp = ca.findControl(xt, xi);
		}
		if (controllable == null || xp == null || s == null) {
			throw new XmlPullParserException("Fail to find paramaters in controllable in 'sensor' element");
		}
		SXControlAssign xss = new SXControlAssign(s, controllable, xp);
		xss.setMapParams(mapMin, mapCenter, mapMax);
		xss.setRangeParams(rangeMin, rangeCenter, rangeMax);
		
		if (xpp.isEmptyElementTag()) {
			return xss;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("sensor")) { // tag consumed by doPatch()
					return xss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'sensor'");
	}

	/**
	 * @param fp
	 * @return
	 */
	public boolean load(FileInputStream fp)
	{
		try {
			FileReader fr = new FileReader(fp.getFD());
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(fr);
			
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if(eventType == XmlPullParser.START_DOCUMENT) {
					patch = new ArrayList<T>();
				} else if(eventType == XmlPullParser.START_TAG) {
					if (doBank(xpp)) {
						
					}
				} else if(eventType == XmlPullParser.END_TAG) {
				} else if(eventType == XmlPullParser.TEXT) {
				}
				eventType = xpp.next();
			}
			
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * @param a
	 * @return
	 */
	public boolean setPatchAdapterItems(ArrayAdapter<Action> a)
	{
		if (a == null) return false;
		a.clear();
		/*
		a.add(new Action(Action.NEW, "*New Patch*"));
		a.add(new Action(Action.CLONE, "*Dup Patch*"));
		a.add(new Action(Action.DELETE, "*Del Patch*"));
		a.add(new Action(Action.RENAME, "*Rename Patch*"));
		a.add(new Action(Action.RELOAD, "*Reload Patch*"));
		a.add(new Action(Action.SAVE, "*Save Patch*"));
		*/
		for (int i=0; i<patch.size(); i++) {
			T p = patch.get(i);
			a.add(new Action(i, p.name));
		}
		return true;
	}
	
}

