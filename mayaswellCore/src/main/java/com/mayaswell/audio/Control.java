package com.mayaswell.audio;


public abstract class Control implements ICControl {
	public interface Host {
		Control controlFor(int id);
	}

	public static final int NOTHING=0;

	public int typeCode;
	public int id;

	public Control() {
		super();
	}
}