package com.mayaswell.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.mayaswell.R;

public class SimpleFileChooser extends ListActivity
{	 
	protected File currentDir;
	protected SFCArrayAdapter adapter;
	protected String[] fileFilter = null;
	protected boolean showHidden = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		String startDirPath = "/";
		if (extras != null) {
			String sdp = extras.getString("startDir");
			String[] ffa = extras.getStringArray("fileFilter");
			if (extras.containsKey("showHidden")) showHidden = extras.getBoolean("showHidden");
			if (sdp != null) startDirPath = sdp;
			if (ffa != null) fileFilter = ffa;
		}
		currentDir = new File(startDirPath);
		fill(currentDir);
	}
	
	private void fill(File f)
	{
		File[]dirs = f.listFiles();
		this.setTitle("Current Dir: "+f.getName());
		List<SFCOption>dir = new ArrayList<SFCOption>();
		List<SFCOption>fls = new ArrayList<SFCOption>();
		try{
			for(File ff: dirs) {
				String name = ff.getName();
				boolean showFile = true;
				if (!name.equals("")) {
					if (name.matches("\\..*") && !showHidden) {
						showFile = false;
					}
					if (fileFilter != null && !ff.isDirectory()) {
						showFile = false;
						for (String ffrxp:fileFilter) {
							if (name.matches(ffrxp)) {
								showFile = true;
								break;
							}
						}
					}
				}
				if (showFile) {
					if(ff.isDirectory())
						dir.add(new SFCOption(ff.getName(),"Folder",ff.getAbsolutePath()));
					else {
						fls.add(new SFCOption(ff.getName(),"File Size: "+ff.length(),ff.getAbsolutePath()));
					}
				}
			}
		} catch(Exception e) {
		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		String fp = f.getParent();
		if(fp != null)
			dir.add(0,new SFCOption("..","Parent Directory",fp));
		adapter = new SFCArrayAdapter(SimpleFileChooser.this,R.layout.file_view,dir);
		this.setListAdapter(adapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		SFCOption o = adapter.getItem(position);
		if(o.getData().equalsIgnoreCase("folder")||o.getData().equalsIgnoreCase("parent directory")){
			currentDir = new File(o.getPath());
			fill(currentDir);
		} else {
			onFileClick(o);
		}
	}
	
	private void onFileClick(SFCOption o)
	{
		Intent data = new Intent();
		data.putExtra("filePath", o.getPath());
		setResult(Activity.RESULT_OK, data);
		finish();
	}
}

