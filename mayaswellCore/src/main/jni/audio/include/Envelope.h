/*
 * Envelope.h
 *
 *  Created on: Oct 21, 2013
 *      Author: dak
 */

#ifndef ENVELOPE_H_
#define ENVELOPE_H_

#include <vector>

#include "ModulationTarget.h"

class Envelope
{
public:
	Envelope(int ntgt, TimeSource &t);
	~Envelope();

	float attackT;
	float decayT;
	float sustainT;
	float sustainL;
	float releaseT;

	float lastValue;

	unsigned char resetMode;
	unsigned char lock;
	TimeSource &timer;
	bool atEnd;

	int nActiveSends;
	std::vector<ModulationTarget> target;

	void setActiveSendCount(int n);
	void set(float attackT2, float decayT2, float sustainL, float sustainT, float releaseT);
	float operator()(void);
	void reset();

protected:
	unsigned long envTime;
};

class EnvelopeHost
{
public:
	bool setEnvelope(int which, unsigned char reset, bool lock, float aT, float dT, float sT, float sL, float rT);
	bool setNEnvelope(int n);
	bool setNEnvelopeTarget(int which, int n);
	bool setEnvelopeTarget(int which, int whichn, int cc, float amt);
	bool setEnvelopeAmount(int which, int whichn, float amt);
	bool setEnvelopeLock(int which, bool lck);
	bool setEnvelopeReset(int which, unsigned char rst);
	bool setEnvelopeAttack(int which, float t);
	bool setEnvelopeDecay(int which, float t);
	bool setEnvelopeSustain(int which, float t);
	bool setEnvelopeSustainLevel(int which, float l);
	bool setEnvelopeRelease(int which, float t);
	bool resetEnvelope(int mxi);
	float getEnvelope(int mxi);

	int hasEnv(int tgt);

protected:
	std::vector<Envelope>	env;
	int						nActiveEnv;
};

inline
Envelope::Envelope(int ntgt, TimeSource &t):
	timer(t)
{
	set(0,0,1,0,0);
	nActiveSends = 0;
	for (int i=0; i<ntgt; i++) {
		target.push_back(ModulationTarget());
	}
	atEnd = false;
	lastValue = 0;
}

inline
Envelope::~Envelope()
{

}

inline void
Envelope::reset()
{
	envTime = 0;
	lastValue = 0;
	atEnd = false;
}

inline void
Envelope::set(float attackT2, float decayT2, float sustainL2, float sustainT2, float releaseT2)
{
	attackT = attackT2;
	decayT = decayT2;
	sustainT = sustainT2;
	sustainL = sustainL2;
	releaseT = releaseT2;
}
#include <android/log.h>
#include "com_mayaswell_audio_Modulator.h"

inline float
Envelope::operator()(void)
{
	float segT = envTime*timer.secsPerCycle;
	envTime++;
	if (lock) {
		segT *= timer.secsPerBeat;
	}
	float v = 0;
	if (segT < attackT) {
		v = segT/attackT;
	} else if ((segT -= attackT) < decayT) {
		v = 1-((1-sustainL)*segT/decayT);
	} else if ((segT -= decayT) < sustainT) {
		v = sustainL;
	} else if ((segT -= sustainT) < releaseT) {
		v = sustainL*(1-(segT/releaseT));
	} else {
		if (resetMode == com_mayaswell_audio_Modulator_RESET_ATEND) {
			envTime = 0;
		} else {
			atEnd = true;
		}
	}
	lastValue = v;

	return v;
}

#endif /* ENVELOPE_H_ */
