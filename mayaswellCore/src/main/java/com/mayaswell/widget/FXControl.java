package com.mayaswell.widget;

import java.util.ArrayList;

import com.mayaswell.R;

import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.FXState;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.widget.FXParamControl.FXParamControlListener;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class FXControl extends BaseControl
{
	public interface FXControlListener
	{
		public void onEnableChanged(FXControl fpc, int fxId, boolean isChecked);
		public void onParamChanged(FXControl fpc, int fxId, int paramId, float amt);
		public void onFXChanged(FXControl fpc, int fxId, FX fx);
	}

	protected ToggleButton enableButton = null;
	protected Spinner fxSelect=null;

	private FXControlListener listener=null;
	private FX fx = null;

	private int fxId = 0;
	private int dfltParamViewWidth = LayoutParams.WRAP_CONTENT;
	private int dfltParamLabelWidth = LayoutParams.WRAP_CONTENT;
	private int fxParamViewEms = 10;
	private ArrayList<FXParamControl> paramControl = new ArrayList<FXParamControl>();
	protected RelativeLayout paramBody = null;

	public FXControl(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public FXControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public FXControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.sg_fx_control, this, true);
		
		label = (TextView)findViewById (R.id.sgCtlLabel);
		fxSelect = (Spinner) findViewById (R.id.sgCtlSpinner);
		enableButton = (ToggleButton) findViewById(R.id.sgCtlToggle);
		paramBody = (RelativeLayout) findViewById(R.id.sgCtlLayout);
		
		label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (displayMode == DisplayMode.SHOW_NAME) {
					setDisplay(DisplayMode.SHOW_VALUE_CTLS);
				} else if (displayMode == DisplayMode.SHOW_VALUE_CTLS && fx != null && fx.countParams() > 0) {
					setDisplay(DisplayMode.SHOW_PARAM_CTLS);
				} else {
					setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		});

		fxSelect.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				Log.d("fx control", "got item selected "+position);
				checkAndNotifyFX();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) 
			{
			}
		});


		enableButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				notifyListener(isChecked);
			}
		});

		dfltParamViewWidth = getResources().getDimensionPixelSize(R.dimen.fxParamViewWidth);
		fxParamViewEms  = getResources().getDimensionPixelSize(R.dimen.fxParamViewEms);
		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			int px = -1;
			int swr = attrs.getAttributeResourceValue(schemaName, "fxSpinnerWidth", -1);
			if (swr >= 0) {
				px = getResources().getDimensionPixelSize(swr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "fxSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setFXSelectWidth(px);
			}
			px = 0;
			int vwr = attrs.getAttributeResourceValue(schemaName, "paramViewWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "paramViewWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setParamViewWidth(px);
			}
			px = 0;
			vwr = attrs.getAttributeResourceValue(schemaName, "paramLabelWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "paramLabelWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setParamLabelWidth(px);
			}
			px = 0;
			vwr = attrs.getAttributeResourceValue(schemaName, "labelWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "labelWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setLabelWidth(px);
			}
		}

	}

	private void setParamViewWidth(int px)
	{
		dfltParamViewWidth  = px;
		for (FXParamControl pc: paramControl ) {
			pc.setValueViewWidth(dfltParamViewWidth);
		}
	}

	private void setParamLabelWidth(int px)
	{
		dfltParamLabelWidth  = px;
		for (FXParamControl pc: paramControl ) {
			pc.setLabelWidth(dfltParamLabelWidth);
		}
	}

	public void setLabelWidth(int px)
	{
		LayoutParams params = (LayoutParams) label.getLayoutParams();
		params.width = px;
	}

	public void setListener(FXControlListener l )
	{
		listener = l;
	}
		
	public void setFXSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) fxSelect.getLayoutParams();
		params.width = px;
	}
	
	private void checkAndNotifyFX()
	{
		FX f = null;
		try {
			SpinnerAdapter csa = fxSelect.getAdapter();
			if (csa != null) {
				int sip = fxSelect.getSelectedItemPosition();
				f = (FX) csa.getItem(sip);
				Log.d("FXControl", String.format("checkAndNotifyFX %d %s", sip, f.getSaveName()));
			}
		} catch (Exception e) {
			
		}
		if (f != null) {
			boolean change = (fx==null || fx.getId() != f.getId());
			setFXType(f, change);
			setFXEnable(true);
			notifyListener(f);
		}
	}
	
	public void setFXEnable(boolean b)
	{
		if (enableButton != null) {
			enableButton.setChecked(b);
		}
	}
	
	public void setFXParamValue(int i, float v)
	{
		if (paramControl != null && i >= 0 && i < paramControl.size()) {
			paramControl.get(i).setValue(v);
//			Log.d("FXControl", "setFXParamValue " + i + ", " + v);
		}
	}

	public void setFXType(FX f, boolean setDefaultParamValue)
	{
		fx = f;
		int fpci = 0;
		int fpcId = 10;
		FXParamControl fpc = null;
		Log.d("FXControl", String.format("setFXType %s %b", f.toString(), setDefaultParamValue));
		if (fx != null) {
			int pi = positionFor(f);
			if (pi >= 0) {
				fxSelect.setSelection(pi);
			}
			for (FXParam p: f.params()) {
				if (fpci < paramControl.size()) {
					fpc = paramControl.get(fpci);
				} else {
					fpc = new FXParamControl(getContext());
					fpc.setId(fpcId);
					fpc.setListener(new FXParamControlListener() {
						@Override
						public void onValueChanged(int fxParamIndex, float amt) {
							notifyListener(fxParamIndex, amt);
//							Log.d("fx", String.format("%d %f", id, amt));
						}
					});
					RelativeLayout.LayoutParams params = rlParams(
							LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
							(fpci<=1)?-1:(fpcId-2), (fpci%2==0)?-1:fpcId-1);
					paramControl.add(fpc);
					paramBody.addView(fpc, params);
				}
				Log.d("FXControl", "setting param "+p.getSaveName());
				fpc.setTo(p, setDefaultParamValue);
				// TODO start using per parameter widths getLabelDisplayEms(), getValueDisplayEms() if > 0 ... need an em width factor 
				fpc.setValueViewWidth(dfltParamViewWidth);
				fpc.setLabelWidth(dfltParamLabelWidth);
				fpci++;
				fpcId++;
			}
		}
		Log.d("FXControl", String.format("setFXType %s %d done", f.getShortName(), fpci));
		setParamControlVisibility(fpci, GONE);
	}
	
	private int positionFor(FX f)
	{
		if (fxSelect != null) {
			SpinnerAdapter s = fxSelect.getAdapter();
			if (s != null) {
				ArrayAdapter<FX> fla = (ArrayAdapter<FX>) s;
				for (int i=0; i<fla.getCount(); i++) {
					FX fi = fla.getItem(i);
					if (fi.getId() == f.getId()) {
						return i;
					}
					
				}
			}
		}
		return -1;
	}

	private void setParamControlVisibility(int fpci, int mode)
	{
		setParamControlVisibility(fpci, paramControl.size(), mode);
	}
	
	private void setParamControlVisibility(int fpci, int n, int mode)
	{
		FXParamControl fpc = null;
		for (;fpci < n; fpci++) {
			fpc = paramControl.get(fpci);
			if (fpc != null) fpc.setVisibility(mode);
		}
	}

	public void setAdapter(ArrayAdapter<FX> erma)
	{
		fxSelect.setAdapter(erma);
	}
	
	public void setDisplay(DisplayMode dm)
	{
		displayMode  = dm;
		switch (displayMode) {
		case SHOW_NAME:
			fxSelect.setVisibility(GONE);
			enableButton.setVisibility(GONE);
			setParamControlVisibility(0, GONE);
			break;
		case SHOW_VALUE_DETAILS:
		case SHOW_VALUE_CTLS:
			fxSelect.setVisibility(VISIBLE);
			enableButton.setVisibility(VISIBLE);
			setParamControlVisibility(0, GONE);
			break;
		case SHOW_PARAM_CTLS:
			fxSelect.setVisibility(VISIBLE);
			enableButton.setVisibility(VISIBLE);
			if (fx != null) {
				setParamControlVisibility(0, fx.countParams(), VISIBLE);
				setParamControlVisibility(fx.countParams(), GONE);
			} else {
				setParamControlVisibility(0, GONE);
			}
			break;
		}
		if (displayMode == DisplayMode.SHOW_PARAM_CTLS && fx != null && fx.getId() != FX.UNITY) {
			checkFullyOpenedSiblings(getClass(), DisplayMode.SHOW_VALUE_CTLS);
		}
		SubPanelFragment.layoutEditorControlParent(this);
	}

	private void notifyListener(FX sip)
	{
		if (listener != null) {
			listener.onFXChanged(this, fxId, sip);
		}
	}
	
	private void notifyListener(boolean isChecked)
	{
		if (listener != null)
			listener.onEnableChanged(this, fxId, isChecked);
	}
	
	protected void notifyListener(int fxParamId, float amt) {
		if (listener != null)
			listener.onParamChanged(this, fxId, fxParamId, amt);
	}

	public boolean setFXState(FXState fxi)
	{
		Log.d("FXControl", "setFXState p0="+(fxi!=null?fxi.param(0):0));
		
		if (fxi != null) {
			setFXType(fxi.getFx(), false);
			setFXEnable(fxi.getEnable());
			int i=0;
			if (fxi.getParams() != null) {
				for (float v: fxi.getParams()) {
					setFXParamValue(i++, v);
				}
			}
		} else {
			setFXType(FX.unityFX, false);
			setFXEnable(true);
		}
		return true;
	}

	public int getFxId() {
		return fxId;
	}

	public void setFxId(int fxId) {
		this.fxId = fxId;
	}

	public FX selectedFX()
	{
		if (fxSelect == null) {
			return FX.unityFX;
		}
		FX fx = (FX) fxSelect.getSelectedItem();
		if (fx == null) {
			return FX.unityFX;
		}
		return fx;
	}

}
