package com.mayaswell.util;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mayaswell.R;
 
 
public class SFCArrayAdapter extends ArrayAdapter<SFCOption>{
	private int id;
	private List<SFCOption>items;
	 
	public SFCArrayAdapter(Context context, int textViewResourceId, List<SFCOption> objects) {
		super(context, textViewResourceId, objects);
		id = textViewResourceId;
		items = objects;
	}
	
	public SFCOption getItem(int i)
	{
		return items.get(i);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(id, null);
		}
		final SFCOption o = items.get(position);
		if (o != null) {
			TextView t1 = (TextView) v.findViewById(R.id.TextView01);
			TextView t2 = (TextView) v.findViewById(R.id.TextView02);
			if(t1!=null)
				t1.setText(o.getName());
			if(t2!=null)
				t2.setText(o.getData());
		}
		return v;
	}
 
}
