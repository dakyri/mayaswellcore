package com.mayaswell.widget;

import com.mayaswell.R;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Controllable.ControllerInfo;
import com.mayaswell.audio.Controllable.XControlAssign;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.util.MWBPActivity;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class MappedXMenuControl extends BaseControl {
	public interface MappedXMenuControlListener {
		public void onValueChanged(MappedXMenuControl sx, ControllerInfo sip, Controllable cp, Control xp,
				float mapMin, float mapCenter, float mapMax, float sMin, float sCenter, float sMax);
		public void onMapParamsChanged(MappedXMenuControl sx, float mapMin, float mapCenter, float mapMax);
		public void onRangeParamsChanged(MappedXMenuControl sx, float mapMin, float mapCenter, float mapMax);
	}

    private final String schemaName = "http://schemas.android.com/apk/res/res-auto";
	
	private Spinner controllerSelectSpinner = null;
	private Spinner controllableSelectSpinner = null;
	private Spinner xParamSpinner = null;
	private RotaryPotView mapMinPot = null;
	private EditText mapMinView = null;
	private RotaryPotView mapCenterPot = null;
	private EditText mapCenterView = null;
	private RotaryPotView mapMaxPot = null;
	private EditText mapMaxView = null;
	private TextView lbl1 = null;
	private TextView lbl2 = null;
	private TextView lbl3 = null;
	private RotaryPotView rangeMinPot = null;
	private EditText rangeMinView = null;
	private RotaryPotView rangeCenterPot = null;
	private EditText rangeCenterView = null;
	private RotaryPotView rangeMaxPot = null;
	private EditText rangeMaxView;
	private TextView lbl4 = null;
	private TextView lbl5 = null;
	private TextView lbl6 = null;

    private MappedXMenuControlListener listener=null;

	private int val=0;

	private XControlAssign controlAssign = null;


	public MappedXMenuControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public MappedXMenuControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public MappedXMenuControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_mapped_x_select_control, this, true);
		
//		this.setPadding(0, 0, 0, 0);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		controllerSelectSpinner = (Spinner)findViewById (R.id.mwControllerSelectSpinner);
		controllableSelectSpinner = (Spinner)findViewById (R.id.mwCtlSpinner);
		xParamSpinner = (Spinner)findViewById (R.id.mwCtlSpinnerX);
		mapMinPot = (RotaryPotView)findViewById (R.id.mwCtlPot);
		mapMinView = (EditText)findViewById (R.id.mwCtlEditText);
		mapCenterPot = (RotaryPotView)findViewById (R.id.mwCtlPot2);
		mapCenterView = (EditText)findViewById (R.id.mwCtlEditText2);
		mapMaxPot = (RotaryPotView)findViewById (R.id.mwCtlPot3);
		mapMaxView = (EditText)findViewById (R.id.mwCtlEditText3);
		lbl1 = (TextView)findViewById (R.id.mwCtlLabel1);
		lbl2 = (TextView)findViewById (R.id.mwCtlLabel2);
		lbl3 = (TextView)findViewById (R.id.mwCtlLabel3);
		rangeMinPot = (RotaryPotView)findViewById (R.id.mwCtlPot4);
		rangeMinView = (EditText)findViewById (R.id.mwCtlEditText4);
		rangeCenterPot = (RotaryPotView)findViewById (R.id.mwCtlPot5);
		rangeCenterView = (EditText)findViewById (R.id.mwCtlEditText5);
		rangeMaxPot = (RotaryPotView)findViewById (R.id.mwCtlPot6);
		rangeMaxView = (EditText)findViewById (R.id.mwCtlEditText6);
		lbl4 = (TextView)findViewById (R.id.mwCtlLabel4);
		lbl5 = (TextView)findViewById (R.id.mwCtlLabel5);
		lbl6 = (TextView)findViewById (R.id.mwCtlLabel6);

		controllerSelectSpinner.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
		    {
				checkAndNotify();
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) 
		    {
		    }
		});
		
		controllableSelectSpinner.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
		    {
	    		setControllable(position);				
				checkAndNotify();
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) 
		    {
		    }
		});
		
		xParamSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
			{
				checkAndNotify();
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		});
		
		label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0)
			{
				if (getDisplayMode() == DisplayMode.SHOW_NAME) {
					setDisplay(DisplayMode.SHOW_VALUE_CTLS);
				} else if (getDisplayMode() == DisplayMode.SHOW_VALUE_CTLS) {
					setDisplay(DisplayMode.SHOW_VALUE_DETAILS);
				} else {
					setDisplay(DisplayMode.SHOW_NAME);
				}
			}		
		});

		mapMinPot.setValueBounds(0, 1);
		mapMinView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < mapMinPot.getMinVal()) v = mapMinPot.getMinVal();
						else if (v > mapMinPot.getMaxVal()) v = mapMinPot.getMaxVal();
						mapMinPot.setValue(v);
						
						notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		mapMinPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				mapMinView.setText(String.format("%.2f", v));
				
				notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
			}
		});
		
		mapCenterPot.setValueBounds(0, 1);
		mapCenterView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < mapCenterPot.getMinVal()) v = mapCenterPot.getMinVal();
						else if (v > mapCenterPot.getMaxVal()) v = mapCenterPot.getMaxVal();
						mapCenterPot.setValue(v);
						
						notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		mapCenterPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				mapCenterView.setText(String.format("%.2f", v));
				
				notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
			}
		});
		
		mapMaxPot.setValueBounds(0, 1);
		mapMaxView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < mapMaxPot.getMinVal()) v = mapMaxPot.getMinVal();
						else if (v > mapMaxPot.getMaxVal()) v = mapMaxPot.getMaxVal();
						mapMaxPot.setValue(v);
						
						notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}

		});
		mapMaxPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				mapMaxView.setText(String.format("%.2f", v));
				
				notifyParamsChange(mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue());
			}
		});
		
		rangeMinPot.setValueBounds(0, 1);
		rangeMinView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < rangeMinPot.getMinVal()) v = rangeMinPot.getMinVal();
						else if (v > rangeMinPot.getMaxVal()) v = rangeMinPot.getMaxVal();
						rangeMinPot.setValue(v);
						
						notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}

		});
		rangeMinPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				rangeMinView.setText(String.format("%.2f", v));
				
				notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
			}
		});
		
		rangeCenterPot.setValueBounds(0, 1);
		rangeCenterView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < rangeCenterPot.getMinVal()) v = rangeCenterPot.getMinVal();
						else if (v > rangeCenterPot.getMaxVal()) v = rangeCenterPot.getMaxVal();
						rangeCenterPot.setValue(v);
						
						notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		rangeCenterPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				rangeCenterView.setText(String.format("%.2f", v));
				
				notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
			}
		});
		
		rangeMaxPot.setValueBounds(0, 1);
		rangeMaxView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < rangeMaxPot.getMinVal()) v = rangeMaxPot.getMinVal();
						else if (v > rangeMaxPot.getMaxVal()) v = rangeMaxPot.getMaxVal();
						rangeMaxPot.setValue(v);
						
						notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
					}
				} catch (NumberFormatException e) {
				}
			}

		});
		rangeMaxPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				rangeMaxView.setText(String.format("%.2f", v));
				
				notifyRangeParamsChange(rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
			}
		});
		
		setMapValues(0,0.5f,1);
		setRangeValues(0,0.5f,1);
		
		setDisplay(DisplayMode.SHOW_VALUE_CTLS);
		
		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			int px = -1;
			int swr = attrs.getAttributeResourceValue(schemaName, "controllerSpinnerWidth", -1);
			if (swr >= 0) {
				px = getResources().getDimensionPixelSize(swr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "controllerSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setControllerSelectWidth(px);
			}
			
			int vwr = attrs.getAttributeResourceValue(schemaName, "targetSpinnerWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "targetSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setTargetSelectWidth(px);
			}
			
			int wwr = attrs.getAttributeResourceValue(schemaName, "paramSpinnerWidth", -1);
			if (wwr >= 0) {
				px = getResources().getDimensionPixelSize(wwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "paramSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setParamSelectWidth(px);
			}
		}
	}
	
	public void setControllerSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) controllerSelectSpinner.getLayoutParams();
		params.width = px;
	}
	
	public void setTargetSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) controllableSelectSpinner.getLayoutParams();
		params.width = px;
	}
	
	public void setParamSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) xParamSpinner.getLayoutParams();
		params.width = px;
	}
	
	public void setMainAdapters(SpinnerAdapter ctrllerAdapter, SpinnerAdapter ctrlableAdapter)
	{
		controllerSelectSpinner.setAdapter(ctrllerAdapter);
		controllableSelectSpinner.setAdapter(ctrlableAdapter);
	}
	
	public void setDisplay(DisplayMode dm) {
		displayMode = dm;
	
		switch (displayMode) {
		case SHOW_NAME:
			controllerSelectSpinner.setVisibility(GONE);
			controllableSelectSpinner.setVisibility(GONE);
			xParamSpinner.setVisibility(GONE);
			mapMinPot.setVisibility(GONE);
			mapMinView.setVisibility(GONE);
			mapCenterPot.setVisibility(GONE);
			mapCenterView.setVisibility(GONE);
			mapMaxPot.setVisibility(GONE);
			mapMaxView.setVisibility(GONE);
			rangeMinPot.setVisibility(GONE);
			rangeMinView.setVisibility(GONE);
			rangeCenterPot.setVisibility(GONE);
			rangeCenterView.setVisibility(GONE);
			rangeMaxPot.setVisibility(GONE);
			rangeMaxView.setVisibility(GONE);
			lbl1.setVisibility(GONE);
			lbl2.setVisibility(GONE);
			lbl3.setVisibility(GONE);
			lbl4.setVisibility(GONE);
			lbl5.setVisibility(GONE);
			lbl6.setVisibility(GONE);
			break;
		case SHOW_VALUE_CTLS:
			controllerSelectSpinner.setVisibility(VISIBLE);
			controllableSelectSpinner.setVisibility(VISIBLE);
			xParamSpinner.setVisibility(VISIBLE);
			mapMinPot.setVisibility(GONE);
			mapMinView.setVisibility(GONE);
			mapCenterPot.setVisibility(GONE);
			mapCenterView.setVisibility(GONE);
			mapMaxPot.setVisibility(GONE);
			mapMaxView.setVisibility(GONE);
			rangeMinPot.setVisibility(GONE);
			rangeMinView.setVisibility(GONE);
			rangeCenterPot.setVisibility(GONE);
			rangeCenterView.setVisibility(GONE);
			rangeMaxPot.setVisibility(GONE);
			rangeMaxView.setVisibility(GONE);
			lbl1.setVisibility(GONE);
			lbl2.setVisibility(GONE);
			lbl3.setVisibility(GONE);
			lbl4.setVisibility(GONE);
			lbl5.setVisibility(GONE);
			lbl6.setVisibility(GONE);
			break;
		case SHOW_VALUE_DETAILS:
			controllerSelectSpinner.setVisibility(VISIBLE);
			controllableSelectSpinner.setVisibility(VISIBLE);
			xParamSpinner.setVisibility(VISIBLE);
			mapMinPot.setVisibility(VISIBLE);
			mapMinView.setVisibility(VISIBLE);
			mapCenterPot.setVisibility(VISIBLE);
			mapCenterView.setVisibility(VISIBLE);
			mapMaxPot.setVisibility(VISIBLE);
			mapMaxView.setVisibility(VISIBLE);
			rangeMinPot.setVisibility(VISIBLE);
			rangeMinView.setVisibility(VISIBLE);
			rangeCenterPot.setVisibility(VISIBLE);
			rangeCenterView.setVisibility(VISIBLE);
			rangeMaxPot.setVisibility(VISIBLE);
			rangeMaxView.setVisibility(VISIBLE);
			lbl1.setVisibility(VISIBLE);
			lbl2.setVisibility(VISIBLE);
			lbl3.setVisibility(VISIBLE);
			lbl4.setVisibility(VISIBLE);
			lbl5.setVisibility(VISIBLE);
			lbl6.setVisibility(VISIBLE);
			break;
		}
		SubPanelFragment.layoutEditorControlParent(this);

	}

	private void checkAndNotify()
	{
		ControllerInfo sip = null;
		ControllableInfo cip = null;
		ControlsAdapter cp = null;
		try {
			SpinnerAdapter csa = controllerSelectSpinner.getAdapter();
			SpinnerAdapter psa = controllableSelectSpinner.getAdapter();
	    	if (csa != null) {
	    		sip = (ControllerInfo) csa.getItem(controllerSelectSpinner.getSelectedItemPosition());
	    	}
			if (psa != null) {
				cip = (ControllableInfo) psa.getItem(controllableSelectSpinner.getSelectedItemPosition());
				
				cp = cip.getInterfaceTgtAdapter();
			}
		} catch (Exception e) {
			
		}
		if (sip != null && cip != null && cp != null) {
			int xi = xParamSpinner.getSelectedItemPosition();
			Control xc = null;
			if (xi >= 0 && xi < cp.getCount()) {
				xc = cp.getItem(xi);
			}
			if (xc == null) {
			} else {
				notifyListener(sip, cip.getControllable(), xc,
						mapMinPot.getValue(), mapCenterPot.getValue(), mapMaxPot.getValue(),
						rangeMinPot.getValue(), rangeCenterPot.getValue(), rangeMaxPot.getValue());
			}
		}
	}
	
	public void setControllable(int position)
	{
		int xpi = xParamSpinner.getSelectedItemPosition();
		ArrayAdapter<Control> oaac = (ArrayAdapter<Control>) xParamSpinner.getAdapter();
		int xpiControlId = 0;
		if (oaac != null) {
			if (xpi < oaac.getCount()) {
				xpiControlId = oaac.getItem(xpi).ccontrolId();
			}
		}
		ArrayAdapter<ControllableInfo> controllableAdapter = null;
		try {
			MWBPActivity<?,?,?> mwbp = (MWBPActivity<?,?,?>) getContext();
			controllableAdapter = mwbp.getControllableAdapter();
		} catch (ClassCastException e) {
			
		}
		if (controllableAdapter == null) {
			Log.d("map x", "No controllable adapter");
			return;
		}
		ControllableInfo cip = controllableAdapter.getItem(position);
		ControlsAdapter aac = cip.getInterfaceTgtAdapter();
		xpi = aac.getControlIndex4Id(xpiControlId);
		xParamSpinner.setAdapter(aac.getAdapter());
		if (xpi >= 0) xParamSpinner.setSelection(xpi);
	}

	public void setValueListener(MappedXMenuControlListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(ControllerInfo sip, Controllable cp, Control xp,
			float mapMin, float mapCenter, float mapMax, float sMin, float sCenter, float sMax)
	{
		if (listener != null)
			listener.onValueChanged(this, sip, cp, xp, mapMin, mapCenter, mapMax, sMin, sCenter, sMax);
	}
	
	private void notifyParamsChange(float mapMin, float mapCenter, float mapMax)
	{
		if (listener != null)
			listener.onMapParamsChanged(this, mapMin, mapCenter, mapMax);
	}
	
	private void notifyRangeParamsChange(float mapMin, float mapCenter, float mapMax) {
		if (listener != null)
			listener.onRangeParamsChanged(this, mapMin, mapCenter, mapMax);
	}
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 */
	public void setSelection(int ppos, int xpos, int ypos)
	{
		controllableSelectSpinner.setSelection(ppos);
		setControllable(ppos);
		xParamSpinner.setSelection(xpos);
	}
	
	public void setController(ControllerInfo cp) {
		if (cp == null) {
			return;
		}
		SpinnerAdapter csa = controllerSelectSpinner.getAdapter();
		for (int i=0; i<csa.getCount(); i++) {
			ControllerInfo ci = (ControllerInfo) csa.getItem(i);
			if (cp.getControllerId() == ci.getControllerId()) {
				controllerSelectSpinner.setSelection(i);
				break;
			}
		}		
	}
	
	public boolean setRangeXControlAssign(XControlAssign xsp)
	{
		if (xsp == null) {
			return false;
		}
		if (xsp.controllable != null) {
			ControlsAdapter aac = null;
			ArrayAdapter<ControllableInfo> controllableAdapter = null;
			try {
				MWBPActivity<?,?,?> mwbp = (MWBPActivity<?,?,?>) getContext();
				controllableAdapter = mwbp.getControllableAdapter();
			} catch (ClassCastException e) {
				
			}
			for (int i=0; i<controllableAdapter.getCount(); i++) {
				ControllableInfo ci = controllableAdapter.getItem(i);
				if (ci.getControllable() == xsp.controllable) {
					controllableSelectSpinner.setSelection(i);
					aac = ci.getInterfaceTgtAdapter();
				}
			}
			if (aac != null) {
				int xpi = aac.getControlIndex4Id(xsp.xp.ccontrolId());
				xParamSpinner.setAdapter(aac.getAdapter());
				xParamSpinner.setSelection(xpi);
//				Log.d("setup rbn x", String.format("%d selected", xpi));
			}
			setMapValues(xsp.mapMin, xsp.mapCenter, xsp.mapMax);
			setRangeValues(xsp.rangeMin, xsp.rangeCenter, xsp.rangeMax);
			setControlAssign(xsp);
		}
		return true;
	}
	
	public void setMapValues(float mapMin, float mapCenter, float mapMax)
	{
		mapMinPot.setValue(mapMin);
		mapMinView.setText(String.format("%.2f", mapMin));
		mapMaxPot.setValue(mapMax);
		mapMaxView.setText(String.format("%.2f", mapMax));
		mapCenterPot.setValue(mapCenter);
		mapCenterView.setText(String.format("%.2f", mapCenter));
	}
	
	public void setRangeValues(float mapMin, float mapCenter, float mapMax)
	{
		rangeMinPot.setValue(mapMin);
		rangeMinView.setText(String.format("%.2f", mapMin));
		rangeMaxPot.setValue(mapMax);
		rangeMaxView.setText(String.format("%.2f", mapMax));
		rangeCenterPot.setValue(mapCenter);
		rangeCenterView.setText(String.format("%.2f", mapCenter));
	}
	
	public int getValue()
	{
		return val;
	}

	public void clearAssignment()
	{
		xParamSpinner.setSelection(0);
	}

	public boolean inUse()
	{
		return xParamSpinner.getSelectedItemPosition() > 0 || controllerSelectSpinner.getSelectedItemPosition() > 0;
	}

	public void setControlAssign(XControlAssign xyc)
	{
		controlAssign = xyc;
	}
	
	public XControlAssign getControlAssign()
	{
		return controlAssign;
	}

}
