package com.mayaswell.widget;

import java.io.File;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class FileChooserView extends TextView {
	protected String path="";
	protected String leaf="";
	private FileChooserView self;

	public interface Listener {
		void chooseFile(FileChooserView self);
		void unchooseFile(FileChooserView self);
	}
	
	protected Listener listener=null;	

	/**
	 * @param context
	 */
	public FileChooserView(Context context) {
		super(context);
		setup();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public FileChooserView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup();
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public FileChooserView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup();
	}
	
	public void setPath(String path)
	{
		if (path == null || path.equals("")) {
			this.path = "";
			this.setText("Click to select file");
		} else {
			this.path = path;
			this.setText(path);
		}
	}

	/**
	 * @param h
	 */
	public void setListener(Listener h)
	{
		listener = h;
	}

	/**
	 * @return
	 */
	protected boolean onClickEvent()
	{
		if (listener != null) listener.chooseFile(self);
		return true;
	}
	
	/**
	 * 
	 */
	protected void setup() {
		self = this;
		setEllipsize (TextUtils.TruncateAt.START);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickEvent();
			}
		});
		this.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v)
			{
				if (path != null && path != "") {
					Builder d = new AlertDialog.Builder(getContext());
					d.setTitle("Clear selected file");
					d.setMessage("Would you like to deselect '"+path+"'?");
					d.setPositiveButton("Totally", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton)
						{
							if (listener != null) listener.unchooseFile(self);
						}
					});
					d.setNegativeButton("Ooops", new DialogInterface.OnClickListener() {
					    public void onClick(DialogInterface dialog, int whichButton)
					    {
					    }
					});
					d.show();
					return false;
				}
				return false;
			}
		});
		
	}
}
