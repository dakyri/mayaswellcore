package com.mayaswell.audio;

public class ExpEnvelope {
	public boolean tempoLock;
	public float attackT;
	public float holdT;
	public float decayT;
	public float attack;
	public float decay;

	public ExpEnvelope(float attackRate2, float decayRate2, float attackT2, float sustainT2, float decayT2)
	{
		this(true, attackRate2, decayRate2, attackT2, sustainT2, decayT2);
	}
	
	public ExpEnvelope()
	{
		this(1, 1, 1, 1, 1);
	}

	public ExpEnvelope(boolean tempoLock2, float attackRate2, float decayRate2, float attackT2, float sustainT2, float decayT2)
	{
		tempoLock = tempoLock2;
		attackT = attackT2;
		decayT = decayT2;
		holdT = sustainT2;
		attack = attackRate2;
		decay = decayRate2;
	}

	public ExpEnvelope clone()
	{
		return new ExpEnvelope(tempoLock, attack, decay, attackT, holdT, decayT);
	}

}
