package com.mayaswell.widget;

public enum DisplayMode {
	SHOW_NAME, SHOW_VALUE_CTLS, SHOW_VALUE_DETAILS, SHOW_TARGET_CTLS, SHOW_PARAM_CTLS;
}
