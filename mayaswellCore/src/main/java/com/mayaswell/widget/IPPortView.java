package com.mayaswell.widget;

import com.mayaswell.R;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ToggleButton;

public class IPPortView extends RelativeLayout {

    private final String schemaName = "http://schemas.android.com/apk/res-auto";
	
	public interface Listener {
		public void portChanged(String host, String port, boolean b);
		public void enableChanged(boolean b);
	}

	protected TextView label = null;
	protected EditText portView = null;
	protected EditText hostView = null;
	protected String defaultHost = "localhost";
	protected String defaultPort = "7000";
	protected Listener listener = null;
	protected ToggleButton enableButton = null;
	protected KeyListener keyListener = null;
	protected int edittableBackgroundResource=-1;
	protected int unedittableBackgroundResource;
	private String displayMode = "normal";
	
	public IPPortView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public IPPortView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public IPPortView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.ip_port_edit, this, true);
		
		label = (TextView)findViewById (R.id.ipLabel);
		hostView = (EditText) findViewById(R.id.ipHost);
		portView = (EditText) findViewById(R.id.ipPort);
		enableButton  = (ToggleButton) findViewById(R.id.ipEnable);
		keyListener  = hostView.getKeyListener();
		if (hostView != null) {
			hostView.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void afterTextChanged(Editable s) {
				    String result = s.toString().replaceAll(" ", "");
				    if (!s.toString().equals(result)) {
				         hostView.setText(result);
				         hostView.setSelection(result.length());
				    }		
				}			
			});
			hostView.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {
						clearFocus();
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(hostView.getWindowToken(), 0);	
						checkAndNotify(true);
						return true;
					}
					return false;
				}

			});
			hostView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					requestFocus();
				}
			});
			hostView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(hostView, InputMethodManager.SHOW_IMPLICIT);	
						hostView.selectAll();
					} else {
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(hostView.getWindowToken(), 0);	
					}
				}
				
			});
		}
		if (portView != null) {
			portView.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void afterTextChanged(Editable s) {
				    String result = s.toString().replaceAll(" ", "");
				    if (!s.toString().equals(result)) {
				    	portView.setText(result);
				    	portView.setSelection(result.length());
				    }		
				}			
			});
			
			portView.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {
			        	clearFocus();
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(portView.getWindowToken(), 0);	
						checkAndNotify(true);
						return true;
					}
					return false;
				}
				
			});
			portView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (displayMode == "normal") requestFocus();
				}
			});
			portView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						((Activity)getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
						imm.showSoftInput(portView, InputMethodManager.SHOW_IMPLICIT);	
			        	portView.selectAll();
					} else {
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(portView.getWindowToken(), 0);	
					}
				}
				
			});
		}
		if (enableButton != null) {
			enableButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (listener != null) {
						listener.enableChanged(enableButton.isChecked());
					}
				}
				
			});
		}
		if (attrs != null) {
			String mode = attrs.getAttributeValue(schemaName, "mode");
			if (mode != null) {
				setMode(mode);
			}
			String lbl = attrs.getAttributeValue(schemaName, "label");
			if (lbl != null) {
				label.setText(lbl);
			}
			String dfh = attrs.getAttributeValue(schemaName, "dfltHost");
			if (dfh != null) {
				String[] a = dfh.split(":");
				defaultHost = a[0];
				if (a.length > 0) {
					defaultPort = a[1];
				}
			} else {
				defaultHost = "localhost";
				defaultPort = "7000";
			}
			setCurrentValue(defaultHost, defaultPort);
			int py = -1;
			int lhr = attrs.getAttributeResourceValue(schemaName, "itemHeight", -1);
			if (lhr >= 0) {
				py = getResources().getDimensionPixelSize(lhr);
			} else {
				String itemHeight = attrs.getAttributeValue(schemaName, "itemHeight");
				if (itemHeight != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			
			int px = -1;
			int lwr = attrs.getAttributeResourceValue(schemaName, "labelWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String labelWidth = attrs.getAttributeValue(schemaName, "labelWidth");
				if (labelWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}
			px = -1;
			lwr = attrs.getAttributeResourceValue(schemaName, "hostWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String hostWidth = attrs.getAttributeValue(schemaName, "hostWidth");
				if (hostWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (py > 0 || px > 0) {
				LayoutParams params = (LayoutParams) hostView.getLayoutParams();
				if (px > 0) params.width = px;
				if (py > 0) params.height = py;
			}
			
			px = -1;
			lwr = attrs.getAttributeResourceValue(schemaName, "portWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String portWidth = attrs.getAttributeValue(schemaName, "portWidth");
				if (portWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (py > 0 || px > 0) {
				LayoutParams params = (LayoutParams) portView.getLayoutParams();
				if (px > 0) params.width = px;
				if (py > 0) params.height = py;
			}
			
			px = -1;
			lwr = attrs.getAttributeResourceValue(schemaName, "enableWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String enableWidth = attrs.getAttributeValue(schemaName, "enableWidth");
				if (enableWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (py > 0 || px > 0) {
				LayoutParams params = (LayoutParams) enableButton.getLayoutParams();
				if (px > 0) params.width = px;
				if (py > 0) params.height = py;
			}
			
			int lsid = attrs.getAttributeResourceValue(schemaName, "labelStyle", -1);
			if (lsid >= 0) {
				if (label != null) {
					label.setTextAppearance(getContext(), lsid);
				}
			}
			int sid = attrs.getAttributeResourceValue(schemaName, "itemStyle", -1);
			if (sid >= 0) {
				Log.d("port", "got style ");
				if (hostView != null) {
					hostView.setTextAppearance(getContext(), sid);
				}
				if (portView != null) {
					portView.setTextAppearance(getContext(), sid);
				}
			}
			int rid = attrs.getAttributeResourceValue(schemaName, "itemBackground", -1);
			if (rid >= 0) {
				edittableBackgroundResource = rid;
				if (hostView != null) {
					hostView.setBackgroundResource(rid);
				}
				if (portView != null) {
					portView.setBackgroundResource(rid);
				}
			}
			int urid = attrs.getAttributeResourceValue(schemaName, "unedittableBackground", -1);
			if (urid >= 0) {
				unedittableBackgroundResource = urid;
			}
			int esid = attrs.getAttributeResourceValue(schemaName, "enableStyle", -1);
			if (esid >= 0) {
				Log.d("port", "got style ");
				if (enableButton != null) {
					enableButton.setTextAppearance(getContext(), esid);
				}
			}
			int erid = attrs.getAttributeResourceValue(schemaName, "enableBackground", -1);
			if (erid >= 0) {
				if (enableButton != null) {
					enableButton.setBackgroundResource(erid);
				}
			}
		}
		if (hostView != null) {
			hostView.setPadding(4, 0, 4, 0);
		}
		if (portView != null) {
			portView.setPadding(4, 0, 4, 0);
		}
		if (enableButton != null) {
			enableButton.setPadding(0, 0, 0, 0);
		}
	}
	
	public void setMode(String mode) {
		if (mode == null) {
			mode = "normal";
		}
		displayMode  = mode;
		if (mode.equals("hostOnly")) {
			if (portView != null) portView.setVisibility(GONE);
			if (hostView != null) {
				hostView.setVisibility(VISIBLE);
				hostView.setKeyListener(keyListener);
				hostView.setBackgroundResource(edittableBackgroundResource);
			}
		} else if (mode.equals("portOnly")) {
			if (portView != null) portView.setVisibility(VISIBLE);
			if (hostView != null) {
				hostView.setVisibility(GONE);
				hostView.setKeyListener(null);
			}
		} else if (mode.equals("hostUneditable")) {
			if (portView != null) portView.setVisibility(VISIBLE);
			if (hostView != null) {
				hostView.setVisibility(VISIBLE);
				hostView.setKeyListener(null);
				if (unedittableBackgroundResource > 0) {
					hostView.setBackgroundResource(unedittableBackgroundResource);
				}
			}
		} else {
			if (portView != null) portView.setVisibility(VISIBLE);
			if (hostView != null) hostView.setVisibility(VISIBLE);
		}
	}

	public void setCurrentValue(String host, String port) {
		if (portView != null) {
			portView.setText(port);
		}
		if (hostView != null) {
			hostView.setText(host);
		}
	}
	
	public void setListener(Listener l) {
		listener = l;
	}

	private void checkAndNotify(boolean fillEmptyHost) {
		if (fillEmptyHost && hostView != null && hostView.equals("")) {
			hostView.setText(defaultHost);
		}
		if (listener  != null) {
			listener.portChanged(hostView != null? hostView.getText().toString(): "", portView.getText().toString(), enableButton!=null?enableButton.isChecked():false);
		}
	}
	

}
