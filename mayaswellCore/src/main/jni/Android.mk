LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

SRC_DIR := audio
LOCAL_MODULE    := audio
LOCAL_CFLAGS    := -Werror -D__GXX_EXPERIMENTAL_CXX0X__
LOCAL_CPPFLAGS  := -std=c++11
APP_STL := gnustl_static
LOCAL_SRC_FILES := $(SRC_DIR)/Envelope.cpp $(SRC_DIR)/Effect.cpp $(SRC_DIR)/FXChain.cpp  $(SRC_DIR)/LFO.cpp $(SRC_DIR)/Filter.cpp $(SRC_DIR)/Oscilator.cpp $(SRC_DIR)/Delay.cpp $(SRC_DIR)/Flange.cpp $(SRC_DIR)/Chorus.cpp $(SRC_DIR)/Phaser.cpp $(SRC_DIR)/FloatBuffer.cpp
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/include $(LOCAL_PATH)/audio/include

include $(BUILD_STATIC_LIBRARY)