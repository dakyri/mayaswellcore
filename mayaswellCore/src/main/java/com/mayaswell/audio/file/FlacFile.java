package com.mayaswell.audio.file;

import android.util.Log;

import net.sourceforge.javaflacencoder.EncodingConfiguration;
import net.sourceforge.javaflacencoder.FLACEncoder;
import net.sourceforge.javaflacencoder.FLACFileOutputStream;
import net.sourceforge.javaflacencoder.FLACOutputStream;

import org.jflac.FLACDecoder;
import org.jflac.PCMProcessor;
import org.jflac.metadata.StreamInfo;
import org.jflac.util.ByteData;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by dak on 2/14/2017.
 *
 * if the file is not cachingDecodedInput (ie no seek table) we'll decode the whole thing into a temporary which is cachingDecodedInput
 *
 * for the moment, this fallback is the only thing in town anyway
 */
public class FlacFile implements AudioFile, PCMProcessor {
	protected final File cacheDir;
	protected FLACEncoder enc;
	protected FLACDecoder dec;
	protected File flacFile;

	protected IOState ioState;				// Specifies the IO State of the Wav File (used for snaity checking)
	protected long numFrames;					// Number of frames within the data section
	protected int numChannels;				// 2 bytes unsigned, 0x0001 (1) to 0xFFFF (65,535)
	protected long sampleRate;				// 4 bytes unsigned, 0x00000001 (1) to 0xFFFFFFFF (4,294,967,295)
	protected int validBits;					// 2 bytes unsigned, 0x0002 (2) to 0xFFFF (65,535)
	protected File decodedCacheFile;
	protected FileOutputStream decodedOutStream;
	protected FileInputStream inStream;
	protected DataInputStream decodedInStream;
	protected boolean ioError;
	protected boolean cachingDecodedInput;
	protected long frameCounter;				// Current number of frames read or written
	protected FLACFileOutputStream flacOutEncStream;

	FlacFile(File systemCacheDir) {
		enc = null;
		dec = null;
		flacFile = null;
		ioState = IOState.CLOSED;
		numFrames = 0;
		numChannels = 1;
		sampleRate = 44100;
		validBits = 16;
		cacheDir = systemCacheDir;
		decodedCacheFile = null;
		decodedOutStream = null;
		decodedInStream = null;
		ioError = false;
		cachingDecodedInput = false;
		frameCounter = 0;
		flacOutEncStream = null;
	}

	FlacFile() {
		this(null);
	}


	/**
	 * currently slurps in whole file and decodes as input in most use cases needs be random and we can't guarantee that
	 * from a flac ... may be possible if seek info is set in metadata
	 * @param file
	 * @throws IOException
	 * @throws AudioFileException
	 */
	@Override
	public void open(File file) throws IOException, AudioFileException {
		if (ioState != IOState.CLOSED) {
			close();
		}
		flacFile = file;
		cachingDecodedInput = true;
		frameCounter = 0;
		if (cachingDecodedInput) {
			if (decodedCacheFile != null) {
				decodedCacheFile.delete();
			}
			decodedCacheFile=File.createTempFile("temp","raw",cacheDir);
			decode(file);
			decodedInStream = new DataInputStream(new FileInputStream(decodedCacheFile));
		}
		ioState = IOState.READING;

	}

	@Override
	public void create(File file, int numChannels, long numFrames, int validBits, long sampleRate) throws IOException, AudioFileException {
		if (ioState != IOState.CLOSED) {
			close();
		}
		this.flacFile = file;
		this.numChannels = numChannels;
		this.numFrames = numFrames;
		this.sampleRate = sampleRate;
		this.validBits = validBits;
		if (flacOutEncStream != null) {
			flacOutEncStream.close();
		}
		flacOutEncStream = new FLACFileOutputStream(flacFile.getPath());
		enc = new FLACEncoder();
		enc.setOutputStream(flacOutEncStream);
		enc.openFLACStream();

		ioState = IOState.WRITING;
	}

	@Override
	public void close() throws IOException {
		if (ioState == IOState.READING) {
			if (decodedInStream != null) {
				decodedInStream.close();
				decodedInStream = null;
			}
			if (decodedOutStream != null) {
				decodedOutStream.close();
				decodedOutStream = null;
			}
			if (decodedCacheFile != null) {
				decodedCacheFile.delete();
				decodedCacheFile = null;
			}
		} else if (ioState == IOState.WRITING) {
			if (enc != null) {
				enc.encodeSamples(0, true); // force finalize
				enc = null;
			}
			if (flacOutEncStream != null) {
				flacOutEncStream.close();
				flacOutEncStream = null;
			}
		}
		ioState = IOState.CLOSED;
	}

	@Override
	public boolean valid(File file) {
		if (file == null) return false;
		String name = file.getName();
		if (name.matches(".*\\.(flac|FLAC|fla|FLA)")) return true;
		return false;
	}

	@Override
	public File getFile() {
		return flacFile;
	}

	@Override
	public String getPath() {
		return flacFile.getPath();
	}

	@Override
	public int getNumChannels() {
		return numChannels;
	}

	@Override
	public long getNumFrames() {
		return numFrames;
	}

	@Override
	public void seekToFrame(long frame) throws IOException, AudioFileException {
		if (ioState == IOState.CLOSED || ioState == IOState.WRITING) {
			throw new IOException("FLAC can't seek while writing or closed");
		}
		if (cachingDecodedInput) {
			if (decodedCacheFile == null) {
				throw new IOException("Non seeking flac file but no cache");
			}
			if (frame > numFrames) {
				throw new IOException("Seek to frame beyond end");
			}
			FileChannel c = inStream.getChannel();
			long cp = c.position();
			int bytesPerSample = (validBits+7)/8;
			long frs = frame*bytesPerSample*numChannels;
			if (frs == cp) {
				Log.d("wave file", "seek frame is in the right spot!");
				return;
			}
			c.position(frs);
		} else {
			throw new IOException("Directly seeking flac not implemented");
		}
	}

	@Override
	public long currentFrame() throws IOException, AudioFileException {
		if (cachingDecodedInput) {
			if (decodedCacheFile == null) {
				throw new IOException("Non seeking flac file but no cache");
			}
			FileChannel c = inStream.getChannel();
			long cp = c.position();
			int bytesPerSample = (validBits+7)/8;
			return cp / (bytesPerSample*numChannels);
		} else {
			throw new IOException("Directly seeking flac not implemented");
		}
	}

	@Override
	public int readFrames(short[] sampleBuffer, int numFramesToRead) throws IOException, AudioFileException {
		return readFrames(sampleBuffer, 0, numFramesToRead);
	}

	@Override
	public int readFrames(short[] sampleBuffer, int offset, int numFramesToRead) throws IOException, AudioFileException {
		if (ioState != IOState.READING) throw new IOException("Cannot read from WavFile instance");

		for (int f=0 ; f<numFramesToRead ; f++) {
			if (frameCounter == numFrames) return f;

			for (int c=0 ; c<numChannels ; c++) {
				sampleBuffer[offset] = readShortSample();
				offset ++;
			}

			frameCounter ++;
		}

		return numFramesToRead;
	}

	@Override
	public int readFrames(float[] sampleBuffer, int numFramesToRead) throws IOException, AudioFileException {
		return readFrames(sampleBuffer, 0, numFramesToRead);
	}

	@Override
	public int readFrames(float[] sampleBuffer, int offset, int numFramesToRead) throws IOException, AudioFileException {
		if (ioState != IOState.READING) throw new IOException("Cannot read from WavFile instance");

		for (int f=0 ; f<numFramesToRead ; f++) {
			if (frameCounter == numFrames) return f;

			for (int c=0 ; c<numChannels ; c++) {
				sampleBuffer[offset] = (float)readFloatSample();
				offset ++;
			}

			frameCounter ++;
		}

		return numFramesToRead;
	}

	@Override
	public int readFrames(double[] sampleBuffer, int numFramesToRead) throws IOException, AudioFileException {
		return readFrames(sampleBuffer, 0, numFramesToRead);
	}

	@Override
	public int readFrames(double[] sampleBuffer, int offset, int numFramesToRead) throws IOException, AudioFileException {
		if (ioState != IOState.READING) throw new IOException("Cannot read from WavFile instance");

		for (int f=0 ; f<numFramesToRead ; f++) {
			if (frameCounter == numFrames) return f;

			for (int c=0 ; c<numChannels ; c++) {
				sampleBuffer[offset] = readFloatSample();
				offset ++;
			}

			frameCounter ++;
		}

		return numFramesToRead;
	}

	private double readFloatSample() throws IOException {
		if (cachingDecodedInput) {
			if (decodedCacheFile == null) {
				throw new IOException("Decoded flac cache not found");
			}
			if (validBits == 16) {
				return decodedInStream.readShort()/Short.MAX_VALUE;
			} else if (validBits == 32) {
				return decodedInStream.readFloat();
			} else if (validBits == 64) {
				return decodedInStream.readDouble();
			}
			throw new IOException("Unexpected sample bit length " + validBits);
		}
		throw new IOException("Direct flac decode not implemented");
	}

	private short readShortSample() throws IOException {
		if (cachingDecodedInput) {
			if (decodedCacheFile == null) {
				throw new IOException("Decoded flac cache not found");
			}
			if (validBits == 16) {
				return decodedInStream.readShort();
			} else if (validBits == 32) {
				return (short)(decodedInStream.readFloat()*Short.MAX_VALUE);
			} else if (validBits == 64) {
				return (short)(decodedInStream.readDouble()*Short.MAX_VALUE);
			}
			throw new IOException("Unexpected sample bit length " + validBits);
		}
		throw new IOException("Direct flac decode not implemented");
	}

	@Override
	public int writeFrames(short[] sampleBuffer, int numFramesToWrite) throws IOException, AudioFileException {
		return writeFrames(sampleBuffer, 0, numFramesToWrite);
	}

	@Override
	public int writeFrames(short[] sampleBuffer, int offset, int numFramesToWrite) throws IOException, AudioFileException {
		if (ioState != IOState.WRITING) throw new IOException("Cannot write to FLAC file: no opend");
		if (enc == null)  throw new IOException("Cannot write to FLAC file: encode not existing");
		if (offset + numFramesToWrite >= sampleBuffer.length) {
			numFramesToWrite = sampleBuffer.length - offset;
		}
		int [] a = new int[numFramesToWrite];
		for (int f=0 ; f<numFramesToWrite ; f++) {
			a[f] = sampleBuffer[f+offset];
			frameCounter++;
		}
		enc.addSamples(a, numFramesToWrite);
		enc.encodeSamples(numFramesToWrite, false);

		return numFramesToWrite;
	}

	@Override
	public int writeFrames(float[] sampleBuffer, int numFramesToWrite) throws IOException, AudioFileException {
		return writeFrames(sampleBuffer, 0, numFramesToWrite);
	}

	@Override
	public int writeFrames(float[] sampleBuffer, int offset, int numFramesToWrite) throws IOException, AudioFileException {
		if (ioState != IOState.WRITING) throw new IOException("Cannot write to FLAC file: no opend");
		if (enc == null)  throw new IOException("Cannot write to FLAC file: encode not existing");
		if (offset + numFramesToWrite >= sampleBuffer.length) {
			numFramesToWrite = sampleBuffer.length - offset;
		}
		int [] a = new int[numFramesToWrite];
		for (int f=0 ; f<numFramesToWrite ; f++) {
			a[f] = (int)sampleBuffer[f+offset]* Short.MAX_VALUE;
			frameCounter++;
		}
		enc.addSamples(a, numFramesToWrite);
		enc.encodeSamples(numFramesToWrite, false);

		return numFramesToWrite;
	}

	@Override
	public int writeFrames(double[] sampleBuffer, int numFramesToWrite) throws IOException, AudioFileException {
		return writeFrames(sampleBuffer, 0, numFramesToWrite);
	}

	@Override
	public int writeFrames(double[] sampleBuffer, int offset, int numFramesToWrite) throws IOException, AudioFileException {
		if (ioState != IOState.WRITING) throw new IOException("Cannot write to FLAC file: no opend");
		if (enc == null)  throw new IOException("Cannot write to FLAC file: encode not existing");
		if (offset + numFramesToWrite >= sampleBuffer.length) {
			numFramesToWrite = sampleBuffer.length - offset;
		}
		int [] a = new int[numFramesToWrite];
		for (int f=0 ; f<numFramesToWrite ; f++) {
			a[f] = (int)sampleBuffer[f+offset]* Short.MAX_VALUE;
			frameCounter++;
		}
		enc.addSamples(a, numFramesToWrite);
		enc.encodeSamples(numFramesToWrite, false);

		return numFramesToWrite;
	}

	// jflac pcmprocessor overrides
	@Override
	public void processStreamInfo(StreamInfo streamInfo) {
		numChannels = streamInfo.getChannels();
		numFrames = streamInfo.getTotalSamples();
		sampleRate = streamInfo.getSampleRate();
		validBits = streamInfo.getBitsPerSample();
	}

	@Override
	public void processPCM(ByteData pcm) {
		try {
			if (cachingDecodedInput) {
				decodedOutStream.write(pcm.getData(), 0, pcm.getLen());
			} else {
				throw new IOException("non cached input unimplemented");
			}
		} catch (IOException e) {
			ioError = true;
		}
	}


	/**
	 * Decode and play an input FLAC flacFile.
	 * @param inFile The input FLAC flacFile name
	 * @throws IOException  Thrown if error reading flacFile
	 */
	public void decode(File inFile) throws IOException {
//        System.out.println("Play [" + inFileName + "]");
		FileInputStream is = new FileInputStream(inFile);
		decodedOutStream = new FileOutputStream(decodedCacheFile);

		FLACDecoder decoder = new FLACDecoder(is);
		decoder.addPCMProcessor(this);
		try {
			decoder.decode();
		} catch (EOFException e) {
			// skip
		}
		is.close();
		decodedOutStream.close();
		decodedOutStream = null;
	}
}
