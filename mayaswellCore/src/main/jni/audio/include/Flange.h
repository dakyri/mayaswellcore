/*
 * Flange.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */

#ifndef FLANGE_H_
#define FLANGE_H_

#include "TwoPoleLowPass.h"
#include "FloatBuffer.h"

class Flange {
public:
	Flange();
	virtual ~Flange();


	void initialize();
	void reset();
	void release();

	void setParameters(float rate, float width, float fb, float delay, float mix);
	void setRate(float r);
	void setWidth (float v);
	void setFeedback (float v);
	void setDelay (float v);
	void setMix(float v);

	void apply (float *signal, long sampleFrames);

	static const int kBufSize=8192;		// must be about 1/5 of a second at given sample rate and must be a power of 2 for masking
protected:
	void setSweep();

	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto
	float paramMix;

	double sweepRate;			// actual calc'd sweep rate
	double step;				// amount to step the sweep each sample
	double sweep;				// current value of sweep in steps behind fill pointer
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 to 1.0
	double sweepSamples;			// sweep width in # of samples
	double maxSweepSamples;	// upper bound of sweep in samples

	double outval1;			// most recent output value (for feedback)
	double outval2;			// most recent output value (for feedback)

	int	   fp;					// fill/write pointer

	FloatBuffer		buf1;
	float*			buf1p;
	FloatBuffer		buf2;
	float*			buf2p;

	TwoPoleLowPassFilter delayControlFilter; // filters condition these params so they can be driven hard
	TwoPoleLowPassFilter mixControlFilter;
};

#define BOTTOM_FREQ 100
#define ROUND(n)		((int)((double)(n)+0.5))
#define PIN(n,min,max) ((n) > (max) ? max : ((n) < (min) ? (min) : (n)))

//
//	MODF - vaguely related to the library routine modf(), this macro breaks a double into
//	integer and fractional components i and f respectively.
//
//	n - input number, a double
//	i - integer portion, an integer (the input number integer portion should fit)
//	f - fractional portion, a double
//
#define	MODF(n,i,f) ((i) = (int)(n), (f) = (n) - (double)(i))

#endif /* FLANGE_H_ */
