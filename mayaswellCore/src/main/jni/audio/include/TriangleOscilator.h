#ifndef TRIANGLEOSC
#define TRIANGLEOSC

#include "AudioSignal.h"

class TriangleOscilator
{
public:
	TriangleOscilator(float amp = 1., float sr=signalSampleRate);
	
	void setSkew(float sk);
	void setAmplitude(float amp);
	void setPhase(float phs);
	void setFrequency(float freq, bool force=false);
	
	Sample operator () (void);
	void operator () (Signal& outSig);
	
private:
	float amp;
	float inc_u;
	float inc_d;
	float inc;
	float peak;
	float currentValue;
	long cycle;
	float frequency;
	bool up;
	float skew;
	float sampleRate;
};

inline void
TriangleOscilator::setPhase(float phs)
{
	float sam = (phs/(2*pi));
	if (sam > 0.5) {
		currentValue = inc_u * (sam-0.5) - amp;
		up = true;
	} else {
		currentValue = amp - inc_d * sam;
		up = false;
	}
}

inline
TriangleOscilator::TriangleOscilator(float a, float sr)
	: sampleRate(sr)
{
	currentValue = peak = amp = a;
	skew = 0;
	up = false;
	frequency = 0;
	
	setFrequency(220);
}

inline void
TriangleOscilator::setAmplitude(float a)
{
	peak = amp = a;
}

inline void
TriangleOscilator::operator()(Signal& outSig)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		outSig[i]=(*this)();
}

inline void
TriangleOscilator::setFrequency(float freq, bool force)
{
	if (freq != frequency || force) {
		frequency = freq;

		cycle = (sampleRate / freq);
		int		rx = (1-skew)*cycle/2.0;
		if (rx < 1) rx = 1;
		inc_u = (2*peak)/rx;
		inc_d = (2*peak)/(cycle-rx);
		inc = up? inc_u: -inc_d;
	}
}

inline void
TriangleOscilator::setSkew(float sk)
{
	skew = (sk<0?0:sk>1?1:sk);
	setFrequency(frequency, true);
}
inline Sample
TriangleOscilator::operator()(void)
{
	currentValue += inc;
	if (currentValue <= -peak) {
		currentValue = -peak;
		inc = inc_u;
		up = true;
	} else if (currentValue >= peak) {
		currentValue = peak;
		inc = -inc_d;
		up = false;
	}

	return currentValue;
}


#endif