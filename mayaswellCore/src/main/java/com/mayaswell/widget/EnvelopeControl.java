package com.mayaswell.widget;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.R;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.widget.InfinitePotView.InfiniteKnobListener;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class EnvelopeControl extends ModulatorControl
{
	public interface EnvelopeControlListener
	{
		public void onAttackChanged(Envelope env, float amt);
		public void onDecayChanged(Envelope env, float amt);
		public void onReleaseChanged(Envelope env, float amt);
		public void onSustainChanged(Envelope env, float amt);
		public void onSustainLevelChanged(Envelope env, float amt);
		public void onResetChanged(Envelope env, ResetMode sip);
		public void onTempoLockChanged(Envelope env, boolean isChecked);
		public void onTargetValueChanged(Envelope env, int targetId, float amt);
		public void onTargetControlChanged(EnvelopeControl me, Envelope env, int targetId, Control tgt, float amt);
	}

	private Spinner rstModeSelect=null;
	private InfinitePotView attPot = null;
	private EditText attView = null;
	private TextView attLbl = null;
	private InfinitePotView decPot = null;
	private EditText decView = null;
	private TextView decLbl = null;
	private InfinitePotView susPot = null;
	private EditText susView = null;
	private TextView susLbl = null;
	private RotaryPotView susLPot = null;
	private EditText susLView = null;
	private InfinitePotView relPot = null;
	private EditText relView = null;
	private TextView relLbl = null;

	private EnvelopeControlListener listener=null;
	private Envelope envelope = null;
	private int envid = -1;

	public EnvelopeControl(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public EnvelopeControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public EnvelopeControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.sg_env_control, this, true);
		
		label = (TextView)findViewById (R.id.sgCtlLabel);
		rstModeSelect = (Spinner) findViewById (R.id.sgCtlSpinner);
		tempoLockButton = (ToggleButton) findViewById(R.id.sgCtlToggle);
		attPot = (InfinitePotView)findViewById (R.id.sgCtlPot);
		attView = (EditText)findViewById (R.id.sgCtlEditText);
		attLbl = (TextView)findViewById (R.id.sgCtlPotLbl1);
		decPot = (InfinitePotView)findViewById (R.id.sgCtlPot2);
		decView = (EditText)findViewById (R.id.sgCtlEditText2);
		decLbl = (TextView)findViewById (R.id.sgCtlPotLbl2);
		susPot = (InfinitePotView)findViewById (R.id.sgCtlPot3);
		susView = (EditText)findViewById (R.id.sgCtlEditText3);
		susLbl = (TextView)findViewById (R.id.sgCtlPotLbl3);
		susLPot = (RotaryPotView)findViewById (R.id.sgCtlPot4);
		susLView = (EditText)findViewById (R.id.sgCtlEditText4);
		relPot = (InfinitePotView)findViewById (R.id.sgCtlPot5);
		relView = (EditText)findViewById (R.id.sgCtlEditText5);
		relLbl = (TextView)findViewById (R.id.sgCtlPotLbl5);
		targetBody = (RelativeLayout) findViewById(R.id.sgCtlLayout);
		
		label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (getDisplayMode() == DisplayMode.SHOW_NAME) {
					setDisplay(DisplayMode.SHOW_VALUE_CTLS);
				} else if (getDisplayMode() == DisplayMode.SHOW_VALUE_CTLS) {
					setDisplay(DisplayMode.SHOW_VALUE_DETAILS);
				} else if (getDisplayMode() == DisplayMode.SHOW_VALUE_DETAILS) {
					setDisplay(DisplayMode.SHOW_TARGET_CTLS);
				} else {
					setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		});
		
		attPot.setValueBounds(0, Float.MAX_VALUE);
		attPot.setCenterVal(1.0f);
		attPot.setFullCircleDelta(4.0f);
		attPot.setKnobListener(new InfiniteKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				attView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListenerAttack(v);
			}
		});
		
		attView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < attPot.getMinVal()) v = attPot.getMinVal();
						else if (v > attPot.getMaxVal()) v = attPot.getMaxVal();
						attPot.setValue(v);
						notifyListenerAttack(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		decPot.setValueBounds(0, Float.MAX_VALUE);
		decPot.setCenterVal(1.0f);
		decPot.setFullCircleDelta(4.0f);
		decPot.setKnobListener(new InfiniteKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				decView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListenerDecay(v);
			}
		});
		
		decView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < decPot.getMinVal()) v = decPot.getMinVal();
						else if (v > decPot.getMaxVal()) v = decPot.getMaxVal();
						decPot.setValue(v);
						notifyListenerDecay(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		susPot.setValueBounds(0, Float.MAX_VALUE);
		susPot.setCenterVal(1.0f);
		susPot.setFullCircleDelta(4.0f);
		susPot.setKnobListener(new InfiniteKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				susView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListenerSustain(v);
			}
		});
		
		susView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < susPot.getMinVal()) v = susPot.getMinVal();
						else if (v > susPot.getMaxVal()) v = susPot.getMaxVal();
						susPot.setValue(v);
						notifyListenerSustain(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		susLPot.setValueBounds(0, 1);
		susLPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				susLView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListenerSustainLevel(v);
			}
		});
		
		susLView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < susLPot.getMinVal()) v = susLPot.getMinVal();
						else if (v > susLPot.getMaxVal()) v = susLPot.getMaxVal();
						susLPot.setValue(v);
						notifyListenerSustainLevel(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		relPot.setValueBounds(0, Float.MAX_VALUE);
		relPot.setCenterVal(1.0f);
		relPot.setFullCircleDelta(4.0f);
		relPot.setKnobListener(new InfiniteKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				relView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListenerRelease(v);
			}
		});
		
		relView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < relPot.getMinVal()) v = relPot.getMinVal();
						else if (v > relPot.getMaxVal()) v = relPot.getMaxVal();
						relPot.setValue(v);
						notifyListenerRelease(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		rstModeSelect.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				checkAndNotifyReset();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) 
			{
			}
		});
		
		tempoLockButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				notifyListener(isChecked);
			}
		});

		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			int px = -1;
			int swr = 0;
			px = 0;
			swr = attrs.getAttributeResourceValue(schemaName, "resetSpinnerWidth", -1);
			if (swr >= 0) {
				px = getResources().getDimensionPixelSize(swr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "resetSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setResetSelectWidth(px);
			}
			px = 0;
			int vwr = attrs.getAttributeResourceValue(schemaName, "viewWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "viewWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				LayoutParams params = (LayoutParams) attView.getLayoutParams();
				params.width = px;
			}
		}

	}
	
	public void setListener(EnvelopeControlListener l )
	{
		listener = l;
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 */
	public float getAttValue()
	{
		return attPot.getValue();
	}
	
	private void checkAndNotifyReset()
	{
		ResetMode sip = null;
		try {
			SpinnerAdapter csa = rstModeSelect.getAdapter();
			if (csa != null) {
				sip = (ResetMode) csa.getItem(rstModeSelect.getSelectedItemPosition());
			}
		} catch (Exception e) {
			
		}
		if (sip != null) {
			notifyListener(sip);
		}
	}
	
	public void setAttValueBounds(float min, float max)
	{
		attPot.setValueBounds(min, max);
	}
	
	public void setAttCenterValue(float cv)
	{
		attPot.setCenterVal(cv);
	}
	
	public void setAdapters(ControlsAdapter tga, ArrayAdapter<ResetMode> erma)
	{
		tgtAdapter  = tga;
		setTargetAdapter(tga);
		rstModeSelect.setAdapter(erma);
		addTargetControl(0);
	}
	
	public void setDisplay(DisplayMode dm)
	{
		displayMode = dm;

		int px = getResources().getDimensionPixelSize(R.dimen.modPotHeight);
		int qx = getResources().getDimensionPixelSize(R.dimen.modTViewWidth);
		int rx = getResources().getDimensionPixelSize(R.dimen.subElementHeight);

		switch (displayMode) {
		case SHOW_NAME:
			attLbl.setVisibility(GONE);
			attPot.setVisibility(GONE);
			attView.setVisibility(GONE);
			decLbl.setVisibility(GONE);
			decPot.setVisibility(GONE);
			decView.setVisibility(GONE);
			susLbl.setVisibility(GONE);
			susPot.setVisibility(GONE);
			susView.setVisibility(GONE);
			susLPot.setVisibility(GONE);
			susLView.setVisibility(GONE);
			relLbl.setVisibility(GONE);
			relPot.setVisibility(GONE);
			relView.setVisibility(GONE);
			rstModeSelect.setVisibility(GONE);
			tempoLockButton.setVisibility(GONE);
			targetBody.setVisibility(GONE);
			break;
		case SHOW_VALUE_CTLS:
			attLbl.setVisibility(GONE);
			attPot.setVisibility(VISIBLE);attPot.setLayoutParams(rlParams(px, px, -1,tempoLockButton.getId()));
			attView.setVisibility(GONE);
			decLbl.setVisibility(GONE);
			decPot.setVisibility(VISIBLE);decPot.setLayoutParams(rlParams(px, px, -1,attPot.getId()));
			decView.setVisibility(GONE);
			susLbl.setVisibility(GONE);
			susPot.setVisibility(VISIBLE);susPot.setLayoutParams(rlParams(px, px, -1,decPot.getId()));
			susView.setVisibility(GONE);
			susLPot.setVisibility(VISIBLE);susLPot.setLayoutParams(rlParams(px, px, -1,susPot.getId()));
			susLView.setVisibility(GONE);
			relLbl.setVisibility(GONE);
			relPot.setVisibility(VISIBLE);relPot.setLayoutParams(rlParams(px, px, -1,susLPot.getId()));
			relView.setVisibility(GONE);
			rstModeSelect.setVisibility(VISIBLE);
			tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
			targetBody.setVisibility(GONE);
			break;
		case SHOW_VALUE_DETAILS:
			attLbl.setVisibility(VISIBLE);attLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, -1,tempoLockButton.getId()));
			attPot.setVisibility(VISIBLE);attPot.setLayoutParams(rlParams(px, px, -1,attLbl.getId()));
			attView.setVisibility(VISIBLE);attView.setLayoutParams(rlParams(qx, rx, -1,attPot.getId()));
			decLbl.setVisibility(VISIBLE);decLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, -1,attView.getId()));
			decPot.setVisibility(VISIBLE);decPot.setLayoutParams(rlParams(px, px, -1,decLbl.getId()));
			decView.setVisibility(VISIBLE);decView.setLayoutParams(rlParams(qx, rx, -1,decPot.getId()));
			susLbl.setVisibility(VISIBLE);susLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, label.getId(),-1));
			susPot.setVisibility(VISIBLE);susPot.setLayoutParams(rlParams(px, px, label.getId(),susLbl.getId()));
			susView.setVisibility(VISIBLE);susView.setLayoutParams(rlParams(qx, rx, label.getId(),susPot.getId()));
			susLPot.setVisibility(VISIBLE);susLPot.setLayoutParams(rlParams(px, px, label.getId(),susView.getId()));
			susLView.setVisibility(VISIBLE);susLView.setLayoutParams(rlParams(qx, rx, label.getId(),susLPot.getId()));
			relLbl.setVisibility(VISIBLE);relLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, label.getId(),susLView.getId()));
			relPot.setVisibility(VISIBLE);relPot.setLayoutParams(rlParams(px, px, label.getId(),relLbl.getId()));
			relView.setVisibility(VISIBLE);relView.setLayoutParams(rlParams(qx, rx, label.getId(),relPot.getId()));
			rstModeSelect.setVisibility(VISIBLE);
			tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
			targetBody.setVisibility(GONE);
			break;
		case SHOW_TARGET_CTLS:
			attLbl.setVisibility(VISIBLE);attLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, -1,tempoLockButton.getId()));
			attPot.setVisibility(VISIBLE);attPot.setLayoutParams(rlParams(px, px, -1,attLbl.getId()));
			attView.setVisibility(VISIBLE);attView.setLayoutParams(rlParams(qx, rx, -1,attPot.getId()));
			decLbl.setVisibility(VISIBLE);decLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, -1,attView.getId()));
			decPot.setVisibility(VISIBLE);decPot.setLayoutParams(rlParams(px, px, -1,decLbl.getId()));
			decView.setVisibility(VISIBLE);decView.setLayoutParams(rlParams(qx, rx, -1,decPot.getId()));
			susLbl.setVisibility(VISIBLE);susLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, label.getId(),-1));
			susPot.setVisibility(VISIBLE);susPot.setLayoutParams(rlParams(px, px, label.getId(),susLbl.getId()));
			susView.setVisibility(VISIBLE);susView.setLayoutParams(rlParams(qx, rx, label.getId(),susPot.getId()));
			susLPot.setVisibility(VISIBLE);susLPot.setLayoutParams(rlParams(px, px, label.getId(),susView.getId()));
			susLView.setVisibility(VISIBLE);susLView.setLayoutParams(rlParams(qx, rx, label.getId(),susLPot.getId()));
			relLbl.setVisibility(VISIBLE);relLbl.setLayoutParams(rlParams(LayoutParams.WRAP_CONTENT, rx, label.getId(),susLView.getId()));
			relPot.setVisibility(VISIBLE);relPot.setLayoutParams(rlParams(px, px, label.getId(),relLbl.getId()));
			relView.setVisibility(VISIBLE);relView.setLayoutParams(rlParams(qx, rx, label.getId(),relPot.getId()));
			rstModeSelect.setVisibility(VISIBLE);
			tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
			targetBody.setVisibility(VISIBLE);
			break;
		}
		if (displayMode == DisplayMode.SHOW_TARGET_CTLS) {
			checkFullyOpenedSiblings(getClass(), DisplayMode.SHOW_VALUE_DETAILS);
		}
		SubPanelFragment.layoutEditorControlParent(this);
	}

	private void notifyListener(ResetMode sip) {
		if (listener != null)
			listener.onResetChanged(envelope , sip);
	}

	private void notifyListenerAttack(float sip)
	{
		if (listener != null)
			listener.onAttackChanged(envelope, sip);
	}
	
	private void notifyListenerDecay(float sip)
	{
		if (listener != null)
			listener.onDecayChanged(envelope, sip);
	}
	
	private void notifyListenerSustain(float sip)
	{
		if (listener != null)
			listener.onSustainChanged(envelope, sip);
	}
	
	private void notifyListenerRelease(float sip)
	{
		if (listener != null)
			listener.onReleaseChanged(envelope, sip);
	}
	
	private void notifyListenerSustainLevel(float sip)
	{
		if (listener != null)
			listener.onSustainLevelChanged(envelope, sip);
	}
	
	private void notifyListener(boolean isChecked)
	{
		if (listener != null)
			listener.onTempoLockChanged(envelope, alwaysTempoLock?true:isChecked);
	}
	
	@Override
	protected void notifyTargetControlChanged(int targetId, Control tgt, float amt)
	{
		super.notifyTargetControlChanged(targetId, tgt, amt);
		if (listener != null) {
			listener.onTargetControlChanged(this, envelope, targetId, tgt, amt);
		}
	}

	@Override
	protected void notifyTargetValueChanged(int targetId, float amt)
	{
		super.notifyTargetValueChanged(targetId, amt);
		if (listener != null) {
			listener.onTargetValueChanged(envelope, targetId, amt);
		}
	}

	public boolean set(Envelope xsp)
	{
		if (xsp == null) {
			xsp = new Envelope();
		}
		if (!super.set(xsp)) {
			return false;
		}
		envelope  = xsp;
		if (xsp != null) {
			attPot.setValue(xsp.attackT);
			attView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.attackT):Float.toString(xsp.attackT));
			decPot.setValue(xsp.attackT);
			decView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.decayT):Float.toString(xsp.decayT));
			susPot.setValue(xsp.attackT);
			susView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.sustainT):Float.toString(xsp.sustainT));
			susLPot.setValue(xsp.attackT);
			susLView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.sustainL):Float.toString(xsp.sustainL));
			relPot.setValue(xsp.attackT);
			relView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.releaseT):Float.toString(xsp.releaseT));
		}
		return true;
	}

	public void clearAssignment()
	{
		super.clearAssignment();
		envelope = null;
		attPot.setValue(attPot.getCenterVal());
		attView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, attPot.getCenterVal()):Float.toString(attPot.getCenterVal()));
		decPot.setValue(decPot.getCenterVal());
		decView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, decPot.getCenterVal()):Float.toString(decPot.getCenterVal()));
		susPot.setValue(susPot.getCenterVal());
		susView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, susPot.getCenterVal()):Float.toString(susPot.getCenterVal()));
		susLPot.setValue(susLPot.getcenterVal());
		susLView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, susLPot.getcenterVal()):Float.toString(susLPot.getcenterVal()));
		relPot.setValue(relPot.getCenterVal());
		relView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, relPot.getCenterVal()):Float.toString(relPot.getCenterVal()));
	}

	public void setAttackTValue(float v)
	{
		if (attPot == null || attView == null) return;
		if (v < attPot.getMinVal()) v = attPot.getMinVal();
		else if (v > attPot.getMaxVal()) v = attPot.getMaxVal();
		attPot.setValue(v);
		attView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}
	
	public void setReleaseTValue(float v) {
		if (decPot == null || decView == null) return;
		if (v < decPot.getMinVal()) v = decPot.getMinVal();
		else if (v > decPot.getMaxVal()) v = decPot.getMaxVal();
		decPot.setValue(v);
		decView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}

	public void setSustainTValue(float v) {
		if (susPot == null || susView == null) return;
		if (v < susPot.getMinVal()) v = susPot.getMinVal();
		else if (v > susPot.getMaxVal()) v = susPot.getMaxVal();
		susPot.setValue(v);
		susView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}

	public void setSustainLValue(float v) {
		if (susLPot == null || susLView == null) return;
		if (v < susLPot.getMinVal()) v = susLPot.getMinVal();
		else if (v > susLPot.getMaxVal()) v = susLPot.getMaxVal();
		susLPot.setValue(v);
		susLView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}

	public void setDecayTValue(float v) {
		if (relPot == null || attView == null) return;
		if (v < relPot.getMinVal()) v = relPot.getMinVal();
		else if (v > relPot.getMaxVal()) v = relPot.getMaxVal();
		relPot.setValue(v);
		relView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}

	public int getEnvid() {
		return envid ;
	}

	public void setEnvid(int envid) {
		this.envid = envid;
	}
	
	public int countValidTargets() {
		if (envelope == null) return 0;
		return envelope.countValidTargets();
	}
	
	@Override
	public void setAlwaysTempoLock(boolean b)
	{
		super.setAlwaysTempoLock(b);
		tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
	}

}
