/*
 * LFO.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#include "LFO.h"

FunctionTable 		sineWavetable(wavetableLength);
FunctionTable 		squareWavetable(wavetableLength);
FunctionTable 		posExpWavetable(wavetableLength);
FunctionTable 		negExpWavetable(wavetableLength);
FunctionTable 		posSawWavetable(wavetableLength);
FunctionTable 		negSawWavetable(wavetableLength);


LFOHost::LFOHost()
{
	nActiveLFO = 0;
}

bool
LFOHost::setLFO(int which, unsigned char wf, unsigned char reset, bool lock, float rate, float phase)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set lfo %d %d %g %g", which, reset, rate, phase);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].set(wf, reset, lock, rate, phase);
	return true;
}

bool
LFOHost::setNLFO(int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo active %d", n);
	if (n <= lfo.size()) {
		nActiveLFO = n;
		return true;
	} else {
		nActiveLFO = lfo.size();
		return false;
	}
}

bool
LFOHost::setNLFOTarget(int which, int n)
{

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo n target %d %d", which, n);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].nActiveSends = n;
	return true;
}

bool
LFOHost::setLFOAmount(int which, int whichn, float amt)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= lfo[which].target.size()) {
		return false;
	}
	lfo[which].target[whichn].amount = amt;
	return true;
}

bool
LFOHost::setLFOLock(int which, bool lck)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].setRate(lfo[which].rate, lck);
	return true;
}

bool
LFOHost::setLFOReset(int which, unsigned char rst)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].setReset(rst);
	return true;
}
bool
LFOHost::setLFOWave(int which, unsigned char wf)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].setWave(wf);
	return true;
}

bool
LFOHost::setLFORate(int which, float rate)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].setRate(rate, lfo[which].lock);
	return true;
}
bool
LFOHost::setLFOPhase(int which, float phase)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].setPhase(phase);
	return true;
}

int
LFOHost::hasLFO(int tgt)
{
	for (int i=0; i<lfo.size(); i++) {
		for (int j=0; j<lfo[i].target.size(); j++) {
			if (lfo[i].target[j].target == tgt) return i;
		}
	}
	return -1;
}

bool
LFOHost::resetLFO(int which)
{
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	lfo[which].reset();
	return true;
}


float
LFOHost::getLFO(int which)
{
	if (which < 0 || which >= lfo.size()) {
		return 0;
	}
	return *(lfo[which]);
}

