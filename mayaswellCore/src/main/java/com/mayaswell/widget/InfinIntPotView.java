package com.mayaswell.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.mayaswell.R;

public class InfinIntPotView extends ImageView 
{
	public interface InfinIntKnobListener {
		public void onKnobChanged(long v);
	}
	
	private float angle = 0f;
	private float centerAngle=-150f;
	protected long minVal = 0;
	protected long centerVal = 0;
	protected long maxVal = Long.MAX_VALUE;
	private long val = 0;
	private double fval = 0;
	private float theta_old=0f;
 
	private boolean tweak = false;
	
	private InfinIntKnobListener listener=null;
	private long delta360 = 360;
	
	public InfinIntPotView(Context context)
	{
		super(context);
		initialize(context, null);
	}

	public InfinIntPotView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize(context, attrs);
	}

	public InfinIntPotView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initialize(context, attrs);
	}
	
	@Override
	public void setImageResource(int resId)
	{
		super.setImageResource(resId);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm)
	{
		super.setImageBitmap(bm);
	}
	
	@Override
	public void setImageDrawable(Drawable drawable)
	{
		super.setImageDrawable(drawable);
	}
	
	public void setKnobListener(InfinIntKnobListener l )
	{
		listener = l;
	}
	
	public void setCenterAngle(float cTh)
	{
	 	centerAngle = cTh;
	}
	
	public void setValueBounds(long min, long max)
	{
		if (max < min) {
			long tmp = min;
			min = max;
			max = tmp;
		}
		
		minVal = min;
		maxVal = max;
	}
	
	public void setCenterVal(long cval)
	{
		centerVal = cval;
	}
	
    public float getCenterVal()
    {
    	return centerVal;
    }
    
	public void setFullCircleDelta(long d)
	{
		delta360  = d;
	}
	
	public long getMinVal() { return minVal; }
	public long getMaxVal() { return maxVal; }
	
	public void setValue(long v)
	{
//		Log.d("pot", "set "+Long.toString(v));
		if (v < minVal) v = minVal;
		else if (v > maxVal) v = maxVal;
		val = v;
		if (Math.round(fval) != v) {
			fval = v;
		}

//		angle = centerAngle+(360/((float)delta360)*(val-centerVal));
		invalidate();
	}
	
	public long getValue()
	{
		return val;
	}
	
	private float getTheta(float x, float y)
	{
		Drawable d = getDrawable();
		if (d == null) return 0;
		
		float width = d.getMinimumWidth();//r.width();
		float height = d.getMinimumHeight();//r.height();
		
 //	   Log.d("pot", Float.toString(width)+", "+Float.toString(height)+": "+Float.toString(r.left)+", "+Float.toString(r.top));
	  
		float sx = x - (width / 2.0f);
		float sy = y - (height / 2.0f);
		 
		float length = (float)Math.sqrt( sx*sx + sy*sy);
		float nx = sx / length;
		float ny = sy / length;
		float theta = (float)Math.atan2( ny, nx );
		
		final float rad2deg = (float)(180.0/Math.PI);
		float theta2 = theta*rad2deg;
		theta2 = (theta2 < 0) ? theta2 + 360.0f : theta2;
		return theta2;
	}
 
	protected long lastDt=0;
	protected long doubleTapTime=500;

	public boolean isTweaking()
	{
		return tweak;
	}
	
	public void initialize(Context context, AttributeSet attrs)
	{
		TypedArray tpAttrs = null;
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;
		tweak = false;
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
//				Log.d("pot", String.format("first touch %d", val));
				int action = event.getAction();
				int actionCode = action & MotionEvent.ACTION_MASK;
				if (actionCode == MotionEvent.ACTION_POINTER_DOWN ||
						actionCode == MotionEvent.ACTION_DOWN) {
					tweak = true;
					float x = event.getX(0);
					float y = event.getY(0);
					theta_old = getTheta(x, y);
				} else if (actionCode == MotionEvent.ACTION_UP)  {
					tweak = false;
					long dt = event.getDownTime();
					long et = event.getEventTime();
					if (dt - lastDt < doubleTapTime) {
						doDoubleTap();
					}
					lastDt = dt;
				} else if (actionCode == MotionEvent.ACTION_MOVE) {
					invalidate();
				
					float x = event.getX(0);
					float y = event.getY(0);
				
					float theta = getTheta(x,y);
					float delta_theta = theta - theta_old;
				
					theta_old = theta;
				
					int direction = (delta_theta > 0) ? 1 : -1;
					
					angle += 3*direction;
					fval += 3*direction*(((double)delta360)/360.0);
					val = Math.round(fval);
//					Log.d("inint", String.format("%g %g %d %d %d", ((double)delta360)/360.0, fval, val, minVal, maxVal));
					if (val < minVal) fval = val = minVal;
					else if (val > maxVal) fval = val = maxVal;
					
//					Log.d("pot", String.format("dir %d val %d", direction, val));
					notifyListener(getValue());
				}
				return true;
			}

			private void doDoubleTap()
			{
				setValue(centerVal);
				angle = centerAngle;
				invalidate();
				notifyListener(getValue());
			}		
		});
		
		if (attrs != null) {
			tpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TeaPot, 0, 0);
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);
			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					long intBoundsA[] = new long[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						if (s.equals("min")) {
							intBoundsA[i++] = Long.MIN_VALUE;
						} else if (s.equals("max")) {
							intBoundsA[i++]	= Long.MAX_VALUE;
						} else {
							intBoundsA[i++] = Integer.parseInt(s);
						}
					}
					setValueBounds(intBoundsA[0], intBoundsA[intBoundsA.length-1]);
					setCenterVal(intBoundsA[intBoundsA.length-2]);
				}
			}
			int delta360 = mwpAttrs.getInt(R.styleable.TeaPot_delta360, -1);
			if (delta360 > 0) {
				setFullCircleDelta(delta360);
			}
		}

		if (tpAttrs != null) tpAttrs.recycle();
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}

	private void notifyListener(long v)
	{
		if (listener != null)
			listener.onKnobChanged(v);
	}
	
	protected void onDraw(Canvas c)
	{
		Drawable d = getDrawable();
		if (d != null) {
			c.rotate(angle,c.getWidth()/2, c.getHeight()/2);   
		}
		super.onDraw(c);
	}
	
}
