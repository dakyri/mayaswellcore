package com.mayaswell.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ViewAnimator;

public class TabWedgeIt extends RelativeLayout {
	private static final int DEFAULT_BUTTON_WIDTH = 20;
	private static final int DEFAULT_BUTTON_HEIGHT = 60;

	public interface TabWedgeItListener {
		void onTabReselected(int ind, Object d);	//	Called when a tab that is already selected is chosen again by the user.
		void onTabSelected(int ind, Object d);	//	Called when a tab enters the selected state.
		void onTabUnselected(int ind, Object d);	// Called when a tab exits the selected state.	
	}

	public class TabInfo {
		public String label;
		public int index;
		public View view;
		public Button button;
		public Object data;
		public long time = 0;

		public TabInfo(String lbl, int ind, View v, Object d) {
			label = lbl;
			index = ind;
			view = v;
			button = null;
			data = d;
		}
		
	}
	
	private final String schemaName = "http://schemas.android.com/apk/res-auto";

	private int buttonBackgroundResource = -1;
	private ViewGroup controlledGroup = null;
	protected ArrayList<TabInfo> tabs = null;
	
	private TabWedgeItListener listener = null;

	private ViewGroup tabSelect_target = null;
	private int tabSelect_backgroundId = -1;
	private int tabSelect_padding = -1;
	private int tabSelect_targetId = -1;
	private int tabSelect_styleId = -1;

	protected int buttonHeight=LayoutParams.WRAP_CONTENT;
	protected int buttonWidth=LayoutParams.WRAP_CONTENT;
	
	private boolean orientVertical = false;
	private int multiSelect = 1;
	private int maxSelectable = -1;
	
	public TabWedgeIt(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public TabWedgeIt(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public TabWedgeIt(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		buttonWidth = DEFAULT_BUTTON_WIDTH;
		buttonHeight = LayoutParams.MATCH_PARENT;
		
		tabs = new ArrayList<TabInfo>();
		if (attrs != null) {
			setOrientVertical(attrs.getAttributeBooleanValue(schemaName, "tabSelect_orientVertical", false));
			if (isOrientVertical()) {
				buttonWidth = LayoutParams.MATCH_PARENT;
				buttonHeight = DEFAULT_BUTTON_HEIGHT;
			}
			int id = attrs.getAttributeResourceValue(schemaName, "tabSelect_background", -1);
//			Log.d("tab wedgeit", String.format("id %d", id));
			if (id >= 0) {
//				Log.d("tab wedgeit", String.format("id %d", id));
				tabSelect_backgroundId  = id; //getResources().getDrawable(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "tabSelect_target", -1);
			if (id >= 0) {
				try {
					tabSelect_targetId = id;
					setTabSelectTarget((ViewAnimator) ((Activity) context).findViewById(id));
				} catch (Exception e) {
					setTabSelectTarget(null);
				}
			}
			id = attrs.getAttributeResourceValue(schemaName, "tabSelect_padding", -1);
			if (id >= 0) {
				tabSelect_padding = getResources().getDimensionPixelSize(id); //getResources().getDrawable(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "tabSelect_width", -1);
			if (id >= 0) {
				buttonWidth = getResources().getDimensionPixelSize(id); //getResources().getDrawable(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "tabSelect_height", -1);
			if (id >= 0) {
				buttonHeight = getResources().getDimensionPixelSize(id); //getResources().getDrawable(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "tabSelect_style", -1);
			if (id >= 0) {
				tabSelect_styleId  = id; //getResources().getDrawable(id);
			}
			setMultiSelect(attrs.getAttributeIntValue(schemaName, "tabSelect_multiSelect", 1));
		}
	}
		
	public void add(String lbl, int ind, View v)
	{
		add(lbl, ind, v, null);
	}
	
	public void add(String lbl, int ind, View v, Object data)
	{
		int mid = getId()+1;
		for (TabInfo ti:tabs) {
			if (ti.button.getId() >= mid) {
				mid = ti.button.getId()+1;
			}
		}
		TabInfo t = new TabInfo(lbl, ind, v, data);
		tabs.add(ind, t);
		Button newTBut = new Button(getContext());
		t.button = newTBut;
		newTBut.setText(lbl);
		newTBut.setId(mid);
		newTBut.setSingleLine();
		if (tabSelect_styleId >= 0) {
			newTBut.setTextAppearance(getContext(), tabSelect_styleId);
			// but this doesn't set the shadowing!!!! so ....
			TypedArray ta =getContext().obtainStyledAttributes(tabSelect_styleId, new int[] {android.R.attr.shadowColor, android.R.attr.shadowRadius});
			int shadowColor = ta.getColor(0, Color.WHITE);
			float shadowR = ta.getFloat(1, -1);
			ta.recycle();
			// don't know why it's necessary to break up like this, but can't seem to get everything at once....
			ta = getContext().obtainStyledAttributes(tabSelect_styleId, new int[] {android.R.attr.shadowDx, android.R.attr.shadowDy});
			float shadowDx = ta.getFloat(0, 1);
			float shadowDy = ta.getFloat(1, 1);
			ta.recycle();
			if (shadowR >= 0) {
				newTBut.setShadowLayer(shadowR, shadowDx, shadowDy, shadowColor);
			}
		}
		LayoutParams rlpParams = buttonLayoutParams(buttonWidth, buttonHeight, ind);
		rlpParams.height = buttonHeight;
		if (!isOrientVertical()) {
			TextPaint tp = newTBut.getPaint();
			rlpParams.width = (int) (tp.measureText(newTBut.getText().toString()) + newTBut.getPaddingLeft() + newTBut.getPaddingRight());
		} else {
			rlpParams.width = buttonWidth;
		}
		newTBut.setGravity(Gravity.CENTER);
		if (tabSelect_backgroundId >= 0) {
			newTBut.setBackgroundResource(tabSelect_backgroundId);
		}
		if (tabSelect_padding > 0) {
			newTBut.setPadding(tabSelect_padding, tabSelect_padding, tabSelect_padding, tabSelect_padding);
		}
		if (tabSelect_targetId >= 0 && getTabSelectTarget() == null) {
			try {
				setTabSelectTarget((ViewAnimator) ((Activity) getContext()).findViewById(tabSelect_targetId));
			} catch (ClassCastException e) {
				
			}
		}
		addView(newTBut, rlpParams);
		
		adjustNeighbour(ind);

//		if (isOrientVertical()) {
//			newTBut.setHeight(10/*getHeight()/(tabs.size()+1)*/);
//		} else {
//			newTBut.setMaxWidth(getWidth()/(tabs.size()+1));
//		}
		
		newTBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v)
			{
				selectTab(indexOfButton(v));
			}

		});
	}
	
	@Override
	protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		if (changed && isOrientVertical()) {
			int maxw = 0;
			for (TabInfo ti: tabs) {
				Button but = ti.button;
				TextPaint tp = but.getPaint();
				int w = (int) (tp.measureText(but.getText().toString()) + but.getPaddingLeft() + but.getPaddingRight());
				if (w > maxw) maxw = w;
			}
			int h = (getHeight()-getPaddingTop()-getPaddingBottom())/tabs.size();
			if (h > 0) {
				for (TabInfo ti : tabs) {
					RelativeLayout.LayoutParams klp = (LayoutParams) ti.button.getLayoutParams();
					if (h != klp.height || maxw != klp.width) {
						klp.width = maxw;
						klp.height = h;
						ti.button.setLayoutParams(klp);
						ti.button.invalidate();
					}
				}
			}
		}
	}

	
	protected void adjustNeighbour(int ind)
	{
		RelativeLayout.LayoutParams kozParams = null;
		
		TabInfo sx1 = null;
		TabInfo sxi = tabs.get(ind);
		int mid = sxi.button.getId();
		if (ind < tabs.size()-1) {
			sx1 = tabs.get(ind+1);
			kozParams = new RelativeLayout.LayoutParams(
					buttonWidth,
					buttonHeight);
			if (isOrientVertical()) {
				kozParams.addRule(RelativeLayout.BELOW, mid);
			} else {
				kozParams.addRule(RelativeLayout.RIGHT_OF, mid);
			}
		}	
		if (sx1 != null && kozParams != null) {
			sx1.button.setLayoutParams(kozParams);
		}
		
//		Log.d("tabwidge", String.format("layout %d %d",buttonWidth, buttonHeight));
	}

	protected LayoutParams buttonLayoutParams(int w, int h, int ind)
	{
		TabInfo sx0;
		LayoutParams rl = new LayoutParams(w, h);
		if (isOrientVertical()) {
			if (ind <= 0) {
				rl.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			} else {
				sx0 = tabs.get(ind-1);
				rl.addRule(RelativeLayout.BELOW, sx0.button.getId());
			}
		} else {
			if (ind <= 0) {
				rl.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			} else {
				sx0 = tabs.get(ind-1);
				rl.addRule(RelativeLayout.RIGHT_OF, sx0.button.getId());
			}
		}
//		Log.d("tabwidge", String.format("%d %d", w, h));
		return rl;
	}

	public int indexOfButton(View v)
	{
		TabInfo bi = infoOfButton(v);
		if (bi != null) {
			return bi.index;
		}
		return -1;
	}
	
	public TabInfo infoOfButton(View v)
	{
		for (TabInfo bi: tabs) {
			if (bi.button == v) {
				return bi;
			}
		}
		return null;
	}
	
	public int countSelected()
	{
		int count=0;
		for (TabInfo bi: tabs) {
			if (bi.button.isSelected()) {
				count++;
			}
		}
		return count;
	}
	
	public TabInfo getSelected(int i)
	{
		for (TabInfo bi: tabs) {
			if (bi.button.isSelected()) {
				if (i-- <= 0) {
					return bi;
				}
			}
		}
		return null;
	}

	public boolean isTabSelected(int i)
	{
		for (TabInfo bi: tabs) {
			if (bi.index == i && bi.button.isSelected()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean selectTab(int ind)
	{
		TabInfo ti = null;
		ArrayList<TabInfo> ta = new ArrayList<TabInfo>();
		for (TabInfo bi: tabs) {
			if (bi.index != ind) {
				if (bi.button.isSelected()) {
					ta.add(bi);
				}
			} else {
				if (bi.button.isSelected()) {
					if (multiSelect != 1) {
						bi.button.setSelected(false);
						if (listener != null) {
							listener.onTabUnselected(bi.index, bi.data);
						}
						return true;
					} else if (listener != null) {
						listener.onTabReselected(bi.index, bi.data);
					}
				}
				ti = bi;
			}
		}
		if (ta.size() > 0) {
			Collections.sort(ta, new Comparator<TabInfo>() {
				@Override
				public int compare(TabInfo lhs, TabInfo rhs) {
					return (int)(lhs.time-rhs.time);
				}
			});
			int nDeselect = ta.size() - multiSelect + 1;
			for (int i=0; i<nDeselect; i++) {
				TabInfo bi = ta.get(i);
				if (listener != null) {
					listener.onTabUnselected(bi.index, bi.data);
				}
				bi.button.setSelected(false);
			}
		}
		if (ti == null) {
			return false;
		}
		displaySelectedChild(ti);
		if (listener != null) {
			listener.onTabSelected(ti.index, ti.data);
		}
		ti.button.setSelected(true);
		ti.time = System.currentTimeMillis();
		return true;
	}

	private void displaySelectedChild(TabInfo ti) {
		if (ti.view != null && getTabSelectTarget() != null) {
			try {
				((ViewAnimator) getTabSelectTarget()).setDisplayedChild(getTabSelectTarget().indexOfChild(ti.view));
			} catch (ClassCastException e) {
				
			}
		}
	}

	public void clear()
	{
		for (TabInfo bi: tabs) {
			removeView(bi.button);
		}
		tabs.clear();
	}

	public void setOrientVertical(boolean ov)
	{
		orientVertical = ov;
	}
	
	public boolean getOrientVertical()
	{
		return orientVertical;
	}
	
	public void setMultiSelect(int ov)
	{
		multiSelect = ov;
	}
	
	public void setListener(TabWedgeItListener l)
	{
		listener = l;
	}
	
	protected int isMultiSelect() {
		return multiSelect;
	}

	protected boolean isOrientVertical() {
		return orientVertical;
	}

	public ViewGroup getTabSelectTarget() {
		return tabSelect_target;
	}

	public void setTabSelectTarget(ViewGroup tabSelect_target) {
		this.tabSelect_target = tabSelect_target;
	}
}
