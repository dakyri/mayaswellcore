package com.mayaswell.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mayaswell.R;

public class PlayButton extends MultiModeButton {

	private static final int[] STATE_PLAYING = {R.attr.state_playing};
	private static final int[] STATE_PAUSED = {R.attr.state_primed};
	private boolean isPaused = false;
	private boolean isPlaying = false;
	private boolean isRecording = false;
	public int id = 0;
	final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
	final float textScaledPx = 10 * densityMultiplier;
	private Paint textBrush = null;
	private Paint positionBrush;
	protected Rect vRect = null;
	protected Rect pdRect = null;

	public PlayButton(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public PlayButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public PlayButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	protected void setup(Context context, AttributeSet attrs, int defStyle) {
		vRect = new Rect();
		pdRect = new Rect();
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(0xff000000);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		positionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		positionBrush.setColor(getResources().getColor(R.color.padLightYellow));
		positionBrush.setStyle(Style.STROKE);
		positionBrush.setStrokeWidth(4);
		if (attrs != null) {
			int pn = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/com.mayaswell.spacegun", "padNumber", 0);
			if (pn > 0) {
				id = pn;
			}
		}
	}

	/**
	 * @param canvas
	 */
	@Override
	protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			getLocalVisibleRect(vRect);
			vRect.offset(0, 0);
			pdRect.set(vRect.left+3, (int)(vRect.top+textScaledPx), vRect.right-3, (int) (vRect.top+textScaledPx+0.5*textScaledPx));
	//		Log.d("pad button", String.format("pdr %d %d %d %d", pdRect.left, pdRect.top, pdRect.right, pdRect.bottom));
	//		Log.d("pad button", String.format("vr %d %d %d %d", vRect.left, vRect.top, vRect.right, vRect.bottom));
	/*		if (padSample != null) {
	//			Log.d("draw button", String.format("playing %b", padSample.isPlaying()));
				if (padSample.isPlaying()) {
					float lp = pdRect.left;
					float rp = pdRect.left+padSample.getCurrentPosition()*(pdRect.right-pdRect.left);
					float mp = (pdRect.bottom+pdRect.top)/2;
	//				canvas.drawLine(lp, pdRect.top, lp, pdRect.bottom, positionBrush);
	//				canvas.drawLine(rp, pdRect.top, rp, pdRect.bottom, positionBrush);
					canvas.drawLine(lp, mp, rp, mp, positionBrush);
				}
				PadSampleState pss = padSample.getState();
				if (pss.name != null) {
					canvas.drawText(pss.name,(vRect.right+vRect.left)/2,vRect.bottom-labelHeightPx, textBrush);
				}
			}
	*/
		}

	/** 
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
			final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
	//		Log.d("draw button", "merge drawable "+Integer.toString(id)+", "+Boolean.toString(isPlaying));
			if (isPaused) {
				mergeDrawableStates(drawableState, STATE_PAUSED);
			}
			if (isPlaying) {
				mergeDrawableStates(drawableState, STATE_PLAYING);
			}
	//		if (isRecording) {
	//			mergeDrawableStates(drawableState, STATE_RECORDING);
	//		}
			return drawableState;
		}

	@Override
	protected boolean onTouchEvent(View v, MotionEvent me) {
	    int action = me.getAction();
	    if(action == MotionEvent.ACTION_DOWN) {
	    	if (!isPlaying) {
	    		dispatchStateChange(ACTIVATED);
	    		isPlaying = true;
	    	} else if (isPlaying) {
	    		dispatchStateChange(DEACTIVATED);
	    		isPlaying = false;
	    	}
	    }
		return super.onTouchEvent(me);
	}

	/** 
	 */
	@Override
	protected void drawableStateChanged() {
			super.drawableStateChanged();
	//		Log.d("pad button", String.format("state change %d", getDrawableState()[0])); 
			invalidate(pdRect);
		}

	public Rect getPositionDisplayRect() {
		return pdRect;
	}

	/**
	 * @param s
	 */
	public void setPlayState(boolean s) {
		isPlaying = s;
		isPaused = false;
		refreshDrawableState();
	}

	/**
	 * @param s
	 */
	public void setPausedState(boolean s) {
		isPaused = s;
		isPlaying = false;
		refreshDrawableState();
	}

	/**
	 * @param s
	 */
	public void setRecordState(boolean s) {
		isRecording = s;
		refreshDrawableState();
	}

}