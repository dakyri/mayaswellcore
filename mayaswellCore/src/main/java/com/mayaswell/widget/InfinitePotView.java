package com.mayaswell.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.mayaswell.R;

/**
 *  floating point variation of the InfinIntPot
 * @author dak
 *
 */
public class InfinitePotView extends ImageView 
{
    public interface InfiniteKnobListener {
    	public void onKnobChanged(float v);
    }
    
    private float angle = 0f;
	private float centerAngle=-150f;
	protected float minVal = 0;
	protected float centerVal = 0;
	protected float maxVal = Float.MAX_VALUE;
	private float val = 0;
    private float theta_old=0f;
 
    private boolean tweak = false;
    
    private InfiniteKnobListener listener=null;
	private float delta360 = 360;
    
    public InfinitePotView(Context context)
    {
		super(context);
		initialize(context, null);
	}

	public InfinitePotView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize(context, attrs);
	}

	public InfinitePotView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initialize(context, attrs);
	}
	
	@Override
	public void setImageResource(int resId)
	{
		super.setImageResource(resId);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm)
	{
		super.setImageBitmap(bm);
	}
	
	@Override
	public void setImageDrawable(Drawable drawable)
	{
		super.setImageDrawable(drawable);
	}
	
    public void setKnobListener(InfiniteKnobListener l )
    {
    	listener = l;
    }
    
    public void setCenterAngle(float cTh)
    {
     	centerAngle = cTh;
    }
    
    public void setValueBounds(float fBoundsA, float fBoundsA2)
    {
    	if (fBoundsA2 < fBoundsA) {
    		float tmp = fBoundsA;
    		fBoundsA = fBoundsA2;
    		fBoundsA2 = tmp;
    	}
    	
    	minVal = fBoundsA;
    	maxVal = fBoundsA2;
    }
    
    public void setCenterVal(float fBoundsA)
    {
    	centerVal = fBoundsA;
    }
    
    public float getCenterVal()
    {
    	return centerVal;
    }
    
    public void setFullCircleDelta(float f)
    {
    	delta360  = f;
    }
    
    public float getMinVal() { return minVal; }
    public float getMaxVal() { return maxVal; }
    
    public void setValue(float v)
    {
//    	Log.d("pot", "set "+Long.toString(v));
    	if (v < minVal) v = minVal;
    	else if (v > maxVal) v = maxVal;
    	val = v;
   	
//    	angle = centerAngle+(360/((float)delta360)*(val-centerVal));
    	invalidate();
    }
    
    public float getValue()
    {
    	return val;
    }
    
    private float getTheta(float x, float y)
	{
		Drawable d = getDrawable();
		if (d == null) return 0;
		
        float width = d.getMinimumWidth();//r.width();
        float height = d.getMinimumHeight();//r.height();
        
 //       Log.d("pot", Float.toString(width)+", "+Float.toString(height)+": "+Float.toString(r.left)+", "+Float.toString(r.top));
      
		float sx = x - (width / 2.0f);
		float sy = y - (height / 2.0f);
		 
		float length = (float)Math.sqrt( sx*sx + sy*sy);
		float nx = sx / length;
		float ny = sy / length;
		float theta = (float)Math.atan2( ny, nx );
		
		final float rad2deg = (float)(180.0/Math.PI);
		float theta2 = theta*rad2deg;
		theta2 = (theta2 < 0) ? theta2 + 360.0f : theta2;
		return theta2;
	}
 
    protected long lastDt=0;
    protected long doubleTapTime=500;

	public boolean isTweaking()
	{
		return tweak;
	}
	
    private final String schemaName = "http://schemas.android.com/apk/res/com.mayaswell";
	public void initialize(Context context, AttributeSet attrs)
	{
		TypedArray tpAttrs = null;
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;

		tweak = false;
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
//				Log.d("pot", String.format("first touch %d", val));
				int action = event.getAction();
				int actionCode = action & MotionEvent.ACTION_MASK;
				if (actionCode == MotionEvent.ACTION_POINTER_DOWN ||
						actionCode == MotionEvent.ACTION_DOWN) {
					tweak = true;
					float x = event.getX(0);
					float y = event.getY(0);
					theta_old = getTheta(x, y);
				} else if (actionCode == MotionEvent.ACTION_UP)  {
					tweak = false;
					long dt = event.getDownTime();
					long et = event.getEventTime();
					if (dt - lastDt < doubleTapTime) {
						doDoubleTap();
					}
					lastDt = dt;
				} else if (actionCode == MotionEvent.ACTION_MOVE) {
					invalidate();
				
					float x = event.getX(0);
					float y = event.getY(0);
				
					float theta = getTheta(x,y);
					float delta_theta = theta - theta_old;
				
					theta_old = theta;
				
					int direction = (delta_theta > 0) ? 1 : -1;
					
					angle += 3*direction;
					val += 3*direction*(((float)delta360)/360.0);
					if (val < minVal) val = minVal;
					else if (val > maxVal) val = maxVal;
					
					notifyListener(getValue());
				}
				return true;
			}

			private void doDoubleTap()
			{
				setValue(centerVal);
				angle = centerAngle;
				invalidate();
				notifyListener(getValue());
			}		
		});
		if (attrs != null) {
			tpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TeaPot, 0, 0);
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);

			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					float fBoundsA[] = new float[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						if (s.equals("min")) {
							fBoundsA[i++] = Float.MIN_VALUE;
						} else if (s.equals("max")) {
							fBoundsA[i++]	= Float.MAX_VALUE;
						} else {
							fBoundsA[i++] = Integer.parseInt(s);
						}
					}
					setValueBounds(fBoundsA[0], fBoundsA[fBoundsA.length-1]);
					setCenterVal(fBoundsA[fBoundsA.length-2]);
				}
			}
			String delta360 = attrs.getAttributeValue(schemaName, "delta360");
			if (delta360 != null) {
				setFullCircleDelta(Float.parseFloat(delta360));
			}
		}
		if (tpAttrs != null) tpAttrs.recycle();
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}

	private void notifyListener(float v)
	{
		if (listener != null)
			listener.onKnobChanged(v);
	}
	
	protected void onDraw(Canvas c)
	{
		Drawable d = getDrawable();
		if (d != null) {
			c.rotate(angle,c.getWidth()/2, c.getHeight()/2);   
		}
		super.onDraw(c);
	}
    
}
