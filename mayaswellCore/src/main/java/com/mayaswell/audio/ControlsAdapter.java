package com.mayaswell.audio;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

public class ControlsAdapter
{
	public void setDropDownViewResource(int ddrid) {
		adapter.setDropDownViewResource(ddrid);
	}

	public int getCount() {
		return adapter.getCount();
	}

	public Control getItem(int i) {
		return adapter.getItem(i);
	}

	public ArrayAdapter<Control> getAdapter() {
		return adapter;
	}

	public int getControlIndex4Id(int xpid) {
		int xpi = -1;
		for (int i=0; i<adapter.getCount(); i++) {
			Control cci =adapter.getItem(i);
			if (cci.ccontrolId() == xpid) {
				xpi = i;
			}
		}
		return xpi;
	}

	public interface ControlsFilter
	{
		 boolean isAvailable(Control c);
	}

	ArrayAdapter<Control> adapter;
	public ControlsAdapter(Context context, int resource) {
		adapter = new ArrayAdapter<Control>(context, resource);
	}

	public ControlsAdapter(Context context, int resource, int textViewResourceId) {
		adapter = new ArrayAdapter<Control>(context, resource, textViewResourceId);
	}

	public ControlsAdapter(Context context, int resource, Control[] objects) {
		adapter = new ArrayAdapter<Control>(context, resource, objects);
	}

	public ControlsAdapter(Context context, int textViewResourceId, List<Control> objects) {
		adapter = new ArrayAdapter<Control>(context, textViewResourceId, objects);
	}

	public ControlsAdapter(Context context, int resource, int textViewResourceId, Control[] objects) {
		adapter = new ArrayAdapter<Control>(context, resource, textViewResourceId, objects);
	}

	public ControlsAdapter(Context context, int resource,
			int textViewResourceId, List<Control> objects) {
		adapter = new ArrayAdapter<Control>(context, resource, textViewResourceId, objects);
	}
	
	private ArrayList<Control> masterList = new ArrayList<Control>();
	private ControlsFilter filter = null;
	
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#add(java.lang.Object)
	 */
	public void add(Control c)
	{
		adapter.add(c);
		masterList.add(c);
	}
	
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#clear()
	 */
//	@Override
	public void clear()
	{
		adapter.clear();
		masterList.clear();
	}
	
	public void setFilter(ControlsFilter c)
	{
		filter  = c;
	}

	public void refilter()
	{
		adapter.clear();
		for (Control c: masterList) {
//			Log.d("master", String.format("%d being checked", c.getTypeCode()));
			if (filter == null || filter.isAvailable(c)) {
//				Log.d("master", String.format("parameter %d now visible", c.getTypeCode()));
				adapter.add(c);
			}
		}
		adapter.notifyDataSetChanged();
	}
	
	public Control findCurrentControl(String nm, int it) {
		for (int i = 0; i< adapter.getCount(); i++) {
			Control cc = adapter.getItem(i);
			if (cc.ccontrolName().equals(nm)) {
				return cc;
			}
		}
		return null;
	}
	
	public Control findControl(String nm, int it) {
		for (Control cc: masterList) {
			if (cc.ccontrolName().equals(nm)) {
				return cc;
			}
		}
		return null;
	}
}
