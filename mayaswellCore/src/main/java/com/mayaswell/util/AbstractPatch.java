package com.mayaswell.util;

import java.util.ArrayList;

public abstract class AbstractPatch extends NamedPatch {

	public static class Action {
		public static final int NOP = -1;
		public static final int NEW = -2;
		public static final int CLONE = -3;
		public static final int BRANCH = -4;
		public static final int DELETE = -5;
		public static final int RENAME = -6;
		public static final int CLEAR = -7;
		public static final int SAVE = -8;
		public static final int RELOAD = -9;
		
		private int op;
		private String lbl;
	
		public Action(int v, String s) { op = v; lbl = s;}
		public int getOp() { return op; }
		
		public String toString()
		{
			return lbl;
		}
	}

	public AbstractPatch() {
		super();
	}
	
	public boolean getMissingFiles(ArrayList<String> fl) {
		return false;
	}
	
	public boolean fixFilePath(String oldPath, String newPath) {
		return false;
	}
}