package com.mayaswell.widget;

/**
 * @author dak
 *  simple generic class bass to use in array adapter for spinners that will give a list of enums 
 * @param <T> the enum type ..
 */
public class MenuData<T> {
	private String str;
	private T mode;
	private int data;

	public MenuData(T pm, String nm) {
		this(pm, 0, nm);
	}
	
	public MenuData(T pm, int d, String nm) {
		setMode(pm);
		str = nm;
		setData(d);
	}
	
	public MenuData(T pm) {
		setMode(pm);
		str = pm.toString();
	}
	
	public MenuData(T pm, int d) {
		this(pm);
		setData(d);
	}
	
	public String toString()
	{
		return str;
	}

	public T getMode() {
		return mode;
	}

	public void setMode(T mode) {
		this.mode = mode;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}
}