package com.mayaswell.widget;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Controllable.XYControlAssign;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.R;
import com.mayaswell.util.MWBPActivity;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * A composite selector widget that controls the parameters for a 2D parameter mapping. It
 * provides spinner for the Controllable, and 2 Control-s
 */
public class PadXYMenuControl extends BaseControl {
    public interface PadXYMenuControlListener {
    	public void onValueChanged(PadXYMenuControl px, Controllable cp, Control xp, Control yp);
    }

    private final String schemaName = "http://schemas.android.com/apk/res-auto";
	
	private Spinner padSelectSpinner = null;
	private Spinner xParamSpinner = null;
	private Spinner yParamSpinner = null;
	
    private PadXYMenuControlListener listener=null;

	private XYControlAssign controlAssign=null;
    
	public PadXYMenuControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public PadXYMenuControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public PadXYMenuControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_padxy_select_control, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		padSelectSpinner = (Spinner)findViewById (R.id.mwCtlSpinner);
		xParamSpinner = (Spinner)findViewById (R.id.mwCtlSpinnerX);
		yParamSpinner = (Spinner)findViewById (R.id.mwCtlSpinnerY);
		
		ArrayAdapter<ControllableInfo> cca = null;
		try {
			MWBPActivity<?,?,?> mwbp = (MWBPActivity<?,?,?>) getContext();
			cca = mwbp.getControllableAdapter();
		} catch (ClassCastException e) {
			
		}
		final ArrayAdapter<ControllableInfo> controllableAdapter = cca;
		if (controllableAdapter == null) {
			Log.d("map x", "No controllable adapter");
			return;
		}
		padSelectSpinner.setAdapter(controllableAdapter);
		
		padSelectSpinner.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
		    {
//		    	Log.d("padSelectSpinner", String.format("item sel %d %d", position, id));
	    		setControllable(position);
		    	ControllableInfo cip = controllableAdapter.getItem(position);
		    	ControlsAdapter cp = cip.getInterfaceTgtAdapter();
				if (cp != null) {
					int yp = yParamSpinner.getSelectedItemPosition();
					int xp = xParamSpinner.getSelectedItemPosition();
					Control yc = null;
					Control xc = null;
					if (xp >= 0 && xp < cp.getCount()) xc = cp.getItem(xp);
					if (yp >= 0 && yp < cp.getCount()) yc = cp.getItem(yp);
					
					if (xc != null && yc != null) {
//				    	Log.d("padSelectSpinner", String.format("got items %s %s", xc.abbrevName(), yc.abbrevName()));
						if (xc.ccontrolId() == yc.ccontrolId() && position != 0) {
							xParamSpinner.setSelection(0);
						} else {
							notifyListener(cip.getControllable(), xc, yc);
						}
					} else {
//				    	Log.d("padSelectSpinner", String.format("not got items"));
					}
				}
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) 
		    {
		    }
		});
		
		xParamSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
			{
				int cpos = padSelectSpinner.getSelectedItemPosition();
				ControllableInfo cip = controllableAdapter.getItem(cpos);
				ControlsAdapter cp = cip.getInterfaceTgtAdapter();
				if (cp != null) {
					Control yc = cp.getItem(yParamSpinner.getSelectedItemPosition());
					Control xc = cp.getItem(position);
					if (xc.ccontrolId() == yc.ccontrolId() && position != 0) {
						xParamSpinner.setSelection(0);
					} else {
						notifyListener(cip.getControllable(), xc, yc);
					}
				}
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		});

		yParamSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
			{
				int cpos = padSelectSpinner.getSelectedItemPosition();
				ControllableInfo cip = controllableAdapter.getItem(cpos);
				ControlsAdapter cp = cip.getInterfaceTgtAdapter();
				if (cp != null) {
					Control xc = cp.getItem(xParamSpinner.getSelectedItemPosition());
					Control yc = cp.getItem(position);
					if (xc.ccontrolId() == yc.ccontrolId() && position != 0) {
						yParamSpinner.setSelection(0);
					} else {
						notifyListener(cip.getControllable(), xc, yc);
					}
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		});

		label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (getDisplayMode() == DisplayMode.SHOW_NAME) {
					setDisplay(DisplayMode.SHOW_VALUE_CTLS);
				} else {
					setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		});
		
		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			String lbl = attrs.getAttributeValue(schemaName, "label");
			if (lbl != null) {
				label.setText(lbl);
			}
			int px = -1;
			int lwr = attrs.getAttributeResourceValue(schemaName, "labelWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String labelWidth = attrs.getAttributeValue(schemaName, "labelWidth");
				if (labelWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setLabelWidth(px);
			}
			
			int vwr = attrs.getAttributeResourceValue(schemaName, "targetSpinnerWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "targetSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setTargetSelectWidth(px);
			}
			
			int wwr = attrs.getAttributeResourceValue(schemaName, "paramSpinnerWidth", -1);
			if (wwr >= 0) {
				px = getResources().getDimensionPixelSize(wwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "paramSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setParamSelectWidth(px);
			}
		}
		setDisplay(DisplayMode.SHOW_VALUE_CTLS);
	}
	
	public void setDisplay(DisplayMode dm)
	{
		displayMode = dm;

		switch (displayMode) {
		case SHOW_NAME:
			padSelectSpinner.setVisibility(GONE);
			xParamSpinner.setVisibility(GONE);
			yParamSpinner.setVisibility(GONE);
			break;
		case SHOW_VALUE_CTLS:
			padSelectSpinner.setVisibility(VISIBLE);
			xParamSpinner.setVisibility(VISIBLE);
			yParamSpinner.setVisibility(VISIBLE);
			break;
		}
		SubPanelFragment.layoutEditorControlParent(this);
	}
	
	public void setLabelWidth(int px)
	{
		LayoutParams params = (LayoutParams) label.getLayoutParams();
		params.width = px;
	}
	
	public void setTargetSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) padSelectSpinner.getLayoutParams();
		params.width = px;
	}
	
	public void setParamSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) xParamSpinner.getLayoutParams();
		params.width = px;
		params = (LayoutParams) yParamSpinner.getLayoutParams();
		params.width = px;
	}

	/**
	 * set the controllable selecter to the given position, adjusting the two Control spinners x- & yParamSpinnger accordingly
	 * @param position
	 */
	public void setControllable(int position)
	{
		int xpi = xParamSpinner.getSelectedItemPosition();
		int ypi = yParamSpinner.getSelectedItemPosition();
//		Log.d("setting", String.format("set controllable %d pos %d selected pos", position, xpi, ypi));

		ArrayAdapter<Control> currentParamAdapter = (ArrayAdapter<Control>) xParamSpinner.getAdapter();
		int xpiv = 0;
		int ypiv = 0;
		if (currentParamAdapter != null) {
			if (xpi < currentParamAdapter.getCount()) {
				xpiv = currentParamAdapter.getItem(xpi).ccontrolId();
			}
			if (ypi < currentParamAdapter.getCount()) {
				ypiv = currentParamAdapter.getItem(ypi).ccontrolId();
			}
		}
//		Log.d("setting", String.format("set controllable %d pos %d selected pos", position, xpiv, ypiv));
		
		ArrayAdapter<ControllableInfo> controllableAdapter = null;
		try {
			MWBPActivity<?,?,?> mwbp = (MWBPActivity<?,?,?>) getContext();
			controllableAdapter = mwbp.getControllableAdapter();
		} catch (ClassCastException e) {
			
		}
		if (controllableAdapter == null) {
			Log.d("map x", "No controllable adapter");
			return;
		}
		ControllableInfo cip = controllableAdapter.getItem(position);
		ControlsAdapter newParamAdapter = cip.getInterfaceTgtAdapter();
//		newParamAdapter.refilter();
		xpi = newParamAdapter.getControlIndex4Id(xpiv);
		ypi = newParamAdapter.getControlIndex4Id(ypiv);
		xParamSpinner.setAdapter(newParamAdapter.getAdapter());
		yParamSpinner.setAdapter(newParamAdapter.getAdapter());
		if (xpi >= 0) xParamSpinner.setSelection(xpi);
		if (ypi >= 0) yParamSpinner.setSelection(ypi);
	}

	public void setValueListener(PadXYMenuControlListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(Controllable cp, Control xp, Control yp)
	{
		if (listener != null)
			listener.onValueChanged(this, cp, xp, yp);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param ppos
	 * @param xpos
	 * @param ypos
	 *
	 */
	public void setSelection(int ppos, int xpos, int ypos)
	{
		padSelectSpinner.setSelection(ppos);
		setControllable(ppos);
		xParamSpinner.setSelection(xpos);
		yParamSpinner.setSelection(ypos);
//		Log.d("setting", Integer.toString(ppos)+", "+Integer.toString(xParamSpinner.getSelectedItemPosition())+", "+Integer.toString(yParamSpinner.getSelectedItemPosition()));
	}
	
	public boolean setXYControlAssign(XYControlAssign xsp)
	{
		if (xsp == null) {
			return false;
		}
		if (xsp.controllable != null) {
			ControlsAdapter aac = null;
			ArrayAdapter<ControllableInfo> controllableAdapter = null;
			try {
				MWBPActivity<?,?,?> mwbp = (MWBPActivity<?,?,?>) getContext();
				controllableAdapter = mwbp.getControllableAdapter();
			} catch (ClassCastException e) {
				
			}
			if (controllableAdapter == null) {
				Log.d("map x", "No controllable adapter");
				return false;
			}
			for (int i=0; i<controllableAdapter.getCount(); i++) {
				ControllableInfo ci = controllableAdapter.getItem(i);
				if (ci.getControllable() == xsp.controllable) {
					padSelectSpinner.setSelection(i);
					aac = ci.getInterfaceTgtAdapter();
				}
			}
			if (aac != null) {
				int xpi = aac.getControlIndex4Id(xsp.xp.ccontrolId());
				int ypi = aac.getControlIndex4Id(xsp.yp.ccontrolId());
				xParamSpinner.setAdapter(aac.getAdapter());
				yParamSpinner.setAdapter(aac.getAdapter());
				xParamSpinner.setSelection(xpi);
				yParamSpinner.setSelection(ypi);
			}
		}
		return true;
	}
	
	public void clearAssignment()
	{
		xParamSpinner.setSelection(0);
		yParamSpinner.setSelection(0);
//		Log.d("setting", String.format("cleared %s", label.getText()));
	}

	public boolean inUse()
	{
		return xParamSpinner.getSelectedItemPosition() > 0 || yParamSpinner.getSelectedItemPosition() > 0;
	}
	
	public void setControlAssign(XYControlAssign xyc)
	{
		controlAssign  = xyc;
	}
	
	public XYControlAssign getControlAssign()
	{
		return controlAssign;
	}

}
