package com.mayaswell.fragment;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.R;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.DisplayMode;
import com.mayaswell.widget.EnvelopeControl;
import com.mayaswell.widget.LFOControl;
import com.mayaswell.widget.EnvelopeControl.EnvelopeControlListener;
import com.mayaswell.widget.LFOControl.LFOControlListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

public class ModControlsFragment<A extends MWBPActivity<?, ?, ?>> extends SubPanelFragment {

	private ArrayList<LFOControl> lfoControl = null;
	private ArrayList<EnvelopeControl> envControl = null;
	
	LFO.Host lfoHost = null;
	Envelope.Host envHost = null;
	protected A mwbp = null;
	
	public ModControlsFragment()
	{
		lfoControl = new ArrayList<LFOControl>();
		envControl = new ArrayList<EnvelopeControl>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onAttach(Activity activity)
	{
		try {
			mwbp = (A) activity;
		} catch (ClassCastException e) {
			Log.d("mod fragment", "not in an mwbp activity");
		}
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		mwbp = null;
		super.onDetach();
	}

	@Override
	public void onHiddenChanged(boolean h)
	{
		Log.d("mod fragment", "hidden is now "+h);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d("ModControlsFragment", "onCreateView");
		View v = inflater.inflate(R.layout.mod_controls_layout, container, true);
		registerForContextMenu(v);
		if (mwbp != null) { // lfoHost and envHost may actually be null, or have values if set asynchronously
			setLFOHost(lfoHost);
			setEnvHost(envHost);
		}
		Log.d("ModControlsFragment", "onCreateView got view "+(v!=null?"true":"false"));
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		populateInitial();
	}

	public void populateInitial() {
		populateInitial(getView());
	}
	protected void populateInitial(View v) {
		if (mwbp == null) {
			return;
		}
		LFOControl lc = addLFOControl(lfoControl.size(), mwbp.getLfoRstModeAdapter(lfoHost), mwbp.getLfoWaveAdapter(lfoHost), mwbp!=null?mwbp.alwaysTempoLock():false, lfoControl);
		EnvelopeControl ec = addEnvControl(envControl.size(), mwbp.getEnvRstModeAdapter(envHost), mwbp!=null?mwbp.alwaysTempoLock():false, envControl);
		if (lfoHost != null) {
			setLFOEditor(lfoHost.lfos());
		}
		if (envHost != null) {
			setEnvEditor(envHost.envs());
		}
		v.requestLayout();
	}
	
	@Override
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		/*
		getView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		layoutEditorControls();
		
		Log.d("mod fragment", "yeah "+getView().getMeasuredWidth()+", "+getView().getMeasuredHeight()+", "+getView().getX() + ", "+getView().getY());
		getView().setVisibility(View.VISIBLE);
		getView().invalidate();
		*/
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getActivity().getMenuInflater();
	    inflater.inflate(R.menu.mod_fragment_context_menu, menu);
	}
	
	@Override
	public void onPrepareOptionsMenu (Menu menu)
	{
		MenuItem miModClr = menu.findItem(R.id.sg_menu_clear_unused_mod);
		MenuItem miModLFO = menu.findItem(R.id.sg_menu_add_lfo_ctrl);
		MenuItem miModEnv = menu.findItem(R.id.sg_menu_add_env_ctrl);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.sg_menu_add_lfo_ctrl) {
			newLFOControl(-1);
			return true;
		}
		if (item.getItemId() == R.id.sg_menu_add_env_ctrl) {
			newEnvControl(-1);
			return true;
		}
		if (item.getItemId() ==  R.id.sg_menu_clear_unused_mod) {
			minimizeUninterestingModulators(null, true, true);
			return true;
		}
		if (item.getItemId() == R.id.sg_menu_mod_help) {
			if (mwbp == null) {
				return false;
			}
			Builder d = new AlertDialog.Builder(getActivity());
			d.setTitle(getResources().getString(R.string.help_title_mod_edit));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_mod_edit)));
			d.setPositiveButton(mwbp.aboutButtonText(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			});
			d.show(); 
			return true;
		}
		return false;
	}
	
	public void setLFOListener(LFOControl p, final int xyci)
	{
		p.setListener(new LFOControlListener() {

			@Override
			public void onRateChanged(LFO l, float amt)
			{
				if (l != null) {
					l.rate = amt;
					if (lfoHost != null) {
						lfoHost.setLFORate(xyci, amt);
						mwbp.ccontrolUpdate(lfoHost, mwbp.lfoRateId(xyci), amt, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onPhaseChanged(LFO l, float amt)
			{
				if (l != null) {
					l.phase = amt;
					if (lfoHost != null) {
						lfoHost.setLFOPhase(xyci, amt);
						mwbp.ccontrolUpdate(lfoHost, mwbp.lfoPhaseId(xyci), amt, Controllable.Controller.INTERFACE);
					}
				}
			}
			
			@Override
			public void onResetChanged(LFO l, ResetMode sip)
			{
				if (l != null) {
					l.trigger = sip;
					if (lfoHost != null) {
						lfoHost.setLFOReset(xyci, sip);
					}
				}
			}

			@Override
			public void onWaveChanged(LFO l, LFWave sip)
			{
				if (l != null) {
					l.waveform = sip;
					if (lfoHost != null) {
						lfoHost.setLFOWave(xyci, sip);
					}
				}
			}

			@Override
			public void onTempoLockChanged(LFO l, boolean isChecked)
			{
				if (l != null) {
					l.tempoLock = isChecked;
					if (lfoHost != null) {
						lfoHost.setLFOLock(xyci, isChecked);
					}
				}
			}

			@Override
			public void onTargetValueChanged(LFO l, int targetId, float amt)
			{
				if (l != null && lfoHost != null && targetId < lfoHost.maxLFOTgt()) {
					while (targetId >= l.target.size()) {
						l.addTarget();
					}
					l.target.get(targetId).amount = amt;
					if (lfoHost != null) {
						lfoHost.setLFOTarget(xyci, targetId, l.target.get(targetId).target, amt);
						mwbp.ccontrolUpdate(lfoHost, mwbp.lfoTargetDepthId(xyci, targetId), amt, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onTargetControlChanged(LFOControl lc, LFO l, int targetId, Control tgt, float amt)
			{
				if (l != null && lfoHost != null && targetId < lfoHost.maxLFOTgt()) {
					while (targetId >= l.target.size()) {
						l.addTarget();
					}
					if (targetId >= lc.countTargetControl()-1 && tgt.ccontrolId() != Control.NOTHING
							&& (lfoHost == null || targetId < lfoHost.maxLFOTgt()-1)) {
						lc.addTargetControl(targetId+1);
					}
					l.target.get(targetId).target = tgt;
					l.target.get(targetId).amount = amt;
					int nValidTgt = 0;
					for (int i=0; i<l.target.size(); i++) {
						ICControl ti = l.target.get(i).target;
						if (ti != null && ti.ccontrolId() != Control.NOTHING) {
							nValidTgt = i+1;
						}
					}
					
					if (lfoHost != null) {
						lfoHost.setNLFOTarget(xyci, nValidTgt);
						ArrayList<LFO> lfos = lfoHost.lfos();
						while (xyci > lfos.size()) {
							lfos.add(null);
						}
						if (xyci == lfos.size()) {
							lfos.add(l);
						}
						if (lfos.get(xyci) == null) {
							lfos.set(xyci, l);
						}

						int nActiveLFO = 0;
						int firstUnassignedLFO = -1;
						for (int i=0; i<lfos.size(); i++) {
							LFO ll = lfos.get(i);
							if (ll != null && ll.countValidTargets() > 0) {
								nActiveLFO = i+1;
							} else {
								if (firstUnassignedLFO < 0 && findLFOControl(i) == null) {
									firstUnassignedLFO = i;
								}
							}
						}
						if (nActiveLFO < lfoHost.maxLFO()) {
							if (firstUnassignedLFO < 0 && findLFOControl(nActiveLFO) == null) {
								firstUnassignedLFO = nActiveLFO;
							}
						}
						lfoHost.setNLFO(nActiveLFO);
						lfoHost.setLFOTarget(xyci, targetId, tgt, l.target.get(targetId).amount);
						
						if (firstUnassignedLFO >= 0) {
							newLFOControl(firstUnassignedLFO);
							minimizeUninterestingModulators(lc, true, true);
						}
						
						if (lfoHost != null && lfoHost.getInterfaceTgtAdapter() != null) {
							lfoHost.getInterfaceTgtAdapter().refilter();
						}
						if (lfoHost != null && lfoHost.getLFOTgtAdapter() != null) {
							lfoHost.getLFOTgtAdapter().refilter();
						}
						if (envHost != null && envHost.getEnvTgtAdapter() != null) {
							envHost.getEnvTgtAdapter().refilter();
						}
					}
				}
			}

		});
	}
	
	public void setEnvListener(EnvelopeControl p, final int xyci)
	{
		p.setListener(new EnvelopeControlListener() {
			@Override
			public void onResetChanged(Envelope e, ResetMode sip) {
				if (e != null) {
					e.trigger = sip;
					if (envHost != null) {
						envHost.setEnvelopeReset(xyci, sip);
					}
				}
			}

			@Override
			public void onTempoLockChanged(Envelope e, boolean isChecked)
			{
				if (e != null) {
					e.tempoLock = isChecked;
					if (envHost != null) {
						envHost.setEnvelopeLock(xyci, isChecked);
					}
				}
			}

			@Override
			public void onAttackChanged(Envelope e, float t) {
				if (e != null) {
					e.attackT = t;
					if (envHost != null) {
						envHost.setEnvelopeAttack(xyci, t);
						mwbp.ccontrolUpdate(envHost, mwbp.envAttackTimeId(xyci), t, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onDecayChanged(Envelope e, float t) {
				if (e != null) {
					e.decayT = t;
					if (envHost != null) {
						envHost.setEnvelopeDecay(xyci, t);
						mwbp.ccontrolUpdate(envHost, mwbp.envDecayTimeId(xyci), t, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onReleaseChanged(Envelope e, float t) {
				if (e != null) {
					e.releaseT = t;
					if (envHost != null) {
						envHost.setEnvelopeRelease(xyci, t);
						mwbp.ccontrolUpdate(envHost, mwbp.envReleaseTimeId(xyci), t, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onSustainChanged(Envelope e, float t) {
				if (e != null) {
					e.sustainT = t;
					if (envHost != null) {
						envHost.setEnvelopeSustain(xyci, t);
						mwbp.ccontrolUpdate(envHost, mwbp.envSustainTimeId(xyci), t, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onSustainLevelChanged(Envelope e, float amt) {
				if (e != null) {
					e.sustainL = amt;
					if (envHost != null) {
						envHost.setEnvelopeSustainLevel(xyci, e.sustainL);
						mwbp.ccontrolUpdate(envHost, mwbp.envSustainLevelId(xyci), amt, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onTargetValueChanged(Envelope e, int targetId, float amt) {
				if (e != null && envHost != null && targetId < envHost.maxEnvTgt()) {
					while (targetId >= e.target.size()) {
						e.addTarget();
					}
					e.target.get(targetId).amount = amt;
					if (envHost != null) {
						envHost.setEnvelopeTarget(xyci, targetId, e.target.get(targetId).target, amt);
						mwbp.ccontrolUpdate(envHost, mwbp.envTargetDepthId(xyci, targetId), amt, Controllable.Controller.INTERFACE);
					}
				}
			}

			@Override
			public void onTargetControlChanged(EnvelopeControl ec, Envelope e, int targetId, Control tgt, float amt)
			{
				if (e != null && envHost != null && targetId < envHost.maxEnvTgt()) {
					while (targetId >= e.target.size()) {
						e.addTarget();
					}
					if (targetId >= ec.countTargetControl()-1 && tgt.ccontrolId() != Control.NOTHING
							&& (envHost == null || targetId < envHost.maxEnvTgt()-1)) {
						ec.addTargetControl(targetId+1);
					}
					e.target.get(targetId).target = tgt;
					e.target.get(targetId).amount = amt;
					int nActiveTarget = 0;
					for (int i=0; i<e.target.size(); i++) {
						ICControl t = e.target.get(i).target;
						if (t != null && t.ccontrolId() != Control.NOTHING) {
							nActiveTarget = i+1;
						}
					}
					if (envHost != null) {
						envHost.setNEnvelopeTarget(xyci, nActiveTarget);
						ArrayList<Envelope> env = envHost.envs();
						while (xyci > env.size()) {
							env.add(null);
						}
						if (xyci == env.size()) {
							env.add(e);
						}
						if (env.get(xyci) == null) {
							env.set(xyci, e);
						}
						int nValidEnv = 0;
						int firstUnassignedEnv = -1;
						for (int i=0; i<env.size(); i++) {
							Envelope ll = env.get(i);
							if (ll != null && ll.countValidTargets() > 0) {
								nValidEnv = i+1;
							} else {
								if (firstUnassignedEnv < 0 && findEnvControl(i) == null) {
									firstUnassignedEnv = i;
								}
							}
						}
						if (nValidEnv < envHost.maxEnv()) {
							if (firstUnassignedEnv < 0 && findEnvControl(nValidEnv) == null) {
								firstUnassignedEnv = nValidEnv;
							}
						}
						Log.d("edit env", String.format("got %d %d env %d %d %d %g", nActiveTarget, firstUnassignedEnv, xyci, targetId, tgt.ccontrolId(), e.target.get(targetId).amount));
						envHost.setNEnvelope(nValidEnv);
						envHost.setEnvelopeTarget(xyci, targetId, tgt, e.target.get(targetId).amount);
											
						if (firstUnassignedEnv >= 0) {
							newEnvControl(firstUnassignedEnv);
							minimizeUninterestingModulators(ec, true, true);
						}
						
						if (envHost != null && envHost.getInterfaceTgtAdapter() != null) {
							envHost.getInterfaceTgtAdapter().refilter();
						}
						if (lfoHost != null && lfoHost.getLFOTgtAdapter() != null) {
							lfoHost.getLFOTgtAdapter().refilter();
						}
						if (envHost != null && envHost.getEnvTgtAdapter() != null) {
							envHost.getEnvTgtAdapter().refilter();
						}
					}
				}
			}

		});
	}
	
	public EnvelopeControl findEnvControl(int envind) {
		for (EnvelopeControl l: envControl) {
			if (l.getEnvid() == envind) {
				return l;
			}
		}
		return null;
	}
	
	public LFOControl findLFOControl(int lfind) {
		for (LFOControl l: lfoControl) {
			if (l.getLFOid() == lfind) {
				return l;
			}
		}
		return null;
	}
	
	public EnvelopeControl addEnvControl(int envind, ArrayAdapter<ResetMode> erma, boolean alwaysTempoLock, ArrayList<EnvelopeControl> list)
	{
		int mid = 1;
		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		View sxy0 = null;
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		if (subPanelHolder.getChildCount() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			sxy0 = (View) subPanelHolder.getChildAt((subPanelHolder.getChildCount()-1));
			mid = sxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, sxy0.getId());
		}
		EnvelopeControl snsx = new EnvelopeControl(this.getActivity());
		snsx.setAlwaysTempoLock(alwaysTempoLock);
		Log.d("env control", "name");
		snsx.setName("ENV"+Integer.toString(envind+1));
		snsx.setEnvid(envind);
		snsx.setAdapters(null, erma);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setId(mid);
		snsx.setDisplay(list.size() == 0? DisplayMode.SHOW_TARGET_CTLS: DisplayMode.SHOW_NAME);
		
		list.add(snsx);
		subPanelHolder.addView(snsx, relativeParams);
		
		setEnvListener(snsx, envind);
		
		return snsx;
	}
	
	public LFOControl addLFOControl(int lfind, ArrayAdapter<ResetMode> erma, ArrayAdapter<LFWave> wfma,
			boolean alwaysTempoLock, ArrayList<LFOControl> list)
	{
		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		int mid = 1;
		View sxy0 = null;
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		
		if (subPanelHolder.getChildCount() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			sxy0 = (View) subPanelHolder.getChildAt((subPanelHolder.getChildCount()-1));
			mid = sxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, sxy0.getId());
		}
		LFOControl snsx = new LFOControl(getActivity());
		snsx.setAlwaysTempoLock(alwaysTempoLock);
		snsx.setName("LFO"+Integer.toString(lfind+1));
		snsx.setLFOid(lfind);
		snsx.setAdapters(null, erma, wfma);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setId(mid);
		snsx.setDisplay(list.size() == 0? DisplayMode.SHOW_TARGET_CTLS: DisplayMode.SHOW_NAME);
		
		list.add(snsx);
		subPanelHolder.addView(snsx, relativeParams);
		
		setLFOListener(snsx, lfind);
		
		return snsx;
	}
	
	public void setLFOEditor(ArrayList<LFO> lfo) 
	{
		int pxi = 0;
		if (lfo == null || mwbp == null) {
			return;
		}
		for (LFO xsp: lfo) {
			if (pxi >= lfoControl.size()) {
				addLFOControl(lfoControl.size(), mwbp.getLfoRstModeAdapter(lfoHost), mwbp.getLfoWaveAdapter(lfoHost), mwbp.alwaysTempoLock(), lfoControl);
			}
			LFOControl lc = lfoControl.get(pxi);
			lc.setTargetAdapter(lfoHost != null? lfoHost.getLFOTgtAdapter(): null);
			lc.set(xsp);
			pxi++;
		}
		for (int i=pxi; i<lfoControl.size(); i++) {
			LFOControl lc = lfoControl.get(pxi);
			lc.clearAssignment();
			lc.setTargetAdapter(lfoHost != null? lfoHost.getLFOTgtAdapter(): null);
			lc.set(mwbp.newLFO());
		}
	}
	
	public void setEnvEditor(ArrayList<Envelope> envelope) 
	{
		int pxi = 0;
		if (envelope == null || mwbp == null) {
			return;
		}
		for (Envelope xsp: envelope) {
			if (pxi >= envControl.size()) {
				addEnvControl(envControl.size(), mwbp.getLfoRstModeAdapter(lfoHost), mwbp.alwaysTempoLock(), envControl);
			}
			EnvelopeControl ec = envControl.get(pxi);
			ec.setTargetAdapter(envHost != null? envHost.getEnvTgtAdapter(): null);
			ec.set(xsp);
			pxi++;
		}
		for (int i=pxi; i<envControl.size(); i++) {
			EnvelopeControl ec = envControl.get(i);
			ec.clearAssignment();
			ec.setTargetAdapter(envHost != null? envHost.getEnvTgtAdapter(): null);
			ec.set(mwbp.newEnv());
		}
	}
	
	private void delLFOControl(LFOControl p)
	{
		int pxi = lfoControl.indexOf(p);
		lfoControl.remove(pxi);
		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		subPanelHolder.removeView(p);
	}
	
	
	private void delEnvControl(EnvelopeControl p)
	{
		int pxi = envControl.indexOf(p);
		envControl.remove(pxi);
		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		subPanelHolder.removeView(p);
	}

	public void newLFOControl(int i) {
		if (i < 0) {
			if (lfoHost != null && lfoControl.size() < lfoHost.maxLFO()) {
				i = lfoControl.size();
			}
		} else if (findLFOControl(i) != null || lfoHost == null || i >= lfoHost.maxLFO()) {
			i = -1;
		}
		if (i >= 0) {
			LFOControl lc = addLFOControl(lfoControl.size(), mwbp.getLfoRstModeAdapter(lfoHost), mwbp.getLfoWaveAdapter(lfoHost), mwbp.alwaysTempoLock(), lfoControl);
			layoutEditorControls();
			if (lfoHost != null) {
				lc.setTargetAdapter(lfoHost.getLFOTgtAdapter());
			}
			lc.set(mwbp.newLFO());
		}
	}

	public void newEnvControl(int i) {
		if (i < 0) {
			if (envHost != null && envControl.size() < envHost.maxEnv()) {
				i = envControl.size();
			}
		} else if (findEnvControl(i) != null || envHost == null || i >= envHost.maxEnv()) {
			
			i = -1;
		}
		if (i >= 0) {
			EnvelopeControl ec = addEnvControl(i, mwbp.getEnvRstModeAdapter(envHost), mwbp.alwaysTempoLock(), envControl);
			layoutEditorControls();
			if (envHost != null) ec.setTargetAdapter(envHost.getEnvTgtAdapter());
			ec.set(mwbp.newEnv());
		}
	}

	public void setLFOHost(LFO.Host host) {
		if (host == null) {
			return;
		}
		lfoHost = host;
		if (mwbp != null) {
			setLFOEditor(lfoHost.lfos());
		}
	}
	
	public void setEnvHost(Envelope.Host host) {
		if (host == null) {
			return;
		}
		envHost = host;
		if (mwbp != null) {
			setEnvEditor(envHost.envs());
		}
	}
	
	private void minimizeUninterestingModulators(BaseControl except, boolean onlyTillFits, boolean onlyUnassigned)
	{
		/*
		 * TODO something about height being too big
		 */
		for (LFOControl lfc: lfoControl) {
			if (lfc != except) {
				if (!onlyUnassigned || lfc.countValidTargets() == 0) {
					lfc.setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		}
		for (EnvelopeControl evc: envControl) {
			if (evc != except || evc.countValidTargets() == 0) {
				if (!onlyUnassigned) {
					evc.setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		}

		layoutEditorControls();
	}

	private LFOControl getLFOControl(int lfo) {
		return lfoControl.get(lfo);
	}

	private EnvelopeControl getEnvControl(int env) {
		return envControl.get(env);
	}

	public void setEnvTargetGain(int lfo, int i, float v) {
		EnvelopeControl l = getEnvControl(lfo); if (l != null) l.setTargetGain(i, v);
	}
	public void setLFOTargetGain(int lfo, int i, float v) {
		LFOControl l = getLFOControl(lfo); if (l != null) l.setTargetGain(i, v);
	}

	public void setPhase(int lfo, float v) {
		LFOControl l = getLFOControl(lfo); if (l != null) l.setPhaseValue(v);
	}

	public void setRate(int lfo, float v) {
		LFOControl l = getLFOControl(lfo); if (l != null) l.setRateValue(v);
	}

	public void setReleaseT(int env, float v) {
		EnvelopeControl l = getEnvControl(env); if (l != null) l.setReleaseTValue(v);
	}

	public void setSustainL(int env, float v) {
		EnvelopeControl l = getEnvControl(env); if (l != null) l.setSustainLValue(v);
	}

	public void setSustainT(int env, float v) {
		EnvelopeControl l = getEnvControl(env); if (l != null) l.setSustainTValue(v);
	}

	public void setDecayT(int env, float v) {
		EnvelopeControl l = getEnvControl(env); if (l != null) l.setDecayTValue(v);
	}

	public void setAttackT(int env, float v) {
		EnvelopeControl l = getEnvControl(env); if (l != null) l.setAttackTValue(v);
	}
}
