package com.mayaswell.widget;

import java.util.ArrayList;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Modulator;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemSelectedListener;

public class ModulatorControl extends BaseControl {
	public interface ModulationTargetListener {
		public void onValueChanged(int targetId2, float amt);
		public void onTargetChanged(int targetId2, Control tgt, float amt);
	}
	
	public class ModulationTargetControl extends LinearLayout
	{
		private Spinner spinner=null;
		private RotaryPotView pot=null;
		private ModulationTargetListener listener=null;
		private int targetId=-1;

		public ModulationTargetControl(Context context)
		{
			super(context);
			setup(context, null, -1);
		}

		public ModulationTargetControl(Context context, AttributeSet attrs)
		{
			super(context, attrs);
			setup(context, attrs, -1);
		}

		public ModulationTargetControl(Context context, AttributeSet attrs, int defStyle)
		{
			super(context, attrs, defStyle);
			setup(context, attrs, defStyle);
		}

		public void setValueBounds(float min, float max)
		{
			pot.setValueBounds(min, max);
		}
		
		public void setCenterValue(float cv)
		{
			pot.setCenterValue(cv);
		}
		
		public void setListener(ModulationTargetListener l )
		{
			listener = l;
		}
		
		private void notifyListener(float v)
		{
			if (listener != null)
				listener.onValueChanged(targetId, v);
		}
		
		private void notifyListener(int targetId2, Control v)
		{
			if (listener != null)
				listener.onTargetChanged(targetId2, v, pot.getValue());
		}
		
		public void setControllerSelectWidth(int px)
		{
			LayoutParams params = (LayoutParams) spinner.getLayoutParams();
			params.width = px;
		}
		
		public void setAdapter(SpinnerAdapter ctrllerAdapter)
		{
			spinner.setAdapter(ctrllerAdapter);
		}
		
		private void checkAndNotify()
		{
			Control sip = null;
			try {
				SpinnerAdapter csa = spinner.getAdapter();
				if (csa != null) {
					sip = (Control) csa.getItem(spinner.getSelectedItemPosition());
				}
			} catch (Exception e) {
				
			}
			if (sip != null) {
				notifyListener(targetId, sip);
			}
		}
		
		protected void setup(Context context, AttributeSet attrs, int defStyle)
		{
			LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
			li.inflate (R.layout.sg_mod_tgt_control, this, true);
			
			spinner = (Spinner)findViewById (R.id.sgCtlSpinner);
//			view = (EditText)findViewById (R.id.sgCtlEditText);
			pot = (RotaryPotView)findViewById (R.id.sgCtlPot);
			pot.setCenterValue(1);
			pot.setValueBounds(-1, 1);
			pot.setValue(1);
			
/*			view.addTextChangedListener(new TextWatcher() {
				@Override
				public void afterTextChanged(Editable tv) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						if (s.length() > 0) {
							float v = Float.parseFloat(s.toString());
							if (v < pot.getMinVal()) v = pot.getMinVal();
							else if (v > pot.getMaxVal()) v = pot.getMaxVal();
							pot.setValue(v);
							notifyListener(v);
						}
					} catch (NumberFormatException e) {
					}
				}
			});*/

			pot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
//					view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
					notifyListener(v);
				}
			});
			
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() 
			{	
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
				{
					checkAndNotify();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) 
				{
				}
			});
			
			if (attrs != null) {
				/*
				String lbl = attrs.getAttributeValue(schemaName, "label");
				if (lbl != null) {
					label.setText(lbl);
				}
				String displayPrecision = attrs.getAttributeValue(schemaName, "displayPrecision");
				if (displayPrecision != null) {
					setValueDisplayPrecision(Integer.parseInt(displayPrecision));
				}*/
				String valueBounds = attrs.getAttributeValue(schemaName, "valueBounds");
				if (valueBounds != null) {
					String [] valueBoundsA = valueBounds.split(",");
					if (valueBoundsA.length >=2) {
						float floatBoundsA[] = new float[valueBoundsA.length];
						int i=0;
						for (String s: valueBoundsA) {
							floatBoundsA[i++] = Float.parseFloat(s);
						}
						setValueBounds(floatBoundsA[0], floatBoundsA[floatBoundsA.length-1]);
						setCenterValue(floatBoundsA[floatBoundsA.length-2]);
					}
				}
				int px = -1;
				int swr = attrs.getAttributeResourceValue(schemaName, "controllerSpinnerWidth", -1);
				if (swr >= 0) {
					px = getResources().getDimensionPixelSize(swr);
				} else {
					px = 0;
					String viewWidth = attrs.getAttributeValue(schemaName, "controllerSpinnerWidth");
					if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
					}
				}
				if (px > 0) {
					setControllerSelectWidth(px);
				}
				/*
				
				int vwr = attrs.getAttributeResourceValue(schemaName, "viewWidth", -1);
				if (vwr >= 0) {
					px = getResources().getDimensionPixelSize(vwr);
				} else {
					px = 0;
					String viewWidth = attrs.getAttributeValue(schemaName, "viewWidth");
					if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
					}
				}
				if (px > 0) {
					LayoutParams params = (LayoutParams) view.getLayoutParams();
					params.width = px;
				}
				*/
			}
		}

		public void setTgtId(int snsxi)
		{
			targetId = snsxi;
		}

		public void clearAssignment()
		{
			pot.setValue(pot.getcenterVal());
			spinner.setSelection(0);
		}

		public void set(ModulationTarget mt)
		{
			if  (mt != null && mt.target != null) {
				SpinnerAdapter wfa = (SpinnerAdapter) spinner.getAdapter();
				if (tgtAdapter != null && wfa != null) {
					int xpi = tgtAdapter.getControlIndex4Id(mt.target.ccontrolId());
					spinner.setSelection(xpi);
				}
				pot.setValue(mt.amount);
			}
		}

		public void setGain(float v) {
			if (pot != null) pot.setValue(v);
		}
	}
	
	
	protected ToggleButton tempoLockButton = null;
	protected Spinner rstModeSelect=null;
	protected RelativeLayout targetBody = null;
	
	protected ControlsAdapter tgtAdapter = null;
	protected ArrayList<ModulationTargetControl> targetControl = new ArrayList<ModulationTargetControl>();
	protected boolean alwaysTempoLock = false;

	public ModulatorControl(Context context) {
		super(context);
	}

	public ModulatorControl(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ModulatorControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	protected void delTargetControl(LFOControl p)
	{
		int pxi = targetControl .indexOf(p);
		targetControl.remove(pxi);
		targetBody.removeView(p);
	}
	
	public ModulationTargetControl addTargetControl(int snsxi)
	{
		int mid = 1;
		ModulationTargetControl sxy0 = null;
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		if (targetControl.size() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		} else {
			sxy0 = targetControl.get(targetControl.size()-1);
			mid = sxy0.getId()+1;
//			Log.d("mod tgt", String.format("adding target view id %d", mid));
			relativeParams.addRule(RelativeLayout.RIGHT_OF, sxy0.getId());
		}
		ModulationTargetControl snsx = new ModulationTargetControl(getContext());
		snsx.setTgtId(snsxi);
		snsx.setAdapter(tgtAdapter != null? tgtAdapter.getAdapter(): null);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.pot.setValue(1);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setId(mid);
		snsx.setListener(new ModulationTargetListener() {
			@Override
			public void onValueChanged(int targetId, float amt) {
//				Log.d("mod tgt", String.format("value changed %d %g", targetId, amt));
				notifyTargetValueChanged(targetId, amt);
			}

			@Override
			public void onTargetChanged(int targetId, Control tgt, float amt) {
//				Log.d("mod tgt", String.format("target changed %d %d %s %g", targetId, tgt.instanceId(), tgt.abbrevName(), amt));
				notifyTargetControlChanged(targetId, tgt, amt);
//				Log.d("mod tgt", String.format("target changed %d %d %s %g", targetId, tgt.instanceId(), tgt.abbrevName(), amt));
			}
		});
		
		targetControl.add(snsx);
		targetBody.addView(snsx, relativeParams);
		
		return snsx;
	}
	
	public void setTargetAdapter(ControlsAdapter controlsAdapter)
	{
		if (controlsAdapter == null) {
			Log.d("modulation", String.format("null target adapter"));
		} else {
			Log.d("modulation", String.format("set target adapt %d items", controlsAdapter.getCount()));
		}
		tgtAdapter = controlsAdapter;
		if (tgtAdapter != null) {
			for (ModulationTargetControl t : targetControl) {
				t.setAdapter(tgtAdapter.getAdapter());
			}
		}
	}
	
	public int countTargetControl()
	{
		return targetControl.size();
	}
	
	protected void notifyTargetControlChanged(int targetId, Control tgt, float amt)
	{
		
	}
	
	protected void notifyTargetValueChanged(int targetId, float amt)
	{
		
	}
	
	public void setResetSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) rstModeSelect.getLayoutParams();
		params.width = px;
	}

	public void clearAssignment()
	{
		tempoLockButton.setChecked(true);
		if (rstModeSelect != null) {
			@SuppressWarnings("unchecked")
			ArrayAdapter<ResetMode> wfa = (ArrayAdapter<ResetMode>) rstModeSelect.getAdapter();
			rstModeSelect.setSelection(wfa.getPosition(ResetMode.ONFIRE));
		}
		for (ModulationTargetControl mt: targetControl) {
			mt.clearAssignment();
		}
	}

	public boolean set(Modulator xsp)
	{
		int pxi = 0;
		if (xsp == null) {
			return false;
		}
		if (alwaysTempoLock) {
			xsp.tempoLock = true;
		} else {
			tempoLockButton.setChecked(xsp.tempoLock);
		}
		if (rstModeSelect != null) {
			@SuppressWarnings("unchecked")
			ArrayAdapter<ResetMode> wfa = (ArrayAdapter<ResetMode>) rstModeSelect.getAdapter();
			rstModeSelect.setSelection(wfa.getPosition(xsp.trigger));
		}
		for (ModulationTarget mt: xsp.target) {
			if (pxi >= targetControl.size()) {
				addTargetControl(targetControl.size());
			}
			if (mt != null) {
				targetControl.get(pxi).set(mt);
			}
			pxi++;
		}
		for (; pxi<targetControl.size(); pxi++) {
			targetControl.get(pxi).clearAssignment();
		}
		return true;
	}

	public void setTargetGain(int i, float v) {
		ModulationTargetControl t = getTargetControl(i);
		if (t != null) {
			t.setGain(v);
		}
	}

	private ModulationTargetControl getTargetControl(int i) {
		return targetControl.get(i);
	}
	
	public void setAlwaysTempoLock(boolean b)
	{
		alwaysTempoLock  = b;
	}
	
}
