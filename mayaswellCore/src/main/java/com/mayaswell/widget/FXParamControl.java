package com.mayaswell.widget;

import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.FXParam.Type;
import com.mayaswell.R;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class FXParamControl extends BaseControl {

	public interface FXParamControlListener
	{
		public void onValueChanged(int pid, float amt);
	}

	private FXParamControlListener listener = null;
	private RotaryPotView pot = null;
	private EditText view = null;
	private FXParam param = null;
	private boolean notifyOnTextChange = true;
	
	private boolean autoFocus = false;
	private double logF;

	public FXParamControl(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public FXParamControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public FXParamControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_tea_pot, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		pot = (RotaryPotView)findViewById (R.id.mwCtlPot);
		view  = (EditText)findViewById (R.id.mwCtlEditText);
				
		pot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v0) {
				float v = 0;
				if (param.isLogScale()) {
					v = pot2v(v0);
					Log.d("fxp", "is log val "+v);
				} else {
					v = v0;
				}
				if (param.getType() ==Type.INT) {
					v = Math.round(v);
//					Log.d("fx param int", "got v "+v+" of "+pot.getValue());
					notifyOnTextChange = false;
					view.setText(Integer.toString((int) v));
					notifyOnTextChange = true;
				} else {
					notifyOnTextChange = false;
					view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
					notifyOnTextChange = true;
				}
				notifyListener(v);
			}

		});

		view.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
//				Log.d("tea", "long");
				if (!view.hasFocus()) {
					view.setFocusable(true);
					view.setFocusableInTouchMode(true);
					view.requestFocus();
					autoFocus = true;
					return true;
				}
				return false;
			}
			
		});
		
		view.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!autoFocus) {
//					if (hasFocus) clearFocus(); // this should never happen, but it does sometimes. 
					return;
				}
				if (hasFocus) {
					autoFocus = false;
					InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);	
				} else {
					InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(view.getWindowToken(), 0);	
				}
			}
			
		});
		
		view.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_DONE) {
		        	clearFocus();
					view.setFocusable(false);
		        }
		        return false;
		    }
		});
		view.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						boolean hitBounds = false;
						if (v < param.getMin()) {
							hitBounds = true;
							v = param.getMin();
						} else if (v > param.getMax()) {
							hitBounds = true;
							v = param.getMax();
						}
						if (hitBounds || notifyOnTextChange) {
							setPotValue(v);
						}
						if (notifyOnTextChange) {
							notifyListener(v);
						}
					}
				} catch (NumberFormatException e) {
				}
			}

		});
	
	}
	
	protected void setPotValue(float v) {
		if (param.isLogScale()) {
			pot.setValue(v2pot(v));
			Log.d("fxp", "is setting pot for v "+v+", logF "+logF+ "min "+ param.getMin() + (Math.log10(v/param.getMin())/logF));
		} else {
			pot.setValue(v);
		}
	}

	protected float v2pot(float v) {
		return (float) (Math.log10(v/param.getMin())/logF);
	}
	protected float pot2v(float v0) {
		return (float) (param.getMin()*Math.pow(10, logF*v0));
	}
	
	public void setTo(FXParam p, boolean setDefaultParamValue)
	{
		param = p;
		if (param == null) {
			view.setText("");
			setName("");
		} else {
			if (param.isLogScale()) {
				setValueBounds(0, 1);
				logF = Math.log10(param.getMax()/param.getMin());
				Log.d("fxp", "is log "+logF);
				setCenterValue(v2pot(param.getCenterValue()));
				if (setDefaultParamValue) {
					notifyOnTextChange = false;
					setPotValue(param.getCenterValue());
					setView(param.getCenterValue());
					notifyOnTextChange = true;
				}
			} else {
				setValueBounds(param.getMin(), param.getMax());
				setCenterValue(param.getCenterValue());
				if (setDefaultParamValue) {
					notifyOnTextChange = false;
					setPotValue(param.getCenterValue());
					setView(param.getCenterValue());
					notifyOnTextChange = true;
				}
			}
			setName(param.toString());
		}
	}
	
	public void setValue(float v)
	{
		notifyOnTextChange  = false;
		setPotValue(v);
		setView(v);
		notifyOnTextChange = true;
	}

	private void setView(float v)
	{
		if (param.getType() == Type.INT) {
			view.setText(Integer.toString((int) v));
		} else {
			view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
		}
	}

	public void setListener(FXParamControlListener l )
	{
		listener  = l;
	}

	public void setValueViewWidth(int px)
	{
		LayoutParams params = (LayoutParams) view.getLayoutParams();
		if (px < 30) px = 30;
		params.width = px;
		// TODO the above check stops Layoout.WRAP being set programmatically, which caused a wierd conflict with touch event on the adjacent pot
	}

	public void setLabelWidth(int px)
	{
		LayoutParams params = (LayoutParams) label.getLayoutParams();
		params.width = px;
	}

	public void setValueBounds(float min, float max)
	{
		pot.setValueBounds(min, max);
	}
	
	public void setCenterValue(float cv)
	{
		pot.setCenterValue(cv);
	}
	
	private void notifyListener(float sip)
	{
		if (listener != null) {
			listener.onValueChanged(param.getId(), sip);
		}
	}
	

}
