package com.mayaswell.audio;

import java.io.File;
import java.io.IOException;

import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.audio.file.AudioFile.Type;
import com.mayaswell.audio.file.AudioFileException;
import com.mayaswell.audio.file.WavFile;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

public abstract class AudioMixer {

	public static final int dfltSampleRate = 44100;
	public static int sampleRate = dfltSampleRate;
	protected boolean isPlaying = false;
	protected int outBufSize = 4096;
	protected int inBufSize = 4096;
	protected int outChannelFormat = AudioFormat.CHANNEL_OUT_STEREO;
	protected int inChannels = AudioFormat.CHANNEL_IN_MONO;
	protected int nOutChannels = 2;
	protected int nInChannels = 1;

	private boolean isRecording = false;
	private boolean recordEnabled = true;
	private PublishSubject<AudioFile> recordSubject = PublishSubject.create();;

	public AudioMixer() {
		outBufSize = AudioTrack.getMinBufferSize(dfltSampleRate, outChannelFormat, AudioFormat.ENCODING_PCM_16BIT);
		inBufSize  = AudioRecord.getMinBufferSize(dfltSampleRate, inChannels, AudioFormat.ENCODING_PCM_16BIT);
		if (outBufSize == AudioTrack.ERROR_BAD_VALUE) {
			// cry
		}
		if (inBufSize == AudioRecord.ERROR_BAD_VALUE) {
//			Log.d("sampler", "recording disabled :(");
			enableRecord(false);
		}
	}

	public int getOutBufsize()
	{
		return outBufSize;
	}

	public Subscription monitorExport(Observer<AudioFile> o) {
		return null;
	}
	public boolean startExport(SamplePlayer s, long frameOffset, File to, Type t) {
		return false;
	}
	
	public boolean stopExport(SamplePlayer s, long frameOffset) {
		return false;
	}

	protected void enableRecord(boolean b) {
		recordEnabled = false;
	}

	public String saveFileExtension() {
		return "wav";
	}
		
	public boolean startRecording(final File file, Observer<AudioFile> observer)
	{
		if (!recordEnabled) {
			return false;
		}
		if (isRecording) {
			return false;
		}

		Subscription s = recordSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
		Thread t;
		t = new Thread() {
			public void run() {
				WavFile outFile = new WavFile();
				try {
					outFile.create(file, nInChannels, Long.MAX_VALUE, 16, dfltSampleRate);
				} catch (IOException e) {
					recorderError("Catastrophe! Some kind of IO exception, creating record file "+file.getPath());
					return;
				} catch (AudioFileException e) {
					recorderError("How Bizzare! An audio file exception, creating record file "+file.getPath());
					return;
				}
				
				isRecording = true;
				AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, dfltSampleRate, inChannels, AudioFormat.ENCODING_PCM_16BIT, inBufSize);
				
				int nFramePerBuf = inBufSize/nInChannels;
				short inBuffer[] = new short[inBufSize];

				recorder.startRecording();
				
				while (isRecording) {
					int nSamplesRead = recorder.read(inBuffer, 0, inBufSize);
					try {
						outFile.writeFrames(inBuffer, nSamplesRead/nInChannels);
					} catch (IOException e) {
						recorderError("Catastrophe! Some kind of IO exception, writing record file "+file.getPath());
						break;
					} catch (AudioFileException e) {
						recorderError("How Bizzare! An audio file exception, writing record file "+file.getPath());
						break;
					}
				}
				
				try {
					outFile.close();
				} catch (IOException e) {
					recorderError("Catastrophe! Some kind of IO exception, finalizing record file "+file.getPath());
				}
				
				recorder.stop();
				recorder.release();

				recordSubject.onNext(outFile);
				outFile = null;
				isRecording = false;
				recordSubject.onCompleted();
			}
		};
		t.start();
		return true;
	}

	public void stopRecording()
	{
		isRecording = false;
	}
	
	public void cancelRecording()
	{

		isRecording = false;
		recordSubject.onCompleted();
	}
	
	public boolean isRecording()
	{
		return isRecording;
	}

	private void recorderError(String s) {
		isRecording = false;
		recordSubject.onError(new Exception(s));
	}

	public float getSampleRate() {
		return sampleRate;
	}
}