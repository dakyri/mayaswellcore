package com.mayaswell.audio;

import android.annotation.SuppressLint;

public class Metric {
	private final int nDivisionPerBeat;
	public float tempo;
	public int nBars;
	public int meter;

	public Metric(float tempo, int nBars, int nBeatPerBar, int nDivisionPerBeat) {
		this.tempo = tempo;
		this.nBars = nBars;
		this.meter = nBeatPerBar;
		this.nDivisionPerBeat = nDivisionPerBeat;
	}

	public Metric(int nb) {
		this(120, 1, nb, 100);
	}
	
	@SuppressLint("DefaultLocale")
	public String getString(double beatCount) {
		int nbar = (int) Math.floor(beatCount / meter);
		double rbt = beatCount % meter;
		int nbt = (int) Math.floor(rbt);
		rbt -= nbt;
		int bfr = (int) Math.floor(rbt*100);
		return String.format("%d:%d.%02d", nbar, nbt, bfr);
	}
}

