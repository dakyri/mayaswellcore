package com.mayaswell.widget;

import com.mayaswell.R;
import com.mayaswell.widget.InfinIntPotView;
import com.mayaswell.widget.InfinIntPotView.InfinIntKnobListener;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InfinIntControl extends LinearLayout {
    public interface InfiniteLongListener {
    	void onValueChanged(long v);
    }

    private final String schemaName = "http://schemas.android.com/apk/res-auto";
	
	protected InfinIntPotView pot = null;
	protected TextView label = null;
	protected EditText view = null;

    private InfiniteLongListener listener=null;
	private boolean notifyTextChange = true;
    
	public InfinIntControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public InfinIntControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public InfinIntControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		TypedArray tpAttrs = null;
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;

		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_infinint_control, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		view = (EditText)findViewById (R.id.mwCtlEditText);
		pot = (InfinIntPotView)findViewById (R.id.mwCtlPot);
		
		view.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						long v = string2Long(s.toString());
						if (v < pot.getMinVal()) v = pot.getMinVal();
						else if (v > pot.getMaxVal()) v = pot.getMaxVal();
						pot.setValue(v);
						if (notifyTextChange) {
							notifyListener(v);
						}
					}
				} catch (NumberFormatException e) {
				}
			}
		});

		pot.setKnobListener(new InfinIntKnobListener() {
			@Override
			public void onKnobChanged(long v) {
				view.setText(long2String(v));
				notifyListener(v);
			}
		});
		
		if (attrs != null) {
			tpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TeaPot, 0, 0);
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);

			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				setLabel(lbl);
			}
			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					long intBoundsA[] = new long[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						if (s.equals("min")) {
							intBoundsA[i++] = Long.MIN_VALUE;
						} else if (s.equals("max")) {
							intBoundsA[i++]	= Long.MAX_VALUE;
						} else {
							intBoundsA[i++] = Integer.parseInt(s);
						}
					}
					setValueBounds(intBoundsA[0], intBoundsA[intBoundsA.length-2], intBoundsA[intBoundsA.length-1]);
				}
			}
			int delta360 = mwpAttrs.getInt(R.styleable.TeaPot_delta360, -1);
			if (delta360 > 0) {
				pot.setFullCircleDelta(delta360);
			}
			int px = (int) mwvAttrs.getDimension(R.styleable.MWView_labelWidth, -1);
			if (px > 0 && label != null) {
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) label.getLayoutParams();
				params.width = px;
			}

			px = (int) mwvAttrs.getDimension(R.styleable.MWView_viewWidth, -1);
			if (px > 0 && view != null) {
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
				params.width = px;
			}
		}
		if (tpAttrs != null) tpAttrs.recycle();
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}

	private void setLabel(String lbl) {
		if (lbl == null || lbl.length() == 0) {
			label.setVisibility(GONE);
		} else {
			label.setVisibility(VISIBLE);
			label.setText(lbl);
		}
	}
	
	public void setValueBounds(long min, long center, long max)
	{
		pot.setValueBounds(min, max);
		pot.setCenterVal(center);
		pot.setCenterAngle(0);
	}
	
    public void setPotListener(InfiniteLongListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(long v)
	{
		if (listener != null)
			listener.onValueChanged(v);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setValue(long v)
	{
//		if (v < pot.getMinVal()) v = pot.getMinVal();
//		else if (v > pot.getMaxVal()) v = pot.getMaxVal();
		pot.setValue(v);
		notifyTextChange  = false;
		view.setText(long2String(v));
		notifyTextChange  = true;
	}
	
	public long getValue()
	{
		return pot.getValue();
	}

	public void setCenterValue(long v)
	{
		pot.setCenterVal(v);
	}
	
	protected long string2Long(String s) {
		return Long.parseLong(s);
	}

	protected String long2String(long v) {
		return Long.toString(v);
	}
}
