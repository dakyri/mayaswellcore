package com.mayaswell.widget;

import android.content.Context;
import java.lang.Math;
import android.util.AttributeSet;
import android.util.Log;

public class RotaryLogScalePot extends RotaryPotView {

	public RotaryLogScalePot(Context context) {
		super(context);
	}

	public RotaryLogScalePot(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RotaryLogScalePot(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
    public void setValueBounds(float min, float max)
    {
    	if (max < min) return;
    	
    	minVal = (float) Math.log(min);
    	maxVal = (float) Math.log(max);
    }
    
    public void setCenterValue(float cv)
    {
    	centerVal = (float) Math.log(cv);
    }
    
    public float getMinVal() { return (float) Math.exp(minVal); }
    public float getMaxVal() { return (float) Math.exp(maxVal); }
    public float getcenterVal() { return (float) Math.exp(centerVal); }
    
    public void setValue(float v)
    {
    	super.setValue((float) Math.log(v));
   }
    
	public float getValue()
    {
    	float v= (float) Math.exp(super.getValue());
    	return v;
    }

}
