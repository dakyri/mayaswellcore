package com.mayaswell.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/**
 * Created by dak on 2/17/2017.
 *
 * Based initially on standard android GestureDetector
 */
public class GyesztorDetector {
	VelocityTracker velocityTracker;
	private float lastX;
	private float lastY;
	private float downX;
	private float downY;
	private MotionEvent currentDownEvent;
	private MotionEvent previousUpEvent;
	private boolean stillDown;
	private boolean deferConfirmSingleTap;
	private boolean inLongPress;
	private boolean inContextClick;
	private boolean alwaysInTapRegion;
	private boolean alwaysInBiggerTapRegion;
	private boolean ignoreNextUpEvent;

	private boolean isLongpressEnabled = true;
	private boolean isDoubleTapping = false;
	private boolean isDoubleTapEnabled = false;

	private int touchSlopSquare;
	private int doubleTapTouchSlopSquare;
	private int doubleTapSlopSquare;
	private int minimumFlingVelocity;
	private int maximumFlingVelocity;

	Handler handler;

	private static final int LONGPRESS_TIMEOUT = 1000;//ViewConfiguration.getLongPressTimeout();
	private static final int TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
	private static final int DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
//	private static final int DOUBLE_TAP_MIN_TIME = 40;
	private static final int DOUBLE_TAP_MIN_TIME = 40; //ViewConfiguration.getDoubleTapMinTime();

	private static final int SHOW_PRESS = 1;
	private static final int LONG_PRESS = 2;
	private static final int TAP = 3;

	public GyesztorDetector(Listener l) {
		setListener(l);
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case SHOW_PRESS:
						listener.onShowPress(currentDownEvent);
						break;

					case LONG_PRESS:
						Log.d("detector ","long press in handler");
						dispatchLongPress();
						break;

					case TAP:
						// If the user's finger is still down, do not count it as a tap
						if (isDoubleTapEnabled) {
							if (!stillDown) {
								listener.onSingleTapConfirmed(currentDownEvent);
							} else {
								deferConfirmSingleTap = true;
							}
						}
						break;

					default:
						throw new RuntimeException("Unknown message " + msg); //never
				}
			}

		};
		init(null);
	}

	private void init(Context context) {
		isLongpressEnabled = true;

		// Fallback to support pre-donuts releases
		int touchSlop, doubleTapSlop, doubleTapTouchSlop;
		if (context == null) {
			//noinspection deprecation
			touchSlop = ViewConfiguration.getTouchSlop();
			doubleTapTouchSlop = touchSlop; // Hack rather than adding a hiden method for this
//			private static final int DOUBLE_TAP_SLOP = 100;
			doubleTapSlop = 100; // ViewConfiguration.getDoubleTapSlop();
			//noinspection deprecation
			minimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
			maximumFlingVelocity = ViewConfiguration.getMaximumFlingVelocity();
		} else {
			final ViewConfiguration configuration = ViewConfiguration.get(context);
			touchSlop = configuration.getScaledTouchSlop();
//			private static final int TOUCH_SLOP = 8;
//			private static final int DOUBLE_TAP_TOUCH_SLOP = TOUCH_SLOP;
			doubleTapTouchSlop = 8;// configuration.getScaledDoubleTapTouchSlop();
			doubleTapSlop = configuration.getScaledDoubleTapSlop();
			minimumFlingVelocity = configuration.getScaledMinimumFlingVelocity();
			maximumFlingVelocity = configuration.getScaledMaximumFlingVelocity();
		}
		touchSlopSquare = touchSlop * touchSlop;
		doubleTapTouchSlopSquare = doubleTapTouchSlop * doubleTapTouchSlop;
		doubleTapSlopSquare = doubleTapSlop * doubleTapSlop;
	}

	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();

		if (velocityTracker == null) {
			velocityTracker = VelocityTracker.obtain();
		}
		velocityTracker.addMovement(ev);

		final boolean pointerUp =
				(action & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_UP;
		final int skipIndex = pointerUp ? ev.getActionIndex() : -1;

		// Determine focal point
		float sumX = 0, sumY = 0;
		final int count = ev.getPointerCount();
		for (int i = 0; i < count; i++) {
			if (skipIndex == i) continue;
			sumX += ev.getX(i);
			sumY += ev.getY(i);
		}
		final int div = pointerUp ? count - 1 : count;
		final float focusX = sumX / div;
		final float focusY = sumY / div;

		boolean handled = false;

		switch (action & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_POINTER_DOWN:

				downX = lastX = focusX;
				downY = lastY = focusY;
				// Cancel long press and taps
				cancelTaps();
				Log.d("onTouch", "ACTION_POINTER_DOWN ");
				break;

			case MotionEvent.ACTION_POINTER_UP:
				downX = lastX = focusX;
				downY = lastY = focusY;

				// Check the dot product of current velocities.
				// If the pointer that left was opposing another velocity vector, clear.
				velocityTracker.computeCurrentVelocity(1000, maximumFlingVelocity);
				final int upIndex = ev.getActionIndex();
				final int id1 = ev.getPointerId(upIndex);
				final float x1 = velocityTracker.getXVelocity(id1);
				final float y1 = velocityTracker.getYVelocity(id1);
				for (int i = 0; i < count; i++) {
					if (i == upIndex) continue;

					final int id2 = ev.getPointerId(i);
					final float x = x1 * velocityTracker.getXVelocity(id2);
					final float y = y1 * velocityTracker.getYVelocity(id2);

					final float dot = x + y;
					if (dot < 0) {
						velocityTracker.clear();
						break;
					}
				}
				Log.d("onTouch", "ACTION_POINTER_UP ");
				break;

			case MotionEvent.ACTION_DOWN:
				Log.d("onTouch", "ACTION_DOWN ");
				if (isDoubleTapEnabled) {
					boolean hadTapMessage = handler.hasMessages(TAP);
					if (hadTapMessage) handler.removeMessages(TAP);
					if ((currentDownEvent != null) && (previousUpEvent != null) && hadTapMessage &&
							isConsideredDoubleTap(currentDownEvent, previousUpEvent, ev)) {
						// This is a second tap
						isDoubleTapping = true;
						// Give a callback with the first tap of the double-tap
						handled |= listener.onDoubleTap(currentDownEvent);
						// Give a callback with down event of the double-tap
						handled |= listener.onDoubleTapEvent(ev);
					} else {
						// This is a first tap
						handler.sendEmptyMessageDelayed(TAP, DOUBLE_TAP_TIMEOUT);
					}
				}

				downX = lastX = focusX;
				downY = lastY = focusY;
				if (currentDownEvent != null) {
					currentDownEvent.recycle();
				}
				currentDownEvent = MotionEvent.obtain(ev);
				alwaysInTapRegion = true;
				alwaysInBiggerTapRegion = true;
				stillDown = true;
				inLongPress = false;
				deferConfirmSingleTap = false;

				if (isLongpressEnabled) {
					handler.removeMessages(LONG_PRESS);
					handler.sendEmptyMessageAtTime(LONG_PRESS, currentDownEvent.getDownTime()
							+ TAP_TIMEOUT + LONGPRESS_TIMEOUT);
				}
				handler.sendEmptyMessageAtTime(SHOW_PRESS, currentDownEvent.getDownTime() + TAP_TIMEOUT);
				handled |= listener.onDown(ev);
				break;

			case MotionEvent.ACTION_MOVE:
				if (inLongPress || inContextClick) {
					break;
				}
				final float scrollX = lastX - focusX;
				final float scrollY = lastY - focusY;
				if (isDoubleTapping) {
					// Give the move events of the double-tap
					handled |= listener.onDoubleTapEvent(ev);
				} else if (alwaysInTapRegion) {
					final int deltaX = (int) (focusX - downX);
					final int deltaY = (int) (focusY - downY);
					int distance = (deltaX * deltaX) + (deltaY * deltaY);
					if (distance > touchSlopSquare) {
						handled = listener.onScroll(currentDownEvent, ev, scrollX, scrollY);
						lastX = focusX;
						lastY = focusY;
						alwaysInTapRegion = false;
						handler.removeMessages(TAP);
						handler.removeMessages(SHOW_PRESS);
						handler.removeMessages(LONG_PRESS);
					}
					if (distance > doubleTapTouchSlopSquare) {
						alwaysInBiggerTapRegion = false;
					}
				} else if ((Math.abs(scrollX) >= 1) || (Math.abs(scrollY) >= 1)) {
					handled = listener.onScroll(currentDownEvent, ev, scrollX, scrollY);
					lastX = focusX;
					lastY = focusY;
				}
				break;

			case MotionEvent.ACTION_UP:
				Log.d("onTouch", "ACTION_UP ");
				stillDown = false;
				MotionEvent currentUpEvent = MotionEvent.obtain(ev);
				if (isDoubleTapping) {
					// Finally, give the up event of the double-tap
					handled |= listener.onDoubleTapEvent(ev);
				} else if (inLongPress) {
					handler.removeMessages(TAP);
					inLongPress = false;
				} else if (alwaysInTapRegion && !ignoreNextUpEvent) {
					Log.d("detector", "single tapup");
					handled = listener.onSingleTapUp(ev);
					if (deferConfirmSingleTap && isDoubleTapEnabled) {
						listener.onSingleTapConfirmed(ev);
					}
				} else if (!ignoreNextUpEvent) {
					// A fling must travel the minimum tap distance
					final VelocityTracker velocityTracker = this.velocityTracker;
					final int pointerId = ev.getPointerId(0);
					velocityTracker.computeCurrentVelocity(1000, maximumFlingVelocity);
					final float velocityY = velocityTracker.getYVelocity(pointerId);
					final float velocityX = velocityTracker.getXVelocity(pointerId);

					if ((Math.abs(velocityY) > minimumFlingVelocity)
							|| (Math.abs(velocityX) > minimumFlingVelocity)){
						handled = listener.onFling(currentDownEvent, ev, velocityX, velocityY);
					}
				}
				if (previousUpEvent != null) {
					previousUpEvent.recycle();
				}
				// Hold the event we obtained above - listeners may have changed the original.
				previousUpEvent = currentUpEvent;
				if (velocityTracker != null) {
					// This may have been cleared when we called out to the
					// application above.
					velocityTracker.recycle();
					velocityTracker = null;
				}
				isDoubleTapping = false;
				deferConfirmSingleTap = false;
				ignoreNextUpEvent = false;
				handler.removeMessages(SHOW_PRESS);
				handler.removeMessages(LONG_PRESS);
				Log.d("detector", "up and removed from q");
				break;

			case MotionEvent.ACTION_CANCEL:
				cancel();
				break;
		}

		return handled;
	}

	private void cancelTaps() {
		handler.removeMessages(SHOW_PRESS);
		handler.removeMessages(LONG_PRESS);
		handler.removeMessages(TAP);
		isDoubleTapping = false;
		alwaysInTapRegion = false;
		alwaysInBiggerTapRegion = false;
		deferConfirmSingleTap = false;
		inLongPress = false;
		inContextClick = false;
		ignoreNextUpEvent = false;
	}

	private void cancel() {
		cancelTaps();
		velocityTracker.recycle();
		velocityTracker = null;
		stillDown = false;
	}

	private boolean isConsideredDoubleTap(MotionEvent firstDown, MotionEvent firstUp,
										  MotionEvent secondDown) {
		if (!alwaysInBiggerTapRegion) {
			return false;
		}

		final long deltaTime = secondDown.getEventTime() - firstUp.getEventTime();
		if (deltaTime > DOUBLE_TAP_TIMEOUT || deltaTime < DOUBLE_TAP_MIN_TIME) {
			return false;
		}

		int deltaX = (int) firstDown.getX() - (int) secondDown.getX();
		int deltaY = (int) firstDown.getY() - (int) secondDown.getY();
		return (deltaX * deltaX + deltaY * deltaY < doubleTapSlopSquare);
	}

	public interface Listener {
		boolean onDown(MotionEvent e);
		void onShowPress(MotionEvent e);
		boolean onSingleTapUp(MotionEvent e);
		boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);
		void onLongPress(MotionEvent e);
		boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);

		boolean onSingleTapConfirmed(MotionEvent e);
		boolean onDoubleTap(MotionEvent e);
		boolean onDoubleTapEvent(MotionEvent e);
	}


	private void dispatchLongPress() {
		handler.removeMessages(TAP);
		deferConfirmSingleTap = false;
		inLongPress = true;
		listener.onLongPress(currentDownEvent);
	}

	private Listener listener = null;
	public void setListener(Listener l) {
		listener = l;
	}
}
