package com.mayaswell.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.HorizontalScrollView;

public class XorizontalScrollView extends HorizontalScrollView {
	public interface Listener {
		void onScrollChanged (int l, int t, int oldl, int oldt);
	}

	public XorizontalScrollView(Context context) {
		super(context);
	}

	public XorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public XorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	protected Listener listener = null;
	public void setListener(Listener l) {
		listener = l;
		
	}
	
	protected void onScrollChanged (int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if (listener != null) {
			listener.onScrollChanged(l, t, oldl, oldt);
		}
	}

}
