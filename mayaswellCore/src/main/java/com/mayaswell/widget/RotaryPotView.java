package com.mayaswell.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class RotaryPotView extends ImageView 
{
    public interface RotaryKnobListener {
    	public void onKnobChanged(float v);
    }
    
    private float angle = 0f;
	protected float minAngle=-150f;
	protected float maxAngle=150f;
	protected float minVal = 0;
	protected float maxVal = 1;
	protected float centerVal=0;
	private float val = 1;
    private float theta_old=0f;
    
    private boolean tweak = false;
 
    private RotaryKnobListener listener=null;
    
    public RotaryPotView(Context context)
    {
		super(context);
		initialize(context, null);
	}

	public RotaryPotView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize(context, attrs);
	}

	public RotaryPotView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initialize(context, attrs);
	}
	
	@Override
	public void setImageResource(int resId)
	{
		super.setImageResource(resId);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm)
	{
		super.setImageBitmap(bm);
	}
	
	@Override
	public void setImageDrawable(Drawable drawable)
	{
		super.setImageDrawable(drawable);
	}
	
    public void setKnobListener(RotaryKnobListener l )
    {
    	listener = l;
    }
    
    public void setAmbit(float minTh, float maxTh)
    {
    	if (maxTh <= minTh) return;
    	
    	minAngle = minTh;
    	maxAngle = maxTh;
    }
    
    public void setValueBounds(float min, float max)
    {
    	if (max < min) return;
    	
    	minVal = min;
    	maxVal = max;
    }
    
    public void setCenterValue(float cv)
    {
    	centerVal = cv;
    }
    
    public float getMinVal() { return minVal; }
    public float getMaxVal() { return maxVal; }
    public float getcenterVal() { return centerVal; }
    
    public void setValue(float v)
    {
    	val = v;
    	angle = val2Angle(val);
    	invalidate();
    }
    
	public float getValue()
    {
    	return val;
    }
    
    private float getTheta(float x, float y)
	{
		Drawable d = getDrawable();
		if (d == null) return 0;
		
        float width = d.getMinimumWidth();//r.width();
        float height = d.getMinimumHeight();//r.height();
        
 //       Log.d("pot", Float.toString(width)+", "+Float.toString(height)+": "+Float.toString(x)+", "+Float.toString(y));
      
		float sx = x - (width / 2.0f);
		float sy = y - (height / 2.0f);
		 
		float theta = (float)Math.atan2( sy, sx );
		
		final float rad2deg = (float)(180.0/Math.PI);
		float theta2 = theta*rad2deg;
		theta2 = (theta2 < 0) ? theta2 + 360.0f : theta2;
		return theta2;
	}
    
    protected long lastDt=0;
	protected long doubleTapTime=500;

	public boolean isTweaking()
	{
		return tweak;
	}
	
	public void initialize(Context context, AttributeSet attrs)
	{
		tweak = false;
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				int actionCode = action & MotionEvent.ACTION_MASK;
//				Log.d("mouse", String.format("%d %d", action, actionCode));
				if (actionCode == MotionEvent.ACTION_POINTER_DOWN ||
						actionCode == MotionEvent.ACTION_DOWN) {
					tweak = true;
					float x = event.getX(0);
					float y = event.getY(0);
					theta_old = getTheta(x, y);
				} else if (actionCode == MotionEvent.ACTION_UP)  {
					tweak = false;
					long dt = event.getDownTime();
					long et = event.getEventTime();
					if (dt - lastDt < doubleTapTime) {
						doDoubleTap();
					}
					lastDt = dt;
				} else if (actionCode == MotionEvent.ACTION_MOVE) {
					invalidate();
				
					float x = event.getX(0);
					float y = event.getY(0);
				
					Drawable d = getDrawable();
			        float width = d.getMinimumWidth();//r.width();
			        float height = d.getMinimumHeight();//r.height();
					float sx = x - (width / 2.0f);
					float sy = y - (height / 2.0f);
					
					float theta = getTheta(x, y);
					float r = (float)Math.sqrt( sx*sx + sy*sy);
					float delta_theta = theta - theta_old;
					if (Math.abs(delta_theta) < 0.001) {
						delta_theta = 0;
					}
				
					theta_old = theta;
				
					int direction = (delta_theta > 0) ? 1 :  (delta_theta <  0)? -1 : 0;
					angle += direction*(width+height)/r;
				
//					Log.d("mouse", String.format("x %g y %g delt %g dir %d ang %g", x, y, delta_theta, direction, angle));
					
					if (angle < minAngle) angle = minAngle;
					else if (angle > maxAngle) angle = maxAngle;
					
					val = angle2Val(angle);
					
					notifyListener(getValue());
				}
				return true;
			}		

			private void doDoubleTap()
			{
				setValue(centerVal);
				notifyListener(getValue());
			}		
		});
	}

	protected float angle2Val(float a)
	{
		return minVal+ ((a-minAngle)/(maxAngle-minAngle))*(maxVal-minVal);
	}

	protected float val2Angle(float v) {
		return minAngle+((maxAngle-minAngle)*(v-minVal)/(maxVal-minVal));
	}

	private void notifyListener(float v)
	{
		if (listener != null)
			listener.onKnobChanged(v);
	}
	
	protected void onDraw(Canvas c)
	{
		Drawable d = getDrawable();
		if (d != null) {
			c.rotate(angle,c.getWidth()/2, c.getHeight()/2);   
		}
		super.onDraw(c);
	}
    
}
