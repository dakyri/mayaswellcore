#ifndef _DELAY
#define _DELAY

#include "AudioSignal.h"
#include "FloatBuffer.h"
#include <android/log.h>

const unsigned		kAudioBufferSize=8192;

class Delay {
public:
	 Delay(int nc, float maxDelaySecs);
	 virtual ~Delay();

	void reset();

	void initialize();
	void release();

	void setBufsize(int bufsize);

#if __cplusplus >= 201103L
	static constexpr float kMinDelay = 0.000001;
#else
	static const float kMinDelay = 0.000001;
#endif
protected:
	void write(float * outSig, int outSigLen, float volume, bool inc=true);
	void overWrite(float * outSig, int outSigLen, float volume);
	void read(float * outSig, int outSigLen, double delaySecs);

	float			maxDelay;
	unsigned int	length;
	short			nChannel;
	unsigned int	inputIndex;
	FloatBuffer		delayBuffer;
	float*			buffPointer;	//multiple of vLen with extended guard
};

class EchoDelay: public Delay {
public:
	EchoDelay(int nc, float maxDelaySecs);

	int	 apply(float *sig, int nFrames);
	void setParameters(float v, float t, float fb);

	void setGain(float v);
	void setTime(float v);
	void setFeedback(float v);

protected:
	float			dlTime;
	float			dlEchoVolume;
	float			dlFeedback;
};


class FBDelay: public Delay {
public:
	FBDelay(int nc, float maxDelaySecs);

	int	 apply(float *sig, int nFrames);
	void setParameters(float t, float fb);

	void setGain(float v);
	void setTime(float v);

protected:
	float			dlTime;
	float			dlFeedback;
};

class SideChainDelay: public Delay {
public:
	SideChainDelay(int nc, float maxDelaySecs);

	int	 apply(float *sig, float *sideSig, int nFrames);
	void setParameters(float v, float t, float fb);

	void setGain(float v);
	void setTime(float v);
	void setFeedback(float v);

protected:
	float			dlTime;
	float			dlEchoVolume;
	float			dlFeedback;

};


inline void
Delay::initialize()
{
	delayBuffer.acquire(buffPointer);
	reset();
}

inline void
Delay::release()
{
	delayBuffer.release();
}

inline void
Delay::reset()
{
	for(unsigned long i=0;i<=length;i++)
		buffPointer[i]=0.;
}

inline void
Delay::write(float * inSig, int inSigLen, float gain, bool inc)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "Delay", "write %d %d <- %g", inputIndex, length, inSig[0]);
	int cnt = inSigLen;
	while (cnt > 0) {
		int cnti = (inputIndex+cnt > length? length - inputIndex : cnt);
		cnt -= cnti;
		for(unsigned long i=0;i<cnti;i++) {
			buffPointer[inputIndex+i] = gain*inSig[i];
		}

		//write guard point
		if(inputIndex==0) {
			for (short i=0; i<nChannel; i++)
				buffPointer[length+i]=buffPointer[0+i];
		}
		if (inc) {
			inputIndex+=cnti;
			if(inputIndex>=length) {
				inputIndex=0;
			}
		}
	}
}


inline void
Delay::overWrite(float * inSig, int inSigLen, float gain)
{
	int cnt = inSigLen;
	while (cnt > 0) {
		int cnti = (inputIndex+cnt > length? length - inputIndex : cnt);
		cnt -= cnti;
		for(unsigned long i=0;i<cnti;i++) {
			buffPointer[inputIndex+i] += gain*inSig[i];
		}

		//write guard point
		if(inputIndex==0) {
			for (short i=0; i<nChannel; i++)
				buffPointer[length+i]=buffPointer[0+i];
		}
		inputIndex+=cnti;
		if(inputIndex>=length)
			inputIndex=0;
	}
}


inline void
Delay::read(float *outSig, int outSigLen, double delaySecs)
{
	double fReadIndex = inputIndex - nChannel* delaySecs * signalSampleRate;
	while(fReadIndex<0)
		fReadIndex+=length;

	unsigned long dReadIndex = (((unsigned long) fReadIndex)/nChannel)*nChannel;
	double frac = fReadIndex-dReadIndex;
//	__android_log_print(ANDROID_LOG_DEBUG, "Delay", "read in %d find %g frac %g fre %ld -> %g", inputIndex, fReadIndex, frac, dReadIndex, buffPointer[dReadIndex]);
	for(unsigned long i=0;i<outSigLen;i++) {
		outSig[i]=buffPointer[dReadIndex] +
				( buffPointer[dReadIndex+nChannel]-buffPointer[dReadIndex] ) *frac;

		dReadIndex++;
		if(dReadIndex>=length) {
			dReadIndex-=length;
		}
	}
}

inline int
EchoDelay::apply(float *outSig, int nFrames)
{
	float	tempSig1[nFrames * nChannel];
	long	outSigLen = nFrames * nChannel;

	read(tempSig1, outSigLen, dlTime);		// (60./ mySeq.seq.tempo) / dtFactor);
	write(outSig, outSigLen, 1-dlFeedback, false);

	for (short i=0; i<outSigLen; i++) {
		outSig[i] += tempSig1[i] * dlEchoVolume;
	}
	overWrite(outSig, outSigLen, dlFeedback);

//	__android_log_print(ANDROID_LOG_DEBUG, "EchoDelay", "done %d %d <- %d", inputIndex, length, nFrames);
	return nFrames;
}

inline int
FBDelay::apply(float *sig, int nFrames)
{
	float	tempLrSig[nChannel*nFrames];
	int			outLen = nChannel*nFrames;
	if (dlFeedback > 0) {
//		__android_log_print(ANDROID_LOG_DEBUG, "Delay", "applying %g %g outlen %d nf %d ii %d nbu %ld", dlTime, dlFeedback, outLen, nFrames, inputIndex, delayBuffer.getLength());
		read(tempLrSig, outLen, dlTime);
		for(short i=0; i<outLen; i++) {
			sig[i] += tempLrSig[i]*dlFeedback;
		}
	}
	write(sig, outLen, 1);

	return nFrames;
}


inline int
SideChainDelay::apply(float *outSig, float *sideSig, int nFrames)
{
	float	tempSig1[nFrames * nChannel];
	long	outSigLen = nFrames * nChannel;

	read(tempSig1, outSigLen, dlTime);		// (60./ mySeq.seq.tempo) / dtFactor);
	write(outSig, outSigLen, 1-dlFeedback, false);

	for (short i=0; i<outSigLen; i++) {
		sideSig[i] += tempSig1[i] * dlEchoVolume;
	}
	overWrite(sideSig, outSigLen, dlFeedback);

	return nFrames;
}

#endif
	
