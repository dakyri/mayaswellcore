/*
 * Effect.cpp
 *
 *  Created on: Dec 1, 2013
 *      Author: dak
 */
#include "Effect.h"
#include "com_mayaswell_audio_FX.h"

Effect::Effect(TimeSource &t, bool lock):
	tempoLock(lock),
	timer(t),
	echoDelay(2, 2),
	fbDelay(2,2)
{
	type = com_mayaswell_audio_FX_UNITY;
	hasTail = false;
	for (int i=0; i<kMaxParams; i++) {
		actualParams.push_back(0);
	}
}

void
Effect::setTempoLock(bool b)
{
	tempoLock = b;
}

float co30(float f, float e)
{
	float sweepMin = 195. * exp((f) *1.38);//2.15);
	return sweepMin + (.8 + e* 19.) * sweepMin;

}
bool
Effect::setType(int ntype, int nparam, float* param)
{
	__android_log_print(ANDROID_LOG_DEBUG, "Effect::setType", "got %d %d %x was %d", ntype, nparam, ((unsigned int)param), type);
	if (ntype == type) {
		return true;
	}
	switch (type) {
	case com_mayaswell_audio_FX_UNITY: {
		break;
	}
	case com_mayaswell_audio_FX_SG_LOW:
	case com_mayaswell_audio_FX_SG_HIGH:
	case com_mayaswell_audio_FX_SG_BAND: {
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_LOW:
	case com_mayaswell_audio_FX_MOOG1_HIGH:
	case com_mayaswell_audio_FX_MOOG1_BAND: {
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_LOW:
	case com_mayaswell_audio_FX_MOOG2_HIGH:
	case com_mayaswell_audio_FX_MOOG2_BAND: {
		break;
	}
	case com_mayaswell_audio_FX_FORMANT: {
		break;
	}
	case com_mayaswell_audio_FX_FBDELAY: {
		fbDelay.release();
		break;
	}
	case com_mayaswell_audio_FX_ECHO: {
		echoDelay.release();
		break;
	}
	case com_mayaswell_audio_FX_ECHO_SIDECHAIN: {
		break;
	}
	case com_mayaswell_audio_FX_DISTORTION:
	case com_mayaswell_audio_FX_RINGMOD: {
		break;
	}

	case com_mayaswell_audio_FX_FLANGE: {
		flange.release();
		break;
	}

	case com_mayaswell_audio_FX_CHORUS: {
		chorus.release();
		break;
	}

	case com_mayaswell_audio_FX_PHASER: {
		phaser.release();
		break;
	}

	}

	hasTail = false;
	__android_log_print(ANDROID_LOG_DEBUG, "Effect::setType", "cleared old resources %d %d %x", ntype, nparam, ((unsigned int)param));
	switch (ntype)
	{
	case com_mayaswell_audio_FX_UNITY: {
		break;
	}
	case com_mayaswell_audio_FX_SG_LOW:
	case com_mayaswell_audio_FX_SG_HIGH:
	case com_mayaswell_audio_FX_SG_BAND: {
		sgL.reset();
		if (nparam >= 2) {
			sgL.setCutoff(co30(param[0], 0));
			sgL.setResonance(param[1]);
		}
		sgR = sgL;
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_LOW:
	case com_mayaswell_audio_FX_MOOG1_HIGH:
	case com_mayaswell_audio_FX_MOOG1_BAND: {
		moog1L.reset();
		if (nparam >= 2) {
			moog1L.setCoefficients(param[0], param[1]);
		}
		moog1R = moog1L;
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_LOW:
	case com_mayaswell_audio_FX_MOOG2_HIGH:
	case com_mayaswell_audio_FX_MOOG2_BAND: {
		moog2L.reset();
		if (nparam >= 2) {
			moog2L.setCoefficients(param[0], param[1]);
		}
		moog2R = moog2L;
		break;
	}
	case com_mayaswell_audio_FX_FORMANT: {
		formantL.reset();
		if (nparam >= 1) {
			formantL.setVowel(param[0]);
		}
		formantR = formantL;
		break;
	}
	case com_mayaswell_audio_FX_FBDELAY: {
		if (nparam >= 2) {
			fbDelay.setParameters(param[1], param[0]);
		}
		fbDelay.initialize();
		hasTail = true;
		break;
	}
	case com_mayaswell_audio_FX_ECHO: {
		if (nparam >= 3) {
			echoDelay.setParameters(param[1], param[0], param[2]);
		}
		echoDelay.initialize();
		hasTail = true;
		break;
	}
	case com_mayaswell_audio_FX_ECHO_SIDECHAIN: {
		break;
	}

	case com_mayaswell_audio_FX_DISTORTION: {
		if (nparam >= 2) {
			distortion.setParameters(param[0], param[1]);
		}
		break;
	}

	case com_mayaswell_audio_FX_RINGMOD: {
		if (nparam >= 1) {
			ringMod.setFrequency(param[0]);
		}
		break;
	}

	case com_mayaswell_audio_FX_FLANGE: {
		if (nparam >= 5) {
			flange.setParameters(param[0], param[1], param[2], param[3], param[4]);
		}
		flange.initialize();
		break;
	}
	case com_mayaswell_audio_FX_CHORUS: {
		if (nparam >= 5) {
			chorus.setParameters(param[0], param[1], param[2], param[3], param[4]);
		}
		chorus.initialize();
		break;
	}
	case com_mayaswell_audio_FX_PHASER: {
		if (nparam >= 5) {
			phaser.setParameters(param[0], param[1], param[2], param[3], param[4]);
		}
		phaser.initialize();
		break;
	}

	}

	__android_log_print(ANDROID_LOG_DEBUG, "Effect::setType", "done the lot %d %d %x", ntype, nparam, ((unsigned int)param));

	type = ntype;
	return true;
}
void
Effect::setEnable(bool nenable)
{
	enable = nenable;
}

bool
Effect:: setParam(int param, float value, bool cacheValue)
{
	if (cacheValue && param >= 0 && param < actualParams.size()) {
		actualParams[param] = value;
	}
	switch (type)
	{
	case com_mayaswell_audio_FX_UNITY: {
		break;
	}
	case com_mayaswell_audio_FX_SG_LOW:
	case com_mayaswell_audio_FX_SG_HIGH:
	case com_mayaswell_audio_FX_SG_BAND: {
		switch (param) {
		case 0:
			sgL.setCutoff(co30(value, 0));
			break;
		case 1:
			sgL.setResonance(value);
			break;
		default:
			return false;
		}
		sgR = sgL;
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_LOW:
	case com_mayaswell_audio_FX_MOOG1_HIGH:
	case com_mayaswell_audio_FX_MOOG1_BAND: {
		switch (param) {
		case 0:
			moog1L.setCoefficients(value, moog1L.resonance);
			break;
		case 1:
			moog1L.setCoefficients(moog1L.frequency, value);
			break;
		default:
			return false;
		}
		moog1R = moog1L;
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_LOW:
	case com_mayaswell_audio_FX_MOOG2_HIGH:
	case com_mayaswell_audio_FX_MOOG2_BAND: {
		switch (param) {
		case 0:
			moog2L.setCoefficients(value, moog2L.resonance);
			break;
		case 1:
			moog2L.setCoefficients(moog2L.frequency, value);
			break;
		default:
			return false;
		}
		moog2R = moog2L;
		break;
	}
	case com_mayaswell_audio_FX_FORMANT: {
		if (param == 0) {
			formantL.setVowel(value);
			formantR = formantL;
		} else {
			return false;
		}
		break;
	}
	case com_mayaswell_audio_FX_FBDELAY: {
		switch (param) {
		case 0:
			if (tempoLock) {
				value *= timer.secsPerBeat;
			}
			fbDelay.setTime(value);
			break;
		case 1:
			fbDelay.setGain(value);
			break;
		default:
			return false;
		}
		break;
	}
	case com_mayaswell_audio_FX_ECHO: {
		switch (param) {
		case 0:
			if (tempoLock) {
				value *= timer.secsPerBeat;
			}
			echoDelay.setTime(value);
			break;
		case 1:
			echoDelay.setGain(value);
			break;
		case 2:
			echoDelay.setFeedback(value);
			break;
		default:
			return false;
		}
		break;
	}
	case com_mayaswell_audio_FX_RINGMOD: {
		switch (param) {
		case 0:
			ringMod.setFrequency(value);
			break;
		default:
			return false;
		}
		break;
	}

	case com_mayaswell_audio_FX_DISTORTION: {
		switch (param) {
		case 0:
			distortion.setParameters(value, distortion.tubeAmount);
			break;
		case 1:
			distortion.setParameters(distortion.dstAmount, value);
			break;
		default:
			return false;
		}
		break;
	}

	case com_mayaswell_audio_FX_FLANGE: {
		switch (param) {
		case 0:
			flange.setRate(value);
			break;
		case 1:
			flange.setWidth(value);
			break;
		case 2:
			flange.setFeedback(value);
			break;
		case 3:
			flange.setDelay(value);
			break;
		case 4:
			flange.setMix(value);
			break;
		case 5:
			break;
		default:
			return false;
		}
		break;
	}
	case com_mayaswell_audio_FX_CHORUS: {
		switch (param) {
		case 0:
			chorus.setRate(value);
			break;
		case 1:
			chorus.setWidth(value);
			break;
		case 2:
			chorus.setFeedback(value);
			break;
		case 3:
			chorus.setDelay(value);
			break;
		case 4:
			chorus.setMix(value);
			break;
		case 5:
			break;
		default:
			return false;
		}
		break;
	}
	case com_mayaswell_audio_FX_PHASER: {
		switch (param) {
		case 0:
			phaser.setRate(value);
			break;
		case 1:
			phaser.setWidth(value);
			break;
		case 2:
			phaser.setFeedback(value);
			break;
		case 3:
			phaser.setStages(value);
			break;
		case 4:
			phaser.setMix(value);
			break;
		case 5:
			break;
		default:
			return false;
		}
		break;
	}

	}

	return true;
}

bool
Effect::setParams(int nparam, float* value)
{
	for (int i=0; i<nparam; i++) {
		if (!setParam(i, value[i])) {
			return false;
		}
	}
	return true;
}

void
Effect::setBufsize(int bufsize)
{
	echoDelay.setBufsize(bufsize);
	fbDelay.setBufsize(bufsize);
}

float
Effect::getParam(int p)
{
	if (p >= 0 && p < actualParams.size()) {
		return actualParams[p];
	}
	return 0;
}

int
Effect::nParams()
{
	switch (type) {
	case com_mayaswell_audio_FX_UNITY: {
		return 0;
	}
	case com_mayaswell_audio_FX_SG_LOW:
	case com_mayaswell_audio_FX_SG_HIGH:
	case com_mayaswell_audio_FX_SG_BAND:
	case com_mayaswell_audio_FX_MOOG1_LOW:
	case com_mayaswell_audio_FX_MOOG1_HIGH:
	case com_mayaswell_audio_FX_MOOG1_BAND:
	case com_mayaswell_audio_FX_MOOG2_LOW:
	case com_mayaswell_audio_FX_MOOG2_HIGH:
	case com_mayaswell_audio_FX_MOOG2_BAND: {
		return 2;
	}
	case com_mayaswell_audio_FX_DISTORTION: {
		return 2;
	}
	case com_mayaswell_audio_FX_RINGMOD:
	case com_mayaswell_audio_FX_FORMANT: {
		return 1;
	}
	case com_mayaswell_audio_FX_FBDELAY: {
		return 2;
	}
	case com_mayaswell_audio_FX_ECHO: {
		return 3;
	}
	case com_mayaswell_audio_FX_ECHO_SIDECHAIN: {
		return 3;
	}
	case com_mayaswell_audio_FX_FLANGE: {
		return 5;
	}
	return 2;
	}
}
