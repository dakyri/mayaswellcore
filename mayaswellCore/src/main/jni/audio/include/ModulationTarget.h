/*
 * Modulator.h
 *
 *  Created on: Oct 22, 2013
 *      Author: dak
 */

#ifndef MODULATORTARGET_H_
#define MODULATORTARGET_H_

#include "com_mayaswell_audio_Modulator.h"
#include "TimeSource.h"

class ModulationTarget
{
public:
	ModulationTarget(int t=0, float a=0)
	{
		target = t;
		amount = a;
	}

	int			target;
	float		amount;
};

#endif /* MODULATORTARGET_H_ */
