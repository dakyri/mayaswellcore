#ifndef _MOOGVCF
#define _MOOGVCF

#include <math.h>
#include "AudioSignal.h"

// Moog VCF variation 1... sourced musicdsp.org
// The second "q =" line previously used exp() - I'm not sure if what I've 
// done is any faster, but this line needs playing with anyway as it controls 
// which frequencies will self-oscillate. I 
// think it could be tweaked to sound better than it currently does. 
//
// Highpass / Bandpass : 
//
// They are only 6dB/oct, but still seem musically useful - the 'fruity' sound 
// of the 24dB/oct lowpass is retained.
//
// Moog 24 dB/oct resonant lowpass VCF 
// References: CSound source code, Stilson/Smith CCRMA paper. 
// Modified by paul.kellett@maxim.abel.co.uk July 2000 

// Lowpass �output: b4
// Highpass output: in - b4;
// Bandpass output: 3.0f * (b3 - b4);

class MoogVCF1
{
public:
	inline MoogVCF1()
	{
		reset();
		frequency = resonance = -1;
		setCoefficients(0,0);
	}
	
	inline void reset()
	{
		b0 = b1 = b2 = b3 = b4 = 0;
	}
	
// Set coefficients given frequency & resonance [0.0...1.0] 
/*
k = 3.6*freq - 1.6*freq*freq -freq; //(Empirical tunning)
p = (k+1)*0.5;
q = res*(e^((1-p)*1.386249);
*/
	inline void setCoefficients(float freq, float res)
	{
		if (freq != frequency || res != resonance) {
			if (freq > 1) {
				freq = 1;
			} else if (freq < 0) {
				freq = 0;
			}
			frequency = freq;
			resonance = res;
		
			q = 1.0 - frequency; 
			p = frequency + 0.8 * frequency * q; 
			k = p + p - 1.0; 
			q = resonance * (1.0 + 0.5 * q * (1.0 - q + 5.6 * q * q)); 
		}
	}
	
	inline void filter(Sample in) // sample in range [-1..1]
	{
		in -= q * b4;							//feedback 
		t1 = b1;	b1 = (in + b0) * p - b1 * k; 
		t2 = b2;	b2 = (b1 + t1) * p - b2 * k; 
		t1 = b3;	b3 = (b2 + t2) * p - b3 * k;
					b4 = (b3 + t1) * p - b4 * k; 
		b4 = b4 - b4 * b4 * b4 * 0.166667;	//clipping 
		b0 = in; 
	}
	
	inline Sample lowPass(Sample in)
	{
		filter(in);
		return b4;
	}

	inline Sample highPass(Sample in)
	{
		filter(in);
		return in - b4;
	}

	inline Sample bandPass(Sample in)
	{
		filter(in);
		return 3.0 * (b3 - b4);
	}
	
	inline void operator=(MoogVCF1&flt)
	{
		k = flt.k;
		p = flt.p;
		q = flt.q;
	}

	float	resonance, frequency;
	float	k, p, q;			//filter coefficients 
	float	b0, b1, b2, b3, b4;	//filter buffers (beware denormals!) 
	float	t1, t2;				//temporary buffers
};


// Moog VCF variation 2... sourced musicdsp.org
// Type : 24db resonant lowpass
// References :CSound source code, Stilson/Smith CCRMA paper., Timo 
// Tossavainen (?) version
//
// in[x] and out[x] are member variables, init to 0.0 the controls: 
//
//fc = cutoff, nearly linear [0,1] -> [0, fs/2] 
// res = resonance [0, 4] -> [no resonance, self-oscillation]
class MoogVCF2
{
public:
	inline MoogVCF2()
	{
		reset();
		frequency = resonance = -1;
		setCoefficients(0,0);
	}
	inline void reset()
	{
		out1 = out2 = out3 = out4 = 0;
		in1 = in2 = in3 = in4 = 0;
	}
	
	inline void calculateFB()
	{
		fb = 4.0*resonance * (1.0 - 0.15 * f * f); 
	}
	inline void calculateF()
	{
		f = frequency * 1.16;
	}
	inline void setResonance(float res)
	{
		if (resonance != res) {
			resonance = res;
			calculateFB();
		}
	}
	inline void setFrequency(float freq)
	{
		if (frequency != freq) {
			if (freq > 1) {
				freq = 1;
			} else if (freq < 0) {
				freq = 0;
			}
			frequency = freq;
			calculateF();
			calculateFB();
		}
	}
	inline void setCoefficients(float freq, float res)
	{
		if (frequency != freq || resonance != res) {
			if (freq > 1) {
				freq = 1;
			} else if (freq < 0) {
				freq = 0;
			}
			frequency = freq;
			resonance = res;
			calculateF();
			calculateFB();
		}
	}
			
	inline void operator=(MoogVCF2&flt)
	{
		f = flt.f;
		fb = flt.fb;
	}

	inline void filter(Sample in)
	{ 
		in -= out4 * fb; 
		in *= 0.35013 * (f*f)*(f*f); 
		out1 = in + 0.3 * in1 + (1 - f) * out1;		// Pole 1
		clip(out1);
		in1 = in;
		out2 = out1 + 0.3 * in2 + (1 - f) * out2;	// Pole 2
		clip(out2);
		in2 = out1;
		out3 = out2 + 0.3 * in3 + (1 - f) * out3;	// Pole 3
		clip(out3);
		in3 = out2;
		out4 = out3 + 0.3 * in4 + (1 - f) * out4;	// Pole 4
		clip(out4);
		in4 = out3;
	}
	
	inline Sample lowPass(Sample in)
	{
		filter(in);
		return out4;
	}

	inline Sample highPass(Sample in)
	{
		filter(in);
		return in - out4;
	}

	inline Sample bandPass(Sample in)
	{
		filter(in);
		return 3.0*(out3-out4);
	}

	
	float	frequency, resonance;
	float	f, fb;
	float	out1, out2, out3, out4;
	float	in1, in2, in3, in4;
};

#endif
