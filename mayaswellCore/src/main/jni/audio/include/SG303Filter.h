#ifndef __303FILTER
#define __303FILTER

#include <math.h>
#include "AudioSignal.h"
#include "FunctionTable.h"
#include "Lookup.h"
#include <android/log.h>

#define FUNCTION_TABLE_SIN

class SG303Filter
{
public:
						SG303Filter(float cf = 100., float rez = 0.);
						~SG303Filter();
	
	SG303Filter&		reset();	//reset filter state
	SG303Filter& 		setCutoff(float cutOffFreq);		//Hz
	SG303Filter&		setResonance(float resonance);		//0. to 1.+
	
	void 				filter(const Sample inSamp);
	Sample				highPass(Sample);
	Sample				lowPass(Sample);
	Sample				bandPass(Sample);
#ifdef FUNCTION_TABLE_SIN
	WrappingLookup		*ftSin;
#endif

	void operator=(SG303Filter &flt);
//private:

	float				cutoff;
	
	float 				f1a,f1b,q1;			//filter coefficients
	float 				d1a,d2a,d1b,d2b;	//filter delay samples
	float				lpa, lpb, hp, bp;
};


// inline members
inline void
SG303Filter::operator=(SG303Filter &flt)
{
	cutoff = flt.cutoff;
	f1a = flt.f1a;
	f1b = flt.f1b;
	q1 = flt.q1;
}

inline SG303Filter&
SG303Filter::setCutoff(float f)
{
	if (cutoff != f) {
		cutoff = f;
#ifdef FUNCTION_TABLE_SIN
		f1a = 2. * (*ftSin)[M_PI * (f/signalSampleRate)];
		f1b = 2. * (*ftSin)[M_PI * ((f*1.01)/signalSampleRate)];
#else
		f1a = 2. * sin(M_PI * (f/signalSampleRate) );
		f1b = 2. * sin(M_PI * ((f*1.01)/signalSampleRate) );
#endif
//		__android_log_print(ANDROID_LOG_DEBUG, "SG303::setCutoff", "sgl %g %g %g", f, f1a, f1b);
	}
	return *this;
}

inline SG303Filter&
SG303Filter::setResonance(float r)
{	
	//resonance squared to accentuate control of low q values
	//(similar mapping to 303)
	q1 = 1./ (1. + (r*r) *12.);
	return *this;
}

#define CF_FT_LENGTH 1024

inline
SG303Filter::SG303Filter(float cf, float rez)
	:d1a(0.), d2a(0.), d1b(0.), d2b(0.)
{
#ifdef FUNCTION_TABLE_SIN
	FunctionTable 		*ft = new FunctionTable(CF_FT_LENGTH);
	
	for(short i=0;i<=CF_FT_LENGTH;i++) {
		(*ft)[i]=sin(2*M_PI*((float)i)/((float)CF_FT_LENGTH));
	}

	ftSin = new WrappingLookup(ft,0,2*M_PI);
#endif
	cutoff = 0;
	setCutoff(cf);
	setResonance(rez);
}

inline SG303Filter::~SG303Filter() {}

inline
SG303Filter& SG303Filter::reset()
{	
	d1a=d2a=d1b=d2b=0;
	return *this;
}

inline void
SG303Filter::filter(const Sample inSamp)
{	
	lpa = f1a*d1a + d2a;
	hp = inSamp - lpa - q1*d1a;
	bp = f1a*hp + d1a;
	d2a = lpa;
	d1a = bp;
	
	clip(d2a);
	clip(d1a);
	
	lpb = f1b*d1b + d2b;
	hp = lpa - lpb - q1*d1b;
	bp = f1b*hp + d1b;
	d2b = lpb;
	d1b = bp;
	
	clip(d2b);
	clip(d1b);
}

inline Sample
SG303Filter::highPass(Sample in)
{
	filter(in);
	return hp;
}

inline Sample
SG303Filter::bandPass(Sample in)
{
	filter(in);
	return bp;
}

inline Sample
SG303Filter::lowPass(Sample in)
{
	filter(in);
	return lpb;
}

#endif
