package com.mayaswell.audio;


public class FXState {
	private FX fx;
	private boolean enable;
	private float [] params=null;
	
	public FXState(FX f, boolean e)
	{
		setFX(f, e);
	}
	
	private void setFX(FX f, boolean e) {
		fx = f;
		enable = e;
		params = new float[f.params().size()];
		for (int i=0; i<params.length; i++) {
			params[i] = f.params().get(i).getCenterValue();
		}
	}
	
	public FX getFx()
	{
		return fx;
	}
	
	public float param(int i)
	{
		if (params == null) return 0;
		if (i >= 0 && i <params.length) {
			return params[i];
		}
		return 0;
	}
	
	public void param(int i, float f)
	{
		if (params == null) return;
		if (i >= 0 && i <params.length) {
			params[i] = f;
		}
	}
	
	public float [] getParams() {
		return params;
	}
	
	public int countParams() {
		return params!=null?params.length:0;
	}

	public FXState clone()
	{
		FXState fs = new FXState(fx, enable);
		int i=0;
		if (params != null) {
			for (float v: params) {
				fs.param(i++, v);
			}
		}	
		return fs;
	}

	public void setType(FX f)
	{
		if (f == null) {
			f = FX.unityFX;
		}
		fx = f;
		if (f.countParams() == 0) {
			params = null;
		} else {
			params = new float[f.countParams()];
			for (FXParam fp: f.params()) {
				params[fp.getId()] = fp.getCenterValue();
			}
		}
	}

	public void setEnable(boolean enable2)
	{
		enable = enable2;
	}

	public boolean getEnable()
	{
		return enable;
	}

}
