package com.mayaswell.audio;

public enum PadMode {
	NORMAL, TOGGLE, MANUAL;
	@Override
	public String toString()
	{
		switch (this) {
		case NORMAL: return "normal";
		case TOGGLE: return "toggle";
		case MANUAL: return "manual";
		}
		return super.toString();
	}
	
	public static PadMode parse(String s)
	{
		if (s.equals("toggle")) return TOGGLE;
		if (s.equals("manual")) return MANUAL;
		return NORMAL;
	}

}

