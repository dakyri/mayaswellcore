#ifndef _FUNCTIONTABLE
#define _FUNCTIONTABLE

class FunctionTable
{
public:
					FunctionTable( unsigned long len);
					~FunctionTable();
				
	float&			operator []	(unsigned long i) const;
	
	unsigned long	Length();		//returns length minus 1
private:
	unsigned long	length;			//length minus 1
	float			*data;
};


//inline members

inline FunctionTable::FunctionTable( unsigned long len) :length(len)
{
	data = new float[length+1];
}

inline FunctionTable::~FunctionTable()
{
	delete [] data;
}

inline float& FunctionTable::operator []	(unsigned long i) const
{
	return data[i];
}

inline unsigned long FunctionTable::Length()
{
	return length;
}

#endif
