/*
 * LFO.h
 *
 *  Created on: Oct 22, 2013
 *      Author: dak
 */

#ifndef LFO_H_
#define LFO_H_
#include <android/log.h>

#include <vector>

#include "ModulationTarget.h"
#include "TableOscilator.h"

const int wavetableLength = 1024;

extern FunctionTable sineWavetable;
extern FunctionTable squareWavetable;
extern FunctionTable posExpWavetable;
extern FunctionTable negExpWavetable;
extern FunctionTable posSawWavetable;
extern FunctionTable negSawWavetable;

class LFO: public TableOscilator
{
public:
	LFO(int ntgt, TimeSource &t);
	~LFO();

	int waveform;
	unsigned char resetMode;
	unsigned char lock;
	float rate;
	float phase;
	int nActiveSends;
	TimeSource &timer;

	std::vector<ModulationTarget> target;

	void set(int wf, unsigned char res, unsigned char l, float r, float ph);
	void setWave(int wf);
	void setPhase(float ph);
	void setRate(float r, unsigned char l);
	void setReset(unsigned char r);
	void resetRate();
	void modulate(float mod);

	void setActiveSendCount(int n);
	void setActiveSends();

	void reset();
#if __cplusplus >= 201103L
	static constexpr float logRmin = -5;
#else
	static const float logRmin = -5;
#endif
};

class LFOHost
{
public:
	LFOHost();

	bool setLFO(int which, unsigned char wf, unsigned char reset, bool lock, float rate, float phs);
	bool setNLFO(int n);
	bool setNLFOTarget(int which, int n);
	bool setLFOTarget(int which, int whichn, int cc, float amt);
	bool setLFOAmount(int which, int whichn, float amt);
	bool setLFOLock(int which, bool lck);
	bool setLFOReset(int which, unsigned char rst);
	bool setLFOWave(int which, unsigned char wf);
	bool setLFORate(int which, float wf);
	bool setLFOPhase(int which, float wf);
	bool setWreckerLFO(int mxi);
	bool resetLFO(int mxi);
	float getLFO(int mxi);

	int hasLFO(int tgt);

protected:
	std::vector<LFO>		lfo;
	int						nActiveLFO;
};

inline
LFO::LFO(int ntgt, TimeSource &t):
	timer(t)
{
	setFunkTable(&sineWavetable);
	waveform = com_mayaswell_audio_Modulator_WF_SIN;
	resetMode = com_mayaswell_audio_Modulator_RESET_ONFIRE;
	lock = true;
	rate = 1;
	phase = 0;
	nActiveSends = 0;
	for (int i=0; i<ntgt; i++) {
		target.push_back(ModulationTarget());
	}
}

inline
LFO::~LFO()
{

}

inline void LFO::set(int wf, unsigned char res, unsigned char l, float r, float ph)
{
	setWave(wf);
	setPhase(ph);
	setRate(r, l);
	setReset(res);
}

inline void
LFO::setWave(int wf)
{
	if (wf != waveform) {
		switch (wf) {
			case com_mayaswell_audio_Modulator_WF_SIN: setFunkTable(&sineWavetable); break;
			case com_mayaswell_audio_Modulator_WF_EXP: setFunkTable(&posExpWavetable); break;
			case com_mayaswell_audio_Modulator_WF_NEGEXP: setFunkTable(&negExpWavetable); break;
			case com_mayaswell_audio_Modulator_WF_SQUARE: setFunkTable(&squareWavetable); break;
			case com_mayaswell_audio_Modulator_WF_SAW: setFunkTable(&posSawWavetable); break;
			case com_mayaswell_audio_Modulator_WF_NEGSAW: setFunkTable(&negSawWavetable); break;
		}
		waveform = wf;
	}

}

inline void
LFO::setReset(unsigned char r)
{
	resetMode = r;
}

inline void
LFO::setPhase(float ph)
{
	if (phase != ph) {
		phase = ph;
		TableOscilator::setPhase(phase);
	}
}

inline void
LFO::reset()
{
	TableOscilator::setPhase(phase);
}

/**
 * rate. measured in beats/seconds per cycle
 */
inline void
LFO::setRate(float r, unsigned char l)
{
	if (rate != r || lock != l) {
		lock = l;
		if (r == 0) r = 0.001;
		rate = r;
		if (lock) {
			setRelativeFrequency(timer.beatsPerSec*timer.secsPerCycle/rate);
		} else {
			setRelativeFrequency(timer.secsPerCycle/rate);
		}
	}
}

inline void
LFO::resetRate()
{
	if (lock) {
		setRelativeFrequency(timer.beatsPerSec*timer.secsPerCycle/rate);
	} else {
		setRelativeFrequency(timer.secsPerCycle/rate);
	}
}

inline void
LFO::modulate(float mod)
{
	if (lock) {
		setRelativeFrequency(mod*timer.beatsPerSec*timer.secsPerCycle/rate);
	} else {
		setRelativeFrequency(mod*timer.secsPerCycle/rate);
	}

}

inline void
LFO::setActiveSendCount(int n)
{
	nActiveSends = n;
}
#endif /* LFO_H_ */
