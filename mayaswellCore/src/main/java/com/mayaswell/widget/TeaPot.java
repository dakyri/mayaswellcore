package com.mayaswell.widget;

import com.mayaswell.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TeaPot extends RelativeLayout {
    public interface TeaPotListener {
    	void onValueChanged(float v);
    }

	private RotaryPotView pot = null;
	private TextView label = null;
	private EditText view = null;

    private TeaPotListener listener=null;
    
	protected String valueDisplayFormat = "%1.5f";

	private boolean notifyTextChange = true;
    
	public TeaPot(Context context)
	{
		super(context);
		setup(context, null);
	}

	public TeaPot(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public TeaPot(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		setValueDisplayPrecision(5);

		TypedArray tpAttrs = null;
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;

		int layoutId = R.layout.mw_tea_pot;
		if (attrs != null) {
			tpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TeaPot, 0, 0);
			layoutId = tpAttrs.getResourceId(R.styleable.TeaPot_layoutId, R.layout.mw_tea_pot);
		}
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (layoutId, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		view = (EditText)findViewById (R.id.mwCtlEditText);
		pot = (RotaryPotView)findViewById (R.id.mwCtlPot);	
		
		Log.d("teadpot", "setting up");
		
		if (view != null) {
			view.addTextChangedListener(new TextWatcher() {
				@Override
				public void afterTextChanged(Editable tv) {
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						if (s.length() > 0) {
							float v = Float.parseFloat(s.toString());
							if (v < pot.getMinVal()) v = pot.getMinVal();
							else if (v > pot.getMaxVal()) v = pot.getMaxVal();
							pot.setValue(v);
							if (notifyTextChange) notifyListener(v);
						}
					} catch (NumberFormatException e) {
					}
				}
			});
			view.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					Log.d("teapot", "got gocus "+hasFocus);
				}
				
			});
		}
		
		if (pot != null) {
			pot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
					view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
					notifyListener(v);
				}
			});
		}
		
		if (attrs != null) {
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);

			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				if (label != null) {
					label.setText(lbl);
				}
			}
			int displayPrecision = mwpAttrs.getInt(R.styleable.MWPot_displayPrecision, -1);
			if (displayPrecision >= 0) {
				setValueDisplayPrecision(displayPrecision);
			}
			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					float floatBoundsA[] = new float[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						floatBoundsA[i++] = Float.parseFloat(s);
					}
					setValueBounds(floatBoundsA[0], floatBoundsA[floatBoundsA.length-1]);
					setCenterValue(floatBoundsA[floatBoundsA.length-2]);
				}
			}
			int px = (int) mwvAttrs.getDimension(R.styleable.MWView_labelWidth, -1);
			if (px > 0 && label != null) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}

			px = (int) mwvAttrs.getDimension(R.styleable.MWView_viewWidth, -1);
			if (px > 0 && view != null) {
				LayoutParams params = (LayoutParams) view.getLayoutParams();
				params.width = px;
			}
		}

		if (tpAttrs != null) tpAttrs.recycle();
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}
	
	public void setValueDisplayPrecision(int i)
	{
		if (i < 1) i = 1; else if (i > 5) i = 5;
		valueDisplayFormat = "%1."+Integer.toString(i)+"f";
	}
	
	public void setValueBounds(float min, float max)
	{
		if (pot != null) 
			pot.setValueBounds(min, max);
	}
	
	public void setCenterValue(float cv)
	{
		if (pot != null)
			pot.setCenterValue(cv);
	}
	
    public void setPotListener(TeaPotListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(float v)
	{
		if (listener != null)
			listener.onValueChanged(v);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setValue(float v)
	{
		if (pot != null) {
			if (v < pot.getMinVal()) v = pot.getMinVal();
			else if (v > pot.getMaxVal()) v = pot.getMaxVal();
			pot.setValue(v);
		}
		if (view != null) {
			notifyTextChange  = false;
			view.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
			notifyTextChange = true;
		}
	}
	
	public float getValue()
	{
		return pot != null? pot.getValue(): 0;
	}
}
