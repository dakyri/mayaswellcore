/*
 * Oscilator.h
 *
 *  Created on: Dec 10, 2015
 *      Author: dak
 */

#ifndef OSCILATOR_H_
#define OSCILATOR_H_

#include "AudioSignal.h"
#include "SawtoothOscilator.h"
#include "SquareOscilator.h"
#include "TriangleOscilator.h"
#include "TableOscilator.h"

#include "com_mayaswell_audio_Controllable_OscilatorWave.h"

class Oscilator {
public:
	Oscilator();
	~Oscilator();

	Sample operator () (void);
	void operator() (Sample *, int n);

	void setWaveform(int type);
	void setShape(float shape);
	void reset();
	void setFrequency(float f);

protected:
	int type;
	float frequency;

	SquareOscilator square;
	SawtoothOscilator saw;
	TriangleOscilator triangle;
	TableOscilator table;
};


inline Sample
Oscilator::operator () (void) {
	switch (type) {
		case com_mayaswell_audio_Controllable_OscilatorWave_UNITY:
			break;
		case com_mayaswell_audio_Controllable_OscilatorWave_SQUARE:
			return square();
		case com_mayaswell_audio_Controllable_OscilatorWave_SAW:
			return saw();
		case com_mayaswell_audio_Controllable_OscilatorWave_TRIANGLE:
			return triangle();

		case com_mayaswell_audio_Controllable_OscilatorWave_SINE:
		case com_mayaswell_audio_Controllable_OscilatorWave_NOISE:
			return 0;
	}
	return 0;
}

inline void
Oscilator::operator () (Sample *p, int n) {
	switch (type) {
		case com_mayaswell_audio_Controllable_OscilatorWave_UNITY:
			break;
		case com_mayaswell_audio_Controllable_OscilatorWave_SQUARE:
			for (int i=0; i<n; i++) {
				p[i] = square();
			}
			break;
		case com_mayaswell_audio_Controllable_OscilatorWave_SAW:
			for (int i=0; i<n; i++) {
				p[i] = saw();
			}
			break;
		case com_mayaswell_audio_Controllable_OscilatorWave_TRIANGLE:
			for (int i=0; i<n; i++) {
				p[i] = triangle();
			}
			break;
		case com_mayaswell_audio_Controllable_OscilatorWave_SINE:
		case com_mayaswell_audio_Controllable_OscilatorWave_NOISE:
			break;
	}
}

#endif /* OSCILATOR_H_ */
