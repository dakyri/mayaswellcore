#ifndef _LOOKUP_H
#define _LOOKUP_H

#include "Signal.h"
#include "FunctionTable.h"

//
// Table lookup classes: Lookup, BoundedLookup, WrappingLookup
// Lookup performs interpolated lookup on table, index must range from
// 0 to table->Length - 1; out of range values will probably result in
// memory protection faults.
//
// BoundedLookup: index values between minimum and maximum map to the range of the
// ftable. index values outside that range map to the edges of the table. 
//
// WrappingLookup: index values outside minimum - maximum wrap around.
//
// NOTE: Unless you are really sure what you are doing you would be advised 
// not to use Lookup.
//

class Lookup
{
public:
					Lookup( FunctionTable* table=0 );
				
	Lookup& 		setFunkTable(FunctionTable* table);
	
	float			operator []	(float index);	//index from minimum to maximum
	void			operator()(Signal& inSig, Signal& outSig);
protected:
	FunctionTable *lookuptable;
};

class BoundedLookup
{
public:
	BoundedLookup(FunctionTable* table=0,float minimum=0.,float maximum=1.);
				
	BoundedLookup& 	setFunkTable(FunctionTable* table);
	BoundedLookup&	setLookupRange(float minimum, float maximum);
	
	float			operator []	(float index);	//index from minimum to maximum
	void			operator()(Signal& inSig, Signal& outSig);
protected:
	float minimum,maximum;
	float maxMmindtl;
	FunctionTable *lookuptable;
};

class WrappingLookup
{
public:
	WrappingLookup(FunctionTable* table=0,float minimum=0.,float maximum=1.);
				
	WrappingLookup& setFunkTable(FunctionTable* table);
	WrappingLookup&	setLookupRange(float minimum, float maximum);
	
	float			operator []	(float index);	//index from minimum to maximum
	void			operator()(Signal& inSig, Signal& outSig);
protected:
	float minimum,maximum;
	float maxMmindtl;
	FunctionTable *lookuptable;
};

//
// Lookup inline members
//

inline Lookup::Lookup(FunctionTable* table): lookuptable(table)	{}

inline Lookup&
Lookup::setFunkTable(FunctionTable* table)
{
	lookuptable = table;
	return *this;
}

inline float
Lookup::operator[](float index)
{
	unsigned long idx = (unsigned long)index;
	float frac = index - idx;
			
	return (*lookuptable)[idx] + 
				( (*lookuptable)[idx+1] - (*lookuptable)[idx] )*frac;
}

inline void
Lookup::operator()(Signal& inSig, Signal& outSig)
{
	for( unsigned long i=0; i< maxFramesPerChunk ; i++)
		outSig[i] = (*this)[ inSig[i] ];
}

//
// BoundedLookup inline members
//

inline
BoundedLookup::BoundedLookup(FunctionTable* table, float dmin, float dmax):
	lookuptable(table), minimum(dmin), maximum(dmax)
{
	if(lookuptable)
		maxMmindtl=(maximum-minimum)/lookuptable->Length();
}

inline BoundedLookup&
BoundedLookup::setFunkTable(FunctionTable* table)
{
	lookuptable = table;
	maxMmindtl=(maximum-minimum)/lookuptable->Length();
	return *this;
}

inline BoundedLookup&
BoundedLookup::setLookupRange(float minimum, float maximum)
{
	this->minimum=minimum;
	this->maximum=maximum;
	maxMmindtl=(maximum-minimum)/lookuptable->Length();
	return *this;
}

inline float
BoundedLookup::operator[](float index)
{
	float didx = (index-minimum)/maxMmindtl;
	long idx = (unsigned long)didx;
	float frac = didx - idx;
	
	if( idx < 0 ){
		return (*lookuptable)[ 0 ]; 
	}else if( idx >= (int)lookuptable->Length() ){
		return (*lookuptable)[ lookuptable->Length() ];
	}
		
	return (*lookuptable)[idx] + 
				( (*lookuptable)[idx+1] - (*lookuptable)[idx] )*frac;
}

inline void
BoundedLookup::operator()(Signal& inSig, Signal& outSig)
{
	for( unsigned long i=0; i< maxFramesPerChunk; i++)
		outSig[i] = (*this)[ inSig[i] ];
}

//
// WrappingLookup inline members
//

inline WrappingLookup::WrappingLookup(FunctionTable* table, float min, float max):
	lookuptable(table), minimum(min), maximum(max)
{
	if(lookuptable)
		maxMmindtl=(maximum-minimum)/lookuptable->Length();
}

inline WrappingLookup&
WrappingLookup::setFunkTable(FunctionTable* table)
{
	lookuptable = table;
	maxMmindtl=(maximum-minimum)/lookuptable->Length();
	return *this;
}

inline WrappingLookup&
WrappingLookup::setLookupRange(float minimum, float maximum)
{
	this->minimum=minimum;
	this->maximum=maximum;
	maxMmindtl=(maximum-minimum)/lookuptable->Length();
	return *this;
}

inline float
WrappingLookup::operator[](float index)
{
	float didx = (index-minimum)/maxMmindtl;
	long idx = (unsigned long)didx;
	float frac = didx - idx;
	
	while( idx < 0 )
		idx += lookuptable->Length();
	while( idx >= (int)lookuptable->Length() )
		idx -= lookuptable->Length();
			
	return (*lookuptable)[idx] + ( (*lookuptable)[idx+1] - (*lookuptable)[idx] )*frac;
}

inline void
WrappingLookup::operator()(Signal& inSig, Signal& outSig)
{
	for( unsigned long i=0; i< maxFramesPerChunk ; i++)
		outSig[i] = (*this)[ inSig[i] ];
}

#endif /* _LOOKUP_H */
