package com.mayaswell.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.mayaswell.R;


/**
 * Created by dak on 5/29/2016.
 */
public class MeterView extends View {
	private int nLamps;
	private float currentValue;

	private int mvH;
	private int mvW;

	private Drawable lampa = null;
	private int lampResId = 0;

	protected static final int[] STATE_ON = {R.attr.state_on};
	protected static final int[] STATE_OFF = {R.attr.state_off};
	private Rect lampaRect = new Rect();
	int nLit = 0;

	public MeterView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public MeterView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public MeterView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		setNLamps(10);
		setValue(0);
		setLampResource(R.drawable.mw_meter_lamp);
		setBackgroundResource(R.drawable.mw_meter_bg);
	}
	public void setLampResource(int resid) {
		lampResId = resid;
		if (lampResId > 0){
			try {
				lampa = ResourcesCompat.getDrawable(getResources(), lampResId, null);
			} catch (Resources.NotFoundException e) {
				lampa = null;
			}
		}
	}


	public void setValue(float v) {
		int nLitNow = (int) Math.round(v * nLamps);
		if (nLitNow != nLit) {
			nLit = nLitNow;
			postInvalidate();
//			Log.d("MeterView", "got vu "+v+" nlit "+nLit);
		}
		currentValue = v;
	}

	public void setNLamps(int n) {
		nLamps = n;
		setValue(currentValue);
	}

	@Override
	protected void onDraw (Canvas canvas) {
		int [] state = onCreateDrawableState(1);
		int [] stateOn = mergeDrawableStates(state, STATE_ON);
		state = onCreateDrawableState(1);
		int [] stateOff = mergeDrawableStates(state, STATE_OFF);
		mvW = getWidth();
		mvH = getHeight();

		int i=0;
		int lH = mvH / nLamps;
		lampaRect.set(0, 0, mvW, lH);
		lampaRect.offsetTo(0,mvH-lH);
		lampa.setState(stateOn);
		for (;i<nLit; i++) {
			lampa.setBounds(lampaRect);
			lampa.draw(canvas);
			lampaRect.offset(0, -lH);
		}
		lampa.setState(stateOff);
		for (;i<nLamps; i++) {
			lampa.setBounds(lampaRect);
			lampa.draw(canvas);
			lampaRect.offset(0, -lH);
		}
	}
}
