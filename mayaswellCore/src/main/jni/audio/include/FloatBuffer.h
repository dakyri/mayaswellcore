/*
 * FloatBuffer.h
 *
 *  Created on: Nov 15, 2013
 *      Author: dak
 */

#ifndef FLOATBUFFER_H_
#define FLOATBUFFER_H_

#include <stdio.h>
#include <vector>

class FloatBuffer {

public:
	FloatBuffer(float *fb = NULL, unsigned long l=0, bool f=false);
	FloatBuffer(unsigned long len);

	bool acquire(float* &buffer);
	void release();

	float *getBuffer();
	unsigned long getLength();
	void setLength(unsigned long);

protected:
	static std::vector<FloatBuffer>	cache;

	float 			*buffer;
	unsigned long	length;
	bool			free;
};

#endif /* FLOATBUFFER_H_ */
