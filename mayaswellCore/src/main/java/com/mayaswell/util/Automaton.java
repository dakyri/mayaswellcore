package com.mayaswell.util;

import java.util.ArrayList;

import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.mayaswell.util.Automaton.Node;

/**
 * @author dak
 *
 * @param <CDT>
 */
public class Automaton<CDT extends CellDataType> {
	public interface Listener {
		void onCellChange(int currentCellItemInd);
		void onNodeChange(int currentNodeId);
	}


	/**
	 * 
	 */
	public class NodeConnection {
		private Node node = null;
		private int weight = 1;
		
		public NodeConnection(Node n, int o) {
			node = n;
			weight = o;
		}
		
		public Node getNode() { return node; }
		public int getWeight() { return weight; }
		public void setWeight(int o) { weight = o; }
	}
	
	
	public class Node {
		private ArrayList<NodeConnection> cnx;
		private ArrayList<CDT> pattern;
		
		public RectF nodeRect = new RectF();
		public RectF cellRect = new RectF();
		public ArrayList<RectF> cnxRect = new ArrayList<RectF>();
		public ArrayList<RectF> cellItemRect = new ArrayList<RectF>();
		private int id;
		
		private int count;
		private int maxLoop;
		
		Node(int id) {
			this(id, (CDT[])null);
		}
		
		/**
		 * @param id 
		 * @param cell
		 */
		Node(int id, CDT... cell) {
			this.id = id;
			pattern = new ArrayList<CDT>();
			cnx = new ArrayList<NodeConnection>();
			count = setMaxLoop(0);
			if (cell != null) {
				for (CDT cdt: cell) {
					pattern.add(cdt);
				}
			}
		}
		
		public int getId()
		{
			return id;
		}
		
		public void setId(int id)
		{
			this.id = id;
		}
		
		@SuppressWarnings("unchecked")
		public Node clone()
		{
			Node n = new Node(id);
			n.cellRect = new RectF(cellRect);
			n.nodeRect = new RectF(nodeRect);
			for (CDT cdt: pattern) {
				n.pattern.add((CDT) cdt.clone());
			}
			return n;
		}
		/**
		 * @param n
		 */
		public NodeConnection addCnx(Node n, int w) {
			NodeConnection nc;
			synchronized(this) {
				if ((nc=findCnx(n)) == null) {
					cnx.add((nc=new NodeConnection(n, w)));
				}
			}
			return nc;
		}
		public NodeConnection addCnx(Node n) {
			return addCnx(n, 1);
		}
		
		/**
		 * @param n
		 */
		public void removeCnx(Node n) {
			synchronized(this) {
				for (NodeConnection nc: cnx) {
					if (nc.getNode() == n) {
						cnx.remove(nc);
						break;
					}
				}
			}
		}
		
		/**
		 * @param n
		 */
		public NodeConnection findCnx(Node n) {
			synchronized(this) {
				for (NodeConnection nc: cnx) {
					if (nc.getNode() == n) {
						return nc;
					}
				}
			}
			return null;
		}
		
		/**
		 */
		public int countCnx() {
			synchronized(this) {
				return cnx.size();
			}
		}
		
		public NodeConnection getCnx(int i) {
			NodeConnection nc;
			synchronized(this) {
				nc = (i>=0 && i < cnx.size()? cnx.get(i): null);
			}
			return nc;
		}
		
		public void clearCell() {
			synchronized(this) {
				pattern.clear();
			}
		}
		
		public ArrayList<CDT> getPattern() {
			return pattern;
		}
		
		public void setCellData(ArrayList<CDT> alc) {
			pattern = alc;
		}
		
		public void addCellData(CDT d) {
			addCellData(pattern.size(), d);
		}
		
		public void addCellData(int i, CDT d) {
			synchronized(this) {
				if (i >= 0 && i<pattern.size()) {
					pattern.add(i, d);
				} else {
					pattern.add(d);
				}
			}
		}
		
		public int countCellData() {
			synchronized(this) {
				return pattern.size();
			}
		}
		
		public CDT getCellData(int i) {
			synchronized(this) {
				return (i >= 0 && i<pattern.size())? pattern.get(i): null;
			}
		}
		
		public void cellDataMov(int ind1, int ind2) {
			synchronized(this) {
				if (pattern != null && ind1 >= 0 && ind1 < pattern.size() && ind2 >= 0) {
					if (ind1 != ind2) {
						CDT i = pattern.remove(ind1);
						if (ind1 < ind2) {
							ind2--;						
						}	
						if (ind2 >= pattern.size()) {
							pattern.add(i);
						} else {
							pattern.add(ind2, i);
						}
					}
				}
			}
		}
		
		public void cellDataRem(int ind) {
			synchronized(this) {
				if (pattern != null && ind >= 0 && ind < pattern.size()) {
					CDT i = pattern.remove(ind);
				}
			}
		}
		
		public int totalWeight() {
			int w = 0;
			for (NodeConnection nc: cnx) {
				w += nc.weight;
			}
			return w;
		}

		public Node next() {
			int w = totalWeight();
			float r = (float) Math.random();
			int cw = 0;
			for (NodeConnection nc: cnx) {
				cw += nc.weight;
				if (r <= ((float)cw)/w) {
					return nc.node;
				}
			}
			Log.d("automaton", "Node::next() gives null, cnx len "+cnx.size());
			return null;
		}

		public void offset(float dx, float dy) {
			cellRect.offset(dx, dy);
			nodeRect.offset(dx, dy);
			for (RectF cdt: cellItemRect) {
				cdt.offset(dx, dy);
			}
			for (RectF cdt: cnxRect) {
				cdt.offset(dx, dy);
			}
			
		}
		
		public void reset() {
			count = 0;
		}

		public int getMaxLoop() {
			return maxLoop;
		}

		public int setMaxLoop(int maxLoop) {
			this.maxLoop = maxLoop;
			return maxLoop;
		}
	}

	protected Node currentNode;
	protected Node root;
	protected ArrayList<Node> nodes;
	protected CDT currentCellItem;
	protected int currentCellItemInd;
	private Listener listener = null;
	private Node forceNextNode = null;
	
	public Automaton()
	{
		setCurrentNode(root = new Node(-1));
		currentCellItem = null;
		currentCellItemInd = -1;
		nodes = new ArrayList<Node>();
		Log.d("automaton", "constructed basic");
	}
	
	public void reset()
	{
		Log.d("automaton", "reset");
		currentCellItemInd = -1;
		setCurrentNode(root);
	}

	public Node getRoot() {
		return root;
	}
	
	public Node resetCurrentNodeItem()
	{
		currentCellItemInd = -1;
		return currentNode;
	}

	public Node getCurrent() {
		return currentNode;
	}

	protected void setCurrentNode(Node current) {
		this.currentNode = current;
	}
	
	protected void setNextNode(Node next) {
		this.forceNextNode  = next;
	}
	
	
	public Node addNode(Node n) {
		Log.d("automaton", "adding node");
		synchronized(this) {
			if (n == null) {
				n = new Node(nodes.size());
			}
			nodes.add(n);
			Log.d("automaton", "now have "+nodes.size());
			return n;	
		}
	}
	
	public Node addNode() {
		return addNode(null);
	}
	
	public void removeNode(Node n) {
		if (n == root) { // this is not a removable node
			return;
		}
		synchronized(this) {
			root.removeCnx(n);
			for (Node np: nodes) {
				if (np != n && n.id >= 0 && np.id >= n.id) {
					np.id--;
				}
				np.removeCnx(n);
			}
			nodes.remove(n);
		}
	}
	
	/**
	 * or size+1? root node should always be there i think
	 * @return
	 */
	public int countNodes() {
		synchronized(this) {
			return nodes.size();
		}
	}
	public Node getNode(int i) {
		synchronized(this) {
			return i>=0 && i<nodes.size()? nodes.get(i): i==nodes.size()? root: null;
		}
	}
	
	public Node findNode(int i) {
		synchronized(this) {
			if (i < 0) {
				return root;
			}
			for (Node n: nodes) {
				if (n.id == i) return n;
			}
			return null;
		}
	}
	
	public CDT next()
	{
		synchronized(this) {
			if (currentNode == null) {
				reset();
			}
			currentCellItemInd++;
			boolean justReset = false;
			while (currentCellItemInd < 0 || currentCellItemInd >= currentNode.pattern.size()) {
				if (forceNextNode != null) {
					currentNode = forceNextNode;
					forceNextNode = null;
				} else {
					currentNode = currentNode.next();
					if (currentNode == null) {
						if (justReset) {
							return null;
						}
						reset();
						justReset = true;
					} else {
						justReset = false;
					}
				}
				currentCellItemInd = 0;
				if (listener != null) {
					listener.onNodeChange(currentNode != null? currentNode.getId() : -1);
				}
			}
			if (listener != null) {
				listener.onCellChange(currentCellItemInd);
			}
			currentCellItem = currentNode.pattern.get(currentCellItemInd);
			Log.d("automaton", String.format("next() node %d cell %d item %s", currentNode.id, currentCellItemInd, currentCellItem.toString()));
		}
		return currentCellItem;
	}
	
	public CDT current()
	{
		return currentCellItem;
	}
	
	public Automaton<CDT> clone() {
		Automaton<CDT> a = new Automaton<CDT>();
		if (!copyInto(a)) {
			return null;
		}
		return a;
	}

	protected boolean copyInto(Automaton<CDT> a) {
		a.root = root.clone();
		for (Node n: nodes) {
			a.nodes.add(n.clone());
		}
		boolean sx = true;
		if (!copyCnxInto(a, root)) {
			sx = false;
		}
		for (Node n: nodes) {
			if (!copyCnxInto(a, n)) {
				sx = false;
			}
		}
		return sx;
	}

	protected boolean copyCnxInto(Automaton<CDT> a, Node n) {
		if (n == null || a == null) {
			return false;
		}
		Node n2 = a.findNode(n.id);
		if (n2 == null) {
			Log.d("automaton", "clone fails to find node "+n.id);
			return false;
		}
		for (NodeConnection nc: n.cnx) {
			if (nc != null && nc.node != null) {
				Node n3 = a.findNode(nc.node.id);
				if (n3 != null) {
					n2.addCnx(n3, nc.weight);
				} else {
					Log.d("automaton", "clone fails to find node "+nc.node.id);
				}
			} else {
				Log.d("automaton", "null connection from node "+n2.id);
			}
		}
		return true;
	}
	
	public void setListener(Listener l) {
		listener = l;
	}

}
