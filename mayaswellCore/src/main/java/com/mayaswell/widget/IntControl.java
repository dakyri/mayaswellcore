package com.mayaswell.widget;

import com.mayaswell.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IntControl extends LinearLayout {
    public interface IntControlListener {
    	public void onValueChanged(int v);
    }

    private final String schemaName = "http://schemas.android.com/apk/res-auto";
	
//	private RotaryPotView pot = null;
	private TextView label = null;
	private EditText view = null;

    private IntControlListener listener=null;

	private int minVal=0;
	private int maxVal=Integer.MAX_VALUE;

	private int val=0;
    
	public IntControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public IntControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public IntControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		TypedArray tpAttrs = null;
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;

		LayoutInflater li = (LayoutInflater)getContext ().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_int_control, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		view = (EditText)findViewById (R.id.mwCtlEditText);
//		pot = (RotaryPotView)findViewById (R.id.sgCtlPot);
		
		view.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						int v = Integer.parseInt(s.toString());
						if (v < minVal) v = minVal;
						else if (v > maxVal) v = maxVal;
						val = v;
//						pot.setValue(v);
						notifyListener(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});

		if (attrs != null) {
			tpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TeaPot, 0, 0);
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);

			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				label.setText(lbl);
			}
			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					int intBoundsA[] = new int[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						intBoundsA[i++] = Integer.parseInt(s);
					}
					setValueBounds(intBoundsA[0], intBoundsA[intBoundsA.length-2], intBoundsA[intBoundsA.length-1]);
				}
			}
			int px = (int) mwvAttrs.getDimension(R.styleable.MWView_labelWidth, -1);
			if (px > 0) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}

			px = (int) mwvAttrs.getDimension(R.styleable.MWView_viewWidth, -1);
			if (px > 0) {
				LayoutParams params = (LayoutParams) view.getLayoutParams();
				params.width = px;
			}
		}

		if (tpAttrs != null) tpAttrs.recycle();
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}
	
	public void setValueBounds(int min, int dflt, int max)
	{
		minVal = min;
		maxVal = max;
	}
	
    public void setValueListener(IntControlListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(int v)
	{
		if (listener != null)
			listener.onValueChanged(v);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setValue(int v)
	{
		if (v < minVal) v = minVal;
		else if (v > maxVal) v = maxVal;
		val = v;
		view.setText(Integer.toString(v));
	}
	
	public int getValue()
	{
		return val;
	}
}
