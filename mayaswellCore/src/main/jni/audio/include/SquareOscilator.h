#ifndef SQUAREOSC
#define SQUAREOSC

#include "AudioSignal.h"

class SquareOscilator
{
public:
							SquareOscilator(float amp = 1.);
	
	void setWidth(float amp);
	void setAmplitude(float amp);
	void setFrequency(float freq, float sampleRate=signalSampleRate);
	void reset();
	
	Sample operator ()	(void);
	void operator ()	(Signal& outSig);
	
private:
	long sample;
	float cycle;
	float amp;
	long up;
	long halfCycle;
	float width;
};


// SQUARE as all hell, and cheesy to boot.

inline SquareOscilator::SquareOscilator(float a)
{
	sample = -1;
	amp = a;
	width = 1;
	setFrequency(256);
}

inline void
SquareOscilator::reset()
{
	sample = -1;
}

inline void
SquareOscilator::setAmplitude(float a)
{
	amp = a;
}

inline void
SquareOscilator::setWidth(float a)
{
	width = (a<0?0:a>1?1:a);
}

inline void
SquareOscilator::setFrequency(float freq, float sampleRate)
{
	cycle = (sampleRate / freq);
	halfCycle = cycle/2;
	up = (1.0+7.0*width)*halfCycle/8.0;
}

	
inline Sample
SquareOscilator::operator()(void)
{
	float	val=0;
	if (++sample < up)
		return amp;
	else if (sample >= halfCycle && sample <= halfCycle+up)
		return -amp;
	if (sample >= cycle)
		sample = -1;
	return val;
}
	
inline void
SquareOscilator::operator()(Signal& outSig)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		outSig[i]=(*this)();
}


#endif
