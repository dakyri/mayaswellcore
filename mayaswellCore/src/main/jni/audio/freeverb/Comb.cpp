// Comb filter implementation
//
// Written by Jezar at Dreampoint, June 2000
// http://www.dreampoint.co.uk
// This code is public domain

#include "Comb.h"

Comb::Comb()
{
	filterstore = 0;
	bufidx = 0;
}

void Comb::setbuffer(float *buf, int size)
{
	buffer = buf; 
	bufsize = size;
}

void Comb::mute()
{
	for (int i=0; i<bufsize; i++)
		buffer[i]=0;
}

void Comb::setdamp(float val)
{
	damp1 = val; 
	damp2 = 1-val;
}

float Comb::getdamp()
{
	return damp1;
}

void Comb::setfeedback(float val)
{
	feedback = val;
}

float Comb::getfeedback()
{
	return feedback;
}

// ends
