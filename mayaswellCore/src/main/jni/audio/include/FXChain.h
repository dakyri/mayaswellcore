/*
 * FXChain.h
 *
 *  Created on: Dec 6, 2014
 *      Author: dak
 */

#ifndef FXCHAIN_H_
#define FXCHAIN_H_

#include <vector>

#include "Effect.h"

#include "TimeSource.h"

class FXChain
{
public:
	FXChain(short _id, int maxFX, int maxFXparam, TimeSource &t, bool lock);
	virtual ~FXChain();

	bool setFXType(int whichFX, int type, int nParams, float *params);
	bool setFXEnable(int whichFX, bool en);
	bool setFXParam(int whichFX, int whichParam, float val, bool cacheValue=true);
	bool setFXParams(int whichFX, int nParams, float *params);
	float getParam(int whichFX, int p);
	int nParams(int whichFX);

	void setSideChainSrc(float *);
	void setSideChainDst(float *);

	unsigned int applyFX(float *buffer, int nFrame);

	short id;
	bool hasTail;

protected:
	std::vector<Effect> fx;

	void checkTail();

	float *sideSrcBuffer;
	float *sideDstBuffer;

};



#endif /* FXCHAIN_H_ */
