/*
 * Flange.cpp
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */


#include <stdio.h>
#include <string.h>
#include <math.h>
//
#include "Flange.h"

#include <android/log.h>

Flange::Flange()
	: delayControlFilter(20.0f)
	, mixControlFilter(20.0f) {
	// TODO Auto-generated constructor stub
	// make sure all instance vars are init'd to some known value
	paramSweepRate = 0.2f;
	paramWidth = 0.3f;
	paramFeedback = 0.0f;
	paramDelay = 0.2f;
	sweepRate = 0.2;
	feedback = 0.0;
	feedbackPhase = 1.0;
	sweepSamples = 0;
	setSweep();
	fp = 0;
	sweep = 0.0;
	outval1 = outval2 = 0.0f;

	// allocate the buffers
	buf1.setLength(kBufSize);
	buf2.setLength(kBufSize);
	buf1p = NULL;
	buf2p = NULL;
}


void
Flange::initialize()
{
	buf1.acquire(buf1p);
	buf2.acquire(buf2p);
	reset();
}

void
Flange::release()
{
	buf1.release();
	buf2.release();
	buf1p = buf2p = NULL;
}

void
Flange::reset()
{
	if (buf1p != NULL) {
		for( int i = 0; i < kBufSize; i++) {
			buf1p[i] = 0.0;
		}
	}
	if (buf2p != NULL) {
		for( int i = 0; i < kBufSize; i++) {
			buf2p[i] = 0.0;
		}
	}
}

Flange::~Flange() {
	release();
}

void
Flange::setParameters(float _rate, float _width, float _fb, float _delay, float _mix)
{
	setRate(_rate);
	setWidth(_width);
	setFeedback(_fb);
	setDelay(_delay);
	setMix(_mix);
}

void
Flange::setRate(float rate)
{
	paramSweepRate = rate;
	// map into param onto 0.05Hz - 10hz with log curve
	sweepRate = pow(10.0,(double)rate);
	sweepRate  -= 1.0;
	sweepRate  *= 1.05556f;
	sweepRate  += 0.05f;

	// finish setup
	setSweep();
}
//
//								setSweep
//
//	Sets up sweep based on rate, depth, and delay as they're all interrelated
//	Assumes _sweepRate and _sweepSamples have all been set by
//	setRate and setWidth and calcs the steps to achieve the desired sweep rate
//
void Flange::setSweep()
{
	// calc # of samples per second we'll need to move to achieve spec'd sweep rate
	step = (double)(sweepSamples * 2.0 * sweepRate) / (double)signalSampleRate;

	// calc max and start sweep at 0.0
	sweep = 0.0;
	maxSweepSamples = sweepSamples;
}


//
//								setWidth
//
//	Maps 0.0-1.0 input to calculated width in samples from 0ms to 10ms
//
void Flange::setWidth (float v)
{
	paramWidth = v;
	// map so that we can spec between 0ms and 10ms
	if( v <= 0.05) {
		// eat some noise on the bottom end
		sweepSamples = 0.0;
	} else {
		// otherwise calc # of samples for the total width
		sweepSamples = v * 0.01 * signalSampleRate;
	}

	// finish setup
	setSweep();
}

//
//								setFeedback
//
void Flange::setFeedback(float v)
{

	paramFeedback = v;
	feedback = v;
}

void Flange::setDelay (float v)
{
	paramDelay = v;
}

//
//								setMix
//
void Flange::setMix(float v)
{
	paramMix = v;
}


/*
void Flange::setMixMode (float v)
{
	debugOutD("setMixMode(%f)\n",v);

	_paramMixMode = v;
	pPrograms[curProgram]->paramMixMode = v;
	_mixMode = ROUND(v * kNumMixModes);
	debugOutD("_mixMode=%f\n", (double)_mixMode);

	switch(_mixMode)
 	{
	case kMixMono:
	default:
		_mixMono = true;
		_mixLeftWet = _mixRightWet = _mixLeftDry = _mixRightDry = 1.0;
		_feedbackPhase = 1.0;
		break;
	case kMixMonoMinus:
		_mixMono = true;
		_mixLeftWet = _mixRightWet = -1.0;
		_mixLeftDry = _mixRightDry = 1.0;
		_feedbackPhase = -1.0;
		break;
	case kMixMonoBoth:
		_mixMono = true;
		_mixLeftWet = _mixLeftDry = _mixRightDry = 1.0;
		_mixRightWet = -1.0;
		_feedbackPhase = 1.0;
		break;
	case kMixStereo:
		_mixMono = false;
		_mixLeftWet = _mixRightWet = 1.0;
		_mixLeftDry = _mixRightDry = 1.0;
		_feedbackPhase = 1.0;
		break;
	case kMixStereoMinus:
		_mixMono = false;
		_mixLeftWet = _mixRightWet = -1.0;
		_mixLeftDry = _mixRightDry = 1.0;
		_feedbackPhase = -1.0;
		break;
	case kMixStereoBoth:
		_mixMono = false;
		_mixLeftWet = _mixLeftDry = _mixRightDry = 1.0;
		_mixRightWet = -1.0;
		_feedbackPhase = 1.0;
		break;
	}
}
//
//								getParameterName
//
void Flange::getParameterName (VstInt32 index, char *label)
{
	debugOut("getParameterName index=%d\n", index);
	switch (index)
	{
		case kRate:    strcpy (label, "Rate");			break;
		case kWidth:   strcpy (label, "Width");			break;
		case kFeedback: strcpy(label, "Feedback");		break;
		case kDelay:	strcpy (label, "Delay");		break;
		case kMix:		strcpy( label, "Mix");			break;
		case kMixMode: strcpy (label, "Mix Mode");		break;
	}
}

//
//								getParameterDisplay
//
void Flange::getParameterDisplay (VstInt32 index, char *text)
{
	debugOut("getParameterDisplay index=%d\n", index);
	char buf[64];
	switch (index)
	{
	case kRate:
		sprintf( buf, "%2.1f", _sweepRate );
		strcpy( text, buf );
		break;
	case kWidth:
		sprintf( buf, "%2.0f", _paramWidth * 100.0 );
		strcpy( text, buf );
		break;
	case kFeedback:
		sprintf( buf, "%2.0f", _feedback * 100.0 );
		strcpy( text,buf );
		break;
	case kDelay:
		sprintf( buf, "%d", _paramDelay);
		break;
	case kMix:
		sprintf( buf, "%2.0f", _paramMix );
		break;
	case kMixMode:
		switch(_mixMode)
		{
		case kMixMono:
			strcpy( text, "mono" );
			break;
		case kMixMonoMinus:
			strcpy( text, "mono-" );
			break;
		case kMixMonoBoth:
			strcpy( text, "mono+/-" );
		case kMixStereo:
			strcpy( text, "stereo" );
			break;
		case kMixStereoMinus:
			strcpy( text, "stereo-" );
			break;
		case kMixStereoBoth:
			strcpy( text, "stereo+/-" );
			break;
		}
		break;
	}
}

//
//								getParameterLabel
//
void Flange::getParameterLabel (VstInt32 index, char *label)
{
	debugOut("getParameterLabel index=%d\n", index);
	switch (index)
	{
		case kRate:		strcpy (label, "Hz");		break;
		case kWidth:	strcpy( label, "%");		break;
		case kFeedback:	strcpy( label, "%");		break;
		case kDelay:	strcpy( label, "");			break;
		case kMix:		strcpy( label, "%");		break;
		case kMixMode:	strcpy( label, "");			break;
	}
}
*/
//
//								fpin
//
//	Pins within the -1 to 1 range
double fpin(double n)
{
	if( n < -0.999) {
		return -0.999;
	} else if( n > 0.999) {
		return 0.999;
	} else {
		return n;
	}
}


//
//								processReplacing
//
//	Main worker routine, does the audio processing for the selected flanging
//
void Flange::apply (float *signal, long sampleFrames)
{
/*	float* in1 = inputs[0];
	float* in2 = inputs[1];
	float* out1 = outputs[0];
	float* out2 = outputs[1];
*/
//	__android_log_print(ANDROID_LOG_DEBUG, "Flange", "apply() in %ld", sampleFrames);
	for(int i=0, j=0; i < sampleFrames; i++, j+=2) {
		// see if we're doing mono
		float inval1, inval2;
		inval1 = signal[j];
		inval2 = signal[j+1];
		double inmix1 = inval1 + feedback * feedbackPhase * outval1;
		double inmix2 = inval2 + feedback * feedbackPhase * outval2;

// __android_log_print(ANDROID_LOG_DEBUG, "Flange", "apply() loop %d %d %d %x %x", i, j, fp, (unsigned int)buf1p, (unsigned int)buf2p);
		buf1p[fp] = inmix1;
		buf2p[fp] = inmix2;
		fp = (fp + 1) & (kBufSize-1);

		// get filtered delay
		double delay = delayControlFilter.process(paramDelay);

		// delay 0.0-1.0 maps to 0.02ms to 10ms (always have at least 1 sample of delay)
		double delaySamples = (delay * signalSampleRate * 0.01) + 1.0;
		delaySamples += sweep;

		// build the two emptying pointers and do linear interpolation
		int ep1, ep2;
		double w1, w2;
		double ep = fp - delaySamples;
		if( ep < 0.0) {
			ep += kBufSize;
		}
		MODF(ep, ep1, w2);
		ep1 &= (kBufSize-1);
		ep2 = ep1 + 1;
		ep2 &= (kBufSize-1);
		w1 = 1.0 - w2;
		outval1 = buf1p[ep1] * w1 + buf1p[ep2] * w2;
		outval2 = buf2p[ep1] * w1 + buf2p[ep2] * w2;

		// get filtered mix
		double mix = mixControlFilter.process(paramMix);
/*
		double f1 = _mixLeftDry * inval1 + _mixLeftWet * mix * outval1;
		double f2 = _mixRightDry * inval2 + _mixRightWet * mix * outval2;
*/
		double f1 = inval1 + mix * outval1;
		double f2 = inval2 + mix * outval2;
		f1 = fpin( f1 );
		f2 = fpin( f2 );

		// pin to desired output range
		signal[j] = (float)PIN( f1, -0.99, 0.99 );
		signal[j+1] = (float)PIN( f2, -0.99, 0.99 );

		// see if we're doing sweep
		if( step != 0.0) {
			sweep += step;// increment the sweep
			if( sweep <= 0.0) {
				sweep = 0.0;// make sure we don't go negative
				step = -step; // and reverse direction
			} else if( sweep >=  maxSweepSamples) {
				step = -step;
			}
		}
	}

}
