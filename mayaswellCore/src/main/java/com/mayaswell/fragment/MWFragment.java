/**
 * 
 */
package com.mayaswell.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author dak
 *
 */
public class MWFragment extends Fragment {
	
	protected final static int LAYOUT_NORMAL = 0;
	protected final static int LAYOUT_IS_GREEDY = 0x01;
	protected final static int LAYOUT_PREFER_MAX_WIDTH = 0x02;
	protected final static int LAYOUT_PREFER_MAX_AREA = 0x04;

	protected int layoutRequirements = LAYOUT_NORMAL;
	
	public MWFragment() {
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
	}
	
	protected void setGreedy()
	{
		layoutRequirements |= LAYOUT_IS_GREEDY;
	}
	
	protected void setPreferMaxArea()
	{
		layoutRequirements |= LAYOUT_PREFER_MAX_AREA;
	}
	
	protected void setPreferMaxWidth()
	{
		layoutRequirements |= LAYOUT_PREFER_MAX_WIDTH;
	}
	
	public boolean isGreedy()
	{
		return (layoutRequirements & LAYOUT_IS_GREEDY) > 0;
	}
	
	public boolean preferMaxArea()
	{
		return (layoutRequirements & LAYOUT_PREFER_MAX_AREA) > 0;
	}

	public boolean preferMaxWidth() {
		return (layoutRequirements & LAYOUT_PREFER_MAX_WIDTH) > 0;
	}

}
