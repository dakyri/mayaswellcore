INNER_SAVED_LOCAL_PATH := $(LOCAL_PATH)

LOCAL_PATH:= $(call my-dir)

COMMONS_PATH:= $(LOCAL_PATH)/$(MAYASWELL_PATH)

include $(CLEAR_VARS)

LOCAL_MODULE    := audio
LOCAL_MODULE_FILENAME := libaudio

LOCAL_CFLAGS    := -Werror -D__GXX_EXPERIMENTAL_CXX0X__
LOCAL_CPPFLAGS  := -std=c++11
LOCAL_SRC_FILES := Envelope.cpp Effect.cpp FXChain.cpp  LFO.cpp Delay.cpp Flange.cpp Chorus.cpp Phaser.cpp FloatBuffer.cpp Filter.cpp
APP_STL := stlport_static
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/include $(LOCAL_PATH)/audio/include

include $(BUILD_STATIC_LIBRARY)

LOCAL_PATH := $(INNER_SAVED_LOCAL_PATH)