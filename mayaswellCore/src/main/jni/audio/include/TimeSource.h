/*
 * TimeSource.h
 *
 *  Created on: Dec 16, 2014
 *      Author: dak
 */

#ifndef TIMESOURCE_H_
#define TIMESOURCE_H_


class TimeSource
{
public:
	float tempo;
	float secsPerBeat;
	float beatsPerSec;

	void setTempo(float t) {
		tempo = t;
		secsPerBeat = 60.0 / t;
		beatsPerSec = t/60;
	}

	void setControlRate(float r) {
		secsPerCycle = r;
	}
	friend class Envelope;
	friend class LFO;
	friend class Effect;
protected:
	float secsPerCycle;
};



#endif /* TIMESOURCE_H_ */
