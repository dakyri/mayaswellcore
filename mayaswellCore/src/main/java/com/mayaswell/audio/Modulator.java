package com.mayaswell.audio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Modulator {
	protected static Control.Host controlHost = null;

//	public final static int RESET_ONSTART=0;
	public final static int RESET_ONFIRE=1;
	public final static int RESET_ONLOOP=2;
	public final static int RESET_ONSLICE=3;
	public final static int RESET_ONATTACK=4;
	public final static int RESET_ATEND=5;
	public final static int RESET_NOT=6;
	
	public final static int WF_SIN=0;
	public final static int WF_SQUARE=1;
	public final static int WF_SAW=2;
	public final static int WF_NEGSAW=3;
	public final static int WF_EXP=4;
	public final static int WF_NEGEXP=5;
	
	public ArrayList<ModulationTarget> target;
	public boolean tempoLock;
	public ResetMode trigger;

	public enum LFWave {
		SIN(WF_SIN), SQUARE(WF_SQUARE), SAW(WF_SAW), NEGSAW(WF_NEGSAW), EXP(WF_EXP), NEGEXP(WF_NEGEXP);
		private byte id;
		LFWave(int id2)
		{
			id = (byte) id2;
		}
		
		public byte code()
		{
			return id;
		}
		
		@Override
		public String toString()
		{
			switch (this) {
			case SIN: return "sin";
			case SQUARE: return "square";
			case SAW: return "saw";
			case NEGSAW: return "negsaw";
			case EXP: return "exp";
			case NEGEXP: return "negexp";
			}
			return super.toString();
		}
		
		public static LFWave parse(String s)
		{
			if (s.equals("sin")) return SIN;
			if (s.equals("square")) return SQUARE;
			if (s.equals("saw")) return SAW;
			if (s.equals("negsaw")) return NEGSAW;
			if (s.equals("exp")) return EXP;
			if (s.equals("negexp")) return NEGEXP;
			return SIN;
		}
	}
	
	public enum ResetMode {
		/*ONSTART(RESET_ONSTART), */ONFIRE(RESET_ONFIRE), ONLOOP(RESET_ONLOOP), ONSLICE(RESET_ONSLICE),
			ONATTACK(RESET_ONATTACK), ATEND(RESET_ATEND), NOT(RESET_NOT);
		private byte id;
		ResetMode(int id2)
		{
			id = (byte) id2;
		}
		
		public byte code()
		{
			return id;
		}
		
		@Override
		public String toString()
		{
			switch (this) {
//			case ONSTART: return "start";
			case ONFIRE: return "onfire";
			case ONLOOP: return "onloop";
			case ONSLICE: return "onslice";
			case ONATTACK: return "attack";
			case ATEND: return "atend";
			case NOT: return "not";
			}
			return super.toString();
		}
		
		public static ResetMode parse(String s)
		{
//			if (s.equals("start")) return ONSTART;
			if (s.equals("onfire")) return ONFIRE;
			if (s.equals("onloop")) return ONLOOP;
			if (s.equals("onslice")) return ONSLICE;
			if (s.equals("attack")) return ONATTACK;
			if (s.equals("atend")) return ATEND;
			if (s.equals("not")) return NOT;
			return ONFIRE;
		}
	}
	
	public static class ModulationTarget implements Parcelable
	{
		public ICControl	target;
		public float		amount;
		
		public ModulationTarget()
		{
			target = null;
			amount = 1;
		}
		
		public ModulationTarget(ICControl target2, float amount2)
		{
			target = target2;
			amount = amount2;
		}

		protected ModulationTarget(Parcel in) {
			amount = in.readFloat();
			int targetid = in.readInt();
			target = (controlHost != null? controlHost.controlFor(targetid):null);
		}

		public ModulationTarget clone()
		{
			return new ModulationTarget(target, amount);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeFloat(amount);
			dest.writeInt(target.ccontrolId());
		}

		public static final Creator<ModulationTarget> CREATOR = new Creator<ModulationTarget>() {
			@Override
			public ModulationTarget createFromParcel(Parcel in) {
				return new ModulationTarget(in);
			}

			@Override
			public ModulationTarget[] newArray(int size) {
				return new ModulationTarget[size];
			}
		};
	}
	
	
	
	public int countValidTargets()
	{
		int n = 0;
		for (ModulationTarget mt: target) {
			if (mt.target != null && mt.target.ccontrolId() > 0) {
				n++;
			}
		}
		return n;
	}

	public void addTarget()
	{
		target.add(new ModulationTarget());
	}
	
	public void clearTarget()
	{
		target.clear();
	}

	public void add(ModulationTarget clone)
	{
		target.add(clone);
	}

	public ModulationTarget createTarget()
	{
		return new ModulationTarget();
	}	

}
