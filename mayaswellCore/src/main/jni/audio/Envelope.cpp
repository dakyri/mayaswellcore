/*
 * Envelope.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#include "Envelope.h"

bool
EnvelopeHost::setEnvelope(int which, unsigned char reset, bool lock, float aT, float dT, float sT, float sL, float rT)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set env %d %d", which, reset);
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].resetMode = reset;
	env[which].lock = lock;
	env[which].attackT = aT;
	env[which].decayT = dT;
	env[which].sustainT = sT;
	env[which].sustainL = sL;
	env[which].releaseT = rT;
	return true;
}

bool
EnvelopeHost::setNEnvelope(int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "n env %d", n);
	if (n <= env.size()) {
		nActiveEnv = n;
		return true;
	} else {
		nActiveEnv = env.size();
		return false;
	}

}

bool
EnvelopeHost::setNEnvelopeTarget(int which, int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "env n target %d %d", which, n);
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].nActiveSends = n;
	return true;
}

bool
EnvelopeHost::setEnvelopeAmount(int which, int whichn, float amt)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= env[which].target.size()) {
		return false;
	}
	env[which].target[whichn].amount = amt;
	return true;
}


bool
EnvelopeHost::setEnvelopeLock(int which, bool lck)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].lock = lck;
	return true;
}

bool
EnvelopeHost::setEnvelopeReset(int which, unsigned char rst)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].resetMode = rst;
	return true;
}

bool
EnvelopeHost::setEnvelopeAttack(int which, float t)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].attackT = t;
	return true;
}

bool
EnvelopeHost::setEnvelopeDecay(int which, float t)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].decayT = t;
	return true;
}

bool
EnvelopeHost::setEnvelopeSustain(int which, float t)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].sustainT = t;
	return true;
}

bool
EnvelopeHost::setEnvelopeSustainLevel(int which, float l)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].sustainL = l;
	return true;
}

bool
EnvelopeHost::setEnvelopeRelease(int which, float t)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].releaseT = t;
	return true;
}



int
EnvelopeHost::hasEnv(int tgt)
{
	for (int i=0; i<env.size(); i++) {
		for (int j=0; j<env[i].target.size(); j++) {
			if (env[i].target[j].target == tgt) return i;
		}
	}
	return -1;
}


bool
EnvelopeHost::resetEnvelope(int which)
{
	if (which < 0 || which >= env.size()) {
		return false;
	}
	env[which].reset();
	return true;
}


float
EnvelopeHost::getEnvelope(int which)
{
	if (which < 0 || which >= env.size()) {
		return 0;
	}
	return env[which].lastValue;
}
