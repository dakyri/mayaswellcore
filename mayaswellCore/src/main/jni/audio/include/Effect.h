/*
 * Effect.h
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#ifndef EFFECT_H_
#define EFFECT_H_

#include "Delay.h"
#include "SG303Filter.h"
#include "FormantFilter.h"
#include "MoogFilter.h"
#include "Flange.h"
#include "Chorus.h"
#include "Phaser.h"
#include "Distortion.h"
#include "RingMod.h"

#include "com_mayaswell_audio_FX.h"

#include "TimeSource.h"

#include <vector>

class Effect {
public:
	Effect(TimeSource &t, bool lock);

	static const int kMaxParams=6;

	void apply(float *buf, unsigned int nFrame);

	bool setType(int type, int nparam, float* value);
	void setEnable(bool enable);
	bool setParam(int param, float value, bool cacheValue=true);
	bool setParams(int nparam, float* value);
	void setBufsize(int bufsize);
	void setTempoLock(bool b);

	float getParam(int p);
	int nParams();

	friend class FXChain;

	TimeSource		&timer;

protected:
	int type;
	bool enable;
	bool hasTail;
	bool tempoLock;

	std::vector<float> actualParams;

	EchoDelay echoDelay;
	FBDelay fbDelay;
	Flange flange;
	Chorus chorus;
	Phaser phaser;
	Distortion distortion;
	SG303Filter sgL, sgR;
	MoogVCF1 moog1L, moog1R;
	MoogVCF2 moog2L, moog2R;
	FormantFilter formantL, formantR;
	RingMod ringMod;
};


inline void
Effect::apply(float *bufp, unsigned int nFrame)
{
	switch (type)
	{
	case com_mayaswell_audio_FX_UNITY: {
		break;
	}
	case com_mayaswell_audio_FX_SG_LOW: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = sgL.lowPass(*bufp);bufp++;
			*bufp = sgR.lowPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_SG_HIGH: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = sgL.highPass(*bufp);bufp++;
			*bufp = sgR.highPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_SG_BAND: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = sgL.bandPass(*bufp);bufp++;
			*bufp = sgR.bandPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_LOW: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog1L.lowPass(*bufp);bufp++;
			*bufp = moog1L.lowPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_HIGH: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog1L.highPass(*bufp);bufp++;
			*bufp = moog1L.bandPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG1_BAND: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog1L.bandPass(*bufp);bufp++;
			*bufp = moog1L.bandPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_LOW: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog2L.lowPass(*bufp);bufp++;
			*bufp = moog2L.lowPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_HIGH: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog2L.highPass(*bufp);bufp++;
			*bufp = moog2L.highPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_MOOG2_BAND: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = moog2L.bandPass(*bufp);bufp++;
			*bufp = moog2L.bandPass(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_FORMANT: {
		float *bnd = bufp+nFrame*2;
		while (bufp<bnd) {
			*bufp = formantL.filter(*bufp);bufp++;
			*bufp = formantR.filter(*bufp);bufp++;
		}
		break;
	}
	case com_mayaswell_audio_FX_FBDELAY: {
		fbDelay.apply(bufp, nFrame);
		break;
	}
	case com_mayaswell_audio_FX_ECHO: {
		echoDelay.apply(bufp, nFrame);
		break;
	}
	case com_mayaswell_audio_FX_ECHO_SIDECHAIN: {
		break;
	}

	case com_mayaswell_audio_FX_FLANGE: {
		flange.apply(bufp, nFrame);
		break;
	}

	case com_mayaswell_audio_FX_CHORUS: {
		chorus.apply(bufp, nFrame);
		break;
	}

	case com_mayaswell_audio_FX_PHASER: {
		phaser.apply(bufp, nFrame);
		break;
	}

	case com_mayaswell_audio_FX_DISTORTION: {
		distortion.apply(bufp, nFrame, 2);
		break;
	}
	case com_mayaswell_audio_FX_RINGMOD: {
		ringMod.apply(bufp, nFrame, 2);
		break;
	}

	}
}
#endif /* EFFECT_H_ */
