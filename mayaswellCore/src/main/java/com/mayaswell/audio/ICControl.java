package com.mayaswell.audio;

public interface ICControl {
	public static final int TYPE_FLOAT=0;
	public static final int TYPE_INT=1;
	

	/**
	 * the main identifier for this control, indicating it's type, and behaviour etc ... expect that this id is unique.
	 * but we have allowed for an instanceId, currently unused, if there's more than one control with the same identity and behaviour TODO 
	 */
	public abstract int ccontrolId();
	
	/**
	 * in case we need to distinguish further
	 */
	public abstract int instanceId();

	/**
	 * used by the save routines this is a unique identifier for the control. if the instanceId is ever necessary, this name should distinguish these
	 */
	public abstract String ccontrolName();
	
	/**
	 * gets the public label for this control ... also use a string id ... atm this is the same, but I assume that the display name and the 
	 * unique type will be different once I have more than the handful of parameters I am dealing with now
	 * @see java.lang.Object#toString()
	 */
	public abstract String fullName();

	/**
	 * @return a shorter name for this control
	 */
	public abstract String abbrevName();
	
	float normalize(float v);
	float denormalize(float v);
	float defaultValue();
	int valueType();
}