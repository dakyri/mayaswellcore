/*
 * Delay.cpp
 *
 *  Created on: Dec 3, 2013
 *      Author: dak
 */
#include "Delay.h"

Delay::Delay(int nc, float maxDelaySecs)
{
	nChannel = nc;
	//
	// The delayBuffer buffer is created as a buffer whose length
	// is two vectorLengths greater than the highest integer multiple
	// of vectorLength contained within sr*maxdelaysecs, plus a guard point
	// which contains the same value as delayBuffer[0]
	//

	maxDelay = maxDelaySecs;

	setBufsize(kAudioBufferSize);

//	buffPointer = new float[length+nChannel];	//+1 for guard point
//	reset();

	inputIndex=0;
}


Delay::~Delay()
{
//	delete [] buffPointer;
	release();
}

void
Delay::setBufsize(int bufSize)
{
	length=(unsigned long)(maxDelay*signalSampleRate*nChannel);
	length/=bufSize;
	length*=bufSize;
	length+= 2 * bufSize;

	delayBuffer.setLength(length+nChannel);
}


EchoDelay::EchoDelay(int nc, float maxDelaySecs):
		Delay(nc, maxDelaySecs)
{
	dlTime = 1;
	dlEchoVolume = 0.5;
	dlFeedback = 0;

}

void
EchoDelay::setParameters(float v, float t, float fb)
{
	setGain(v);
	setTime(t);
	setFeedback(fb);
}

void
EchoDelay::setGain(float v)
{
	dlEchoVolume = (v<0? 0: v>1? 1: v);
}

void
EchoDelay::setTime(float t)
{
	dlTime = (t<kMinDelay? kMinDelay: t > maxDelay? maxDelay: t);
}

void
EchoDelay::setFeedback(float fb)
{
	dlFeedback = (fb<0? 0: fb>1? 1: fb);
}


SideChainDelay::SideChainDelay(int nc, float maxDelaySecs):
		Delay(nc, maxDelaySecs)
{
	dlTime = 1;
	dlEchoVolume = 0.5;
	dlFeedback = 0;

}

void
SideChainDelay::setParameters(float v, float t, float fb)
{
	setGain(v);
	setTime(t);
	setFeedback(fb);
}

void
SideChainDelay::setGain(float v)
{
	dlEchoVolume = (v<0? 0: v>1? 1: v);
}

void
SideChainDelay::setTime(float t)
{
	dlTime = (t<kMinDelay? kMinDelay: t > maxDelay? maxDelay: t);
}

void
SideChainDelay::setFeedback(float fb)
{
	dlFeedback = (fb<0? 0: fb>1? 1: fb);
}

FBDelay::FBDelay(int nc, float maxDelaySecs):
		Delay(nc, maxDelaySecs)
{
	dlTime = 1;
	dlFeedback = 0;
}

void
FBDelay::setParameters(float t, float fb)
{
	setTime(t);
	setGain(fb);
}


void
FBDelay::setGain(float fb)
{
	dlFeedback = (fb<0? 0: fb>1? 1: fb);
}

void
FBDelay::setTime(float t)
{
	dlTime = (t<kMinDelay? kMinDelay: t > maxDelay? maxDelay: t);
}
