package com.mayaswell.audio;

import java.util.ArrayList;

import android.text.Editable;
import android.util.Log;

import com.mayaswell.util.AbstractPatch;

public class Sequence<P extends AbstractPatch> {
	public enum Type {
		ALGORITHM, PATCH, JUMP
	}
	public class Step {
//		public P patch;
		public Type type=Type.PATCH;
		public int patchIdx;
		public int count;
		public float probability = 1;
		
		public Step(Type t, int pidx, int c) {
//			patch = p;
			patchIdx = pidx;
			count = c;
		}
		
		public Step()
		{
			this(Type.PATCH, -1, 0);
//			this(null, 0);
		}
	}
	
	public String name;
	protected ArrayList<Step> steps;
	
	public Sequence(String nm)
	{
		name = nm;
		steps = new ArrayList<Step>();
	}
	
	public String toString()
	{
		return name;
	}
	
	public ArrayList<Step> getSteps()
	{
		return steps;
	}
	
	public Step getStep(int i)
	{
		if (i >= 0 && i < steps.size()) {
			return steps.get(i);
		}
		return null;
	}
	
	public Step pushStep(/*P*/int p, int c)
	{
		Step s = new Step(Type.JUMP, p, c);
		steps.add(s);
		return s;
	}
	
	private void addStep(/*P*/int patch, int count) {
		addStep(Type.PATCH, patch, count, -1, 1);
	}
	
	public Step addStep(Type type, int patchIdx, int count, int idxpos, float pr)
	{
		Step s = new Step(type, patchIdx, count);
		if (idxpos>=0 && idxpos<steps.size()) {
			steps.add(idxpos, s);
		} else {
			steps.add(s);
		}
 		return s;
	}
	
	public int countSteps()
	{
		return steps.size();
	}
	
	public boolean delStep(int i) {
		if (i < 0 || i >= steps.size()) {
			return false;
		}
		steps.remove(i);
		return true;
	}

	public void setName(String text) {
		name = text;
	}
	
	public Sequence<P> clone()
	{
		Sequence<P> s = new Sequence<P>(name);
		for (Step ps: steps) {
			s.addStep(ps.patchIdx, ps.count);
		}
		return s;		
	}
	
	public void dump()
	{
		int i=0;
		for (Step s: steps) {
			i++;
			Log.d("PatchSequence ", "Step "+i+": patch "+s.patchIdx+", count "+s.count);
		}
	}

}
