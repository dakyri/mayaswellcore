package com.mayaswell.audio;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;

public class OSCModule {

	protected ArrayList<Map> mappedParams = new ArrayList<Map>();
	protected OSCPortIn receiver = null;
	protected int inPort = 7000;
	protected String inHost = "127.0.0.1";
	protected HashMap<String, OSCListener> listeners = new HashMap<String, OSCListener>();

	public class Map {

		/**
		 * the mapping address contains tags for numerics to distinguish particular buses/pads
		 * @param ma the mapping address
		 * @param ta the type string
		 */
		public Map(String ma, String ta)
		{
			Matcher m = Pattern.compile("\\d").matcher(ma);
			int n = 0;
			while (m.find()) {
				n++;
			}
			nNumericsMapped = n;
			addressMapping = Pattern.compile(ma.replace("\\d", "(\\d+)"));
			oscAddress = ma.replace("\\d", "*");
			if (ta != null && ta.length() > 0) {
				expectedTypes = new int[ta.length()];
				int i = 0;
				for (char c: ta.toCharArray()) {
					switch(c) {
					case 'i': { // int32
						expectedTypes[i] = System.identityHashCode(Integer.class);
						break;
					}
					case 'f': { // float32
						expectedTypes[i] = System.identityHashCode(Float.class);
						break;
					}
					case 's': { // OSC-string
						expectedTypes[i] = 0;
						break;
					}
					case 'b': { // OSC-blob
						expectedTypes[i] = 0;
						break;
					}
					case 'h': { //  64 bit big-endian two's complement integer
						expectedTypes[i] = 0;
						break;
					}
					case 't': { // OSC-timetag
						expectedTypes[i] = 0;
						break;
					}
					case 'd': { //64 bit ("double") IEEE 754 floating point number
						expectedTypes[i] = 0;
						break;
					}
					case 'c': { // an ascii character, sent as 32 bits
						expectedTypes[i] = 0;
						break;
					}
					case 'r': { //32 bit RGBA color
						expectedTypes[i] = 0;
						break;
					}
					case 'm': { //4 byte MIDI message. Bytes from MSB to LSB are: port id, status byte, data1, data2
						expectedTypes[i] = 0;
						break;
					}
					case '[': { //Indicates the beginning of an array. The tags following are for data in the Array until a close brace tag is reached.
						expectedTypes[i] = 0;
						break;
					}
					case ']': { //Indicates the end of an array.
						expectedTypes[i] = 0;
						break;
					}
					default: {
						expectedTypes[i] = 0;
						break;
					}
					}
					i++;
				}
			}
		}
		
		public void setControllable(String ctype)
		{
			if (ctype == null) {
				ctype = "nothing";
			}
			controllableType = ctype;
		}
		
		public void process(OSCMessage msg) {
			String path=msg.getAddress();
			Matcher m = addressMapping.matcher(path);
			if (m.matches()) { // the groups will be numerics in the address
				// m.group[1...] are any ints found in the match
				int xpi=0;
				for (Object o: msg.getArguments()) {
					int thc = System.identityHashCode(o.getClass());
					if (thc == expectedTypes[xpi]) {
						
					}
					xpi++;
				}
			}
		}
		
		protected String oscAddress;
		protected Pattern addressMapping;
		protected int nNumericsMapped;
		protected int[] expectedTypes;
		protected String controllableType;
		protected ICControl[] cControls;
	}
	
	public class MappingListener implements OSCListener
	{
		public MappingListener() {
		}


		@Override
		public void acceptMessage(Date date, OSCMessage msg) {
			// TODO Auto-generated method stub
			
		}
	}

	/**
	 * Get IP address from first non-localhost interface
	 * @param useIPv4  true=return ipv4, false=return ipv6
	 * @return  address or empty string
	 */
	public static String getIPAddress(boolean useIPv4) {
	    try {
	        List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
	        for (NetworkInterface intf : interfaces) {
	            List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
	            for (InetAddress addr : addrs) {
	                if (!addr.isLoopbackAddress()) {
	                    String sAddr = addr.getHostAddress().toUpperCase();
	                    boolean isIPv4 = addr instanceof Inet4Address;
	                    if (useIPv4) {
	                        if (isIPv4) 
	                            return sAddr;
	                    } else {
	                        if (!isIPv4) {
	                            int delim = sAddr.indexOf('%'); // drop ip6 port suffix
	                            return delim<0 ? sAddr : sAddr.substring(0, delim);
	                        }
	                    }
	                }
	            }
	        }
	    } catch (Exception ex) { } // for now eat exceptions
	    return "";
	}

	public void setInputEnabled(boolean enable) {
		Log.d("OSC", "enable = "+enable);
		if (enable) {
			try {
				if (receiver != null) {
					receiver.stopListening();
					receiver.close();
				}
				receiver = new OSCPortIn(inPort);
			} catch (SocketException e) {
				e.printStackTrace();
			}
			if (receiver != null) {
				Log.d("OSC", "starting reciever "+inPort);
				for (String k: listeners.keySet()) {
					receiver.addListener(k, listeners.get(k));
				}
				receiver.startListening();
			}
		} else {
			if (receiver != null) {
				receiver.stopListening();
				receiver.close();
			}
		}
	
	}

	public void setInputPort(String portName) {
		try {
			int i = Integer.parseInt(portName);
			inPort = i;
		} catch (NumberFormatException e) {
			
		}
	}

	public OSCModule() {
		super();
	}

	public int getPort() {
		return inPort;
	}

	public String getHost() {
		return inHost=getIPAddress(true);
	}

}