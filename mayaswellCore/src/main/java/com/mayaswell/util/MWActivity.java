package com.mayaswell.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import com.mayaswell.widget.FileChooserView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;

public class MWActivity extends Activity {
	protected File currentBankFile = null;
	
	protected static String []plusButtonText = {"Whatever", "Aw .. Crap", ";("};
	protected static String []aboutButtonText = {"Whatever", "Yeah, right", "Did I really need to know that?"};

	protected  EditText saveFileEditText = null;
	
	protected String defaultBankFileName = null;
	
	protected String fileEnding;

	protected Handler periodicUpdateHandler = null;
	protected Runnable periodicUpdater = null;
	protected long uiUpdateRate = 100;

	public MWActivity(String fe) 
	{
		super();
		fileEnding = fe;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		periodicUpdateHandler = new Handler();
		periodicUpdater  = new Runnable() {
			@Override 
			public void run()
			{
				onPeriodicUiUpdate();
				periodicUpdateHandler.postDelayed(periodicUpdater, uiUpdateRate);
			}

		};		
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	protected File getStorageBaseDir() {
		return getExternalFilesDir(null);
	}
	
	protected String getCurrentBankBaseDir()
	{
		String bd = "/";
		if (currentBankFile != null) {
			File currentBankDir = currentBankFile.getParentFile();
			bd = currentBankDir.getAbsolutePath();
		} else {
			File extSD = getStorageBaseDir();
			if (extSD != null) {
				bd = extSD.getAbsolutePath();
			}
		}
//		Log.d("base dir", "got bas dir as "+bd);
		return bd;
	}

	protected void bailAlog(String string)
	{
		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Going so soon?");
		d.setMessage(string);
		d.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
				cleanVerifyCutAndRun(false);
			}
		});
		d.setNeutralButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}

	protected void srslyFialog(final String fnm, final boolean andQuit, final boolean andState)
	{
		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Are you sure about that?");
		d.setMessage("'"+fnm+"'"+" exists. Overwrite?");
		d.setPositiveButton("Totally", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				String pnm = getCurrentBankBaseDir();
				currentBankFile = new File(pnm, fnm);
				saveCurrentBank(andQuit, andState);
			}
		});
		d.setNegativeButton("Ooops", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				saveFialog(fnm, null, andQuit, andState);
			}
		});
		d.show(); 
	}
	
	protected void saveCurrentBank(boolean andQuit, boolean andState) {
	}
	
	protected  boolean cleanVerifyCutAndRun(boolean b) {
		return true;
	}


	protected void saveFialog(final String fnm, final String msg, final boolean andQuit, final boolean andState)
	{
		if (saveFileEditText == null) {
			saveFileEditText = new EditText(this);
		}
		
		saveFileEditText.setHint(fnm);
		saveFileEditText.setText(fnm);
	
		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Save Bank as ...");
		d.setMessage("Enter file name for bank");
		ViewGroup vp = (ViewGroup) saveFileEditText.getParent();
		if (vp != null) {
			vp.removeView(saveFileEditText);
		}
		d.setView(saveFileEditText);
		d.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String tfnm = saveFileEditText.getText().toString();
				if (!tfnm.matches(".*\\"+fileEnding)) {
					tfnm = tfnm + fileEnding;
				}
				String pnm = getCurrentBankBaseDir();
				File sff = new File(pnm, tfnm);
				dialog.dismiss();
				if (sff.exists()) {
					srslyFialog(tfnm, andQuit, andState);
				} else {
					currentBankFile = sff;
					saveCurrentBank(andQuit, andState);
				}
			}
		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	public String aboutButtonText()
	{
		int pbi = (int) Math.floor(Math.random()*aboutButtonText.length);
		return aboutButtonText[pbi];
	}
	
	public static boolean errorIsShowing = false;
	
	public void mafFalog(String msg)
	{
		mafFalog(msg, "Something annoying happened");
	}
	
	public void mafFalog(String msg, String title)
	{
		if (errorIsShowing) {
			Log.d("error control", "display of spurious error skipped: "+msg);
			return;
		}
		errorIsShowing = true;
		int pbi = (int) Math.floor(Math.random()*plusButtonText.length);
		Builder d = new AlertDialog.Builder(this);
		d.setTitle(title);
		d.setMessage(msg);
		d.setPositiveButton(plusButtonText[pbi], new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
				errorIsShowing = false;
			}
		});
		d.setCancelable(true);
		d.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
				errorIsShowing = false;
			}
		});
		d.show(); 
	}
	
	
	protected boolean saveBank(AbstractBank<?, ?> b, String fileName, boolean internal)
	{
		if (b == null || fileName == null) return false;
		FileOutputStream fp=null;
		try {
			if (internal) {
//				Log.d("save", "saving internal "+fileName);
				fp = openFileOutput(fileName, MODE_WORLD_READABLE|MODE_WORLD_WRITEABLE);		
			} else {
				File appDir = getStorageBaseDir();
				File bankFile = new File(appDir, fileName);
				
				fp = new FileOutputStream(bankFile);
			}
			b.save(fp);
			fp.close();
		} catch (IOException e) {
			e.printStackTrace();
			mafFalog("IO exception while saving bank "+e.getMessage());
		}
		return true;
	}
	
	protected boolean saveBank(AbstractBank<?, ?> b, File file)
	{
		if (b == null || file == null) return false;
		FileOutputStream fp=null;
		try {		
			fp = new FileOutputStream(file);
			b.save(fp);
			fp.close();
		} catch (IOException e) {
			e.printStackTrace();
			mafFalog("IO exception while saving bank "+e.getMessage());
		}
		return true;
	}

	/****************************************** 
	 * Events and runners
	 ******************************************/
	protected void onPeriodicUiUpdate() {
	}

	protected void startPeriodicUiUpdate()
	{
//		Log.d("updater", "start periodic");
		periodicUpdater.run(); 
	}
	
	protected void stopPeriodicUiUpdate()
	{
		periodicUpdateHandler.removeCallbacks(periodicUpdater);
	}

	/**
	 * handles messages from buffer control, audio mixer etc.
	 * @param msg
	 * @return
	 */
	protected boolean handleErrorMessage(String msg, int severity) {
		switch (severity) {
		case ErrorLevel.WARNING_EVENT: {
			if (errorIsShowing) {
				Log.d("error", "skip formal display of error ");
				return true;
			}
			mafFalog(msg);
			return true;
		}
		case ErrorLevel.FNF_ERROR_EVENT: {
			if (errorIsShowing) {
				Log.d("error", "skip formal display of error ");
				return true;
			}
			mafFalog("File +'"+msg+"' not found");
			return true;
		}
		case ErrorLevel.ERROR_EVENT: {
			if (errorIsShowing) {
				Log.d("error", "skip formal display of error ");
				return true;
			}
			mafFalog(msg);
			return true;
		}
		case ErrorLevel.FATAL_EVENT: {
			if (errorIsShowing) {
				Log.d("error", "skip formal display of error ");
				return true;
			}
			mafFalog(msg);
			return true;
		}
		}
		return false;
	}
}
