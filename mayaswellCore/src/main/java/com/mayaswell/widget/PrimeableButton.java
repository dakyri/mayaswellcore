package com.mayaswell.widget;

import com.mayaswell.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class PrimeableButton extends Button {

	private static final int[] STATE_PRIMED = {R.attr.state_primed};
	private boolean isPrimed=false;

	public PrimeableButton(Context context) {
		super(context);
	}

	public PrimeableButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PrimeableButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/** 
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
//		Log.d("draw button", "merge drawable "+Integer.toString(id)+", "+Boolean.toString(isPlaying));
		if (isPrimed) {
			mergeDrawableStates(drawableState, STATE_PRIMED);
		}
		return drawableState;
	}
	
	/** 
	 */
	@Override
	protected void drawableStateChanged ()
	{
		super.drawableStateChanged();
	}
	
	/**
	 * @param s
	 */
	public void setPrimedState(boolean s)
	{
		isPrimed = s;
		refreshDrawableState();
	}
	
	public boolean getPrimedState()
	{
		return isPrimed;
	}
}
