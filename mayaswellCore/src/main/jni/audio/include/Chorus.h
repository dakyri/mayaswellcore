/*
 * Chorus.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */

#ifndef CHORUS_H_
#define CHORUS_H_

#include "FloatBuffer.h"
#include "AudioSignal.h"

class Chorus {
public:
	Chorus();
	virtual ~Chorus();

	void initialize();
	void reset();
	void release();

	void apply(float *signal, long sampleFrames);

	void setParameters(float rate, float width, float fb, float delay, float mix);
	void setRate (float v);
	void setWidth (float v);
	void setFeedback (float v);
	void setDelay (float v);
	void setMix(float v);
	void setSweep(void);

	static const int kBufSize=8192;		// must be about 1/5 of a second at given sample rate
protected:
	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto
	float paramMix;		// ditto
	double sweepRate;			// actual calc'd sweep rate
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 to 1.0
	int sweepSamples;			// sweep width in # of samples
	int delaySamples;		// number of samples to run behind filling pointer
	double minSweepSamples;	// lower bound of calculated sweep range, calc'd by setSweep from rate, width, delay
	double maxSweepSamples;	// upper bound, ditto
	int mixMode;				// mapped to supported mix modes
	int fp;					// fill/write pointer
	double step;				// amount to step the sweep each sample
	double sweep;

	FloatBuffer buf;
	float* bufp;
	// output mixing
//	double mixLeftWet;
//	double mixLeftDry;
//	double mixRightWet;
//	double mixRightDry;
};
#define BOTTOM_FREQ 100
#define ROUND(n)		((int)((double)(n)+0.5))
#define PIN(n,min,max) ((n) > (max) ? max : ((n) < (min) ? (min) : (n)))

//
//	modf - vaguely related to the library routine modf(), this macro breaks a double into
//	integer and fractional components i and f respectively.
//
//	n - input number, a double
//	i - integer portion, an integer (the input number integer portion should fit)
//	f - fractional portion, a double
//
#define	MODF(n,i,f) ((i) = (int)(n), (f) = (n) - (double)(i))
#define	NUM_MIX_MODES	7
#define	NUM_DELAYS	11


#endif /* CHORUS_H_ */
