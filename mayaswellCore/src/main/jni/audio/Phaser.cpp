/*
 * Phaser.cpp
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */


#include <stdio.h>
#include <string.h>
#include <math.h>

#include "Phaser.h"

Phaser::Phaser() {
	setRate(0.1);
	setWidth(0.8);
	setFeedback(0.0);
	setStages(0.0);
//	setMixMode(0.0);
}

Phaser::~Phaser() {
}

void
Phaser::initialize()
{
}

void
Phaser::release()
{
}

void
Phaser::reset()
{
}


void
Phaser::setParameters(float _rate, float _width, float _fb, int _stages, float _mix)
{
	setRate(_rate);
	setWidth(_width);
	setFeedback(_fb);
	setStages(_stages);
	setMix(_mix);
}


//
//								setRate
//
void Phaser::setRate (float rate)
{
	paramSweepRate = rate;
	setSweep();
}


//
//								setWidth
//
void Phaser::setWidth (float v)
{
	paramWidth = v;
	setSweep();
}

//
//								setFeedback
//
void Phaser::setFeedback(float v)
{
	paramFeedback = v;
	feedback = v;
}

//
//								setStages
//
//	Expects input from 1,2,3,4,5
//
void Phaser::setStages (int v)
{
	paramStages = v;
	stages = v;
}

void Phaser::setMix(float v)
{
	paramMix = v;
}


/*
//
//								setMixMode
//
//	Expects input 0.0, 0.2, 0.4, 0.6, 0.8 and maps to the five supported mix modes
//
void Phaser::setMixMode (float v)
{
	_paramMixMode = v;
	pPrograms[curProgram]->paramMixMode = v;
	_mixMode = (int)(v * 5.0);
	switch(_mixMode)
	{
	case kMixMono:
	default:
		_mixLeftWet = _mixRightWet = 1.0f;
		_mixLeftDry = _mixRightDry = 1.0f;
		_feedbackPhase = 1.0;
		break;
	case kMixMonoMinus:
		_mixLeftWet = _mixRightWet = -1.0f;
		_mixLeftDry = _mixRightDry = 1.0f;
		_feedbackPhase = -1.0;
		break;
	case kMixWetLeft:
		_mixLeftWet = 1.0f;
		_mixLeftDry = 0.0f;
		_mixRightWet = 0.0f;
		_mixRightDry = 1.0f;
		break;
	case kMixWetRight:
		_mixLeftWet = 0.0f;
		_mixLeftDry = 1.0f;
		_mixRightWet = 1.0f;
		_mixRightDry = 0.0f;
		break;
	case kMixStereo:
		_mixLeftWet = 1.0f;
		_mixLeftDry = 1.0f;
		_mixRightWet = -1.0f;
		_mixRightDry = 1.0f;
		break;
	}
}
*/
//
//								setSweep
//
//	Sets up sweep based on rate and width
//
void Phaser::setSweep()
{
	// calc the actual sweep rate
	double rate = pow(10.0,(double)paramSweepRate);
	rate -= 1.0;
	rate *= 1.1f;
	rate += 0.1f;
	sweepRate = rate;
	double range = paramWidth * 6.0f;

	// do the rest of the inits here just for fun
	wp = minwp = (pi * BOTTOM_FREQ) / (double)signalSampleRate;
	maxwp = (pi * BOTTOM_FREQ * range) / (double)signalSampleRate;

	// figure out increment to multiply by each time
	sweepFactor = pow( (double)range, (double)(sweepRate / (signalSampleRate / 2.0)) );

	// init the all pass line
	lx1 = ly1 =
	lx2 = ly2 =
	lx3 = ly3 =
	lx4 = ly4 =
	lx5 = ly5 =
	lx6 = ly6 =
	lx7 = ly7 =
	lx8 = ly8 =
	lx9 = ly9 =
	lx10 = ly10 = 0.0f;
}


/*
//
//								getParameterName
//
void Phaser::getParameterName (VstInt32 index, char *label)
{
	switch (index)
	{
		case kRate:    strcpy (label, "Rate");			break;
		case kWidth:   strcpy (label, "Width");			break;
		case kFeedback: strcpy(label, "Feedback");		break;
		case kStages:  strcpy (label, "Stages");		break;
		case kMixMode: strcpy (label, "Mix Mode");		break;
	}
}

//
//								getParameterDisplay
//
void Phaser::getParameterDisplay (VstInt32 index, char *text)
{
	char buf[256];
	switch (index)
	{
	case kRate:
		sprintf( buf, "%2.1f", _sweepRate );
		strcpy( text, buf );
		break;
	case kWidth:
		sprintf( buf, "%2.0f", _width * 100.0 );
		strcpy( text, buf );
		break;
	case kFeedback:
		sprintf( buf, "%2.0f", _feedback * 100.0 );
		strcpy( text,buf );
		break;
	case kStages:
		sprintf( buf, "%d", _stages );
		break;
	case kMixMode:
		switch(_mixMode)
		{
		case kMixMono:
			strcpy( text, "mono" );
			break;
		case kMixMonoMinus:
			strcpy( text, "-mono" );
			break;
		case kMixWetLeft:
			strcpy( text, "wet left" );
			break;
		case kMixWetRight:
			strcpy( text, "wet right" );
			break;
		case kMixStereo:
			strcpy( text, "stereo" );
			break;
		}
		break;
	}
}

//
//								getParameterLabel
//
void Phaser::getParameterLabel (VstInt32 index, char *label)
{
	switch (index)
	{
		case kRate:		strcpy (label, "Hz");		break;
		case kWidth:	strcpy( label, "%");		break;
		case kFeedback:	strcpy( label, "%");		break;
		case kStages:	strcpy( label, "");			break;
		case kMixMode:	strcpy( label, "");			break;
	}
}
*/
//
//								processReplacing
//
void Phaser::apply(float *signal, long sampleFrames)
{
/*	float* in1 = inputs[0];
	float* in2 = inputs[1];
	float* out1 = outputs[0];
	float* out2 = outputs[1];
*/
	switch( stages) {
	case 1:
		for( int i=0, j=0; i < sampleFrames; i++, j+=2 ) {
			double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

			// get input value
			float in1 = signal[j];
			float in2 = signal[j+1];
			float inval = (in1 + in2) / 2.0f;
			double inmix = inval  + feedback * feedbackPhase * ly2;

			// run thru the all pass filters
			ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
			lx1 = inmix;
			ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
			lx2 = ly1;

			// develop output mix
/*			out1[i] = (float)PIN(mixLeftDry * inval + mixLeftWet * ly2,-0.99,0.99);
			out2[i] = (float)PIN(mixRightDry * inval + mixRightWet * ly2,-0.99,0.99);*/
			signal[j] = (float)PIN(in1 + paramMix*ly2,-0.99,0.99);
			signal[j+1] = (float)PIN(in2 + paramMix*ly2,-0.99,0.99);

			// step the sweep
			wp *= sweepFactor;                    // adjust freq of filters
			if(wp > maxwp || wp < minwp) {
				sweepFactor = 1.0 / sweepFactor;	// reverse
			}
		}
		break;

	case 2:
	default:
		for( int i=0, j=0; i < sampleFrames; i++, j+=2 ) {
			double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

			// get input value
			float in1 = signal[j];
			float in2 = signal[j+1];
			float inval = (in1 + in2) / 2.0f;
			double inmix = inval + feedback * feedbackPhase * ly4;

			// run thru the all pass filters
			ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
			lx1 = inmix;
			ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
			lx2 = ly1;
			ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
			lx3 = ly2;
			ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
			lx4 = ly3;

/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
			out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
			signal[j] = (float)tanh(inval + ly2);
			signal[j+1] = (float)tanh(inval + ly2);

			// step the sweep
			wp *= sweepFactor;                    // adjust freq of filters
			if(wp > maxwp || wp < minwp) {
				sweepFactor = 1.0 / sweepFactor;	// reverse
			}
		}
		break;
	case 3:
		for( int i=0, j=0; i < sampleFrames; i++, j+=2 ) {
			double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

			// get input value
			float in1 = signal[j];
			float in2 = signal[j+1];
			float inval = (in1 + in2) / 2.0f;
			double inmix = inval + feedback * feedbackPhase * ly6;

			// run thru the all pass filters
			ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
			lx1 = inmix;
			ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
			lx2 = ly1;
			ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
			lx3 = ly2;
			ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
			lx4 = ly3;
			ly5 = coef * (ly5 + ly4) - lx5;			// do 5th filter
			lx5 = ly4;
			ly6 = coef * (ly6 + ly5) - lx6;			// do 6th filter
			lx6 = ly5;

/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
			out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
			signal[j] = (float)tanh(in1 + ly2);
			signal[j+1] = (float)tanh(in2 + ly2);

			// step the sweep
			wp *= sweepFactor;                    // adjust freq of filters
			if(wp > maxwp || wp < minwp) {
				sweepFactor = 1.0 / sweepFactor;	// reverse
			}
		}
		break;

	case 4:
		for( int i=0, j=0; i < sampleFrames; i++, j+=2 ) {
			double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

			// get input value
			float in1 = signal[j];
			float in2 = signal[j+1];
			float inval = (in1 + in2) / 2.0f;
			double inmix = inval + feedback * feedbackPhase * ly8;

			// run thru the all pass filters
			ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
			lx1 = inmix;
			ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
			lx2 = ly1;
			ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
			lx3 = ly2;
			ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
			lx4 = ly3;
			ly5 = coef * (ly5 + ly4) - lx5;			// do 5th filter
			lx5 = ly4;
			ly6 = coef * (ly6 + ly5) - lx6;			// do 6th filter
			lx6 = ly5;
			ly7 = coef * (ly7 + ly6) - lx7;			// do 7th filter
			lx7 = ly6;
			ly8 = coef * (ly8 + ly7) - lx8;			// do 8th filter
			lx8 = ly7;

/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
			out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
			signal[j] = (float)tanh(in1 + ly2);
			signal[j+1] = (float)tanh(in2 + ly2);

			// step the sweep
			wp *= sweepFactor;                    // adjust freq of filters
			if(wp > maxwp || wp < minwp) {
				sweepFactor = 1.0 / sweepFactor;	// reverse
			}
		}
		break;
	case 5:
		for( int i=0, j=0; i < sampleFrames; i++, j+=2 ) {
			double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

			// get input value
			// get input value
			float in1 = signal[j];
			float in2 = signal[j+1];
			float inval = (in1 + in2) / 2.0f;
			double inmix = inval + feedback * feedbackPhase * ly10;

			// run thru the all pass filters
			ly1  = coef * (ly1 + inmix) - lx1;			// do 1st filter
			lx1  = inmix;
			ly2  = coef * (ly2 + ly1) - lx2;			// do 2nd filter
			lx2  = ly1;
			ly3  = coef * (ly3 + ly2) - lx3;			// do 3rd filter
			lx3  = ly2;
			ly4  = coef * (ly4 + ly3) - lx4;			// do 4th filter
			lx4  = ly3;
			ly5  = coef * (ly5 + ly4) - lx5;			// do 5th filter
			lx5  = ly4;
			ly6  = coef * (ly6 + ly5) - lx6;			// do 6th filter
			lx6  = ly5;
			ly7  = coef * (ly7 + ly6) - lx7;			// do 7th filter
			lx7  = ly6;
			ly8  = coef * (ly8 + ly7) - lx8;			// do 8th filter
			lx8  = ly7;
			ly9  = coef * (ly9 + ly8) - lx9;			// do 9th filter
			lx9  = ly8;
			ly10 = coef * (ly10 + ly9) - lx10;			// do 10th filter
			lx10 = ly9;

/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
			out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
			signal[j] = (float)tanh(in1 + ly2);
			signal[j+1] = (float)tanh(in2 + ly2);

			// step the sweep
			wp *= sweepFactor;                    // adjust freq of filters
			if(wp > maxwp || wp < minwp) {
				sweepFactor = 1.0 / sweepFactor;	// reverse
			}
		}
		break;
	}

}
