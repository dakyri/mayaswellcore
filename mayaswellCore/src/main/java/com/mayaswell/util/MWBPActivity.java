package com.mayaswell.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mayaswell.R;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.util.AbstractPatch.Action;

/**
 * base for activities that have patches, banks, and modulators.
 *
 * @param <B>
 * @param <P>
 * @param <C>
 */
public abstract class MWBPActivity<B extends AbstractBank<P, C>, P extends AbstractPatch, C extends Control>
		extends MWActivity {

	protected P currentPatch = null;
	protected int currentPatchInd = -1;
	protected B currentBank = null;

	protected Spinner patchSelector = null;
	protected EditText patchNameEditView=null;
	
	protected ArrayAdapter<Action> patchSelectAdapter = null;
	
	public MWBPActivity(String fe) {
		super(fe);
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final UncaughtExceptionHandler defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		Thread.currentThread().setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				File extSD = getExternalFilesDir(null); // this may as well be in the app local storage
				File sff = new File(extSD, "lastcrash.txt");
				try {
					PrintWriter pww = new PrintWriter(sff);
					e.printStackTrace();
					e.printStackTrace(pww);
					pww.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				if (defaultUEH != null) {
					defaultUEH.uncaughtException(thread, e);
				}
			}
		});
	}
	
	public P getCurrentPatch()
	{
		return currentPatch;
	}
	
	public B getBank()
	{
		return currentBank;
	}

	/**
	 *   selects a patch, shove the info at the pads and assorted bits and force display updates
	 * @param cpInd
	 */
	public void setCurrentPatchInd(int cpInd)
	{

		if (currentBank == null || currentBank.numPatches() == 0) {
			Log.d("MWBPActivity", "scpi: null or empty bank");
			currentPatchInd = -1;
			currentPatch = null;
			return;
		}
		Log.d("MWBPActivity", "scpi "+cpInd+", "+Integer.toString(currentBank.numPatches()));
		if (cpInd < 0 || cpInd >= currentBank.numPatches()) {
			cpInd = 0;
		}
		currentPatch  = currentBank.clonePatch(cpInd);
		currentPatchInd = cpInd;
		setPatch(currentPatch);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setPatchSelector(currentPatchInd);
			}
		});
	}
	
	public void savePatch(int ind, P patch)
	{
		if (currentBank != null && patch != null && ind >= 0 && ind < currentBank.numPatches()) {
			currentBank.saveCopy(ind, patch);
		}
	}

	protected void reloadCurrentPatch() {
		if (currentBank != null) {
			setCurrentPatchInd(currentPatchInd);
		}
	}
	protected void setCurrentPatchState(P p)
	{
		
	}

	protected void setPatchSelector(int i) {
		
	}
	
	protected void setPatch(P p) {
		
	}

	public void ccontrolUpdate(Controllable c, int ccontrolId, float v, int changedBy) {
	}
	
	protected void onNameChangeCurrentPatch()
	{
		patchNameEditView.setVisibility(View.GONE);
		if (patchNameEditView.length() > 0) {
			
		}
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(patchNameEditView.getWindowToken(), 0);//InputMethodManager.HIDE_NOT_ALWAYS
		P p = null;
		try {
			if (patchNameEditView.length() > 0 && currentBank!=null && ((p=currentBank.patch.get(currentPatchInd))!=null)) {
				Editable e = patchNameEditView.getText();
				String pnm=new String(e.toString());
				try {
					pnm = pnm.replaceAll("[\\&\\<\\>]", "_");
					p.name = pnm;
					currentPatch.name = pnm;
					setCurrentPatchItems(currentBank);
					setPatchSelector(currentPatchInd);
				} catch (Exception ex) {
					
				}
			}
		} catch (IndexOutOfBoundsException e) {
 		
 		}
	}
	protected void setCurrentPatchItems(B b) {
		if (b != null && patchSelectAdapter != null) {
			b.setPatchAdapterItems(patchSelectAdapter);
		}
	}

	protected void setupPatchControls() {
		patchSelectAdapter = new ArrayAdapter<Action>(this, R.layout.mw_spinner_item);
		patchSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		if (currentBank != null) {
			setCurrentPatchItems(currentBank);
		}
		if (patchSelector != null) {
			patchSelector.setAdapter(patchSelectAdapter);
			patchSelector.setOnLongClickListener(new OnLongClickListener() {
	
				@Override
				public boolean onLongClick(View v) {
					renPatch();
					return true;
				}
				
			});
			
			patchSelector.setOnItemSelectedListener(new OnItemSelectedListener() 
			{	
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
				{
					Action b = patchSelectAdapter.getItem(position);
					if (currentBank != null) {
						int cbi = b.getOp(); // previously included things that are now menu ops
						if (cbi >= 0 && cbi < currentBank.numPatches()) {
							setCurrentPatchInd(cbi);
						}
					}
					}
	
				@Override
				public void onNothingSelected(AdapterView<?> parentView) 
				{
				}
			});
		}
		if (patchNameEditView != null) {
			patchNameEditView.addTextChangedListener(new TextWatcher() {
				@Override	public void afterTextChanged(Editable s) { }
				@Override	public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
				@Override	public void onTextChanged(CharSequence s, int start, int before, int count) { }
			});
			patchNameEditView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {				
						onNameChangeCurrentPatch();
						return true;
					}
					return false;
				}			
			});
			patchNameEditView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						onNameChangeCurrentPatch();
					}
				}
			});
		}
	}

	
	protected void newPatch() {
		if (currentBank != null) {
			int npInd = currentBank.add(currentBank.newPatch());
			if (npInd >= 0) {
				setCurrentPatchItems(currentBank);
				savePatch(currentPatchInd, currentPatch);
				setCurrentPatchInd(npInd);
			} else {
				setPatchSelector(currentPatchInd);
			}
		}
	}

	protected void delPatch() {
		if (currentBank != null) {
			if (currentBank.numPatches() > 1) {
				if (currentPatchInd < 0) {
					currentPatchInd = 0;
				} else if (currentPatchInd >= currentBank.numPatches()) {
					currentPatchInd = currentBank.numPatches()-1;
				}
				int npInd = currentBank.deletePatch(currentPatchInd);
				if (npInd >= 0) {
					setCurrentPatchItems(currentBank);
					setCurrentPatchInd(npInd);
				} else {
					setPatchSelector(0);
				}
			} else {
				setPatchSelector(0);
			}
		}
	}

	protected void renPatch() {
		setPatchSelector(currentPatchInd);
		if (currentBank != null && currentBank.numPatches() > 0) {
			try {
				P p = currentBank.patch.get(currentPatchInd);
				if (p != null) {
					patchNameEditView.setText(p.name);
					patchNameEditView.setVisibility(View.VISIBLE);
					patchNameEditView.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(patchNameEditView, 0);//InputMethodManager.SHOW_FORCED
				}
			} catch (IndexOutOfBoundsException e) {
				
			}

		}
	}


/**
 * clone: copies the working patch, saves the current one, and selects the new one
 * !! the working patch is cloned from the bank, not a reference to it
 */
	protected void branchPatch() {
		if (currentBank != null) {
			if (currentPatch != null) {
				Log.d("branch", "cpi "+currentPatch.name+" "+currentPatchInd+currentBank.patch.size());
				P p = clonePatch(currentPatch);
				p.name = currentBank.makeIncrementalName(currentPatch.name);
				setCurrentPatchState(p);
				int npInd = currentBank.add(p);
				setCurrentPatchItems(currentBank);
//				savePatch(currentPatchInd, currentPatch);
				setCurrentPatchInd(npInd);
			}
		}
	}
	
	public LFO newLFO()
	{
		return new LFO();
	}
	
	public Envelope newEnv()
	{
		return new Envelope();
	}
	
	public abstract P clonePatch(P p);

	public abstract ArrayAdapter<ResetMode> getLfoRstModeAdapter(LFO.Host h); 
	public abstract ArrayAdapter<ResetMode> getEnvRstModeAdapter(Envelope.Host h); 
	public abstract ArrayAdapter<LFWave> getLfoWaveAdapter(LFO.Host h);
	public abstract ArrayAdapter<FX> getFXAdapter(FX.Host h);
	public abstract ArrayAdapter<ControllableInfo> getControllableAdapter();

	public abstract int lfoRateId(int xyci);
	public abstract int lfoPhaseId(int xyci);
	public abstract int lfoTargetDepthId(int xyci, int tid);
	public abstract int envAttackTimeId(int xyci);
	public abstract int envDecayTimeId(int xyci);
	public abstract int envReleaseTimeId(int xyci);
	public abstract int envSustainTimeId(int xyci);
	public abstract int envSustainLevelId(int xyci);
	public abstract int envTargetDepthId(int xyci, int tid);
	
	public abstract boolean alwaysTempoLock();

	/****************************************
	 * stuff for samples files and searching
	 * consider pushing to a sublclass TODO
	 ****************************************/
	protected String lastSampleFolder=null;
	protected String defaultSampleSearchPath = null;
		
	/**
	 * 
	 * @param name
	 * @param which
	 */
	public void launchSampleFinder(String name, int which) {
		String [] fileFilter = {name};
		String startDir = "/";
		Intent request =new Intent(MWBPActivity.this, SimpleFileChooser.class);
		
		if (lastSampleFolder != null) {
			startDir = lastSampleFolder;
		} else if (defaultSampleSearchPath != null && defaultSampleSearchPath.length() > 0 && defaultSampleSearchPath.charAt(0) == '/') {
			startDir = defaultSampleSearchPath;
		} else {
			File extSD = Environment.getExternalStorageDirectory();
			if (extSD != null) {
				File storageBase = extSD.getParentFile();
				if (storageBase != null) startDir  = storageBase.getAbsolutePath();
			}
		}
		Log.d("whatever", startDir+" pr "+(defaultSampleSearchPath!=null?defaultSampleSearchPath:"null"));
		request.putExtra("startDir", startDir);
		request.putExtra("fileFilter", fileFilter);
		request.putExtra("showHidden", false);
		startActivityForResult(request, which);

		/* on sg2 only useful option given here is a 34d party
	    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
	    intent.setType("image/*");
	    if (intent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(intent, 0);
	    }				*/
	}


}
