package com.mayaswell.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class LongTouchEditText extends EditText {
	public interface Listener {
		void onBlur();
	}

	public LongTouchEditText(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public LongTouchEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public LongTouchEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		longTouchSetup(context);
	}

	@Override
	protected void onAttachedToWindow ()
	{
		longTouchSetup(getContext());
	}
	
	private boolean autoFocus = false;
	private LongTouchEditText view=null;
	private GestureDetector gestureDetector = null;
	private Listener listener = null;
	
	public void setListener(Listener l) {
		listener = l;
	}

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return gestureDetector .onTouchEvent(e);
    }
	public void longTouchSetup(Context context) {
//		Log.d("tea", "long touch setup");
		view = this;
		setFocusable(false);
		setLongClickable(true);
		setTextIsSelectable(true);
		gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
			   @Override
				public boolean onDown(MotionEvent e) {
					return true;
				}
				@Override
				public boolean onDoubleTap(MotionEvent e) {
					if (!hasFocus()) {
						autoFocus = true;
						setFocusable(true);
						setFocusableInTouchMode(true);
						requestFocus();
					}
					return true;
				}
				
				@Override
				public void onLongPress(MotionEvent e) {
					if (!hasFocus()) {
						autoFocus = true;
						setFocusable(true);
						setFocusableInTouchMode(true);
						requestFocus();
					}
					return;
				}
		});
		setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
//			Log.d("tea", "long");
				if (!hasFocus()) {
					autoFocus = true;
					setFocusable(true);
					setFocusableInTouchMode(true);
					requestFocus();
					return true;
				}
				return false;
			}
			
		});
		
		setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
//				Log.d("tea", String.format("%b %b %b", autoFocus, hasFocus, view.isFocusable()));
				if (!autoFocus) {
					if (view.isFocusable()) {
						if (hasFocus) clearFocus();
						setFocusable(false);
						InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(view.getWindowToken(), 0);	
						}
//					if (hasFocus) clearFocus(); // this should never happen, but it does sometimes. 
					return;
				}
				if (hasFocus) {
					autoFocus = false;
					selectAll();					
					InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
				} else {
					InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(view.getWindowToken(), 0);	
					if (listener != null) {
						listener.onBlur();
					}
				}
			}
			
		});
		setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					clearFocus();
					view.setFocusable(false);
				}
				return false;
			}
		});
	}

}
