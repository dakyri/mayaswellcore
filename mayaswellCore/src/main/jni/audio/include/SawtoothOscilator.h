#ifndef SAWTOOTHOSC
#define SAWTOOTHOSC

#include <android/log.h>

#include "AudioSignal.h"

class SawtoothOscilator
{
public:
	SawtoothOscilator(float amp = 1.);
	
	SawtoothOscilator& setAmplitude(float amp);
	SawtoothOscilator& setFrequency(float freq, float sampleRate);
	SawtoothOscilator& setFrequency(float freq);
	SawtoothOscilator& reset();
	
	Sample operator ()	(void);
	void operator ()	(Signal& outSig);
	
private:
	float 					currentValue;
	float 					inc;
	float 					min;
	float 					rval;
};


// SAWTOOTH

inline SawtoothOscilator::SawtoothOscilator(float amp)
	: currentValue(1.)
{
	min = -amp;
	rval = 2*amp;
	setFrequency(440);
}

inline SawtoothOscilator& SawtoothOscilator::setAmplitude(float amp)
{
	currentValue /= inc;
	inc = ( 2 * amp ) / ( rval / inc );
	currentValue *= inc;
	min = -amp;
	rval = 2*amp;
	
	return *this;
}

inline SawtoothOscilator&
SawtoothOscilator::reset()
{
	currentValue = 1;
//	__android_log_print(ANDROID_LOG_DEBUG, "Sawtooth", "reset, inc %g", inc);
	return *this;
}

inline SawtoothOscilator&
SawtoothOscilator::setFrequency(float freq, float sampleRate)
{
	inc = rval / (sampleRate / freq);
//	__android_log_print(ANDROID_LOG_DEBUG, "Sawtooth", "set freq %g inc %g with sr", freq, inc);
	return *this;
}

inline SawtoothOscilator& SawtoothOscilator::setFrequency(float freq)
{
	inc = rval / (signalSampleRate / freq);
//	__android_log_print(ANDROID_LOG_DEBUG, "Sawtooth", "set freq %g inc %g", freq, inc);
	return *this;
}
	
inline Sample SawtoothOscilator::operator()(void)
{
	currentValue -= inc;
	if(currentValue < min)
		currentValue += rval;
//	__android_log_print(ANDROID_LOG_DEBUG, "Sawtooth", "() %g inc %g", currentValue, inc);

	return currentValue;
}
	
inline void SawtoothOscilator::operator()(Signal& outSig)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		outSig[i]=(*this)();
}


#endif
