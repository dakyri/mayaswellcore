/*
 * Filter.cpp
 *
 *  Created on: Dec 9, 2013
 *      Author: dak
 */

#include "Filter.h"
#include "com_mayaswell_audio_Controllable_Filter.h"

#include <android/log.h>

/********************************************************
 * Filter class
 ********************************************************/
Filter::Filter()
{
	type = com_mayaswell_audio_Controllable_Filter_UNITY;
}

Filter::~Filter()
{

}
void
Filter::reset()
{
	switch (type) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		sg.reset();
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
		moog1.reset();
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
		moog2.reset();
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		formant.reset();
		break;
	}
	}
}

void
Filter::setType(int t)
{
	type = t;
}

void
Filter::setFrequency(float f)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set filter %d to %g Hz", type, f);
	switch (type) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		sg.setCutoff(f);
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setType", "sgl %g %g", sg.f1a, sg.f1b);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setFrequency", "moog1 %g %g", f/nyquist, moog1.resonance);
		moog1.setCoefficients(f/nyquist, moog1.resonance);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setFrequency", "moog1 %g %g", f/nyquist, moog2.resonance);
		moog2.setFrequency(f/nyquist);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		formant.setVowel(f);
		break;
	}
	}
}

void
Filter::setResonance(float r)
{
	if (r < 0) r = 0; else if (r > 1) r = 1;
	switch (type) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		sg.setResonance(r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setFrequency", "moog1 %g %g", moog1.frequency, r);
		moog1.setCoefficients(moog1.frequency, r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
		moog2.setResonance(r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		break;
	}
	}
}

void
Filter::set(int t, float f, float r)
{
	switch (t) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		sg.reset();
		sg.setCutoff(f);
		sg.setResonance(r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setType", "moog1 %g %g", f, r);
		moog1.reset();
		moog1.frequency = moog1.resonance = 0;
		moog1.setCoefficients(f/nyquist, r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
//		__android_log_print(ANDROID_LOG_DEBUG, "Filter::setType", "moog2 %g %g", f, r);
		moog2.reset();
		moog2.setCoefficients(f/nyquist, r);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		formant.reset();
		formant.setVowel(f);
		break;
	}
	}
	type = t;
}



/********************************************************
 * Mapping of filter params
 ********************************************************/
void
FilterHost::calc303FilterSweep(float f, float e, float &sweepMin, float &sweepMax, float inc)
{
	inc +=f;
	if (inc > 3.0) inc = 3.0;
	else if (inc < 0.0) inc = 0.0;
	// The maxima AND minima of the filterEnvelope change according to the
	// envelope amount. an envelope amount of zero still gives some enveloping

	//give us an approximateley log scale, it sucks though.
	sweepMin = 195. * exp((inc) *1.38);//2.15);
	sweepMax = sweepMin + (.8 + e* 19.) * sweepMin;
}

float
FilterHost::cubicMap(float p)
{
	p= 0.2 + 0.75*p;
	p *= p*p;	// cubic param map
	return p*nyquist;
}


void
FilterHost::setFilterSweepBounds(float f, float e)
{
	if (f < 0) f = 0; else if (f > 1) f = 1;
	if (e < 0) e = 0; else if (e > 1) e = 1;
	switch (filterType) {
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		calc303FilterSweep(f, e, filterSweepMin, filterSweepMax);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
		filterSweepMax = cubicMap(f);
		filterSweepMin = cubicMap(e);
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
		filterSweepMax = cubicMap(f);
		filterSweepMin = cubicMap(e);
		break;
	}
	default:
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		filterSweepMax = f;
		filterSweepMin = e;
		break;
	}
	}

}

