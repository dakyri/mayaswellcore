package com.mayaswell.util;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;

public class BankSanitizer {
	
	private static BankSanitizer instance=null;
	private static boolean checking = false;

	public static synchronized BankSanitizer getInstance()
	{
		if (instance == null) instance = new BankSanitizer();
		return instance;
	}
	
	public static synchronized boolean isChecking() {
		return checking;
	}
	
	public static synchronized void check(AbstractBank<?,?> b, MWBPActivity<?,?,?> c, AbstractPatch initialPatch, String initialMissing) {
		if (checking) {
			return;
		}
		checking = true;
		if (b == null) {
			return;
		}
		if (c == null) {
			return;
		}
		
		getInstance().sanitize(b, c, initialPatch, initialMissing);
	}

	public static void fileBrowserResult(String filePath) {
		boolean b = false;
		if (filePath != null) {
			getInstance().fileBrowseResult(filePath);
		} else {
			getInstance().sanitizerResume();
		}
	}

	private BankSanitizer()
	{
	}
	
	protected void searchAlog(final File file)
	{
		Builder d = new AlertDialog.Builder(activity);
		d.setTitle("Sample Not Found");
		d.setMessage(file.getAbsolutePath()+" was not found. Browse file system for it?");
		d.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
// quote (,),!,+,=,{,},[,],^,$ as they're all valid filename characters which are also used in R.E.s
				String match = "([\\(\\)\\+\\!\\=\\{\\}\\[\\]\\^\\$])";
				String fnm = file.getName().replaceAll(match, "\\\\$1");
				activity.launchSampleFinder(fnm, EventID.SANITIZE_FINDER);
			}
		});
		d.setNeutralButton("Skip", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				sanitizerResume();
			}
		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finishAndCleanup();
			}
		});
		d.show();
	}

	protected void fixAlog(final String baseO, final String baseN)
	{
		Builder d = new AlertDialog.Builder(activity);
		d.setTitle("Fix other references");
		d.setMessage("Substitute references to "+baseO+" to "+baseN);
		d.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				fixMatchingBasePaths(baseO, baseN);
				sanitizerResume();
			}

		});
		d.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				sanitizerResume();
			}
		});
		d.show();
	}

	private boolean liveChangesMade = false;
	private ArrayList<String> pathsToFix = null;
	private ArrayList<AbstractPatch> patchesToFix = null;
	private MWBPActivity<?,?,?> activity = null;
	int tfsI = -1;
	//				setCurrentPatchState(getCurrentPatch());

	private void fileBrowseResult(String filePath) {
		if (tfsI >= 0 && tfsI < pathsToFix.size()) {
			String s = pathsToFix.get(tfsI);
			repairPass1(s, filePath);
		} else {
			sanitizerResume();
		}
	}

	/**
	 * substitute instances of oldpath with newPath in all broken patches, try to find this a basic root shift
	 * this substitution corresponds to, and possibly apply it
	 * @param oldPath
	 * @param newPath
	 */
	private void repairPass1(String oldPath, String newPath) {
		// silently fix other instances of oldPath
		for (AbstractPatch p: patchesToFix) {
			if (p.fixFilePath(oldPath, newPath)) {
				if (activity != null && p == activity.currentPatch) {
					liveChangesMade = true;
				}
			}
		}
		File rootN = new File(newPath);
		File rootO = new File(oldPath);
		
		// work out what the root shift is
		do {
			rootN = rootN.getParentFile();
			rootO = rootO.getParentFile();
		} while ((rootN.getName().equals(rootO.getName())));
		String baseN = rootN.getAbsolutePath();
		String baseO = rootO.getAbsolutePath();

		fixAlog(baseO, baseN);
	}

	/**
	 * check through our remaining list of dodgy files and if any are found by this substitution, then
	 * make those changes ... and remove this path from teh list to check
	 * @param oldBase
	 * @param newBase
	 */
	private void fixMatchingBasePaths(String oldBase, String newBase) {
		if (oldBase == null || newBase == null) {
			return;
		}
		for (int i=tfsI; i<pathsToFix.size(); i++) {
			String oldPath = pathsToFix.get(i);
			if (oldPath != null && oldPath.startsWith(oldBase)) {
				String newPath = oldPath.replaceFirst(oldBase, newBase);
				File f = new File(newPath);
				if (f.exists()) {
					pathsToFix.set(i, null);
					for (AbstractPatch p: patchesToFix) {
						if (p.fixFilePath(oldPath, newPath)) {
							if (activity != null && p == activity.currentPatch) {
								liveChangesMade = true;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * resume scanning the main path list until we find a non null path to fix and then initiate a dialog, and exit the loop. that dialog should
	 * eventually bring in a result that will continue processing this list ... this will happen asynchronously
	 */
	private void sanitizerResume()
	{
		tfsI++;
		for (; tfsI<pathsToFix.size(); tfsI++) {
			String s = pathsToFix.get(tfsI);
			if (s != null) {
				searchAlog(new File(s));
				break;
			}
		}
//		Log.d("whoa", String.format("%d and %d", tfsI, pathsToFix.size()));
		if (tfsI >= pathsToFix.size()) {
			finishAndCleanup();
		}
	}
	
	private void sanitize(AbstractBank<?,?> b, MWBPActivity<?,?,?> c, AbstractPatch initialPatch, String initialMissing) {
		if (b == null || b.patch == null) {
			return;
		}
		activity = c;
		pathsToFix = new ArrayList<String>();
		patchesToFix = new ArrayList<AbstractPatch>();
		liveChangesMade = false;
		if (initialPatch != null) {
			patchesToFix.add(initialPatch);
		}
		if (initialMissing != null) {
			pathsToFix.add(initialMissing);
		}
		for (AbstractPatch p: b.patch) {
			if (p.getMissingFiles(pathsToFix)) {
				if (!patchesToFix.contains(p)) {
					patchesToFix.add(p);
				}
			}
		}
		if (pathsToFix.size() > 0) {
			tfsI = 0;
			searchAlog(new File(pathsToFix.get(0)));
		}
	}

	public static synchronized void finish() {
		checking = false;
	}

	protected void finishAndCleanup() {
		finish();
		if (liveChangesMade && activity != null) {
			activity.setCurrentPatchInd(activity.currentPatchInd);
		}
	}

}
