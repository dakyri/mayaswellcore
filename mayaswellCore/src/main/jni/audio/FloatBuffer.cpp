/*
 * FloatBuffer.cpp
 *
 *  Created on: Dec 6, 2013
 *      Author: dak
 */

#include "FloatBuffer.h"
#include <android/log.h>

std::vector<FloatBuffer> FloatBuffer::cache;

FloatBuffer::FloatBuffer(float *fb, unsigned long l, bool f)
{
	buffer = fb;
	free = f;
	length = l;
}

void
FloatBuffer::release()
{
	for (int i=0; i<cache.size(); i++) {
		if (cache[i].buffer == buffer) {
			cache[i].free = true;
			return;
		}
	}

}

unsigned long
FloatBuffer::getLength()
{
	return length;
}

void
FloatBuffer::setLength(unsigned long size)
{
	length = size;
}

bool
FloatBuffer::acquire(float* &buf)
{
	for (int i=0; i<cache.size(); i++) {
		if (cache[i].free && cache[i].length > length) {
			cache[i].free = false;
			__android_log_print(ANDROID_LOG_DEBUG, "FloatBuffer", "find 1 %d", (int) length);
			buf = buffer = cache[i].buffer;
			return true;
		}
	}
	__android_log_print(ANDROID_LOG_DEBUG, "FloatBuffer", "allocate 1 %d",  (int)length);
	float* fb = new float[length];
	buf = buffer = fb;
	cache.push_back(FloatBuffer(fb, length, false));
	return false;
}


