/*
 * Chorus.cpp
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "Chorus.h"

Chorus::Chorus() {
	paramSweepRate = 0.2f;
	paramWidth = 0.3f;
	paramFeedback = 0.0f;
	paramDelay = 0.2f;
	paramMix = 0.5f;
	sweepRate = 0.2;
	feedback = 0.0;
	feedbackPhase = 1.0;
	sweepSamples = 0;
	delaySamples = 22;
	setSweep();
	fp = 0;
	sweep = 0.0;
//	mixLeftWet =
//	mixLeftDry =
//	mixRightWet =
//	mixRightDry = 0.5f;

	buf.setLength(kBufSize);;
}

Chorus::~Chorus() {
	release();
}

void
Chorus::initialize()
{
	buf.acquire(bufp);
	reset();
}

void
Chorus::release()
{
	buf.release();
	bufp = NULL;
}

void
Chorus::reset()
{
	if (bufp != NULL) {
		for( int i = 0; i < kBufSize; i++) {
			bufp[i] = 0.0;
		}
	}
}

void
Chorus::setParameters(float _rate, float _width, float _fb, float _delay, float _mix)
{
	setRate(_rate);
	setWidth(_width);
	setFeedback(_fb);
	setDelay(_delay);
	setMix(_mix);
}

//
//								setRate
//
void Chorus::setRate (float rate)
{
	paramSweepRate = rate;
	// map into param onto desired sweep range with log curve
	sweepRate = pow(10.0,(double)paramSweepRate);
	sweepRate  -= 1.0;
	sweepRate  *= 1.1f;
	sweepRate  += 0.1f;

	// finish setup
	setSweep();
}


//
//								setWidth
//
//	Maps 0.0-1.0 input to calculated width in samples from 0ms to 50ms
//
void Chorus::setWidth (float v)
{
	paramWidth = v;
	// map so that we can spec between 0ms and 50ms
	sweepSamples = ROUND(v * 0.05 * signalSampleRate);

	// finish setup
	setSweep();
}

//
//								setDelay
//
//	Expects input from 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
//
void Chorus::setDelay (float v)
{
	paramDelay = v;
	// make onto desired values applying log curve
	double delay = pow(10.0, (double)v * 2.0)/1000.0;		// map logarithmically and convert to seconds
	delaySamples = ROUND(delay * signalSampleRate);

	// finish setup
	setSweep();
}

//
//								setSweep
//
//	Sets up sweep based on rate, depth, and delay as they're all interrelated
//	Assumes _sweepRate, _sweepSamples, and _delaySamples have all been set by
//	setRate, setWidth, and setDelay
//
void Chorus::setSweep()
{
	// calc # of samples per second we'll need to move to achieve spec'd sweep rate
	step = (double)(sweepSamples * 2.0 * sweepRate) / (double)signalSampleRate;
	if( step <= 1.0 ) {
		printf( "_step out of range: %f\n", step );
	}

	// calc min and max sweep now
	minSweepSamples = delaySamples;
	maxSweepSamples = delaySamples + sweepSamples;

	// set intial sweep pointer to midrange
	sweep = (minSweepSamples + maxSweepSamples) / 2;
}


//
//								setFeedback
//
void Chorus::setFeedback(float v)
{
	paramFeedback = v;
	feedback = v;
}

void Chorus::setMix(float v)
{
	paramMix = v;
}


/*
//								setMixMode
//
//	Expects input 0.0, 0.2, 0.4, 0.6, 0.8 and maps to the five supported mix modes
//
void Chorus::setMixMode (float v)
{
	_paramMixMode = v;
	programs[curProgram].paramMixMode = v;
	_mixMode = (int)(v * NUM_MIX_MODES);
	switch(_mixMode)
	{
	case kMixMono:
	default:
		_mixLeftWet = _mixRightWet = 1.0;
		_mixLeftDry = _mixRightDry = 1.0f;
		_feedbackPhase = 1.0;
		break;
	case kMixWetOnly:
		_mixLeftWet = _mixRightWet = 1.0f;
		_mixLeftDry = _mixRightDry = 1.0;
		_feedbackPhase = -1.0;
		break;
	case kMixWetLeft:
		_mixLeftWet = 1.0f;
		_mixLeftDry = 0.0f;
		_mixRightWet = 0.0f;
		_mixRightDry = 1.0f;
		break;
	case kMixWetRight:
		_mixLeftWet = 0.0f;
		_mixLeftDry = 1.0f;
		_mixRightWet = 1.0f;
		_mixRightDry = 0.0f;
		break;
	case kMixStereo:
		_mixLeftWet = 1.0f;
		_mixLeftDry = 1.0f;
		_mixRightWet = -1.0f;
		_mixRightDry = 1.0f;
		break;
	}
}

*/


/*
//
//								getParameterName
//
void Chorus::getParameterName (VstInt32 index, char *label)
{
	switch (index)
	{
		case kRate:    strcpy (label, "Rate");			break;
		case kWidth:   strcpy (label, "Width");			break;
		case kFeedback: strcpy(label, "Feedback");		break;
		case kDelay:	strcpy (label, "Delay");		break;
		case kMixMode: strcpy (label, "Mix Mode");		break;
	}
}

//
//								getParameterDisplay
//
void Chorus::getParameterDisplay (VstInt32 index, char *text)
{
	char buf[64];
	switch (index)
	{
	case kRate:
		sprintf( buf, "%2.1f", _sweepRate );
		strcpy( text, buf );
		break;
	case kWidth:
		sprintf( buf, "%2.0f", _paramWidth * 100.0 );
		strcpy( text, buf );
		break;
	case kFeedback:
		sprintf( buf, "%2.0f", _feedback * 100.0 );
		strcpy( text,buf );
		break;
	case kDelay:
		sprintf( buf, "%d", _paramDelay);
		break;
	case kMixMode:
		switch(_mixMode)
		{
		case kMixMono:
			strcpy( text, "mono" );
			break;
		case kMixWetOnly:
			strcpy( text, "wet only" );
			break;
		case kMixWetLeft:
			strcpy( text, "wet left" );
			break;
		case kMixWetRight:
			strcpy( text, "wet right" );
			break;
		case kMixWetLeft75:
			strcpy( text, "wet left 75%" );
			break;
		case kMixWetRight75:
			strcpy( text, "wet right 75%" );
			break;
		case kMixStereo:
			strcpy( text, "stereo" );
			break;
		}
		break;
	}
}

//
//								getParameterLabel
//
void Chorus::getParameterLabel (VstInt32 index, char *label)
{
	switch (index)
	{
		case kRate:		strcpy (label, "Hz");		break;
		case kWidth:	strcpy( label, "%");		break;
		case kFeedback:	strcpy( label, "%");		break;
		case kDelay:	strcpy( label, "");			break;
		case kMixMode:	strcpy( label, "");			break;
	}
}
*/
//
//								processReplacing
//
void Chorus::apply(float *signal, long sampleFrames)
{
	/*
	float* in1 = inputs[0];
	float* in2 = inputs[1];
	float* out1 = outputs[0];
	float* out2 = outputs[1];
*/
	for (int i=0, j=0; i < sampleFrames; i++, j+=2) {
		// assemble mono input value and store it in circle queue
		float in1 = signal[j];
		float in2 = signal[j+1];

		float inval = (in1 + in2) / 2.0f;
		bufp[fp] = inval;
		fp = (fp + 1) & (kBufSize-1);

		// build the two emptying pointers and do linear interpolation
		int ep1, ep2;
		double w1, w2;
		double ep = fp - sweep;
		MODF(ep, ep1, w2);
		ep1 &= (kBufSize-1);
		ep2 = ep1 + 1;
		ep2 &= (kBufSize-1);
		w1 = 1.0 - w2;
		double outval = bufp[ep1] * w1 + bufp[ep2] * w2;

		float mix = paramMix;
		// develop output mix
/*		out1[i] = (float)PIN(mixLeftDry * inval + mixLeftWet * outval, -0.99, 0.99);
		out2[i] = (float)PIN(mixRightDry * inval + mixRightWet * outval,-0.99, 0.99);*/
		signal[j] = (float)PIN(in1 + mix*outval, -0.99, 0.99);
		signal[j+1] = (float)PIN(in2 + mix*outval,-0.99, 0.99);

		// increment the sweep
		sweep += step;
		if( sweep >= maxSweepSamples || sweep <= minSweepSamples ) {
			step = -step;
		}

	}

}
