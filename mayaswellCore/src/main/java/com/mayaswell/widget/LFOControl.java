package com.mayaswell.widget;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.R;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class LFOControl extends ModulatorControl
{
	public interface LFOControlListener
	{
		void onRateChanged(LFO lfo, float amt);
		void onPhaseChanged(LFO lfo, float amt);
		void onResetChanged(LFO lfo, ResetMode sip);
		void onWaveChanged(LFO lfo, LFWave sip);
		void onTempoLockChanged(LFO lfo, boolean isChecked);
		void onTargetValueChanged(LFO lfo, int targetId, float amt);
		void onTargetControlChanged(LFOControl me, LFO lfo, int targetId, Control tgt, float amt);
	}

	private Spinner wfSelect=null;
	private RotaryPotView ratePot = null;
	private EditText rateView = null;

	private LFOControlListener listener=null;
	private LFO lfo = null;
	private int lfid = -1;

	public LFOControl(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public LFOControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public LFOControl(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.sg_lfo_control, this, true);
		
		label = (TextView)findViewById (R.id.sgCtlLabel);
		ratePot = (RotaryPotView)findViewById (R.id.sgCtlPot);
		rateView = (EditText)findViewById (R.id.sgCtlEditText);
		rstModeSelect = (Spinner) findViewById (R.id.sgCtlSpinner);
		wfSelect = (Spinner) findViewById (R.id.sgCtlSpinnerX);
		tempoLockButton = (ToggleButton) findViewById(R.id.sgCtlToggle);
		targetBody = (RelativeLayout) findViewById(R.id.sgCtlLayout);
		
		rateView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}
	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (s.length() > 0) {
						float v = Float.parseFloat(s.toString());
						if (v < ratePot.getMinVal()) v = ratePot.getMinVal();
						else if (v > ratePot.getMaxVal()) v = ratePot.getMaxVal();
						ratePot.setValue(v);
						notifyListener(v);
					}
				} catch (NumberFormatException e) {
				}
			}
		});
		
		label.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (displayMode == DisplayMode.SHOW_NAME) {
					setDisplay(DisplayMode.SHOW_VALUE_CTLS);
				} else if (displayMode == DisplayMode.SHOW_VALUE_CTLS) {
					setDisplay(DisplayMode.SHOW_TARGET_CTLS);
				} else {
					setDisplay(DisplayMode.SHOW_NAME);
				}
			}
		});
		
		ratePot.setValueBounds(LFO.NormalizedMinRate, LFO.NormalizedMaxRate);
		ratePot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
			@Override
			public void onKnobChanged(float v) {
				rateView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
				notifyListener(v);
			}
		});
		
		rstModeSelect.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				checkAndNotifyReset();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) 
			{
			}
		});
		
		wfSelect.setOnItemSelectedListener(new OnItemSelectedListener() 
		{	
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				checkAndNotifyWave();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) 
			{
			}
		});
		
		tempoLockButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				notifyListener(alwaysTempoLock?true:isChecked);
			}
		});

		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			int px = -1;
			int swr = attrs.getAttributeResourceValue(schemaName, "waveSpinnerWidth", -1);
			if (swr >= 0) {
				px = getResources().getDimensionPixelSize(swr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "waveSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setWaveSelectWidth(px);
			}
			px = 0;
			swr = attrs.getAttributeResourceValue(schemaName, "resetSpinnerWidth", -1);
			if (swr >= 0) {
				px = getResources().getDimensionPixelSize(swr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "resetSpinnerWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				setResetSelectWidth(px);
			}
			px = 0;
			int vwr = attrs.getAttributeResourceValue(schemaName, "viewWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "viewWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0) {
				LayoutParams params = (LayoutParams) rateView.getLayoutParams();
				params.width = px;
			}
		}

	}
	
	public void setListener(LFOControlListener l )
	{
		listener = l;
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setRateValue(float v)
	{
		if (v < ratePot.getMinVal()) v = ratePot.getMinVal();
		else if (v > ratePot.getMaxVal()) v = ratePot.getMaxVal();
		ratePot.setValue(v);
		rateView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
	}
	
	public float getRateValue()
	{
		return ratePot.getValue();
	}
	
	public void setPhaseValue(float v) {
// TODO
	}

	public void setWaveSelectWidth(int px)
	{
		LayoutParams params = (LayoutParams) wfSelect.getLayoutParams();
		params.width = px;
	}
	
	private void checkAndNotifyReset()
	{
		ResetMode sip = null;
		try {
			SpinnerAdapter csa = rstModeSelect.getAdapter();
			if (csa != null) {
				sip = (ResetMode) csa.getItem(rstModeSelect.getSelectedItemPosition());
			}
		} catch (Exception e) {
			
		}
		if (sip != null) {
			notifyListener(sip);
		}
	}
	
	public void setRateValueBounds(float min, float max)
	{
		ratePot.setValueBounds(min, max);
	}
	
	public void setRateCenterValue(float cv)
	{
		ratePot.setCenterValue(cv);
	}
	
	private void checkAndNotifyWave()
	{
		LFWave sip = null;
		try {
			SpinnerAdapter csa = wfSelect.getAdapter();
			if (csa != null) {
				sip = (LFWave) csa.getItem(wfSelect.getSelectedItemPosition());
			}
		} catch (Exception e) {
			
		}
		if (sip != null) {
			notifyListener(sip);
		}
	}
	
	public void setAdapters(ControlsAdapter tga, ArrayAdapter<ResetMode> erma, ArrayAdapter<LFWave> wfma)
	{
		setTargetAdapter(tga);
		rstModeSelect.setAdapter(erma);
		wfSelect.setAdapter(wfma);
		addTargetControl(0);
	}
	
	public void setDisplay(DisplayMode dm)
	{
		displayMode  = dm;
		switch (displayMode) {
		case SHOW_NAME:
			ratePot.setVisibility(GONE);
			rateView.setVisibility(GONE);
			rstModeSelect.setVisibility(GONE);
			wfSelect.setVisibility(GONE);
			tempoLockButton.setVisibility(GONE);
			targetBody.setVisibility(GONE);
			break;
		case SHOW_VALUE_DETAILS:
		case SHOW_VALUE_CTLS:
			ratePot.setVisibility(VISIBLE);
			rateView.setVisibility(VISIBLE);
			rstModeSelect.setVisibility(VISIBLE);
			wfSelect.setVisibility(VISIBLE);
			tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
			targetBody.setVisibility(GONE);
			break;
		case SHOW_TARGET_CTLS:
			ratePot.setVisibility(VISIBLE);
			rateView.setVisibility(VISIBLE);
			rstModeSelect.setVisibility(VISIBLE);
			wfSelect.setVisibility(VISIBLE);
			tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
			targetBody.setVisibility(VISIBLE);
			break;
		}
		if (displayMode == DisplayMode.SHOW_TARGET_CTLS) {
			checkFullyOpenedSiblings(getClass(), DisplayMode.SHOW_VALUE_CTLS);
		}
		SubPanelFragment.layoutEditorControlParent(this);
	}

	private void notifyListener(LFWave sip)
	{
		if (listener != null)
			listener.onWaveChanged(lfo, sip);
	}
	
	private void notifyListener(ResetMode sip) {
		if (listener != null)
			listener.onResetChanged(lfo, sip);
	}

	private void notifyListener(float sip)
	{
		if (listener != null)
			listener.onRateChanged(lfo, sip);
	}
	
	private void notifyListener(boolean isChecked)
	{
		if (listener != null)
			listener.onTempoLockChanged(lfo, isChecked);
	}
	
	@Override
	protected void notifyTargetControlChanged(int targetId, Control tgt, float amt)
	{
		super.notifyTargetControlChanged(targetId, tgt, amt);
		if (listener != null) {
			listener.onTargetControlChanged(this, lfo, targetId, tgt, amt);
		}
	}

	@Override
	protected void notifyTargetValueChanged(int targetId, float amt)
	{
		super.notifyTargetValueChanged(targetId, amt);
		if (listener != null) {
			listener.onTargetValueChanged(lfo, targetId, amt);
		}
	}

	public boolean set(LFO xsp)
	{
		if (xsp == null) {
			xsp = new LFO();
		}
		if (!super.set(xsp)) {
			return false;
		}
		lfo  = xsp;
		if (xsp != null) {
			ratePot.setValue(xsp.rate);
			rateView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, xsp.rate):Float.toString(xsp.rate));
			@SuppressWarnings("unchecked")
			ArrayAdapter<LFWave> wfa = (ArrayAdapter<LFWave>) wfSelect.getAdapter();
			wfSelect.setSelection(wfa.getPosition(xsp.waveform));
		}
		return true;
	}

	public void clearAssignment()
	{
		super.clearAssignment();
		lfo = null;
		ratePot.setValue(ratePot.getcenterVal());
		rateView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, ratePot.getcenterVal()):Float.toString(ratePot.getcenterVal()));
		@SuppressWarnings("unchecked")
		ArrayAdapter<LFWave> wfa = (ArrayAdapter<LFWave>) wfSelect.getAdapter();
		wfSelect.setSelection(wfa.getPosition(LFWave.SIN));
	}

	public int getLFOid() {
		return lfid;
	}

	public void setLFOid(int lfid) {
		this.lfid = lfid;
	}
	
	public int countValidTargets() {
		if (lfo == null) return 0;
		return lfo.countValidTargets();
	}
	
	@Override
	public void setAlwaysTempoLock(boolean b)
	{
		super.setAlwaysTempoLock(b);
		tempoLockButton.setVisibility(alwaysTempoLock? GONE: VISIBLE);
	}
}
