package com.mayaswell.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mayaswell.R;
import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.SamplePlayer;

import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * cleaner rewrite of the com.mayaswell.widget.SampleView
 */
public class BaseSampleView extends View {
	protected boolean rightTouchFirst=false;
	protected float densityMultiplier = getContext().getResources().getDisplayMetrics().density;

	protected Bufferator.SampleInfo currentSampleInfo = null;

	protected Path[] samplePaths = null;
	protected Bitmap sampleBitmap = null;
	protected Rect sampleBitmapSrc = null;

	protected int svW=0;
	protected int svH=0;
	protected Rect svRect = null;
	protected float scrollL=1;
	protected float scrollX=0;

	protected float defaultSampleRate=44100;
	private float minScrollerLen = 0.1f;

	protected Paint textBrush = null;
	protected Paint gridBrush = null;
	protected Paint regionBrush = null;
	protected Paint dataBrush = null;
	protected Paint envelopeBrush = null;
	protected Paint cursorBrush = null;

	protected int cursorColor = 0xffaa3333;
	protected int textColor = 0xdd4285F4;
	protected int regionColor = 0x66F7A60A;
	protected int dataColor = 0xff333377;
	protected int envelopeColor = 0xff111111;
	protected int gridMinorCol = 0xaa888888;
	protected int gridMajorCol = 0xaa444444;

	float textScaledPx = 10 * densityMultiplier;

	//	protected float gridInterval = 0.1f;
//	protected long gridStartPx = 0;
//	protected int colorIdx0;
	protected long regionStart = 0;
	protected long regionLength = 0;
	protected float pxRs;
	protected float pxRe;

	protected double gridWidth = 10;
	protected float gridRate = 1f;
	protected int gridMajorSubdivision = 10;
	protected int gridMinorSubdivision = 10;
	private int scaleFactor;
	private int[] gridDiv;
	private int gridBeatSubDivIndex;
	private static float minGridW = 3;
	private double gridWidthValue = 1;


	public BaseSampleView(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public BaseSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public BaseSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		checkBoundingBox();


		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = 10 * densityMultiplier;

		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Paint.Align.LEFT);

		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setStyle(Paint.Style.STROKE);

		cursorBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		cursorBrush.setStyle(Paint.Style.STROKE);

		regionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		regionBrush.setStyle(Paint.Style.FILL);

		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);

		envelopeBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		envelopeBrush.setStyle(Paint.Style.STROKE);

//		scrollerBack = getResources().getDrawable(com.mayaswell.R.drawable.mw_scroller_bg);
//		scrollerKnob = getResources().getDrawable(com.mayaswell.R.drawable.mw_scroller_knob);

//		scrollerHeight = getResources().getDimensionPixelSize(com.mayaswell.R.dimen.mwSvScrollerHeight);
//		scrollerBackRect = new Rect();
//		scrollerKnobRect = new Rect();
		setScroller(scrollX, scrollL);
		if (attrs != null) {
			TypedArray svAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SampleView, 0, 0);
			TypedArray mvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);

			textColor  = mvAttrs.getColor(R.styleable.MWView_labelColor, textColor);
			gridMinorCol  = svAttrs.getColor(R.styleable.SampleView_gridMinColor, gridMinorCol);
			gridMajorCol  = svAttrs.getColor(R.styleable.SampleView_gridMajColor, gridMajorCol);
			dataColor  = svAttrs.getColor(R.styleable.SampleView_dataColor, dataColor);
			envelopeColor  = svAttrs.getColor(R.styleable.SampleView_envelopeColor, envelopeColor);
			regionColor  = svAttrs.getColor(R.styleable.SampleView_regionColor, regionColor);
			cursorColor  = svAttrs.getColor(R.styleable.SampleView_cursorColor, cursorColor);

			svAttrs.recycle();
			mvAttrs.recycle();
		}
		textBrush.setColor(textColor);
		gridBrush.setColor(gridMinorCol);
		cursorBrush.setColor(cursorColor);
		regionBrush.setColor(regionColor);
		dataBrush.setColor(dataColor);
		envelopeBrush.setColor(envelopeColor);
		setScroller(0,1);

		setGridDiv(10, 10, 10, 6);
		setGridBeatSubDivIndex(1);
	}

	protected Action1<Path[]> onPathsReceived = new Action1<Path[]>() {
		@Override
		public void call(Path[] paths) {
			setSamplePaths(paths);
			invalidate();
		}

	};

	protected Action1<Throwable> onError = new Action1<Throwable>() {
		@Override
		public void call(Throwable e) {
//			Log.d("checkBoundingBox", "got error");
			e.printStackTrace();
		}
	};

	protected Action0 onCompleted = new Action0() {
		@Override
		public void call() {
//			Log.d("checkBoundingBox", "got completion");
		}
	};

	private boolean checkBoundingBox() {
		boolean change = false;
		if (svW == 0 || svH == 0) {
			int w = getMeasuredWidth();
			int h = getMeasuredHeight();
			if (w != svW || h != svH) {
				svW = w;
				svH = h;
				change = true;
			}
//			Log.d("sample view", "check bounds "+svW+", "+svH);
			svRect = new Rect(0, 0, svW, svH);
		}
		return change;
	}

	/**
	 * @param start
	 * @param len
	 */
	protected void setScroller(float start, float len)
	{
		if (len < minScrollerLen) len = minScrollerLen ; else if (len > 1) len = 1;
		if (start < 0) start = 0; else if (start > 1) start = 1;
		scrollX = start;
		scrollL = len;
		/*
		int l = (int) ((svW-2)*len);
		if (l < minScrollerKnobW) l = minScrollerKnobW;
		maxScrollerKnobX = (svW-2-l);
		int x = (int) (start*maxScrollerKnobX);
		if (x < 0) x = 0;
		scrollerKnobRect.set(x+1, svH-scrollerHeight+2, x+l, svH-2);
		*/
//		Log.d("sampleview", "set scroller "+x+", "+start+", "+len);
		setSampleBitmapSrc();

		if (currentSampleInfo != null && svW > 0 && scrollL != 0) {
			setGridParameters();
//			Log.d("setup grid", String.format("%g displayed px 0 %g int %g secs p grid %g px start %d", secsDisplayed, screenPx0, gridInterval, gridWidth, gridStartPx));
		}
//		setCheckRegionValid();
	}

	@Override
	protected void onDraw (Canvas canvas)
	{
		checkBoundingBox();
		Log.d("draw", "got asmmple "+(currentSampleInfo!=null?currentSampleInfo.path:"<>"));
		if (currentSampleInfo != null) {

			if (gridWidth > 0) {
				drawGrid(canvas, true);
			}
			/*
			if (pxRs < svW && pxRe > 0) {
				if (pxRs < 0) pxRs = 0;
				if (pxRe > svW) pxRe = svW;
				canvas.drawRect(pxRs, 0, pxRe, svH-scrollerHeight, regionBrush);
			}*/

			if (sampleBitmap != null) {
//				for (int k=0; k<samplePaths.length; k++) {
//					canvas.drawPath(samplePaths[k], dataBrush);
//				}
				canvas.drawBitmap(sampleBitmap, sampleBitmapSrc, svRect, null);
			}


		}

//		scrollerBack.setBounds(scrollerBackRect);
//		scrollerBack.draw(canvas);
		
//		scrollerKnob.setBounds(scrollerKnobRect);
//		scrollerKnob.draw(canvas);*/
	}


	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		if (checkBoundingBox()) {
			if (currentSampleInfo != null) {
				if (samplePaths == null || currentSampleInfo.gfxCache == null || currentSampleInfo.gfxCache.length < svW) {
					currentSampleInfo.getSamplePaths(svW, svH).subscribe(onPathsReceived, onError, onCompleted);
				}
			}

		}
		// was here to force update of related parameters todo should move to async code
		setTo(currentSampleInfo);
		setScroller(scrollX, scrollL);
//		Log.d("grsampleview", "got size change "+w+", "+h);
	}

	public void setSamplePaths(Path[] samplePaths)
	{
//		Log.d("sample view", "set sample paths "+(samplePaths!=null?samplePaths.length:0)+" channel");
//		checkBoundingBox();
		this.samplePaths = samplePaths;
		if (samplePaths != null) {
			Bitmap b = Bitmap.createBitmap(svW, svH, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(b);
			for (int k = 0; k< samplePaths.length; k++) {
				canvas.drawPath(samplePaths[k], dataBrush);
			}
			sampleBitmap = b;
			setSampleBitmapSrc();
		} else {
			sampleBitmap = null;
		}
	}

	protected void setSampleBitmapSrc()
	{
		if (sampleBitmap != null) {
			int h = sampleBitmap.getHeight();
			int w = sampleBitmap.getWidth();
			int b = (int) (scrollX*(1-scrollL)*w);
			sampleBitmapSrc  = new Rect(b, 0, (int) (b+scrollL*w), h);
		}
	}

	protected long px2frame(float px)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentSampleInfo == null) return 0;
		float p = (scrollX*(1-scrollL)+scrollL*(px/svW));
//		Log.d("sc", String.format("px2f %f %f %f", p, scrollX, scrollL));
//		if (p < 0) p = 0; else if (p > 1) p = 1;
		return (long) (currentSampleInfo.getFrameCount()*p);
	}

	protected float frame2px(long frame)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentSampleInfo == null) return 0;
		float p = (((float)frame)/currentSampleInfo.getFrameCount())-scrollX*(1-scrollL);
		return svW*p/scrollL;
	}

	protected float dur2w(long frame)
	{
		if (svW == 0) return 0;
		if (scrollL == 0) return 0;
		if (currentSampleInfo == null) return 0;
		float p = (((float)frame)/currentSampleInfo.getFrameCount());
		return svW*p/scrollL;
	}

	protected float secs2px(float s)
	{
		return frame2px((long) (s*defaultSampleRate));
	}

	protected float px2secs(int px)
	{
		return ((float)px2frame(px))/defaultSampleRate;
	}


	protected void drawGrid(Canvas canvas, boolean doLabel) {
		if (gridWidth > 0) {
			float pxRs = frame2px(0);
			float gridX = pxRs;
			int i = 0;
			nextLabelXMin = 0;
			while (gridX < svW) {
				if (gridX >= 0) {
					drawGridLine(canvas, gridX, i, doLabel);
				}
				i++;
				gridX += gridWidth;
			}
			gridX = (float) (pxRs-gridWidth);
			i = (gridMinorSubdivision*gridMajorSubdivision)-1;
			while (gridX > 0) {
				drawGridLine(canvas, gridX, i, doLabel);
				i--;
				gridX -= gridWidth;
			}
		}
	}

	private float nextLabelXMin=0;
	private float labelGap = 2;
	protected void drawGridLine(Canvas canvas, float gridX, int i, boolean doLabel) {
		boolean drawLine = true;
		boolean drawLabel = false;
		if (i % gridMinorSubdivision == 0) {
			drawLabel = doLabel;
			if (i % (gridMinorSubdivision*gridMajorSubdivision) == 0) {
				gridBrush.setStrokeWidth(2);
			} else {
				gridBrush.setStrokeWidth(1);
			}
			gridBrush.setColor(gridMajorCol);
		} else {
			if (gridWidth < 2) {
				drawLine = false;
			} else {
				gridBrush.setStrokeWidth(1);
				gridBrush.setColor(gridMinorCol);
			}
		}
		if (drawLine) {
			canvas.drawLine(gridX, 0, gridX, svH, gridBrush);
		}
		if (drawLabel) {
			if (gridX >= nextLabelXMin) {
				float v = (float) (((float) i) * gridWidthValue);
				String xls = String.format("%.2f", v);
				float ylsl = textBrush.measureText(xls);
				float dnx = gridX;
				nextLabelXMin = gridX + ylsl + labelGap;
				float dny = textBrush.descent() - textBrush.ascent();
				canvas.drawText(xls, dnx, dny, textBrush);
			}

		}

	}


	/*
	@Override
	protected void onTouchDown(MotionEvent e) {
		float px0 = frame2px(regionStart);
		float px1 = frame2px(regionStart+regionLength);
		int x0 = (int) e.getX();
		if (x0 > px1-5) {
			touchOffset = 0;
			rightTouchFirst = true;
		} else {
			rightTouchFirst = false;
			if (x0 > px0 && x0 < px1-5) {
				touchOffset = (int) (x0-px0);
			} else if (x0 <= px0){
				touchOffset = 0;
			}
		}
	}*/
	/*
	@Override
	protected void onTouchSelecting(MotionEvent e) {
		long fr0=0;
		long fr1=0;
		boolean hasLength = false;
		boolean hasStart = false;
		if (rightTouchFirst) {
			fr1 = px2frame(e.getX());
			fr0 = regionStart;
			hasLength = true;
		} else {
			fr0 = px2frame(e.getX()-touchOffset);
			hasStart = true;
		}
		if (e.getPointerCount() > 1) {
			if (rightTouchFirst) {
				fr0 = px2frame(e.getX(1));
				hasStart = true;
			} else {
				fr1 = px2frame(e.getX(1));
				hasLength = true;
			}
		}
		if (hasStart) {
			if (fr0 < 0) fr0 = 0;
			if (fr0 != regionStart && listener != null) {
				listener.onRegionStartChanged(fr0);
			}
			setRegionStart(fr0);
		}
		if (hasLength) {
			if (fr1 > fr0) {
				if ((fr1-fr0) != regionLength && listener != null) {
					listener.onRegionLengthChanged(fr1-fr0);
				}
				setRegionLength(fr1-fr0);
			}
		}
		invalidate();
	}*/

	public void setGridMetric(float tempo, int maj, int min, int sub, int sup) {
		gridRate = tempo/60;
		setGridDiv(sub, min, maj, sup);
		setGridParameters();
	}

	private void setGridDiv(int ... div) {
		int [] g = new int[div.length];
		int j=0;
		for (int i: div) {
			g[j++] = (i>=2)?i:2;
		}
		gridDiv = g;
	}

	
	private int grid4(int sf) {
		sf += gridBeatSubDivIndex;
		if (gridDiv == null) return 2;
		if (sf < 0) return gridDiv[0];
		if (sf >= gridDiv.length) return gridDiv[gridDiv.length-1];
		return gridDiv[sf];
	}

	public void setGridParameters() {
		if (currentSampleInfo == null || currentSampleInfo.getFrameCount() == 0) {
			gridWidth = 0;
			return;
		}
		double secsDisplayed = ((double)(scrollL * currentSampleInfo.getFrameCount()))/defaultSampleRate;
		double beatsDisplayed = secsDisplayed * gridRate;
		double beatWidth = ((double)svW / beatsDisplayed);

//		Log.d("GRSampleView", "Grid params t "+secsDisplayed+", beatsDisplayed "+beatsDisplayed+", beatWidth "+beatWidth);

		int sf = 0;
		if (beatWidth > 0) {
			double gw = beatWidth / grid4(0);
			double gwv = 1.0 /grid4(0);
			if (gw >= minGridW) {
				while (gw / grid4(sf-1) >= minGridW) {
					sf--;
					gw = gw / grid4(sf);
					gwv = gwv / grid4(sf);
				}
			} else {
				while (gw * grid4(sf+1) < minGridW) {
					sf++;
					gw = gw * grid4(sf);
					gwv = gwv * grid4(sf);
				}
			}
			gridWidth = gw;
			gridWidthValue = gwv;
		} else {
			gridWidth = 0;
			gridWidthValue = 1;
		}
		scaleFactor = sf;
		gridMinorSubdivision = grid4(sf);
		gridMajorSubdivision = grid4(sf+1);
//		Log.d("GRSampleView", "Grid params wid "+gridWidth+", gwv "+gridWidthValue+", scale "+scaleFactor+", "+gridMinorSubdivision+", "+gridMajorSubdivision);
	}

	public boolean setTo(Bufferator.SampleInfo p)
	{
//		Log.d("setToPlayer", String.format("setting to padState %d %d path %s pad %s", svW, svH, p!=null?p.path:"null", q!=null?q.toString():"<>"));
		if (p == null) {
			setSamplePaths(null);
			currentSampleInfo = null;
			return false;
		}
		currentSampleInfo = p;
		if (samplePaths == null || currentSampleInfo == null || currentSampleInfo.gfxCache.length > svW) {
//			Log.d("setToPlayer", String.format("requesting paths!! inf is %s", currentSampleInfo!=null?currentSampleInfo.path:"<>"));
			currentSampleInfo.getSamplePaths(svW, svH).subscribe(onPathsReceived, onError, onCompleted);
		} else {
//			Log.d("setToPlayer", String.format("set:: already got paths paths %d!!", currentSampleInfo.gfxCache.length));
		}

		setScroller(0,1);
		setGridParameters();
		invalidate();
		return true;
	}

	public void setRegionStart(long start)
	{
		regionStart = start;
		setCheckRegionValid();
	}

	public void setRegionLength(long length)
	{
		regionLength = length;
		setCheckRegionValid();
	}

	protected void setCheckRegionValid()
	{
		float pxRsO = pxRs;
		float pxReO = pxRe;
		pxRs = frame2px(regionStart);
		pxRe = frame2px(regionStart+regionLength);
		if (pxRs < pxRsO) pxRsO = pxRs;
		if (pxRe > pxReO) pxReO = pxRe;
//		Log.d("sample view", String.format("bounds of region %f %f w %d",pxRs, pxRe, svW));
		invalidate((int)pxRsO-20,0,(int)pxReO+20,svH);
	}


	public void setGridBeatSubDivIndex(int g) {
		gridBeatSubDivIndex = g;
	}
}

