package com.mayaswell.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Minor over-rider RecyclerView to not intercept childrens touches. Allows RotaryPotView and scrolling of the RecyclerView
 * to both work
 */
public class RecyclistView extends RecyclerView {
//	private GestureDetectorCompat detector;

	public RecyclistView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public RecyclistView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public RecyclistView(Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		/*
		detector = new GestureDetectorCompat(getContext(), new GestureDetector.OnGestureListener() {
			@Override
			public boolean onDown(MotionEvent e) {
				return false;
			}

			@Override
			public void onShowPress(MotionEvent e) {
			}

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				Log.d("RecyclistView", " onSingleTapUp() ");
				return false;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return false;
			}

			@Override
			public void onLongPress(MotionEvent ev) {
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				Log.d("RecyclistView", " fling() " + velocityX + ", " + velocityY);
				return false;
			}
		});
		*/
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		boolean b = super.onTouchEvent(event);
//		Log.d("repsyck", "on touch "+b);
//		detector.onTouchEvent(event);
		return b;
	}

	private boolean forceNoIntercept = false;

	public void setForceNoIntercept(boolean b) {
		forceNoIntercept = b;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		boolean b = super.onInterceptTouchEvent(ev);
//		Log.d("repsyck", "on intercept touch "+b);
		if (forceNoIntercept) {
			return false;
		}
		return b;
	}

}
