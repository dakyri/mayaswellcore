package com.mayaswell.util;

import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;

import java.util.ArrayList;

/**
 * Created by dak on 10/12/2016.
 */
public class MWUtils {
	public static ArrayList<FX> defaultFX() {
		ArrayList<FX> fxl = new ArrayList<FX>();
		fxl.add(new FX(FX.UNITY, "Unity", "unity", "Unity"
		));
		fxl.add(new FX(FX.FBDELAY, "Delay", "delay", "Delay",
				new FXParam(0, FXParam.Type.TIME, "delay time",  "time",  "dT", 0, 0.5f, 4),
				new FXParam(1, FXParam.Type.FLOAT, "feedback",  "fb",  "fb", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.ECHO, "Echo", "echo", "Echo",
				new FXParam(0, FXParam.Type.TIME, "delay time",  "time",  "dT", 0, 0.5f, 4),
				new FXParam(1, FXParam.Type.FLOAT, "volume",  "vol",  "vol", 0, 0.5f, 1),
				new FXParam(2, FXParam.Type.FLOAT, "feedback",  "fb",  "fb", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.FLANGE, "Flange", "flange", "Flange",
				new FXParam(0, FXParam.Type.FLOAT, "rate",  "rate",  "rate", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "width",  "width",  "width", 0, 0.5f, 1),
				new FXParam(2, FXParam.Type.FLOAT, "feedback",  "fb",  "fb", 0, 0.5f, 1),
				new FXParam(3, FXParam.Type.FLOAT, "delay",  "delay",  "delay", 0, 0.5f, 1),
				new FXParam(4, FXParam.Type.FLOAT, "mix",  "mix",  "mix", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.CHORUS, "Chorus", "chorus", "Chorus",
				new FXParam(0, FXParam.Type.FLOAT, "rate",  "rate",  "rate", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "width",  "width",  "width", 0, 0.5f, 1),
				new FXParam(2, FXParam.Type.FLOAT, "feedback",  "fb",  "fb", 0, 0.5f, 1),
				new FXParam(3, FXParam.Type.FLOAT, "delay",  "delay",  "delay", 0, 0.5f, 1),
				new FXParam(4, FXParam.Type.FLOAT, "mix",  "mix",  "mix", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.PHASER, "Phaser", "phaser", "phaser",
				new FXParam(0, FXParam.Type.FLOAT, "rate",  "rate",  "rate", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "width",  "width",  "width", 0, 0.5f, 1),
				new FXParam(2, FXParam.Type.FLOAT, "feedback",  "fb",  "fb", 0, 0.5f, 1),
				new FXParam(3, FXParam.Type.INT, "stages",  "stages",  "stages", 1, 2, 5),
				new FXParam(4, FXParam.Type.FLOAT, "mix",  "mix",  "mix", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.SG_LOW, "SG L.Pass", "sg-lp", "SG Lo",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.SG_BAND, "SG B.Pass", "sg-bp", "SG Band",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.SG_HIGH, "SG H.Pass", "sg-hp", "SG Hi",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG1_LOW, "Moog Lo", "moog1-lp", "Moog Lo",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG1_BAND, "Moog Band", "moog1-bp", "Moog Band",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG1_HIGH, "Moog Hi", "moog1-hp", "Moog Hi",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG2_LOW, "VCF3 Lo", "moog1-lp", "VCF3 Lo",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG2_BAND, "VCF3 Band", "moog1-bp", "VCF3 Band",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.MOOG2_HIGH, "VCF3 Hi", "moog1-hp", "VCF3 Hi",
				new FXParam(0, FXParam.Type.FLOAT, "cutoff",  "cutoff",  "freq", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "resonance",  "res",  "res", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.DISTORTION, "Distortion", "dist", "dist",
				new FXParam(0, FXParam.Type.FLOAT, "soft clip",  "clip",  "clip", 0, 0.5f, 1),
				new FXParam(1, FXParam.Type.FLOAT, "tube",  "tube",  "tube", 0, 0.5f, 1)
		));
		fxl.add(new FX(FX.RINGMOD, "Ring Mod", "ring", "ring",
				new FXParam(0, FXParam.Type.FLOAT, "frequency",  "freq",  "freq", 10, 2000, 10000, true)
		));
		fxl.add(new FX(FX.FORMANT, "Formant", "formant", "Formant",
				new FXParam(0, FXParam.Type.FLOAT, "vowel",  "vwl",  "vwl", 0, 0.5f, 1)
		));
		return fxl;
	}


}
