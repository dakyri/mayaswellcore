package com.mayaswell.audio;

import android.content.Context;
import android.graphics.Path;

import com.mayaswell.audio.Bufferator.SampleInfo;

import rx.Observable;

public abstract class SamplePlayer extends AudioPlayer {
	public abstract SampleInfo getSampleInfo();
	public abstract long getRegionStart();
	public abstract long getRegionLength();
	public abstract long getFrameCount();
	public abstract float getCurrentPosition();
//	public abstract Path[] getSamplePaths(int svW, int i);
	public abstract Observable<Path[]> getSamplePaths(int svW, int i);
	public abstract float proportion2secs(float scrollL);
	
	public static final int PLAY_COMPLETE=3;
	public static final int SET_SAMPLE_DATA = 4;

	public interface Manager {
		Context getContext();
		int countSamplePlayers();
		SamplePlayer getSamplePlayer(int i);
		void startSamplePlayer(SamplePlayer p, boolean b);
		void stopSamplePlayer(SamplePlayer p);
		float getSampleRate();
	}
	
	public SamplePlayer() {
		
	}
	
	protected SampleInfo currentSampleInfo = null;
	protected int currentLoopFrame = 0;
	protected int currentLoopCount = 0;
	protected int currentDirection = 1;
	
	public int nTotalFrames = 0;
	protected int nChannels = 0;

	public boolean hasSample(SampleInfo si)
	{
		return si != null && currentSampleInfo != null && si == currentSampleInfo;
	}
	
}

