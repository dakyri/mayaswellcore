package com.mayaswell.widget;

import com.mayaswell.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StringControl extends LinearLayout {
    public interface StringControlListener {
    	public void onValueChanged(String s);
    }

	private TextView label = null;
	private EditText view = null;

    private StringControlListener listener=null;

	private String val="";
    
	public StringControl(Context context)
	{
		super(context);
		setup(context, null);
	}

	public StringControl(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	public StringControl(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs)
	{
		TypedArray mwvAttrs = null;
		TypedArray mwpAttrs = null;

		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.mw_string_control, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		view = (EditText)findViewById (R.id.mwCtlEditText);
		
		view.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable tv) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s != null) notifyListener(s.toString());
			}
		});

		if (attrs != null) {
			mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);
			
			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				label.setText(lbl);
			}
			int px = (int) mwvAttrs.getDimension(R.styleable.MWView_labelWidth, -1);
			if (px > 0) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}

			px = (int) mwvAttrs.getDimension(R.styleable.MWView_viewWidth, -1);
			if (px > 0) {
				LayoutParams params = (LayoutParams) view.getLayoutParams();
				params.width = px;
			}
		}
		if (mwvAttrs != null) mwvAttrs.recycle();
		if (mwpAttrs != null) mwpAttrs.recycle();
	}
	
    public void setValueListener(StringControlListener l )
    {
    	listener = l;
    }
    
	private void notifyListener(String s)
	{
		if (listener != null)
			listener.onValueChanged(s);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setValue(String v)
	{
		view.setText(v);
	}
	
	public String getValue()
	{
		return val;
	}
}
