package com.mayaswell.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.audio.file.AudioFileException;
import com.mayaswell.audio.file.WavFile;
import com.mayaswell.util.ErrorLevel;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Path;
import android.os.Debug;
import android.util.Log;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class Bufferator
{	
	public static final int MAX_REQUESTS_PER_CYCLE = 10;

	public static class SampleGfxInfo
	{
		public SampleGfxInfo(int length, float[][] dataMin, float[][] dataMax) {
			this.length = length;
			this.dataMin = dataMin;
			this.dataMax = dataMax;
		}
		public SampleGfxInfo() {
			this(0, null, null);
		}
		public float[][] dataMin;
		public float[][] dataMax;
		public int length;
	}

	public class BufferatorException extends Exception {
		public final int severity;
		private final String path;

		public BufferatorException(String msg, int severity) {
			this(msg, severity, "");
		}

		public BufferatorException(String msg, int severity, String path) {
			super(msg);
			this.severity = severity;
			this.path = path;
		}

		public String getPath() {
			return path;
		}
	}
	
	public class SampleChunkInfo
	{
		SampleChunkInfo(int start, int length, int ci, float [] dta)
		{
			this.id = ci;
			this.startFrame = start;
			this.data = dta;
			this.nFrames = length;
			this.timeStamp = System.currentTimeMillis();
		}
		public int id = 0;
		public float[] data;
		public int startFrame;
		public int nFrames;
		public long timeStamp;
		public int nextChunkStartFrame() {
			return startFrame+nFrames;
		}
		public int prevChunkEndFrame() {
			return startFrame-1;
		}
	}

	public class SampleInfo
	{
		private final SampleInfo self;
		private int id = -1;
		public int nTotalFrames = 0;
		public short nChannels = 0;
		public String path = null;
		public int refCount = 0;
		private AudioFile audioFile = null;
		private int chunkChannelCount;
		
		public SampleChunkInfo [] sampleChunk = null;

		public SampleGfxInfo gfxCache = null;
		private int[] reqChunkAuThread = null;
		private int[] reqChunkIfThread = null;

		public SampleInfo(String path, int id)
		{
			this.path = path;
			this.id = id;
			refCount = 0;
			nTotalFrames = 0;
			nChannels = 0;
			sampleChunk = null;
			gfxCache = null;
			reqChunkAuThread = new int[MAX_REQUESTS_PER_CYCLE];
			reqChunkIfThread = new int[MAX_REQUESTS_PER_CYCLE];
			chunkChannelCount = 0;
			self = this;
			for (int i=0; i<reqChunkAuThread.length; i++) {
				reqChunkAuThread[i] = -1;
			}
			for (int i=0; i<reqChunkIfThread.length; i++) {
				reqChunkIfThread[i] = -1;
			}
		}

		public boolean setPath(String p) {
			path = p;
			return setSampleData();
		}
		
		protected boolean setSampleData()
		{
			if (path == null || path.equals("")) {
				return false;
			}
			nTotalFrames = 0;
			try {
				audioFile = getAudioFile(path);
				nChannels = (short) audioFile.getNumChannels();
				nTotalFrames = (int) audioFile.getNumFrames();
				Log.d("bufferator", String.format("set to %s %d chan %d frame", path, nChannels, nTotalFrames));
				
				sampleChunk = allocateChunkBuffers(nTotalFrames, nChannels);
				if (sampleChunk == null) {
					dispatchMafError("Something stupid went wrong allocating buffers. Oops!");
					return false;
				}
				
				requestChunk(0, 2); // request start chunks from the interface request list
				requestChunk(1, 2);
//				for (int i=0; i<sampleChunk.length; i++) {
//					sampleChunk[i] = readChunk(i);
//				}
			} catch (OutOfMemoryError e) {
				if (!fixMemoryPanic()) {
					dispatchMafError("Sadly, there is no room just now, reading "+path);
				}
				releaseResources();
				return false;
			} catch (FileNotFoundException e) {
				dispatchMaf(path, ErrorLevel.FNF_ERROR_EVENT);
				return false;
			} catch (IOException e) {
				dispatchMafError("Catastrophe! Some kind of IO exception! "+e.getMessage());
				releaseResources();
				return false;
			} catch (AudioFileException e) {
				dispatchMafError("How Bizzare! A wav file exception! "+e.getMessage());
				releaseResources();
				return false;
			}
			System.gc();
			return true;
		}

		public Observable<SampleGfxInfo> getMinMax(final int npoints) {
			return Observable.create(new Observable.OnSubscribe<SampleGfxInfo>() {
				@Override
				public void call(Subscriber<? super SampleGfxInfo> subscriber) {
					if (path == null || path.equals("") || nTotalFrames==0 || nChannels == 0 || npoints == 0) {
						Log.d("Bufferator::minmax()", "null data checked "+npoints + " path "+(path != null? path: "<>"));
						subscriber.onCompleted();
						return;
					}
					synchronized (self) { // only one request per SampleInfo simultaneous: this ensures cache gets used
						if (gfxCache != null && gfxCache.length >= npoints) {
							Log.d("Bufferator::minmax()", "found graphic cache " + npoints);
							subscriber.onNext(gfxCache);
							subscriber.onCompleted();
							return;
						}
						Log.d("Bufferator::minmax()", String.format("in thread to make collect %d point of %d paths %s", npoints, nTotalFrames, path));
						int tsp = 0;
						int nFramesPerPx = (int) Math.ceil((float) nTotalFrames / npoints);
						int iw = 0;
						float[][] pMin = new float[nChannels][npoints];
						float[][] pMax = new float[nChannels][npoints];

						AudioFile cmmAudioFile;
//					gfxCache = mokMinMax(nChannels, npoints);
						try {
							cmmAudioFile = getAudioFile(path);
							int framesRead = 0;
							float[] buffer = new float[nFramesPerPx * nChannels];

							while (tsp < nTotalFrames * nChannels && iw < npoints) {
								for (int k = 0; k < nChannels; k++) {
									pMin[k][iw] = pMax[k][iw] = 0;
								}
								framesRead = cmmAudioFile.readFrames(buffer, nFramesPerPx);

//							Log.d("Bufferator::minmax()", String.format("in thread to read %d %d", framesRead, nFramesPerPx));
								tsp = 0;
								for (int j = 0; j < nFramesPerPx && j < framesRead; j++) {
									for (int k = 0; k < nChannels; k++) {
										float s = buffer[tsp + k];
										if (s < pMin[k][iw]) pMin[k][iw] = s;
										else if (s > pMax[k][iw]) pMax[k][iw] = s;
									}
									tsp += nChannels;
								}

								iw++;
							}
							cmmAudioFile.close();
							Log.d("Bufferator::minmax()", String.format("got %d %d", pMin[0].length, pMax[0].length));
							subscriber.onNext(gfxCache=new SampleGfxInfo(npoints, pMin, pMax));
							subscriber.onCompleted();
						} catch (IOException e) {
							subscriber.onError(
								new Exception("Catastrophe! Some kind of IO exception, building image for " + path + ", " + e.getMessage()));
						} catch (AudioFileException e) {
							subscriber.onError(
								new Exception("How Bizzare! An audio file exception, while building image for " + path + ", " + e.getMessage()));
						} catch (Exception e) {
							subscriber.onError(
								new Exception("Completely suprising error " + e.getClass().getCanonicalName() + ", " + e.getMessage()));
						}
					}
				}
			})
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread());
		}

		/**
		 * reads and allocates a new chunk info buffer 
		 *
		 * @param chunk
		 * @return the new buffer info
		 * @throws IOException
		 * @throws AudioFileException
		 */
		public SampleChunkInfo readChunk(int chunk) throws IOException, AudioFileException
		{
			try {
				if (audioFile == null) return null;
				float []buffer = new float[FRAMES_PER_CHUNK*nChannels];
				int csf = chunkStartFrame(chunk);
				audioFile.seekToFrame(csf);
				int framesRead = audioFile.readFrames(buffer, FRAMES_PER_CHUNK);
				SampleChunkInfo sci = new SampleChunkInfo(csf, framesRead, chunk, buffer);
				return sci;
			} catch (OutOfMemoryError e) {
				if (!fixMemoryPanic()) {
					dispatchMafError("Ouch. An out of memory error while trying to read "+(path!=null?path:"(null)"));
				}
				System.gc();
				return null;
			}
		}

		/**
		 * @see #getChunk(int)
		 * @param currentDataFrame address of the needed chunk
		 * @return the chunk, or null if not found
		 */
		public SampleChunkInfo getChunkFor(int currentDataFrame) {
			return getChunk(chunk4Frame(currentDataFrame));
		}
		
		/**
		 *  gets a reference to the buffer for the requested chunk ... at the moment, assume we are being called by the
		 *  audio thread, which is the only place at the moment where it is called from. if not found, we will try to
		 *  request it
		 * @param chunk the index of the needed chunk
		 * @return the chunk, or null if not found
		 */
		public SampleChunkInfo getChunk(int chunk)
		{
			if (sampleChunk == null || chunk < 0 || chunk >= sampleChunk.length)
				return null;
			SampleChunkInfo sci = sampleChunk[chunk];
			if (sci == null || sci.data == null) {
				requestChunk(chunk, 1);
				return null;
			} else {
		// TODO check overhead of this
				sci.timeStamp = System.currentTimeMillis();
			}
			return sci;

		}

		/**
		 * @param requestedChunk which chunk we want
		 * @param from which thread is requesting ... keep separate lists to avoid synch issues, 1 for audio list, else interface list
		 * @return false if the chunk is valid, in range, but can't be requested; true otherwise
		 */
		public boolean requestChunk(int requestedChunk, int from)
		{
			int [] reqChunk;
			if (sampleChunk == null || requestedChunk > sampleChunk.length) {
				return true;
			}
			if (from == 1) reqChunk = reqChunkAuThread;
			else reqChunk = reqChunkIfThread;
			
			for (int i=0; i<reqChunk.length; i++) {
				if (reqChunk[i] == requestedChunk) {
					return true;
				} else if (reqChunk[i] == -1) {
					reqChunk[i] = requestedChunk;
					return true;
				}
			}
			return false;
		}

		public void updateLoadedChunks() throws IOException, AudioFileException
		{
			processChunkRequests(reqChunkAuThread);
			processChunkRequests(reqChunkIfThread);
		}

		private void processChunkRequests(int[] reqList) throws IOException, AudioFileException
		{
			for (int i=0; i<reqList.length; i++) {
				int ch = reqList[i];
				if (ch >= 0 && ch <sampleChunk.length) {
					if (sampleChunk[ch] == null || sampleChunk[ch].data == null) {
						Log.d("bufferator", String.format("process %d/%d requests %d %s", i, reqList.length, ch, path));
						SampleChunkInfo sci = readChunk(ch);
						if (sci != null) {
							sampleChunk[ch] = sci;
							chunkChannelCount += nChannels;
						}
					}
				}
				reqList[i] = -1;
			}
		}

		public int trimAllocatedChunks(int fsi)
		{
			if (nTotalFrames == 0 || sampleChunk == null || sampleChunk.length == 0) {
				return 0;
			}
			if (fsi <= 0) {
				return 0;
			}
			if (chunkChannelCount <= refCount*nChannels*minBufferPerSample) {
				return 0;
			}
			// find the fsi oldest chunks
			SampleChunkInfo togo[] = new SampleChunkInfo[fsi];
			for (int i=0; i<togo.length; i++) {
				togo[i] = null;
			}
			
			for (SampleChunkInfo sci: sampleChunk) {
				if (sci != null) {
					if (!isRequiredChunk(sci.id)) {
						checkAddOldest(sci, togo);
					}
				}
			}
			int fsid = 0;
			for (int i=0; i<togo.length; i++) {
				SampleChunkInfo sci = togo[i];
				if (sci != null) {
// that data should be safe to clear now, but just in case this chunk's being accessed in another thread we should maybe leave it
// till the system decides it has no references ...
					sci.data = null;
					sampleChunk[sci.id] = null;
					togo[i] = null;
					fsid++;
					chunkChannelCount -= nChannels;
					Log.d("bufferator", String.format("just freed %d in %s, count is  %d", sci.id, path, chunkChannelCount));
				}
			}
			return fsid;
		}

		private void checkAddOldest(SampleChunkInfo sci, SampleChunkInfo[] togo)
		{
			int fnd = -1;
			for (int i=0; i<togo.length; i++) {
				SampleChunkInfo scit = togo[i];
				if (scit == null) {
					fnd = i;
					break;
				} else if (sci.timeStamp < scit.timeStamp) {
					fnd = i;
					break;
				} else {
				}
			}
			if (fnd >= 0) {
				for (int i=togo.length-1; i>fnd; i--) {
					togo[i] = togo[i-1];
				}
				togo[fnd] = sci;
			}
		}

		public void releaseResources()
		{
			nTotalFrames = 0;
			if (sampleChunk != null) {
				for (int i=0; i<sampleChunk.length; i++) {
					SampleChunkInfo sci = sampleChunk[i];
					if (sci != null) {
						sci.data = null;
					}
					sampleChunk[i] = null;
				}
			}
			sampleChunk = null;
			try {
				audioFile.close();
			} catch (IOException e) {
// ignore error ... 
			}
			audioFile = null;
			System.gc();
		}

		private int[] requiredChunks = new int[DEFAULT_CACHE_SIZE];
		private int nRequiredChunks = 0;
		public void clearRequiredChunks()
		{
			nRequiredChunks = 0;
		}

		public void addRequiredChunk(int reqdChunk)
		{
			if (isRequiredChunk(reqdChunk)) {
				return;
			}
			if (requiredChunks != null && nRequiredChunks < requiredChunks.length-1) {
				requiredChunks[nRequiredChunks++] = reqdChunk;
			}
		}
		public boolean isRequiredChunk(int c)
		{
			if (requiredChunks == null) return false;
			for (int ch:requiredChunks) {
				if (ch == c) return true;
			}
			return false;
		}

		public Observable<Path[]> getSamplePaths(final int w, final int h) {
//			Log.d("Bufferaetor", "getSamplePaths sample "+path+" for "+w+", "+h);
			return getMinMax(w).map(new Func1<SampleGfxInfo,Path[]>() {
				@Override
				public Path[] call(SampleGfxInfo gfc) {
//					Log.d("Bufferaetor", "got minimaxa "+path+" len "+gfc.length+", "+gfc.dataMax[0].length+" for "+w+", "+h);
					Path[] drawPath = new Path[nChannels];

					float zp[] = new float[nChannels];
					float ch = h/nChannels;
					for (int k=0; k<nChannels; k++) {
						zp[k] = (ch*k+(ch/2));
						drawPath[k] = new Path();
						drawPath[k].moveTo(0, zp[k]);
						for (int j=0; j<gfc.dataMax[k].length; j++) {
							drawPath[k].lineTo(j, zp[k]+(ch*gfc.dataMax[k][j]/2));
						}
						drawPath[k].lineTo(gfc.dataMin[k].length, zp[k]);
						for (int j=gfc.dataMin[k].length-1; j>=0; j--) {
							drawPath[k].lineTo(j, zp[k]+(ch*gfc.dataMin[k][j]/2));
						}
						drawPath[k].close();
					}
					return drawPath;
				}
			});
		}

		public float getFrameCount() {
			return nTotalFrames;
		}
	}

	public static final int DEFAULT_CACHE_SIZE = 9;
	public static final int FRAMES_PER_CHUNK = 64*1024;

	private static Bufferator instance = null;

	private SampleInfo[] cache = null;
	private boolean isRunning = true;
	private int nCached = 0; // keeping track mainly for statistical purposes
	
	private float maxMemory = 0;
	private float threshMemory = 0;
	private float availMemory = 0;
	private float totalPssMemory = 0;
	private float bufferRamLimit = 0.9f; // how close to the edge we are sailing ... ie proportion of the total spare we are going to run up to
	private float fairBufferMemory = 0;
	private int fairChunkBufferChannelCount=0;
	private int initialChunkBufferChannelCount=0;
	public int minBufferPerSample = 4;
	private Thread runnerThread = null;

	private BehaviorSubject<BufferatorException> stateSubject = BehaviorSubject.create();
	private PublishSubject<SampleInfo> gfxSubject = PublishSubject.create();

	protected SamplePlayer.Manager sampleManager = null;

	public static Bufferator init(SamplePlayer.Manager spg) {
		if (instance != null) return instance;
		return (instance = new Bufferator(spg));
	}
	
	private Bufferator(SamplePlayer.Manager spm)
	{
		this.sampleManager = spm;
		setSampleCacheSize(DEFAULT_CACHE_SIZE);
		
		checkAvailableHeap();
	}

	public Subscription monitorState(Action1<BufferatorException> action) {
		return stateSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(action);
	}

	public Subscription monitorGfx(Action1<SampleInfo> observer) {
		return gfxSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
	}

	public boolean fixMemoryPanic()
	{
		int totalChunkChannel = 0;
		for (int i=0; i< cache.length; i++) {
			SampleInfo si = cache[i];
			if (si != null) {
				totalChunkChannel += si.chunkChannelCount;
			}
		}
		fairChunkBufferChannelCount = (int) (bufferRamLimit * totalChunkChannel);
		Log.d("bufferator", String.format("memory panic with %d chunk/channels, bound now set to %d initially %d", totalChunkChannel, fairChunkBufferChannelCount, initialChunkBufferChannelCount));
		return true;
	}

	private void checkAvailableHeap()
	{
		if (sampleManager == null || sampleManager.getContext() == null) {
			return;
		}
		ActivityManager am = (ActivityManager) sampleManager.getContext() .getSystemService(Context.ACTIVITY_SERVICE);
		ActivityManager.MemoryInfo aMemInf = new ActivityManager.MemoryInfo();
		am.getMemoryInfo(aMemInf);
		Runtime rt = Runtime.getRuntime();
		
		Debug.MemoryInfo dMemInf = new Debug.MemoryInfo();
		Debug.getMemoryInfo(dMemInf);
		
		maxMemory = rt.maxMemory()/(1024f*1024f);
		threshMemory =aMemInf.threshold/(1024f*1024f);
		availMemory = aMemInf.availMem/(1024f*1024f);
		totalPssMemory = dMemInf.getTotalPss()/1024f;
		
		fairBufferMemory  = ((maxMemory - totalPssMemory)*bufferRamLimit);
		int chunkBufferChannelBytes = FRAMES_PER_CHUNK*4;
		fairChunkBufferChannelCount = (((int) fairBufferMemory*(1024*1024)))/chunkBufferChannelBytes;
		initialChunkBufferChannelCount = fairChunkBufferChannelCount;
		
		Log.d("memory", String.format("thresh %fMB avail %fMB max %fMB totalPss %fMB -> %d channel-chunks of size %d (%fMB allowed total)", threshMemory, availMemory, maxMemory, totalPssMemory, fairChunkBufferChannelCount, chunkBufferChannelBytes, fairBufferMemory));

// S2 .... thresh 64.000000MB avail 256.933594MB max 48.000000MB class 48 large class 128
// S2 with large heap: thresh 64.000000MB avail 424.964844MB max 128.000000MB class 48 large class 128
		
	}

	public boolean runner()
	{
		if (runnerThread != null) {
			return true;
		}
		runnerThread = new Thread() {
			public void run() {
				isRunning = true;
				
				setPriority(Thread.NORM_PRIORITY);
				while (isRunning) {
					try {
//						Log.d("bufferateor", String.format("%d cache at top of main", cache.length));
						for (int i=0; i< cache.length; i++) {
							SampleInfo si = cache[i];
							if (si != null) {
								try {
									si.updateLoadedChunks();
								} catch (IOException e) {
									dispatchMafError("IO exception in reader thread"+", "+e.getMessage());
								} catch (AudioFileException e) {
									dispatchMafError("Audio file exception in reader thread reading "+(si.path!=null?si.path:"(null)")+", "+e.getMessage());
								}
							}
						}
						int totalChunkChannel = 0;
						int nSampleInfoRefs = 0;
						for (int i=0; i< cache.length; i++) {
							SampleInfo si = cache[i];
							if (si != null) {
								totalChunkChannel += si.chunkChannelCount;
								nSampleInfoRefs += si.refCount;
							}
						}
//						Log.d("bufferateor", String.format("%d %d about to trim", totalChunkChannel, fairChunkBufferChannelCount));
						if (totalChunkChannel > fairChunkBufferChannelCount) { // lets trim the fat!
							int totalChunksSaved = 0;
							// make a list of things we really should keep
							for (SampleInfo si: cache) {
								if (si != null) {
									si.clearRequiredChunks();
								}
							}
							for (int i = 0; i< sampleManager.countSamplePlayers(); i++) {
								SamplePlayer ps = sampleManager.getSamplePlayer(i);
								if (ps != null) {
									SampleInfo si = ps.getSampleInfo();
									if (si != null) {
										si.addRequiredChunk(chunk4Frame((int) ps.getRegionStart()));
									}
								}
							}
							// first minimize stuff that is inactive
//							Log.d("bufferateor", String.format("checking active players"));
							for (int i = 0; i< sampleManager.countSamplePlayers(); i++) {
								SamplePlayer ps = sampleManager.getSamplePlayer(i);
								if (!ps.isPlaying()) {
									SampleInfo si = ps.getSampleInfo();
									if (si != null) {
										boolean isInActiveUse = false;
										for (int ix = i+1; ix< sampleManager.countSamplePlayers(); ix++) {
											SamplePlayer pps = sampleManager.getSamplePlayer(ix);
											if (pps != ps && si == pps.getSampleInfo()) {
												if (pps.isPlaying()) {
													isInActiveUse = true;
													break;
												}
											}
										}
										if (!isInActiveUse) {
											totalChunksSaved += si.trimAllocatedChunks(totalChunkChannel-totalChunksSaved-fairChunkBufferChannelCount);
										}
									}
									if (totalChunkChannel - totalChunksSaved <= fairChunkBufferChannelCount) {
										break;
									}
								}
							}
//							Log.d("bufferateor", String.format("checking chunks to trim %d %d %d", totalChunkChannel, totalChunksSaved, fairChunkBufferChannelCount));
							if (totalChunkChannel - totalChunksSaved > fairChunkBufferChannelCount) {
								int fatPerInfoRef = ((totalChunkChannel - totalChunksSaved) - fairChunkBufferChannelCount + nSampleInfoRefs)/nSampleInfoRefs;
								for (int i=0; i< cache.length; i++) {
									SampleInfo si = cache[i];
									if (si != null) {
										int fsi = (fatPerInfoRef*si.nChannels)/si.refCount;
										int ftpsi = si.trimAllocatedChunks(fsi);
										totalChunksSaved += ftpsi;
										if (totalChunkChannel - totalChunksSaved <= fairChunkBufferChannelCount) {
											break;
										}
									}
								}
							} else { // that's a relief!!!
								
							}
							System.gc();
						}
					} catch (Exception e) {
						Log.d("bufferator", "Ouch. Generic exception ... trying to carry on "+e.getMessage());
						e.printStackTrace();
					}
					// TODO should be a wait on something really
//					Log.d("bufferator", "about to sleep ");
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						Log.d("bufferator", "interrupted "+e.getMessage());
					}
				}
				runnerThread = null;
			}
		};
		runnerThread.start();
		return true;
	}
	
	/**
	 * sets the sample info cache to n. it is expected n is low ... ie it is the number of pads, maximum
	 * @param n
	 */
	public void setSampleCacheSize(int n)
	{
		SampleInfo[] newCache = new SampleInfo[n];
		for (int i=0; i<n; i++) {
			newCache[i] = null;
		}
		nCached = 0;
		if (cache != null) {
			for (int i=0; i<cache.length && i<n; i++) {
				newCache[i] = cache[i];
				if (cache[i] != null) nCached++;
			}
		}
		cache = newCache;
	}
	
	/**
	 * adds this path to the cache, will have already checked that 'path' is not already added. this will be called from the
	 * interface thread only. cache is read by the buffer runner only. the sample info stuff is linked separately by audio
	 * thread and should be thread safe
	 * @param path
	 * @param mxi
	 * @return
	 */
	private SampleInfo add(String path, int mxi)
	{
		SampleInfo p = new SampleInfo(path, mxi);
		if (!p.setSampleData()) {
			return null;
		}
		int i = 0;
		for (i=0; i<cache.length; i++) {
			if (cache[i] == null) {
				cache[i] = p;
				nCached++;
				break;
			}
		}
		if (i == cache.length) {
			p = null;
		}
		return p;
	}

	public static SampleInfo load(String path)
	{
		if (path == null || path.equals("")) {
			return null;
		}
		int mxi = 1;
		SampleInfo fnd = null;
		for (SampleInfo sci:instance.cache) {
			if (sci != null) {
				if (sci.path.equals(path)) {
					fnd = sci;
					break;
				}
				if (sci.id > mxi) {
					mxi = sci.id+1;
				}
			}
		}
		if (fnd == null) {
			Log.d("bufferator", String.format("load, adding new %s", path));
			fnd = instance.add(path, mxi);
		}
		if (fnd != null) {
			fnd.refCount++;
		}
		if (fnd != null) {
			Log.d("bufferator", String.format("find %s id %d ref %d cache size %d", fnd.path, fnd.id, fnd.refCount, instance.nCached));
		} else {
			Log.d("bufferator", "fnd is null at end of load");
		}
		return fnd;
	}

	public static void free(SampleInfo inf)
	{
		if (inf == null) return;
		inf.refCount--;
		if (inf.refCount == 0) {
			for (int i=0; i<instance.cache.length; i++) {
				if (instance.cache[i] == inf) {
					SampleInfo p = instance.cache[i];
					instance.cache[i] = null;
					instance.nCached--;
					p.releaseResources();
					p = null;
					break;
				}
			}
		}
		System.gc();
		Log.d("bufferator", String.format("delete %s id %d ref %d cache size %d", inf.path, inf.id, inf.refCount, instance.nCached));
	}
	
	public static AudioFile getAudioFile(String path) throws IOException, AudioFileException
	{
		WavFile w = new WavFile();
		w.open(new File(path));
		return w;
	}

	public static int chunkStartFrame(int chunkInd)
	{
		return chunkInd * Bufferator.FRAMES_PER_CHUNK;
	}
	
	public static int chunk4Frame(int fr)
	{
		int chunkInd =  fr/Bufferator.FRAMES_PER_CHUNK;
		if (chunkInd < 0) return 0;
		return chunkInd;
	}

	private static SampleChunkInfo[] allocateChunkBuffers(int nTotalFrames, short nChannels)
	{
		int nc = (nTotalFrames+FRAMES_PER_CHUNK-1)/FRAMES_PER_CHUNK;
		if (nc <= 0) nc = 1;
		SampleChunkInfo [] sci = new SampleChunkInfo[nc];
		
		for (int i=0; i<sci.length; i++) {
			sci[i] = null;
		}
		
		return sci;
	}

	public static void cleanup()
	{
		for (int i=0; i<instance.cache.length; i++) {
			if (instance.cache[i] != null) {
				instance.cache[i].releaseResources();
			}
			instance.cache[i] = null;
		}
	}

	public static Bufferator getInstance() {
		return instance;
	}
	
	public static boolean run()
	{
		return instance.runner();
	}
	
	public static boolean stop()
	{
		instance.isRunning = false;
		instance.stateSubject.onCompleted();
		instance.gfxSubject.onCompleted();
		return false;
	}
	
	protected void dispatchGfxConstruct(SampleInfo tgt)
	{
		gfxSubject.onNext(tgt);
	}

	protected void dispatchMafError(String txt)
	{
		dispatchMaf(txt, ErrorLevel.ERROR_EVENT);
	}
	
	protected void dispatchMafWarning(String txt)
	{
		dispatchMaf(txt, ErrorLevel.WARNING_EVENT);
	}
	
	protected void dispatchMafFatal(String txt)
	{
		dispatchMaf(txt, ErrorLevel.FATAL_EVENT);
	}

	protected void dispatchMaf(String txt, int severity) {
		stateSubject.onNext(new BufferatorException(txt, severity));
	}

}
