package com.mayaswell.audio;

import java.util.ArrayList;

import android.util.Log;

public class FX {
	public interface Host extends Controllable {
		
	}
	
	public static final int UNITY=0;
	public static final int SG_LOW=1;
	public static final int SG_HIGH=2;
	public static final int SG_BAND=3;
	public static final int MOOG1_LOW=4;
	public static final int MOOG1_HIGH=5;
	public static final int MOOG1_BAND=6;
	public static final int MOOG2_LOW=7;
	public static final int MOOG2_HIGH=8;
	public static final int MOOG2_BAND=9;
	public static final int FORMANT=10;
	public static final int FBDELAY=20;
	public static final int ECHO=21;
	public static final int ECHO_SIDECHAIN=22;
	public static final int FLANGE=30;
	public static final int CHORUS=31;
	public static final int PHASER=32;
	public static final int PITCH_SHIFT=33;
	public static final int DISTORTION=34;
	public static final int RINGMOD=35;
	
	public static final FX unityFX = new FX();
	
	private int id;
	private String displayName;
	private String saveName;
	private String shortName;
	private ArrayList<FXParam> param;
	
	public FX()
	{
		this(UNITY, "Unity", "unity", "unity");
	}
	
	public FX(int id, String displayName, String saveName, String shortName, FXParam...ps)
	{
		this.id = id;
		this.displayName = displayName;
		this.saveName = saveName;
		this.shortName = shortName;
		this.param = new ArrayList<FXParam>();
		for (FXParam p: ps) {
			param.add(p);
		}
	}
	
	public String toString()
	{
		return displayName;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getSaveName()
	{
		return saveName;
	}
	
	public String getShortName()
	{
		return shortName;
	}

	public ArrayList<FXParam> params() {
		return param;
	}
	
	public int countParams()
	{
		return param != null? param.size():0;
	}

	public static FX parse(String v, ArrayList<FX> fxList) {
		for (FX f:fxList) {
//			Log.d("parse fx name", String.format("%s - %s", f.saveName, v));
			if (f.saveName.equals(v)) {
				return f;
			}
		}
		return unityFX;
	}

	public FXParam parseParam(String v)
	{
		if (param == null) return null;
		for (FXParam p: param) {
			if (p.getSaveName().equals(v)) {
				return p;
			}
		}
		return null;
	}
}
