package com.mayaswell.fragment;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.DisplayMode;

public class SubPanelFragment extends MWFragment {

	public static void layoutEditorControlParent(View v)
	{
		try {
			RelativeLayout rl = (RelativeLayout) v.getParent();
			if (rl != null) {
				layoutEditorControls(rl, 0, -1);
			}
		} catch (ClassCastException e) {
			
		}
	}

	public void layoutEditorControls()
	{
		try {
			RelativeLayout subPanelHolder = (RelativeLayout) getView();
			layoutEditorControls(subPanelHolder, 0, -1);
		} catch (ClassCastException e) {
		}
	}
	
	public static void layoutEditorControls(RelativeLayout subPanelHolder, int from, int topRowBelow)
	{
		Log.d("layout", String.format("layoutEditorControls from %d below %d", from, topRowBelow));
		if (subPanelHolder == null) {
			return;
		}
		int nc = subPanelHolder.getChildCount();
		
		int lastMinId = -1;
		int firstMinId = -1;
		Log.d("layout", String.format("layoutEditorControls nc %d", nc));
		for (int i=from; i<nc; i++) {
			try {
				BaseControl mc = (BaseControl) subPanelHolder.getChildAt(i);
				RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				if (mc.getDisplayMode() == DisplayMode.SHOW_NAME) {
					if (topRowBelow < 0) {
						relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
					} else {
						relativeParams.addRule(RelativeLayout.BELOW, topRowBelow);
					}
					
					if (lastMinId < 0) {
						lastMinId = firstMinId = mc.getId();
						relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
					} else {
						relativeParams.addRule(RelativeLayout.RIGHT_OF, lastMinId);
						lastMinId = mc.getId();
					}
					mc.setLayoutParams(relativeParams);
				}
			} catch (ClassCastException e) {
				
			}
		}
		int lastMaxId = -1;
		for (int i=0; i<nc; i++) {
			try {
				BaseControl mc = (BaseControl) subPanelHolder.getChildAt(i);
				RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				if (mc.getDisplayMode() != DisplayMode.SHOW_NAME) {
					if (lastMaxId >= 0) {
						relativeParams.addRule(RelativeLayout.BELOW, lastMaxId);
					} else if (lastMinId >= 0) {
						relativeParams.addRule(RelativeLayout.BELOW, lastMinId);
					} else 	if (topRowBelow >= 0) {
						relativeParams.addRule(RelativeLayout.BELOW, topRowBelow);
					} else {
						relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
					}
					lastMaxId = mc.getId();
					mc.setLayoutParams(relativeParams);
				}
			} catch (ClassCastException e) {
				
			}
		}
		Log.d("layout", String.format("layoutEditorControls done for nc %d", nc));
	}
	

}
