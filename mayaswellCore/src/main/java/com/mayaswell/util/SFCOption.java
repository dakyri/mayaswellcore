package com.mayaswell.util;

public class SFCOption implements Comparable<SFCOption> {
	private String name;
	private String data;
	private String path;
	 
	public SFCOption(String n,String d,String p)
	{
		name = n;
		data = d;
		path = p;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getData()
	{
		return data;
	}
	
	public String getPath()
	{
		return path;
	}
	
	@Override
	public int compareTo(SFCOption o)
	{
		if(this.name != null)
			return this.name.toLowerCase().compareTo(o.getName().toLowerCase());
		else
			throw new IllegalArgumentException();
	}
}