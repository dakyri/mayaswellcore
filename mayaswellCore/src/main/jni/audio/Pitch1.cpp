/*
 * Pitch1.cpp
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "AudioSignal.h"
#include "Pitch1.h"

Pitch1::Pitch1() {
	/*
		pPrograms[0] = new Pitch1Program( 0.515f, 0.48f, 0.0f, 0.1f, 0.0f, "Harmonizer");
		pPrograms[1] = new Pitch1Program( 0.5f, 0.0f, 0.0f, 0.1f, 0.0f, "Octave Up");
		pPrograms[2] = new Pitch1Program( 0.5f, 0.96f, 0.0f, 0.1f, 0.0f, "Octave Down");

		pPrograms[3] = new Pitch1Program( 0.53f, 0.48f, 0.0f, 0.1f, 0.0f, "Deep Harmonizer");
		pPrograms[4] = new Pitch1Program( 0.5f, 0.2f, 0.0f, 0.1f, 0.0f, "Fifth Up");
		pPrograms[5] = new Pitch1Program( 0.5f, 0.76f, 0.0f, 0.1f, 0.0f, "Fifth Down");
		pPrograms[6] = new Pitch1Program( 0.515f, 0.48f, 0.0f, 0.1f, 0.857f, "Stereo Harmonizer");
		pPrograms[7] = new Pitch1Program( 0.5f, 0.52f, 0.3f, 0.9f, 0.0f, "Descending Echoes");
	*/
		// make sure all instance vars are init'd to some known value
		paramFinePitch = 0.5f;
		paramCoarsePitch = 0.48f;
		paramFeedback = 0.0f;
		paramDelay = 0.0f;
		paramMixMode = 0.0f;
		sweepRate = 0.2;
		feedback = 0.0;
		feedbackPhase = 1.0;
		sweepSamples = 0;
		delaySamples = 22;
		sweepInc = 0.0;
		setSweep();
		mixMode = 0;
		fp = 0;

		outval = 0.0f;
//		mixLeftWet =
//		mixLeftDry =
//		mixRightWet =
//		mixRightDry = 0.5f;

		// allocate the buffer
		buf = new double[kBufSize];
		for( int i = 0; i < kBufSize; i++) {
			buf[i] = 0.0;
		}

}

Pitch1::~Pitch1() {
	if( buf )
		delete[] buf;
}

//
//								setFine
//
//	Sets the fine pitch shift amount
//
void Pitch1::setFine(float finePitch)
{
	paramFinePitch = finePitch;

	// finish setup
	setSweep();
}


//
//								setCoarse
//
//	Maps 0.0-1.0 input to calculated width in samples from 0ms to 10ms
//
void Pitch1::setCoarse (float v)
{
	paramCoarsePitch = v;

	// finish setup
	setSweep();
}

//
//								setDelay
//
//	Map to the expect 0-100ms range.
//
void Pitch1::setDelay (float v)
{
	paramDelay = v;

	// convert incoming float to int sample count for up to 100ms of delay
	delaySamples = 0.1 * signalSampleRate * v;

	// pin to a minimum value so our sweep never collides with the filling pointer
	delaySamples = delaySamples < kMinDelaySamples ? kMinDelaySamples : delaySamples;

	// finish setup
	setSweep();
}

//
//								setSweep
//
//	Sets up sweep based on pitch delta and delay as they're interrelated
//	Assumes paramFinePitch, paramCoarsePitch, and delaySamples have all been set by
//	setFine, setCoarse, and setDelay
//
void Pitch1::setSweep()
{
	// calc the total pitch delta
	double semiTones = 12.0 - ROUND(paramCoarsePitch * kNumPitches);
	double fine = (2.0 * paramFinePitch) - 1.0;
	double pitchDelta = pow(kSemiTone,semiTones + fine);

	// see if we're increasing or decreasing
	increasing = pitchDelta >= 1.0;

	// calc the # of samples in the sweep, 15ms minimum, scaled up to 50ms for an octave up,
	// and 32.5ms for an octave down
	double absDelta = fabs(pitchDelta - 1.0);
	sweepSamples = (kMinSweep + (kMaxSweep-kMinSweep) * absDelta) * signalSampleRate;

	// fix up the pitchDelta to become the sweepInc
	sweepInc = pitchDelta - 1.0;
	sweepInc = -sweepInc;

	// assign initial pointers
	sweepB = sweepSamples / 2.0;
	sweepA = increasing ? sweepSamples : 0.0;
}


//
//								setFeedback
//
void Pitch1::setFeedback(float v)
{
	paramFeedback = v;
	feedback = v;
}


/*
//								setMixMode
//
//	Expects input 0.0, 0.2, 0.4, 0.6, 0.8 and maps to the five supported mix modes
//
void Pitch1::setMixMode (float v)
{
	paramMixMode = v;
	pPrograms[curProgram]->paramMixMode = v;
	mixMode = (int)ROUND(v * NUMMIXMODES);
	switch(mixMode)
	{
	case kMixMono:
	default:
		mixLeftWet = mixRightWet = 1.0f;
		mixLeftDry = mixRightDry = 1.0f;
		feedbackPhase = 1.0;
		break;
	case kMixWetOnly:
		mixLeftWet = mixRightWet = 1.0f;
		mixLeftDry = mixRightDry = 0.0f;
		feedbackPhase = 1.0;
		break;
	case kMixWetLeft:
		mixLeftWet = mixRightDry = 1.0;
		mixRightWet = mixLeftDry = 0.0;
		feedbackPhase = 1.0;
		break;
	case kMixWetRight:
		mixLeftWet = mixRightDry = 0.0;
		mixRightWet = mixLeftDry = 1.0;
		feedbackPhase = 1.0;
		break;
	case kMixWetLeftish:
		mixLeftWet = mixRightDry = 0.87;
		mixRightWet = mixLeftDry = 0.13;
		feedbackPhase = 1.0;
		break;
	case kMixWetRightish:
		mixLeftWet = mixRightDry = 0.13;
		mixRightWet = mixLeftDry = 0.87;
		feedbackPhase = 1.0;
		break;
	case kMixStereo:
		mixLeftWet = 1.0f;
		mixLeftDry = 1.0f;
		mixRightWet = -1.0f;
		mixRightDry = 1.0f;
		break;
	}
}

*/


/*
//
//								getParameterName
//
void Pitch1::getParameterName (VstInt32 index, char *label)
{
	switch (index)
	{
		case kFinePitch:    strcpy (label, "Fine Pitch");			break;
		case kCoarsePitch:   strcpy (label, "Coarse Pitch");		break;
		case kFeedback: strcpy(label, "Feedback");					break;
		case kDelay:	strcpy (label, "Delay");					break;
		case kMixMode: strcpy (label, "Mix Mode");					break;
	}
}

//
//								getParameterDisplay
//
void Pitch1::getParameterDisplay (VstInt32 index, char *text)
{
	char buf[64];
	switch (index)
	{
	case kFinePitch:
		sprintf( buf, "%f", paramFinePitch);
		strcpy( text, buf );
		break;
	case kCoarsePitch:
		sprintf( buf, "%f", paramCoarsePitch);
		strcpy( text, buf );
		break;
	case kFeedback:
		sprintf( buf, "%2.0f", feedback * 100.0 );
		strcpy( text,buf );
		break;
	case kDelay:
		sprintf( buf, "%d", paramDelay);
		break;
	case kMixMode:
		switch(mixMode)
		{
		case kMixMono:
			strcpy( text, "mono" );
			break;
		case kMixWetOnly:
			strcpy( text, "mono wet only" );
			break;
		case kMixWetLeft:
			strcpy( text, "wet left" );
			break;
		case kMixWetRight:
			strcpy( text, "wet right" );
			break;
		case kMixWetLeftish:
			strcpy( text, "wet part left" );
			break;
		case kMixWetRightish:
			strcpy( text, "wet part right" );
			break;
		case kMixStereo:
			strcpy( text, "stereo" );
			break;
		}
		break;
	}
}

//
//								getParameterLabel
//
void Pitch1::getParameterLabel (VstInt32 index, char *label)
{
	switch (index)
	{
		case kFinePitch:		strcpy (label, "");			break;
		case kCoarsePitch:	strcpy( label, "semi-tones");	break;
		case kFeedback:	strcpy( label, "%");				break;
		case kDelay:	strcpy( label, "");					break;
		case kMixMode:	strcpy( label, "");					break;
	}
}
*/
//
//								processReplacing
//
void
Pitch1::apply(float *signal, long sampleFrames)
{
	/*
	float* in1 = inputs[0];
	float* in2 = inputs[1];
	float* out1 = outputs[0];
	float* out2 = outputs[1];
*/
	// iterate thru sample frames
	for(int i=0, j=0; i < sampleFrames; i++, j+=2) {
		float in1 = signal[j];
		float in2 = signal[j+1];

		// assemble mono input value and store it in circle queue
		float inval = (in1 + in2) / 2.0f;
		double inmix = inval + feedback * feedbackPhase * outval;

		buf[fp] = inmix;
		fp = (fp + 1) & (kBufSize-1);

		// do the two taps
		outval = 0.0;

		// channel A build the two emptying pointers and do linear interpolation
		int ep1, ep2;
		double w1, w2;
		double ep = fp - delaySamples;
		ep -= sweepA;
		if( ep < kBufSize ) {
			ep += kBufSize;
		}
		MODF(ep, ep1, w2);
		ep1 &= (kBufSize-1);
		ep2 = ep1 + 1;
		ep2 &= (kBufSize-1);
		w1 = 1.0 - w2;
		double tapout = buf[ep1] * w1 + buf[ep2] * w2;
		double fade = sin((sweepA / sweepSamples) * pi);
		tapout *= fade;
		outval += tapout;

		// step the sweep
		sweepA += sweepInc;
		if( sweepA < 0 ) {
			sweepA = sweepSamples;
		} else if( sweepA >= sweepSamples) {
			sweepA = 0.0;
		}

		// channel B build the two emptying pointers and do linear interpolation
		ep = fp - delaySamples;
		ep -= sweepB;
		if(ep < kBufSize) {
			ep += kBufSize;
		}
		MODF(ep, ep1, w2);
		ep1 &= (kBufSize);
		ep2 = ep1 + 1;
		ep2 &= (kBufSize-1);
		w1 = 1.0 - w2;
		tapout = buf[ep1] * w1 + buf[ep2] * w2;
		fade = sin((sweepB / sweepSamples) * pi);
		tapout *= fade;
		outval += tapout;

		// step the sweep
		sweepB += sweepInc;
		if( sweepB < 0 ) {
			sweepB = sweepSamples;
		} else if( sweepB >= sweepSamples) {
			sweepB = 0.0;
		}

		// develop output mix
//		out1[i] = (float)PIN(mixLeftDry * inval + mixLeftWet * outval,-0.99,0.99);
//		out2[i] = (float)PIN(mixRightDry * inval + mixRightWet * outval,-0.99,0.99);
		signal[j] = (float)PIN(in1 + outval,-0.99,0.99);
		signal[j+1] = (float)PIN(in2 + outval,-0.99,0.99);

	} // end of loop iterating thru sample frames
}
