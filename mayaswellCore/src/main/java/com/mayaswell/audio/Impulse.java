package com.mayaswell.audio;

import java.util.ArrayList;

import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;

public class Impulse extends Modulator {
	public float min;
	public float max;
	public ResetMode trigger;

	public Impulse(ResetMode trigger, float min, float max) {
		this.trigger = trigger;
		this.min = min;
		this.max = max;
		target = new ArrayList<ModulationTarget>(); 
	}
	
	public Impulse()
	{
		this(ResetMode.ONFIRE, -1, 1);
	}

	public Impulse clone()
	{
		Impulse impulse = new Impulse(trigger, min, max);
		for (ModulationTarget m: target) {
			impulse.add(m.clone());
		}
		return impulse;
	}
}
