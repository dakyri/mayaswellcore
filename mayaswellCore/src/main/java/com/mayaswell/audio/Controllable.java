package com.mayaswell.audio;

import android.content.Context;

public interface Controllable
{
	public static final String NOTHING = "nothing";
	public static final String PAD = "pad";
	public static final String BUS = "bus";
	public static final String SYNTH = "synth";
	public static final String BASS = "bass";
	
	public static final int TUNE_RANGE_SEMITONES = 24;
	
	public interface ControllableManager {
		Controllable findControllable(String nm, int ind);
	}
	
	public interface SensorControlManager {
		SensorInfo findSensor(String nm, int ind);
	}
	
	public class Controller {

		public static final int INTERFACE =0;
		public static final int SENSOR = 1;
		public static final int RIBBON = 2;
		public static final int TOUCHPAD = 3;
		public static final int OSC = 4;
		
	}
	
	public abstract class ControlAssign
	{
		public static final String NOTHING = "nothing";
		public static final String TOUCH = "touch";
		
		public Controllable controllable=null;
		
		abstract boolean refreshNormalizedValue();
		abstract boolean setNormalizedValue(float... arguments);
		abstract String labelString();
		abstract String valueString();
		abstract boolean isValid();
	}
	
	public class XYControlAssign extends ControlAssign
	{
		public Control xp=null;
		public Control yp=null;
		
		public String controllerType=null;
		public int controllerId=0;
		
		public float x; // true x value
		public float y;  // true y value
		public float nx; // normalized x value (ie from [0,1]
		public float ny; // normalized y value
		public int trackId; // used when tracking this controller on the touch pad
		
		public boolean enableOnTouch = false;
		public boolean disableOnTouch = false;

		public int l; // l,t,r,b ... bounds for drawing optimization
		public int t;
		public int r;
		public int b;
		
		protected String valueDisplayFormat = "%.3f";
		
		public XYControlAssign(Controllable c, Control cx, Control cy)
		{
			setTo(c, cx, cy);
		}
		
		public XYControlAssign()
		{
			setTo(null, null, null);
		}

		public boolean equals(XYControlAssign a)
		{
			if (a == null) return false;
			if (a.controllable != controllable) return false;
			if (a.xp == null || a.yp == null || a.xp.typeCode == Control.NOTHING || a.yp.typeCode == Control.NOTHING) return false;
			if (xp == null || xp.typeCode == Control.NOTHING || yp == null || yp.typeCode == Control.NOTHING) return false;
			if ((a.xp == xp || a.xp == yp) && (a.yp == xp || a.yp == yp)) {
				return true;
			}
			return false;
		}
		
		public boolean equals(Controllable cp, Control xp2, Control yp2)
		{
			if (cp != controllable) return false;
			if (xp2 == null || yp2 == null || xp2.typeCode == Control.NOTHING || yp2.typeCode == Control.NOTHING) return false;
			if (xp == null || yp == null || xp.typeCode == Control.NOTHING || yp.typeCode == Control.NOTHING) return false;
			if ((xp2 == xp || xp2 == yp) && (yp2 == xp || yp2 == yp)) {
				return true;
			}
			return false;
		}
		
		public void setTo(Controllable c, Control cx, Control cy)
		{
			xp = cx;
			yp = cy;
			controllable = c;
			controllerType = null;
			controllerId = 0;
			trackId = -1;
			x = y = nx = ny = 0;
			l = t = r = b = 0;
			setValueDisplayPrecision(3);
		}
		
		public void setValueDisplayPrecision(int i)
		{
			if (i < 1) i = 1; else if (i > 5) i = 5;
			valueDisplayFormat = "%."+Integer.toString(i)+"f";
		}
		
		public boolean overlaps(XYControlAssign a)
		{
			if (a == null) return false;
			if (a.controllable != controllable) return false;
			if (a.xp != null && a.xp.typeCode != Control.NOTHING) {
				if (a.xp == xp || a.xp == yp) {
					return true;
				}
			}
			if (a.yp != null && a.yp.typeCode != Control.NOTHING) {
				if (a.yp == xp || a.yp == yp) {
					return true;
				}
			}
			return false;
		}
		
		public XYControlAssign clone()
		{
			return new XYControlAssign(controllable, xp, yp);
		}

		public boolean isValid()
		{
			return (controllable != null) &&
					(xp != null) && (yp != null) && 
					(xp.ccontrolId() != Control.NOTHING) && (yp.ccontrolId() != Control.NOTHING);
		}

		public boolean refreshNormalizedValue()
		{
			if (!isValid()) return false;
			x = controllable.floatValue(xp);
			y = controllable.floatValue(yp);
			nx = xp.normalize(x);
			ny = yp.normalize(y);
			return true;
		}

		@Override
		public boolean setNormalizedValue(float... pn)
		{
			if (!isValid()) return false;
			nx = pn[0];
			ny = pn[1];
			x = xp.denormalize(nx);
			y = yp.denormalize(ny);
			controllable.setValue(xp, x);
			controllable.setValue(yp, y);
			return true;
		}

		public String labelString()
		{
			return (controllable!=null?controllable.toString():"none") + ": "+ (xp!=null?xp.fullName():"none")+", "+(yp!=null?yp.fullName():"none");
		}

		public String valueString()
		{
			return String.format(valueDisplayFormat, x)+", "+String.format(valueDisplayFormat, y);
		}


	}
	
	/**
	 * XControlAssign
	 */
	public abstract class XControlAssign extends ControlAssign
	{
		public Control xp=null;
		
		public float x; // true x value
		public float nx; // normalized x value (ie from [0,1]
		
		public float mapMin = 0;
		public float mapCenter = 0.5f;
		public float mapMax = 1;
		public float rangeMin = 0;
		public float rangeCenter = 0.5f;
		public float rangeMax = 1;
		
		public void setTo(Controllable c, Control cx, float mmin, float mcnt, float mmax, float rmin, float rcnt, float rmax)
		{
			controllable = c;
			xp = cx;
			mapMin = 0;
			mapCenter = 0.5f;
			mapMax = 1;
			rangeMin = 0;
			rangeCenter = 0.5f;
			rangeMax = 1;
			x = nx =  0;
		}
		
		public void setMapParams(float min, float cnt, float max)
		{
			mapMin = min;
			mapCenter = cnt;
			mapMax = max;
		}
		
		public void setRangeParams(float min, float cnt, float max)
		{
			if (min > max) {
				float t = min;
				min = max;
				max = t;
			}
			if (cnt < min) cnt = min;
			else if (cnt > max) cnt = max;
			
			rangeMin = min;
			rangeCenter = cnt;
			rangeMax = max;
		}
		
		/**
		 * the normalized value passed is the normalized sensor setting ... the controller responds to values of this between the range given by
		 * [sensorRangeMin ... sensorRangeCenter ... sensorRangeMax] and maps those onto the values [mapMin .. mapCenter .. mapMax]
		 * @param pn the normalized sensor setting.
		 * @see com.mayaswell.audio.Controllable.ControlAssign#setNormalizedValue(float[])
		 */
		public boolean setNormalizedValue(float... pn)
		{
			if (!isValid()) return false;
			float sv = pn[0];
			if (sv > rangeCenter) {
				if (sv > rangeMax) sv = rangeMax;
				nx = mapCenter+(mapMax-mapCenter)*(sv-rangeCenter)/(rangeMax-rangeCenter);
			} else {
				if (sv < rangeMin) sv = rangeMin;
				nx = mapCenter-(mapCenter-mapMin)*(rangeCenter-sv)/(rangeCenter-rangeMin);
			}
			if (nx < 0) nx = 0; else if (nx > 1) nx = 1;
			x = xp.denormalize(nx);
			controllable.setValue(xp, x);
			return true;
		}
		
		public boolean refreshNormalizedValue()
		{
			if (!isValid()) return false;
			x = controllable.floatValue(xp);
			nx = xp.normalize(x);
			return true;
		}

		public String labelString()
		{
			return (controllable!=null?controllable.toString():"none") + ": "+ (xp!=null?xp.fullName():"none");
		}

		public String abbrevLabelString()
		{
			return (controllable!=null?controllable.abbrevName():"none") + ": "+ (xp!=null?xp.abbrevName():"none");
		}

		public String valueString()
		{
			return String.format("%.3f", x);
		}

		public boolean isValid()
		{
			return (controllable != null) &&
					(xp != null) && 
					(xp.ccontrolId() != Control.NOTHING);
		}
		
		public boolean equals(Controllable cp, Control xp2)
		{
			if (cp != controllable) return false;
			if (xp2 == null || xp == null) return false;
			if (xp.typeCode == Control.NOTHING || xp2.typeCode == Control.NOTHING) return false;
			if (xp2 == xp) {
				return true;
			}
			return false;
		}
		
		public boolean equals(XControlAssign a)
		{
			if (a == null) return false;
			if (a.xp == null || xp == null) return false;
			if (a.controllable != controllable) return false;
			if (a.xp.typeCode == Control.NOTHING || xp.typeCode == Control.NOTHING) return false;
			if ((a.xp == xp)) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * RXControlAssign
	 *
	 */
	public class RXControlAssign extends XControlAssign
	{
		public RXControlAssign(Controllable c, Control cx)
		{
			setTo(c, cx);
		}
		
		public RXControlAssign()
		{
			setTo(null, null);
		}
		
		public void setTo(Controllable c, Control cx)
		{
			setTo(c, cx, 0, 0.5f, 1.0f, 0, 0.5f, 1.0f);
		}
		
		public RXControlAssign clone()
		{
			RXControlAssign sca = new RXControlAssign(controllable, xp);
			sca.setMapParams(mapMin, mapCenter, mapMax);
			sca.setRangeParams(rangeMin, rangeCenter, rangeMax);
			return sca;
		}

	}
	
	/**
	 * SXControlAssign
	 *
	 */
	public class SXControlAssign extends XControlAssign
	{
		public SensorInfo sensor = null;
		
		public SXControlAssign(SensorInfo s, Controllable c, Control cx)
		{
			setTo(s, c, cx);
		}
		
		public SXControlAssign()
		{
			setTo(null, null, null);
		}
		
		public void setTo(SensorInfo s, Controllable c, Control cx)
		{
			setTo(s, c, cx, 0, 0.5f, 1.0f, 0, 0.5f, 1.0f);
		}
		
		public void setTo(SensorInfo s, Controllable c, Control cx, float mmin, float mcnt, float mmax, float rmin, float rcnt, float rmax)
		{
			sensor = s;
			setTo(c, cx, mmin, mcnt, mmax, rmin, rcnt, rmax);
		}
		

		public int getSensorId()
		{
			return sensor != null? sensor.controllerId:SensorInfo.NONE;
		}
		
		public String getSensorName()
		{
			return sensor != null? sensor.name:"";
		}

		public String getSensorTag()
		{
			return (sensor != null)? sensor.tag:"none";
		}


		public boolean equals(SXControlAssign a)
		{
			if (a == null) return false;
			if (a.sensor == null || sensor == null) return false;
			if (a.sensor.getControllerId() != sensor.getControllerId()) return false;
			if (a.xp == null || xp == null) return false;
			if (a.controllable != controllable) return false;
			if (a.xp.typeCode == Control.NOTHING || xp.typeCode == Control.NOTHING) return false;
			if ((a.xp == xp)) {
				return true;
			}
			return false;
		}
		
		public boolean equals(SensorInfo sensor2, Controllable cp, Control xp2)
		{
			if (sensor2 == null || sensor == null) return false;
			if (sensor2.getControllerId() != sensor.getControllerId()) return false;
			if (cp != controllable) return false;
			if (xp2.typeCode == Control.NOTHING || xp2.typeCode == Control.NOTHING) return false;
			if (xp2 == xp) {
				return true;
			}
			return false;
		}
		
		public SXControlAssign clone()
		{
			SXControlAssign sca = new SXControlAssign(sensor, controllable, xp);
			sca.setMapParams(mapMin, mapCenter, mapMax);
			sca.setRangeParams(rangeMin, rangeCenter, rangeMax);
			return sca;
		}

		public boolean isValid()
		{
			return (sensor != null) && super.isValid();
		}
	}
	
	public class ControllableInfo
	{
		private Controllable controllable;
		private String name;

		public ControllableInfo(Controllable c, String nm)
		{
			controllable = c;
			name = nm;
		}
		
		public ControlsAdapter getInterfaceTgtAdapter()
		{
			ControlsAdapter ca = controllable.getInterfaceTgtAdapter();
			return controllable.getInterfaceTgtAdapter();
		}
		
		public Controllable getControllable()
		{
			return controllable;
		}
		
		public String toString()
		{
			return name;
		}
	}
	
	public class ControllerInfo
	{
		protected int controllerId=0;
		protected String name;
		protected String tag;

		public ControllerInfo(int sid, String nm, String t)
		{
			controllerId = sid;
			name = nm;
			tag = t;
		}
		
		public String getTag()
		{
			return tag;
		}
		
		public int getControllerId()
		{
			return controllerId;
		}
		
		public String toString()
		{
			return name;
		}
	}
	
	public static class SensorInfo extends ControllerInfo
	{
		public SensorInfo(int sid, String nm, String t) {
			super(sid, nm, t);
		}
		public static final int NONE=0;
		public static final int ROT_X=1;
		public static final int ROT_Y=2;
		public static final int ROT_Z=3;
	}
	
	public class OscilatorWave {
		public static final int UNITY=0;
		public static final int SQUARE=1;
		public static final int SAW=2;
		public static final int SINE=3;
		public static final int NOISE=4;
		public static final int TRIANGLE=5;

		private int id;
		private String displayName;
		private String saveName;
		private String shortName;

		public OscilatorWave()
		{
			this(UNITY, "Unity", "unity", "unity");
		}

		public OscilatorWave(int id, String displayName, String saveName, String shortName) {
			this.id = id;
			this.displayName = displayName;
			this.saveName = saveName;
			this.shortName = shortName;
		}

		public String toString()
		{
			return displayName;
		}

		public int getId()
		{
			return id;
		}

		public String getSaveName()
		{
			return saveName;
		}
	}
	
	public class Filter {
		public static final int UNITY=0;
		public static final int SG_LOW=1;
		public static final int SG_HIGH=2;
		public static final int SG_BAND=3;
		public static final int MOOG1_LOW=4;
		public static final int MOOG1_HIGH=5;
		public static final int MOOG1_BAND=6;
		public static final int MOOG2_LOW=7;
		public static final int MOOG2_HIGH=8;
		public static final int MOOG2_BAND=9;
		public static final int FORMANT=10;
		
		private int id;
		private String displayName;
		private String saveName;
		private String shortName;
		
		public float frequency;
		public float resonance;
		public float envMod;
		
		public Filter()
		{
			this(UNITY, "Unity", "unity", "unity");
		}
		
		public Filter(int id, String displayName, String saveName, String shortName)
		{
			this.id = id;
			this.displayName = displayName;
			this.saveName = saveName;
			this.shortName = shortName;
			frequency = resonance = envMod = 0;
		}
		
		public String toString()
		{
			return displayName;
		}
		
		public int getId()
		{
			return id;
		}
		
		public String getSaveName()
		{
			return saveName;
		}
		
		public String getShortName()
		{
			return shortName;
		}

		public void setEnvMod(float em)
		{
			envMod = em;
		}

		public void setResonance(float r)
		{
			resonance = r;
		}

		public void setFrequency(float f)
		{
			frequency = f;
		}

		public void setType(Filter mf)
		{
			id = mf.id;
			displayName = mf.displayName;
			shortName = mf.shortName;
			saveName = mf.saveName;
		}

	}
	
	/** returns an adapter of controls which are mappable onto interface elements eg touch bay, ribbons, sensors */
	public ControlsAdapter getInterfaceTgtAdapter();	
	public void setupControlsAdapter(Context c, int tvrid, int ddrid);
	public int controllableId();
	public String controllableType();
	public float floatValue(Control cc);
	public void setValue(Control cc, float v);
	public String abbrevName();
}
