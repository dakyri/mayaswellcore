#ifndef __ANDROID_SIGNAL_H
#define __ANDROID_SIGNAL_H

#include <stdlib.h>

typedef float	Sample;

const unsigned long	maxFramesPerChunk=64;
const float signalSampleRate=44100.0;
const float nyquist=signalSampleRate/2;
const unsigned int framesPerControlCycle=64;
const float pi=3.141592653589793;
const float pi_2=(pi/2.0);

class Signal
{
public:
					Signal(Sample dc);
					Signal(const Signal& sig);
	
					~Signal();
	
	Signal&			operator =	(const Signal& sig);
	Signal&			operator =	(Sample dc);

	Sample& 		operator []	(unsigned long i) const;
	
	Signal& 		operator +=	(const Signal& op);
	Signal& 		operator +=	(Sample op);
	Signal& 		operator -=	(const Signal& op);
	Signal& 		operator -=	(Sample op);
	Signal& 		operator *=	(const Signal& op);
	Signal& 		operator *=	(Sample op);
	Signal& 		operator /=	(const Signal& op);
	Signal& 		operator /=	(Sample op);
	
	//multiply accumulate:
	Signal&			mac( Signal s, Sample op );
	Signal&			mac( Sample op, Signal s );
	Signal&			mac( Signal s1, Signal s2 );
	
	//multiply assign
	Signal&			mass( Signal s, Sample op );
	Signal&			mass( Sample op, Signal s );
	Signal&			mass( Signal s1, Signal s2 );
	
	
	static float	vectorRate();
	static float	vectorDuration();

	Sample 			*sampArray;
};


inline Sample&
Signal::operator [] (unsigned long i) const
{
	return sampArray[i];
}

inline Signal& Signal::operator =	(const Signal& sig)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]=sig[i];
	return *this;
}

inline Signal& Signal::operator =	(Sample dc)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]=dc;
	return *this;
}

inline Signal::Signal( Sample dc = 0.)
{
	sampArray = new Sample[maxFramesPerChunk];
	
	*this = dc;
}

inline Signal::Signal( const Signal& sig )
{
	sampArray = new Sample[maxFramesPerChunk];
	
	*this = sig;
}

inline Signal::~Signal()
{
	delete [] sampArray;
}

inline Signal& Signal::operator +=(const Signal& op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]+=op[i];
	return *this;
}

inline Signal& Signal::operator +=(Sample op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]+=op;
	return *this;
}
	
inline Signal& Signal::operator -=(const Signal& op)
		{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]-=op[i];
	return *this;
}

inline Signal& Signal::operator -=(Sample op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]-=op;
	return *this;
}

inline Signal& Signal::operator /=(const Signal& op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]/=op[i];
	return *this;
}

inline Signal& Signal::operator /=(Sample op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]/=op;
	return *this;
}
	
inline Signal& Signal::operator *=(const Signal& op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]*=op[i];
	return *this;	
}

inline Signal& Signal::operator *=(Sample op)
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]*=op;
	return *this;	
}

inline Signal& Signal::mac( Signal s, Sample op )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]+=s[i]*op;
	return *this;
}

inline Signal& Signal::mac( Sample op, Signal s )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]+=s[i]*op;
	return *this;
}

inline Signal& Signal::mac( Signal s1, Signal s2 )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]+=s1[i]*s2[i];
	return *this;
}

inline Signal& Signal::mass( Signal s, Sample op )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]=s[i]*op;
	return *this;
}

inline Signal& Signal::mass( Sample op, Signal s )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]=s[i]*op;
	return *this;
}

inline Signal& Signal::mass( Signal s1, Signal s2 )
{
	for(unsigned long i=0;i<maxFramesPerChunk;i++)
		sampArray[i]=s1[i]*s2[i];
	return *this;
}

inline float Signal::vectorRate()
{
	return signalSampleRate / (float) maxFramesPerChunk;
}

inline float Signal::vectorDuration()
{
	return 1. / vectorRate();
}

void clip(float &samp);
inline void clip(float& samp)
{	//fastest way I could find to clip a signal
	char x = (char)samp;
	if(x<0)	{
		samp=-1.;
	} else if (x > 0){
		samp=1.;
	}
}


void softClip(float &v);
inline void softClip(float &v)
{
	if (v > 0.8) {
		v = (v < 1.6)?0.8 + (v-0.8)/4:1.0;
	} else if (v < -0.8) {
		v = (v > -1.6)?-0.8 + (v+0.8)/4:-1.0;
	}
}

float wideClip(float x);
inline float wideClip(float x)
{
	return x > 2? 2: x < -2? -2: x;
}

void freqCheck(float &f);
inline void freqCheck(float &f)
{
	if (f<1.0) f = 1.0;
	if (f>(signalSampleRate/2-500)) f = (signalSampleRate/2-500);
}

inline float
interpolate(float d1, float d2, float p)
{
	return d1+p*(d2-d1);
}


#define SAMPLE_FOREVER	0x7fffffff

#endif
