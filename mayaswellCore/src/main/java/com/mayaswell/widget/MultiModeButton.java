/**
 * 
 */
package com.mayaswell.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * @author dak
 *
 */
public class MultiModeButton extends Button {
	public static final int ACTIVATED = 1;
	public static final int DEACTIVATED = 2;
	
	public final int TOGGLE_MODE = 1;
	public final int HOLD_MODE = 2;

	int mode=TOGGLE_MODE;
	
	public interface Listener {
		void onStateChanged(MultiModeButton b, int mode);
	}
	
	protected Listener listener=null;
	/**
	 * @param context
	 */
	public MultiModeButton(Context context) {
		super(context);
		setup();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public MultiModeButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup();
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public MultiModeButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup();
	}
	
	/**
	 * @param what
	 */
	protected void dispatchStateChange(int what)
	{
		if (listener != null) {
			listener.onStateChanged(this, what);
		}
	}
	
	
	/**
	 * @param h
	 */
	public void setListener(Listener h)
	{
		listener = h;
	}

	/**
	 * @param canvas
	 */
	@Override
	protected void onDraw (Canvas canvas)
	{
		super.onDraw(canvas);
	}
	
	/**
	 * @param v
	 */
	protected boolean onLongClickEvent(View v)
	{
		return true;
	}

	/**
	 * @param v
	 * @param me
	 * @return
	 */
	protected boolean onTouchEvent(View v, MotionEvent me)
	{
	    int action = me.getAction();
	    if(action == MotionEvent.ACTION_DOWN) {
	    	dispatchStateChange(ACTIVATED);
	    } else if (action == MotionEvent.ACTION_UP) {
	    	dispatchStateChange(DEACTIVATED);
	    }
		return super.onTouchEvent(me);
	}
	
	protected boolean onClickEvent()
	{
		return true;
	}
	
	@Override
	public boolean onHoverEvent(MotionEvent event)
	{
		return super.onHoverEvent(event);
	}
	
	protected void setup() {
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent me) {
			    return onTouchEvent(v, me);
			}
		});
		setOnLongClickListener(new OnLongClickListener() {
		    @Override
		    public boolean onLongClick(View v) {
		    	return onLongClickEvent(v);
		    }
		});
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickEvent();
			}
		});
		setOnHoverListener(new View.OnHoverListener() {	
			@Override
			public boolean onHover(View v, MotionEvent event) {
				return onHoverEvent(event);
			}
		});
	}
}
