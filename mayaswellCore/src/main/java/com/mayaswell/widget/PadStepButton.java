package com.mayaswell.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

import com.mayaswell.R;
import com.mayaswell.widget.MultiModeButton.Listener;

public class PadStepButton extends Button {
	public interface Listener {
		void onStateChanged(PadStepButton b, int mode);
	}
	
	protected Listener listener=null;

	public static final int ACTIVATED = 1;
	public static final int DEACTIVATED = 2;

	protected static final int[] STATE_PLAYING = {R.attr.state_playing};
	protected static final int[] STATE_ACTIVE = {R.attr.state_active};
	
	protected boolean isPlaying = false;
	protected boolean isActive = false;
	
	public int id = 0;
	protected final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
	protected final float textScaledPx = 10 * densityMultiplier;
	protected Paint textBrush = null;
	protected Paint positionBrush;
	protected Rect vRect = null;
	protected Rect pdRect = null;
	protected int lampResId = 0;
	protected Drawable lamp = null;

	public PadStepButton(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public PadStepButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public PadStepButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	@SuppressLint("ClickableViewAccessibility")
	protected void setup(Context context, AttributeSet attrs, int defStyle) {
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent me) {
			    return onTouchEvent(v, me);
			}
		});
		setOnLongClickListener(new OnLongClickListener() {
		    @Override
		    public boolean onLongClick(View v) {
		    	return onLongClickEvent(v);
		    }
		});
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickEvent();
			}
		});
		
		
		
		vRect = new Rect();
		pdRect = new Rect();
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setColor(0xff000000);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		
		positionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		positionBrush.setColor(getResources().getColor(R.color.padLightYellow));
		positionBrush.setStyle(Style.STROKE);
		positionBrush.setStrokeWidth(4);
		lampResId = R.drawable.mw_drumpad_lamp;
		lamp = null;
		if (attrs != null) {
			int pn = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/com.mayaswell.spacegun", "stepNumber", 0);
			if (pn > 0) {
				id = pn;
			}
			pn = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/com.mayaswell.spacegun", "lampResource", 0);
			if (pn > 0) {
				lampResId = pn;
			}
		}
		setLampResource(lampResId);
	}
	
	protected boolean onLongClickEvent(View v) {
		return false;
	}

	protected boolean onClickEvent()
	{
		return true;
	}
	
	protected boolean onTouchEvent(View v, MotionEvent me) {
	    touchResponse(me);
		return super.onTouchEvent(me);
	}

	protected void touchResponse(MotionEvent me) {
		int action = me.getAction();
	    if(action == MotionEvent.ACTION_UP) {
	    	if (isActive) {
	    		dispatchStateChange(DEACTIVATED);
	    		isActive = false;
	    	} else {
	    		dispatchStateChange(ACTIVATED);
	    		isActive = true;
	    	}
	    }
	}
	
	public void setLampResource(int resid) {
		lampResId = resid;
		if (lampResId > 0){
			try {
				lamp = ResourcesCompat.getDrawable(getResources(), lampResId, null);
			} catch (NotFoundException e) {
				lamp = null;
			}
		}
	}

	/**
	 * @param canvas
	 */
	@Override
	protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
//			Log.d("draw stuff", getWidth()+" "+getHeight()+" "+id);

//			getLocalVisibleRect(vRect);
			vRect.set(0, 0, getWidth(), getHeight());
			pdRect.set(vRect.left+3, (int)(vRect.top+textScaledPx), vRect.right-3, (int) (vRect.top+textScaledPx+0.5*textScaledPx));
			canvas.drawText(Integer.toString(id),(vRect.right+vRect.left)/2,vRect.bottom-textScaledPx, textBrush);
			if (lamp != null) {
				lamp.setBounds((vRect.right+vRect.left)/2-25, vRect.top+20, (vRect.right+vRect.left)/2+25, vRect.top+35);
				lamp.draw(canvas);
			}
	/*		if (padSample != null) {
	//			Log.d("draw button", String.format("playing %b", padSample.isPlaying()));
				if (padSample.isPlaying()) {
					float lp = pdRect.left;
					float rp = pdRect.left+padSample.getCurrentPosition()*(pdRect.right-pdRect.left);
					float mp = (pdRect.bottom+pdRect.top)/2;
	//				canvas.drawLine(lp, pdRect.top, lp, pdRect.bottom, positionBrush);
	//				canvas.drawLine(rp, pdRect.top, rp, pdRect.bottom, positionBrush);
					canvas.drawLine(lp, mp, rp, mp, positionBrush);
				}
				PadSampleState pss = padSample.getState();
				if (pss.name != null) {
				}
			}
	*/
		}

	/** 
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
			final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
	//		Log.d("draw button", "merge drawable "+Integer.toString(id)+", "+Boolean.toString(isPlaying));
			if (isActive) {
				mergeDrawableStates(drawableState, STATE_ACTIVE);
			}
			if (isPlaying) {
				mergeDrawableStates(drawableState, STATE_PLAYING);
			}
	//		if (isRecording) {
	//			mergeDrawableStates(drawableState, STATE_RECORDING);
	//		}
			return drawableState;
		}

	/** 
	 */
	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();
//		Log.d("pad button", String.format("state change %d", getDrawableState()[0])); 
		invalidate(pdRect);
		if (lamp != null) {
			lamp.setState(getDrawableState());
		}
	}

	public Rect getPositionDisplayRect() {
		return pdRect;
	}

	/**
	 * @param s
	 */
	public void setPlayState(boolean s) {
		isPlaying = s;
		refreshDrawableState();
	}

	/**
	 * @param s
	 */
	public void setActiveState(boolean s) {
		isActive = s;
		refreshDrawableState();
	}

	public void setStepId(int bi) {
		id = bi;
	}
	
	public int getStepId() {
		return id;
	}
	
	/**
	 * @param what
	 */
	protected void dispatchStateChange(int what)
	{
		if (listener != null) {
			listener.onStateChanged(this, what);
		}
	}
	
	
	/**
	 * @param h
	 */
	public void setListener(Listener h)
	{
		listener = h;
	}

}