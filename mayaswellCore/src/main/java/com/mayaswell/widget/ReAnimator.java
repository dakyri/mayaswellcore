package com.mayaswell.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewAnimator;

/**
 * @author dak
 *  ReAnimator ... tentative attempt to make something that is configurable between a ViewAnimator
 *  and a frame layout.
 */
public class ReAnimator extends ViewAnimator {

	public ReAnimator(Context context) {
		super(context);
	}

	public ReAnimator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

}
