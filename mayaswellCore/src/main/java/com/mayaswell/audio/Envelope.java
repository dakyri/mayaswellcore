package com.mayaswell.audio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.mayaswell.audio.Modulator.ResetMode;


public class Envelope extends Modulator implements Parcelable
{
	public interface Host extends Controllable {
		boolean setNEnvelope(int n);
		boolean setEnvList(ArrayList<Envelope> envelope);
		boolean setEnvelopeLock(int xyci, boolean amt);
		boolean setEnvelopeReset(int xyci, ResetMode amt);
		boolean setEnvelopeAttack(int xyci, float t);
		boolean setEnvelopeDecay(int xyci, float t);
		boolean setEnvelopeSustain(int xyci, float t);
		boolean setEnvelopeSustainLevel(int xyci, float l);
		boolean setEnvelopeRelease(int xyci, float t);
		boolean setNEnvelopeTarget(int xyci, int n);
		boolean setEnvelopeTarget(int xyci, int xycj, ICControl target, float amt);
		boolean setEnvelopeTargetAmt(int xyci, int xycj, float amt);
		int maxEnv();
		int maxEnvTgt();
		ControlsAdapter getEnvTgtAdapter();
		ControlsAdapter getInterfaceTgtAdapter();
		
		ArrayList<Envelope> envs();
		
		boolean alwaysLockedTempo();
	}
	
	public static final float NormalizedBeatPerSegment=8;
	public float		attackT;
	public float		decayT;
	public float		sustainT;
	public float		sustainL;
	public float		releaseT;
	
	public Envelope(float attackT2, float sustainT2, float decayT2, float sustainL2, float releaseT2)
	{
		this(true, ResetMode.ONFIRE, attackT2, decayT2, sustainT2, sustainL2, releaseT2);
	}
	
	public Envelope()
	{
		this(0, 0, 0, 1, 1);
	}

	public Envelope(boolean tempoLock2, ResetMode trigger2, float attackT2, float decayT2, float sustainT2, float sustainL2, float releaseT2)
	{
		tempoLock = tempoLock2;
		trigger = trigger2;
		attackT = attackT2;
		decayT = decayT2;
		sustainT = sustainT2;
		sustainL = sustainL2;
		releaseT = releaseT2;
		target = new ArrayList<ModulationTarget>(); 
	}

	public Envelope clone()
	{
		Envelope env = new Envelope(tempoLock, trigger, attackT, decayT, sustainT, sustainL, releaseT);
		for (ModulationTarget m: target) {
			env.add(m.clone());
		}
		return env;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public Envelope(Parcel in) {
		attackT = in.readFloat();
		decayT = in.readFloat();
		sustainT = in.readFloat();
		sustainL = in.readFloat();
		releaseT = in.readFloat();

		tempoLock = in.readInt() != 0;
		trigger = ResetMode.parse(in.readString());
		target = new ArrayList<ModulationTarget>();
		in.readTypedList(target, ModulationTarget.CREATOR);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeFloat(attackT);
		dest.writeFloat(decayT);
		dest.writeFloat(sustainT);
		dest.writeFloat(sustainL);
		dest.writeFloat(releaseT);

		dest.writeInt(tempoLock? 1 : 0);
		dest.writeString(trigger.toString());
		dest.writeTypedList(target);
	}

	public static Parcelable.Creator CREATOR = new Parcelable.Creator() {

		@Override
		public Envelope createFromParcel(Parcel in) {
			return new Envelope(in);
		}

		@Override
		public Envelope[] newArray(int i) {
			return new Envelope[i];
		}
	};

}
