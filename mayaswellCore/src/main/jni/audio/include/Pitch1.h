/*
 * Pitch1.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */

#ifndef PITCH1_H_
#define PITCH1_H_

class Pitch1 {
public:
	Pitch1();
	virtual ~Pitch1();

	void setFine (float v);
	void setCoarse (float v);
	void setFeedback (float v);
	void setDelay (float v);
	void setMixMode (float v);
	void apply(float *signal, long sampleFrames);
protected:
	void setSweep(void);

	float paramFinePitch;		// 0.0-1.0 passed in
	float paramCoarsePitch;	// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto
	float paramMixMode;		// ditto
	double sweepRate;			// actual calc'd sweep rate
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 to 1.0
	double sweepSamples;		// samples to use in sweep
	double  delaySamples;		// number of samples to run behind filling pointer
	double sweepA;				// sweep position for channel A
	double sweepB;				// sweep position for channel B
	int	  mixMode;				// mapped to supported mix modes
	double *buf;				// stored sound
	int	   fp;					// fill/write pointer
	double outval;				// most recent output value (for feedback)
	double sweepInc;			// calculated by desired pitch deviation
	bool increasing;			// flag for pitch increasing/decreasing

	// output mixing
//	double mixLeftWet;
//	double mixLeftDry;
//	double mixRightWet;
//	double mixRightDry;

};

const int kBufSize=8192;		// must be about 1/5 of a second at given sample rate
#define ROUND(n)		((int)((double)(n)+0.5))
#define PIN(n,min,max)  ((n) > (max) ? (max) : ((n) < (min) ? (min) : (n)))
const float kMinSweep=0.015;
const float kMaxSweep=0.1;
const float kSemiTone=1.0594631;

//
//	MODF - vaguely related to the library routine modf(), this macro breaks a double into
//	integer and fractional components i and f respectively.
//
//	n - input number, a double
//	i - integer portion, an integer (the input number integer portion should fit)
//	f - fractional portion, a double
//
#define	MODF(n,i,f) ((i) = (int)(n), (f) = (n) - (double)(i))


#define	NUM_MIX_MODES	7
const int kNumPitches=25;
const int kMinDelaySamples=22.0;

class Pitch1;


#endif /* PITCH1_H_ */
