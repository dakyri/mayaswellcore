/*
 * Filter.h
 *
 *  Created on: Dec 9, 2013
 *      Author: dak
 */

#ifndef FILTER_H_
#define FILTER_H_

#include "SG303Filter.h"
#include "MoogFilter.h"
#include "FormantFilter.h"

#include "com_mayaswell_audio_Controllable_Filter.h"

class Filter
{
public:
	Filter();
	~Filter();

	void operator=(Filter &);
	void apply(float *buf, unsigned int nFrames);
	void reset();
	void setType(int t);
	void setFrequency(float f);
	void setResonance(float r);
	void set(int t, float f, float r);

	int				type;
	float			frequency;
	float			resonance;

	SG303Filter		sg;
	MoogVCF1		moog1;
	MoogVCF2		moog2;
	FormantFilter	formant;
};

class FilterHost
{
protected:
	void calc303FilterSweep(float f, float e, float &sweepMin, float &sweepMax, float inc=0.0);
	float cubicMap(float p);
	void setFilterSweepBounds(float f, float e);

	int					filterType;
	float				filterFrequency;
	float				filterResonance;
	float				filterEnvMod;
	float				filterSweepMin;
	float				filterSweepMax;

};

inline void
Filter::operator=(Filter &f)
{
	type = f.type;
	switch (f.type) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH:
	case com_mayaswell_audio_Controllable_Filter_SG_BAND:
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		sg = f.sg;
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
		moog1 = f.moog1;
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH:
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
		moog2 = f.moog2;
		break;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		formant = f.formant;
		break;
	}
	}
}

inline void
Filter::apply(float *bufp, unsigned int nFrames)
{
	const float *bnd = bufp + nFrames;
	switch (type) {
	case com_mayaswell_audio_Controllable_Filter_UNITY: {
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_LOW: {
		for (; bufp<bnd; bufp++) {
			*bufp = sg.lowPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_HIGH: {
		for (; bufp<bnd; bufp++) {
			*bufp = sg.highPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_SG_BAND: {
		for (; bufp<bnd; bufp++) {
			*bufp = sg.bandPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_LOW: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog1.lowPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_HIGH: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog1.highPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG1_BAND: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog1.bandPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_LOW: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog2.lowPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_HIGH: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog2.highPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_MOOG2_BAND: {
		for (; bufp<bnd; bufp++) {
			*bufp = moog2.bandPass(*bufp);
		}
		return;
	}
	case com_mayaswell_audio_Controllable_Filter_FORMANT: {
		for (; bufp<bnd; bufp++) {
			*bufp = formant.filter(*bufp);
		}
		break;
	}
	}
}


#endif /* FILTER_H_ */
