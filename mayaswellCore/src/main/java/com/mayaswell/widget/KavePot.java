package com.mayaswell.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.mayaswell.R;

/**
 * Created by dak on 3/13/2017.
 */
public class KavePot extends View {

	private Paint edgeBrush;
	private float marginalMargin = 2;
	protected boolean showTextValue = true;

	public interface Listener {
		public void onKnobChanged(float v);
	}

	protected float densityMultiplier = getContext().getResources().getDisplayMetrics().density;

	private float angle = 0f;
	protected float minAngle=-225f;
	protected float maxAngle=45f;
	protected float minVal = 0;
	protected float maxVal = 1;
	protected float centerVal=0;
	private float val = 1;
	private float normVal = 1;
	private float theta_old=0f;
	private Listener listener = null;
	protected long lastDt=0;
	protected long doubleTapTime=500;
	private boolean tweak = false;
	private RectF arcRect = new RectF();
	private RectF labelRect = new RectF();
	private RectF valueRect = new RectF();
	protected Paint arcBrush = new Paint();
	protected Paint textBrush = new Paint();
	protected int textColor = 0xff4285F4;
	protected int arcColor = 0xff333377;
	protected int baseColor = 0x22333377;
	protected int arcWidth = 20;
	private float labelFontSize =10;
	float labelHeightPx = labelFontSize * densityMultiplier;
	private int arcRadius = 0;

	protected String valueDisplayFormat = "%1.5f";
	private String label = "";

	public KavePot(Context context) {
		super(context);
		setup(context, null);
	}

	public KavePot(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs);
	}

	public KavePot(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	private void setup(Context context, AttributeSet attrs) {
		tweak = false;
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				int actionCode = action & MotionEvent.ACTION_MASK;
//				Log.d("mouse", String.format("%d %d", action, actionCode));
				if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_DOWN) {
					tweak = true;
					float x = event.getX(0);
					float y = event.getY(0);
					theta_old = getTheta(x, y);
				} else if (actionCode == MotionEvent.ACTION_UP)  {
					tweak = false;
					long dt = event.getDownTime();
					long et = event.getEventTime();
					if (dt - lastDt < doubleTapTime) {
						doDoubleTap();
					}
					lastDt = dt;
				} else if (actionCode == MotionEvent.ACTION_MOVE) {
					invalidate();

					float x = event.getX(0);
					float y = event.getY(0);

					float width = arcRect.width();
					float height = arcRect.height();
					float sx = x - (width / 2.0f);
					float sy = y - (height / 2.0f);

					float theta = getTheta(x, y);
					float r = (float)Math.sqrt( sx*sx + sy*sy);
					float delta_theta = theta - theta_old;
					if (Math.abs(delta_theta) < 0.001) {
						delta_theta = 0;
					}

					theta_old = theta;

					int direction = (delta_theta > 0) ? 1 :  (delta_theta <  0)? -1 : 0;
					angle += direction*(width+height)/r;

//					Log.d("mouse", String.format("x %g y %g delt %g dir %d ang %g theta %g ", x, y, delta_theta, direction, angle, theta));

					if (angle < 0) angle = 0;
					else if (angle > maxAngle-minAngle) angle = maxAngle-minAngle;

					normVal = angle2Norm(angle);
					val = norm2Val(normVal);

					notifyListener(getValue());
				}
				return true;
			}

			private void doDoubleTap()
			{
				setValue(centerVal);
				notifyListener(getValue());
			}
		});

		if (attrs != null) {
			TypedArray kpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.KavePot, 0, 0);
			TypedArray mwvAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWView, 0, 0);
			TypedArray mwpAttrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MWPot, 0, 0);

			String lbl = mwvAttrs.getString(R.styleable.MWView_label);
			if (lbl != null) {
				label = lbl;
			}
			int displayPrecision = mwpAttrs.getInt(R.styleable.MWPot_displayPrecision, -1);
			if (displayPrecision >= 0) {
				setValueDisplayPrecision(displayPrecision);
			}
			showTextValue = kpAttrs.getBoolean(R.styleable.KavePot_showTextValue, showTextValue);
			arcWidth = (int) kpAttrs.getDimension(R.styleable.KavePot_arcWidth, arcWidth);
			labelFontSize = mwvAttrs.getDimension(R.styleable.MWView_labelFontSize, labelFontSize);

			String valueBounds = mwpAttrs.getString(R.styleable.MWPot_valueBounds);
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					float floatBoundsA[] = new float[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						floatBoundsA[i++] = Float.parseFloat(s);
					}
					setValueBounds(floatBoundsA[0], floatBoundsA[floatBoundsA.length-1]);
					setCenterValue(floatBoundsA[floatBoundsA.length-2]);
				}
			}

			textColor  = kpAttrs.getColor(R.styleable.KavePot_textColor, textColor);
			arcColor  = kpAttrs.getColor(R.styleable.KavePot_arcColor, arcColor);
			baseColor  = kpAttrs.getColor(R.styleable.KavePot_baseColor, baseColor);

			kpAttrs.recycle();
			mwvAttrs.recycle();
			mwpAttrs.recycle();
		}

		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(labelFontSize * densityMultiplier);
		textBrush.setTextAlign(Paint.Align.CENTER);
		textBrush.setColor(textColor);

		arcBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		arcBrush.setStyle(Paint.Style.STROKE);
		arcBrush.setStrokeJoin(Paint.Join.BEVEL);
		arcBrush.setStrokeCap(Paint.Cap.BUTT);
		arcBrush.setStrokeWidth(arcWidth);
		arcBrush.setColor(arcColor);
		/*

		arcBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		arcBrush.setStyle(Paint.Style.FILL);
		arcBrush.setStrokeJoin(Paint.Join.ROUND);
		arcBrush.setStrokeCap(Paint.Cap.ROUND);
		arcBrush.setStrokeWidth(strokeWidth);
		arcBrush.setColor(arcColor);
		arcBrush.setAntiAlias(true);
		arcBrush.setStrokeWidth(arcRadius / 14.0f);
*/
		edgeBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		edgeBrush.setColor(0xff000000);
		edgeBrush.setStyle(Paint.Style.STROKE);
		edgeBrush.setStrokeWidth(1);
		
		if (label == null) {
			labelHeightPx = 0;
		} else {
			labelHeightPx = textBrush.descent()-textBrush.ascent();
		}
		val = centerVal;
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		int width = right-left;
		int height = bottom - top - (int) labelHeightPx;
		int r = width < height? width / 2 : height / 2;
		if (arcRadius == 0 || arcRadius > r) {
			arcRadius = r - (int)(arcWidth / 2);
		}
//		Log.d("lay", String.format("%d %d %d %g %d %d", width, height, arcRadius, arcWidth, r, (int)(arcWidth / 2)));
		float bx = width/2;
		float by = (arcWidth / 2);
		arcRect.set(bx - arcRadius,by, bx + arcRadius, by + 2 * arcRadius);
		labelRect.set(0, arcRect.bottom + marginalMargin, width, arcRect.bottom + marginalMargin + labelHeightPx);
		by = (arcRect.top + arcRect.bottom)/2 - textBrush.ascent()/2.0f;
		float h = labelHeightPx;
//		Log.d("layout", String.format("got %g %g %g", by, textBrush.ascent(), labelHeightPx) );
		valueRect.set(arcRect.left+arcWidth/2+marginalMargin, by-h, arcRect.right-arcWidth/2-marginalMargin, by);

		setValue(val);
	}

	public void setValueDisplayPrecision(int i)
	{
		if (i < 1) i = 1; else if (i > 5) i = 5;
		valueDisplayFormat = "%1."+Integer.toString(i)+"f";
	}

	protected float norm2Val(float n)
	{
		return minVal+ n*(maxVal-minVal);
	}

	protected float angle2Norm(float a)
	{
		return (a/*-minAngle*/)/(maxAngle-minAngle);
	}

	protected float norm2Angle(float n) {
		return /*minAngle+*/((maxAngle-minAngle)*n);
	}

	protected float val2Norm(float v) {
		return (v-minVal)/(maxVal-minVal);
	}

	public void setValueBounds(float min, float max)
	{
		if (max < min) return;

		minVal = min;
		maxVal = max;
	}

	public void setCenterValue(float cv)
	{
		centerVal = cv;
	}

	public float getMinVal() { return minVal; }
	public float getMaxVal() { return maxVal; }
	public float getcenterVal() { return centerVal; }

	private void notifyListener(float v)
	{
		if (listener != null)
			listener.onKnobChanged(v);
	}

	protected void onDraw(Canvas c)
	{
//		c.drawRect(arcRect, arcBrush);
//		Log.d("drawfffs", String.format("%g %g %g %g", arcRect.left, arcRect.right, arcRect.bottom, arcRect.top));
//		c.drawRect(labelRect.left,labelRect.top, labelRect.right-1, labelRect.bottom-1, edgeBrush);
//		c.drawRect(arcRect.left,arcRect.top, arcRect.right-1, arcRect.bottom-1, edgeBrush);
//		c.drawRect(arcRect, edgeBrush);

//		Log.d("ondraw", String.format("%g %g %g", angle, normVal, val));

		arcBrush.setColor(arcColor);
		c.drawArc(arcRect, minAngle, angle, false, arcBrush);
		arcBrush.setColor(baseColor);
		c.drawArc(arcRect, minAngle+angle, maxAngle-minAngle-angle, false, arcBrush);

		if (label != null) {
			textBrush.setTextAlign(Paint.Align.CENTER);
			c.drawText(label, (labelRect.left+labelRect.right)/2, labelRect.bottom-textBrush.descent(), textBrush);
		}

		if (showTextValue) {
			c.clipRect(valueRect);
			String vs = (valueDisplayFormat != null ? String.format(valueDisplayFormat, val) : Float.toString(val));
			float w = textBrush.measureText(vs);
			if (w > valueRect.width()) {
				textBrush.setTextAlign(Paint.Align.LEFT);
				c.drawText(vs, valueRect.left, valueRect.bottom, textBrush);
			} else {
				textBrush.setTextAlign(Paint.Align.CENTER);
				c.drawText(vs, (valueRect.left + valueRect.right) / 2, valueRect.bottom, textBrush);
			}
		}
	}

	public void drawDonut(Canvas canvas, Paint paint, float start,float sweep)
	{
		/*
		myPath.reset();
		myPath.arcTo(outterCircle, start, sweep, false);
		myPath.arcTo(innerCircle, start+sweep, -sweep, false);
		myPath.close();
		canvas.drawPath(myPath, paint);
		*/
	}

	public void setValue(float v)
	{
		val = v;
		normVal = val2Norm(val);
		angle = norm2Angle(normVal);
		invalidateArc();
	}

	private void invalidateArc() {
		invalidate((int)arcRect.left, (int)arcRect.top, (int)arcRect.right, (int)arcRect.bottom);
	}


	public float getValue()
	{
		return val;
	}

	private float getTheta(float x, float y)
	{
		float width = arcRect.width();
		float height = arcRect.height();

		float sx = x - (width / 2.0f);
		float sy = y - (height / 2.0f);

		float theta = (float)Math.atan2( sy, sx );

		final float rad2deg = (float)(180.0/Math.PI);
		float theta2 = theta*rad2deg;
		theta2 = (theta2 < 0) ? theta2 + 360.0f : theta2;
		return theta2;
	}

	public boolean isTweaking()
	{
		return tweak;
	}
}
